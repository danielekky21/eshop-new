function windowWidth(){
    if ($(window).width() > 991) {
        $(window).scroll(function(){
            var a = $(window).scrollTop();      
            if (a >= 115) {
                $('ul.head').addClass("sticky");
                $('body').addClass("top48");
            }  else {
                $('ul.head').removeClass("sticky");
                $('body').removeClass("top48");
            }
        });
    }
    if ($(window).width() < 991) {
        $(window).scroll(function(){
            var b = $(window).scrollTop();      
            if (b >= 115) {
                $('ul.head').removeClass("sticky");
                $('body').removeClass("top48");
            }
        });
    }
}

$(document).ready(function () {
    $('.click-search').click(function(){
        $('.form-search').addClass('open');
    });
    $('html').click(function(e) {
        if ( ! $(e.target).parents().is('.click-search') && ! $(e.target).is('.form-search')) {
            $('.form-search').removeClass('open');
        }
    });

    windowWidth();

    $(window).on('resize', function(){
        windowWidth();
    });

    $('.menu > ul > li:has( > ul)').addClass('menu-dropdown-icon');
    
    $('.menu > ul > li > ul:not(:has(ul))').addClass('normal-sub');
    
    //$(".menu > ul").before("<a href=\"#\" class=\"menu-mobile\">Navigation</a>");

    $(".menu > ul > li").mouseenter(function (e) {
        if ($(window).width() > 991) {
            $(this).children("ul").fadeToggle(150).css("display","block");
            e.preventDefault();
        }
    });
    $(".menu > ul > li").mouseleave(function (e) {
       if ($(window).width() > 991) {
            $(this).children("ul").css("display", "none");
            e.preventDefault();
        }
    });

    $(".menu > ul > li").click(function () {
        if ($(window).width() <= 991) {
            $(this).children("ul").fadeToggle(150);
        }
    });
    
    $(".menu-mobile").click(function (e) {
        $(".menu > ul").toggleClass('show-on-mobile');
        e.preventDefault();
    });
    
});