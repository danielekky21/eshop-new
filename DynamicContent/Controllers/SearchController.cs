﻿using DynamicContent.Helper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DynamicContent.Controllers
{
    public class SearchController : Controller
    {
        //
        // GET: /Search/

        public ActionResult Index()
        {
            string find = this.Request.QueryString["find"];
            ViewData["find"] = find;
            return View("~/Views/Pages/search.cshtml");
        }
        public ActionResult brand() 
        {
            string id = this.Request.QueryString["id"];
            string[] split = id.Split('-');
            ViewData["id"] = split[0];
            return View("~/Views/Pages/search.cshtml");
        }
    }
}
