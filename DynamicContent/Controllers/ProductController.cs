﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DynamicContent.Controllers
{
    public class ProductController : Controller
    {
        //
        // GET: /Product/
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        public ActionResult Index(int id)
        {
            var getId = obj.GetProductByColor(id);
            int prodId = Convert.ToInt32(getId.product);
            var prod = obj.GetAllProduct(prodId).FirstOrDefault();
            DynamicContent.Models.tbl_product_view pv = new DynamicContent.Models.tbl_product_view();
            pv.product_id = prodId;
            pv.created_date = DateTime.Now;
            db.tbl_product_view.Add(pv);
            db.SaveChanges();
            //var tc = obj.GetTenantCategory(prod.tenant_category.ToString()).FirstOrDefault();
            //var cat = tc.name.ToLower();
            var category = "";
            var day = DateTime.Now.DayOfWeek.ToString();
            var timeNow = DateTime.Now.TimeOfDay;
            ViewBag.isFlashSale = false;
           
            var flashSaleData = db.FlashSaleHeaders.FirstOrDefault(x => x.FlashSaleDay == day && x.IsActive == true && x.FlashSaleTimeBegin <= timeNow && x.FlashSaleTimeEnd >= timeNow);
            if (flashSaleData != null)
            {
                var flashSaleDetailData = db.FlashHeaderDetails.FirstOrDefault(x => x.ProductID == pv.product_id && x.FlashSaleHeaderID == flashSaleData.FlashSaleHeaderID && x.isActive == true && x.qty > 0 );
                if (flashSaleDetailData != null)
                {
                    ViewBag.TimeNow = DateTime.Now.TimeOfDay;
                    ViewBag.isFlashSale = true;
                    ViewBag.FlashSaleData = flashSaleData;
                    ViewBag.FlashSaleDetail = flashSaleDetailData;
                }
            }
            //switch (cat)
            //{
            //    case "fashion":
            //        category = "fashion";
            //        break;
            //    case "jewelries & watches":
            //        category = "jewelries-watches";
            //        break;
            //    case "home & furnishing":
            //        category = "home-furnishing";
            //        break;
            //    case "beauty & health":
            //        category = "beauty-health";
            //        break;
            //    case "speciality shop":
            //        category = "speciality-shop";
            //        break;
            //}
            //ViewData["category"] = category;
            ViewData["id-product"] = id;
            return View("~/Views/Pages/product-detail.cshtml");
        }
        public ActionResult dummy()
        {
            return View("~/Views/Pages/newFrontPage.cshtml.cshtml");
        }
        public ActionResult Preview(int id)
        {
            string det = Request.QueryString["det"];
            if (det == "true")
            {

            }
            else {
                var prod = obj.GetProductTmp(id).FirstOrDefault();
                //var tc = obj.GetTenantCategory(prod.tenant_category.ToString()).FirstOrDefault();
                var pc = obj.GetProductColorTmp(id).FirstOrDefault();
                //ViewData["category"] = tc.name;

                ViewData["id-product"] = (pc != null) ? pc.ID : 0;
            }
            

            return View("~/Views/Pages/preview.cshtml");
        }

        public ActionResult PreviewApproval(int id)
        {
            string det = Request.QueryString["det"];
            if (det == "true")
            {

            }
            else
            {
                var prod = obj.GetProduct(id).FirstOrDefault();
                var pc = obj.GetProductColor(id).FirstOrDefault();

                ViewData["id-product"] = (pc != null) ? pc.ID : 0;
            }


            return View("~/Views/Pages/previewApproval.cshtml");
        }

    }
}
