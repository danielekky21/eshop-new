﻿using DynamicContent.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Web.WebPages.OAuth;
using Facebook;
using System.Web.SessionState;

namespace DynamicContent.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();

        public ActionResult Index()
        {
            return View("~/Views/Pages/login.cshtml");
        }
        
        public ActionResult RegistFromFacebook()
        {
            
            var user = new DynamicContent.Models.tbl_user_register();
            FacebookUserModel data = null;
            if (TempData["facebookUser"] != null)
            {
                data = (FacebookUserModel)TempData["facebookUser"];
            }
            string gender = "";
            if(data.gender == "female")
            {
                gender = "women";
            }else if(data.gender == "male")
            {
                gender = "men";
            }
            //user.firstName = data.first_name;
            //user.lastName = data.last_name;
            //user.email = data.email;
            //user.gender = gender;
            //user.regist_date = DateTime.Now;
            //user.modified_date = DateTime.Now;
            //obj.UserRegister(user);
            //var u = obj.GetUserLogin(user.email);
            //_sessionFront.userIdLogin = u.ID;
            //_sessionFront.firstName = u.firstName;
            //_sessionFront.lastName = u.lastName;
            //TempData["msgSuccess"] = "success: Successfully add new user";
            //if (HttpContext.Session["fromLogin"] != null)
            //{
            //    string fromLogin = HttpContext.Session["fromLogin"].ToString();
            //    return RedirectToAction(fromLogin);
            //}
            //else
            //{
            //    return RedirectToAction("Myaccount", "Customer");
            //}
            ViewData["facebookUser"] = data;
            return RedirectToAction("Login");
        }

        [HttpGet]
        public ActionResult UserDetails()
        {
            var client = new FacebookClient(Session["accessToken"].ToString());
            dynamic fbresult = client.Get("me?fields=id,email,first_name,last_name,gender,locale,link,timezone,location,picture");
            FacebookUserModel facebookUser = Newtonsoft.Json.JsonConvert.DeserializeObject<FacebookUserModel>(fbresult.ToString());
            var userlogin = obj.GetUserLogin(facebookUser.email);
            if (userlogin != null)
            {
                string sessionId = System.Web.HttpContext.Current.Session.SessionID;

                _sessionFront.firstName = userlogin.firstName;
                _sessionFront.lastName = userlogin.lastName;
                _sessionFront.userIdLogin = userlogin.ID;
                _sessionFront.sessionId = sessionId;

                obj.UpdateSessionId(facebookUser.email, sessionId);

                if (HttpContext.Session["fromLogin"] != null)
                {
                    string fromLogin = HttpContext.Session["fromLogin"].ToString();
                    return RedirectToAction(fromLogin, "Customer");
                }
                else
                {
                    return RedirectToAction("Myaccount" , "Customer");
                }

            }
            else
            {
                ViewData["facebookUser"] = facebookUser;
                //return RedirectToAction("Login");
                return View("~/Views/Pages/login.cshtml");
                //return RedirectToAction("RegistFromFacebook");
            }            
        }

        [HttpPost]
        public ActionResult FacebookLogin(FacebookLoginModel model)
        { 
            Session["uid"] = model.uid;
            Session["accessToken"] = model.accessToken;
            return RedirectToAction("UserDetails");
            //return Json(new { success = true });
        }

        

    }
}
