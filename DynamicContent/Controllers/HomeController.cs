﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace DynamicContent.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        public ActionResult Index(string id)
        {
            System.Web.HttpBrowserCapabilitiesBase myBrowserCaps = Request.Browser;
            if ((myBrowserCaps).IsMobileDevice)
            {
                Session["isMobile"] = true;
            }
            else
            {
                Session["isMobile"] = false;
            }
            ViewData["category"] = id;
            ViewBag.TimeNow = DateTime.Now.TimeOfDay;
            ViewBag.IsFlashSale = false;
            ViewBag.FlashSaleHeaderData = new DynamicContent.Models.FlashSaleHeader();
            ViewBag.FlashSaleDetail = new DynamicContent.Models.FlashHeaderDetail();
            var day = DateTime.Now.DayOfWeek.ToString();
            var timeNow = DateTime.Now.TimeOfDay;
            var flashSaleData = db.FlashSaleHeaders.FirstOrDefault(x => x.FlashSaleDay == day && x.IsActive == true && x.FlashSaleTimeBegin <= timeNow && x.FlashSaleTimeEnd >= timeNow);
            if(flashSaleData != null)
            {
                var flashSaleDetailData = db.FlashHeaderDetails.Where(x => x.FlashSaleHeaderID == flashSaleData.FlashSaleHeaderID && x.isActive == true).Take(8).ToList();
                foreach (var item in flashSaleDetailData)
                {
                    item.tbl_product.images = db.tbl_product_image.FirstOrDefault(x => x.product == item.ProductID).images;
                }
               
                ViewBag.IsFlashSale = true;
                ViewBag.FlashSaleHeaderData = flashSaleData;
                ViewBag.FlashSaleDetail = flashSaleDetailData;
            }
            ViewBag.TopPicks = db.TopPicks.Where(x => x.IsActive == true).OrderBy(x => x.sort).ToList();
            return View("~/Views/Pages/home.cshtml");
        }
        public ActionResult Login()
        {
            ViewData["category"] = "";
            return View("~/Views/Pages/home.cshtml");
        }
        public ActionResult FlashDeal()
        {
            ViewBag.TimeNow = DateTime.Now.TimeOfDay;
            ViewBag.IsFlashSale = false;
            ViewBag.FlashSaleHeaderData = new DynamicContent.Models.FlashSaleHeader();
            ViewBag.FlashSaleDetail = new List<DynamicContent.Models.FlashHeaderDetail>();
            var day = DateTime.Now.DayOfWeek.ToString();
            var timeNow = DateTime.Now.TimeOfDay;
            var flashSaleData = db.FlashSaleHeaders.FirstOrDefault(x => x.FlashSaleDay == day && x.IsActive == true && x.FlashSaleTimeBegin <= timeNow && x.FlashSaleTimeEnd >= timeNow);
            if (flashSaleData != null)
            {
                var flashSaleDetailData = db.FlashHeaderDetails.Where(x => x.FlashSaleHeaderID == flashSaleData.FlashSaleHeaderID && x.isActive == true).ToList();
                foreach (var item in flashSaleDetailData)
                {
                    item.tbl_product.images = db.tbl_product_image.FirstOrDefault(x => x.product == item.ProductID).images;
                }

                ViewBag.IsFlashSale = true;
                ViewBag.FlashSaleHeaderData = flashSaleData;
                ViewBag.FlashSaleDetail = flashSaleDetailData;
            }
            return View();
        }
        public ActionResult nowOpen()
        {
            ViewData["category"] = "";
            return View("~/Views/Pages/nowOpen.cshtml");
        }

        public string AutoCompleteProduct(string term)
        {
            var product = (from r in db.tbl_product
                           join c in db.tbl_product_color on r.ID equals c.product
                           join i in db.tbl_product_image on c.ID equals i.color
                           where r.name.ToLower().Contains(term.ToLower()) && r.status == true
                           select new { cat = "PRODUCT", c.ID, r.name }).Distinct().Concat((from t in db.tbl_tenant
                                                                                            where t.name.ToLower().Contains(term.ToLower())
                                                                                            select new { cat = "BRAND", t.ID, t.name }
                         ));
            var json = new JavaScriptSerializer().Serialize(product);
            return json;

           // return Json(product, JsonRequestBehavior.AllowGet);
        }
        

    }
}
