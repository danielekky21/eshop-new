﻿using DynamicContent.Helper;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Globalization;
using Newtonsoft.Json;
using TweetSharp;
using System.Drawing;
using System.Web.Script.Serialization;

namespace DynamicContent.Controllers
{

    public class AuthController : Controller
    {
        //
        // GET: /Auth/
        public string baseURL
        {
            get
            {
                return string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            }
        }
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        private string _consumerKey = System.Configuration.ConfigurationManager.AppSettings["consumerKey"];
        private string _consumerSecret = System.Configuration.ConfigurationManager.AppSettings["consumerSecret"];
        private string _AccessToken = System.Configuration.ConfigurationManager.AppSettings["AccessToken"];
        private string _AccessTokenSecret = System.Configuration.ConfigurationManager.AppSettings["AccessTokenSecret"];

        public ActionResult Index()
        {
           
            return View("~/Views/Pages/login.cshtml");
        }

        //public ActionResult TwitterLogin()
        //{
        //    // Step 1 - Retrieve an OAuth Request Token
        //    TwitterService service = new TwitterService(_consumerKey, _consumerSecret);

        //    var url = Url.Action("AuthorizeCallback", "Auth", null, "http");
        //    // This is the registered callback URL
        //    OAuthRequestToken requestToken = service.GetRequestToken(url);

        //    // Step 2 - Redirect to the OAuth Authorization URL
        //    Uri uri = service.GetAuthorizationUri(requestToken);
        //    return new RedirectResult(uri.ToString(), false /*permanent*/);
        //}

        //public ActionResult AuthorizeCallback(string oauth_token, string oauth_verifier)
        //{
        //    var requestToken = new OAuthRequestToken { Token = oauth_token };

        //    TwitterService service = new TwitterService(_consumerKey, _consumerSecret);
        //    OAuthAccessToken accessToken = service.GetAccessToken(requestToken, oauth_verifier);

        //    service.AuthenticateWith(accessToken.Token, accessToken.TokenSecret);
        //    TwitterUser user = service.VerifyCredentials(new VerifyCredentialsOptions());

        //    FormsAuthentication.SetAuthCookie(user.ScreenName, false);
        //    string Nonce = GenerateNonce();
        //    string timestamp = GenerateTimeStamp();
        //    TwitterUrls TwitterUrls = new TwitterUrls("https://api.twitter.com/1.1/account/verify_credentials.json");
        //    List<KeyValuePair<string, string>> Parameters = new List<KeyValuePair<string, string>>();
        //    Parameters.Add(new KeyValuePair<string, string>("include_email", "true"));
        //    Parameters.Add(new KeyValuePair<string, string>("oauth_consumer_key", _consumerKey));
        //    Parameters.Add(new KeyValuePair<string, string>("oauth_nonce", Nonce));
        //    Parameters.Add(new KeyValuePair<string, string>("oauth_signature_method", "HMAC-SHA1"));
        //    Parameters.Add(new KeyValuePair<string, string>("oauth_timestamp", timestamp));
        //    Parameters.Add(new KeyValuePair<string, string>("oauth_token", oauth_token));
        //    Parameters.Add(new KeyValuePair<string, string>("oauth_version", "1.0"));
        //    string signature = TwitterUrls.GenerateSignature(_consumerSecret, Parameters, accessToken.TokenSecret);
        //    Parameters.Insert(3, new KeyValuePair<string, string>("oauth_signature", signature));

        //    string retVal = string.Empty;

        //    string url = TwitterUrls.GenerateCallingUrls(Parameters);
        //    Console.WriteLine(url);
        //    using (WebClient client = new WebClient())
        //    {
        //        retVal = client.DownloadString(url);
        //    }

        //    if (retVal == "")
        //        retVal = "stream was empty.";
        //    //TwitterUserModel userModel = new TwitterUserModel();
        //    //userModel.id = user.Id.ToString();
        //    //userModel.name = user.Name;
        //    return RedirectToAction("Index");
        //}

        //TWITTER SECTION

        public ActionResult TwitterLogin()
        {
            // pass in the consumerkey, consumersecret, and return url to get back the token
            NameValueCollection dict = new TwitterClient().GenerateTokenUrl(_consumerKey, _consumerSecret, baseURL + "Auth/TwitterCallback");
            // set a session var so we can use it when twitter calls us back
            Session["dict"] = dict;
            // call "authenticate" not "authorize" as the twitter docs say so the user doesn't have to reauthorize the app everytime
            return Redirect("https://api.twitter.com/oauth/authenticate?oauth_token=" + dict["oauth_token"]);
        }

        public ActionResult TwitterCallback(string oauth_token, string oauth_verifier)
        {
            TwitterClient twitterClient = new TwitterClient();
            NameValueCollection dict = (NameValueCollection)Session["dict"];
            NameValueCollection UserDictionary = HttpUtility.ParseQueryString(twitterClient.GetAccessToken(_consumerKey, _consumerSecret, oauth_token, oauth_verifier, dict));
            TwitterUserModel twitterUser = JsonConvert.DeserializeObject<TwitterUserModel>(twitterClient.GetTwitterUser(_consumerKey, _consumerSecret, UserDictionary));
            // do whatever you need with the twitter user (add to database, set formsauth cookie etc)
            TempData["twitterUser"] = twitterUser;
            var userlogin = obj.GetUserByTwitter(twitterUser.id);
            if (userlogin != null)
            {
                _sessionFront.firstName = userlogin.firstName;
                _sessionFront.lastName = userlogin.lastName;
                _sessionFront.userIdLogin = userlogin.ID;

                string sessionId = System.Web.HttpContext.Current.Session.SessionID;
                _sessionFront.sessionId = sessionId;

                obj.UpdateSessionId(userlogin.email, sessionId);

                if (HttpContext.Session["fromLogin"] != null)
                {
                    string fromLogin = HttpContext.Session["fromLogin"].ToString();
                    return RedirectToAction(fromLogin, "Customer");
                }
                else
                {
                    return RedirectToAction("Myaccount", "Customer");
                }
            }
            else
                return RedirectToAction("CheckTwitterLogin");
        }

        public ActionResult CheckTwitterLogin()
        {
            TwitterUserModel data = null;
            if (TempData["twitterUser"] != null)
            {
                data = (TwitterUserModel)TempData["twitterUser"];
            }
            if (data != null)
            {
                if (data.email != null)
                {
                    var userlogin = obj.GetUserLogin(data.email);
                    if (userlogin != null)
                    {
                        _sessionFront.firstName = userlogin.firstName;
                        _sessionFront.lastName = userlogin.lastName;
                        _sessionFront.userIdLogin = userlogin.ID;

                        string sessionId = System.Web.HttpContext.Current.Session.SessionID;
                        _sessionFront.sessionId = sessionId;

                        obj.UpdateSessionId(userlogin.email, sessionId);

                        if (HttpContext.Session["fromLogin"] != null)
                        {
                            string fromLogin = HttpContext.Session["fromLogin"].ToString();
                            return RedirectToAction(fromLogin, "Customer");
                        }
                        else
                        {
                            return RedirectToAction("Myaccount", "Customer");
                        }

                    }
                    else
                    {
                        ViewData["twitterUserEmail"] = data;
                        //return RedirectToAction("RegistTwitterEmail");
                        return View("~/Views/Pages/twitterlogin.cshtml");
                    }
                }
                else
                {
                    ViewData["twitterUserEmail"] = data;
                    //return RedirectToAction("CompleteTwitterLogin"); 
                    return View("~/Views/Pages/twitterlogin.cshtml");
                }
            }
            else
                return RedirectToAction("Login","Customer");
        }
        [HttpPost]
        public ActionResult CompleteTwitterLogin()
        {
            var user = new DynamicContent.Models.tbl_user_register();
            var userlogin = obj.GetUserLogin(Request.Form["email"]);
            if (userlogin != null)
            {
                _sessionFront.firstName = userlogin.firstName;
                _sessionFront.lastName = userlogin.lastName;
                _sessionFront.userIdLogin = userlogin.ID;

                string sessionId = System.Web.HttpContext.Current.Session.SessionID;
                _sessionFront.sessionId = sessionId;

                obj.UpdateSessionId(userlogin.email, sessionId);

                if (HttpContext.Session["fromLogin"] != null)
                {
                    string fromLogin = HttpContext.Session["fromLogin"].ToString();
                    return RedirectToAction(fromLogin, "Customer");
                }
                else
                {
                    return RedirectToAction("Myaccount", "Customer");
                }

            }
            else
            {
                user.firstName = Request.Form["name"];
                user.email = Request.Form["email"];
                user.twitter_id = Request["id"];
                user.password = CustomEncryption.HashStringSHA1(Request["password"]);
                user.gender = "";
                user.regist_date = DateTime.Now;
                user.modified_date = DateTime.Now;
                obj.UserRegister(user);
                var u = obj.GetUserLogin(user.email);
                _sessionFront.userIdLogin = u.ID;
                _sessionFront.firstName = u.firstName;
                _sessionFront.lastName = u.lastName;

                string sessionId = System.Web.HttpContext.Current.Session.SessionID;
                _sessionFront.sessionId = sessionId;

                obj.UpdateSessionId(user.email, sessionId);

                TempData["msgSuccess"] = "success: Successfully add new user";
                if (HttpContext.Session["fromLogin"] != null)
                {
                    string fromLogin = HttpContext.Session["fromLogin"].ToString();
                    return RedirectToAction(fromLogin);
                }
                else
                {
                    return RedirectToAction("Myaccount", "Customer");
                }
            }

        }

        public ActionResult RegistTwitterEmail()
        {
            var user = new DynamicContent.Models.tbl_user_register();
            TwitterUserModel data = null;
            if (TempData["twitterUserEmail"] != null)
            {
                data = (TwitterUserModel)TempData["twitterUserEmail"];
            }
            user.firstName = data.name;
            user.email = data.email;
            user.gender = "";
            user.regist_date = DateTime.Now;
            user.modified_date = DateTime.Now;
            obj.UserRegister(user);
            var u = obj.GetUserLogin(user.email);
            _sessionFront.userIdLogin = u.ID;
            _sessionFront.firstName = u.firstName;
            _sessionFront.lastName = u.lastName;

            string sessionId = System.Web.HttpContext.Current.Session.SessionID;
            _sessionFront.sessionId = sessionId;

            obj.UpdateSessionId(user.email, sessionId);

            TempData["msgSuccess"] = "success: Successfully add new user";
            if (HttpContext.Session["fromLogin"] != null)
            {
                string fromLogin = HttpContext.Session["fromLogin"].ToString();
                return RedirectToAction(fromLogin);
            }
            else
            {
                return RedirectToAction("Myaccount", "Customer");
            }
        }
        public class TwitterClient
        {
            private const string OAuthVersion = "1.0";
            private string GenerateNonce()
            {

                return Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture)));
            }

            private string GenerateTimeStamp()
            {
                // this just returns a unix epoch timestamp
                var _timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0);
                var _oathTimestamp = Convert.ToInt64(_timeSpan.TotalSeconds).ToString(CultureInfo.InvariantCulture);
                return _oathTimestamp.ToString();
            }
            public NameValueCollection GenerateTokenUrl(string ConsumerKey, string ConsumerSecret, string CallBackUrl)
            {
                // get the nonce value
                string Nonce = GenerateNonce();
                // get the timestamp
                string timestamp = GenerateTimeStamp();
                // instantiate the twitterurls passing in the request_token url to the constructor
                TwitterUrls TwitterUrls = new TwitterUrls("https://api.twitter.com/oauth/request_token");
                string TokenResponse = string.Empty;
                //construct the parameter list for this request
                List<KeyValuePair<string, string>> Parameters = new List<KeyValuePair<string, string>>();
                Parameters.Add(new KeyValuePair<string, string>("oauth_callback", CallBackUrl));
                Parameters.Add(new KeyValuePair<string, string>("oauth_consumer_key", ConsumerKey));
                Parameters.Add(new KeyValuePair<string, string>("oauth_nonce", Nonce));
                Parameters.Add(new KeyValuePair<string, string>("oauth_signature_method", "HMAC-SHA1"));
                Parameters.Add(new KeyValuePair<string, string>("oauth_timestamp", timestamp));
                Parameters.Add(new KeyValuePair<string, string>("oauth_version", OAuthVersion));
                // generate the signature (I translated this process from PHP)
                string signature = TwitterUrls.GenerateSignature(ConsumerSecret, Parameters);
                // I read somewhere that the parameter order matters so insert the signature parameter in the same sequence as was done in PHP...too lazy to change it and test it now
                Parameters.Insert(3, new KeyValuePair<string, string>("oauth_signature", signature));
                // generate the url to call
                string url = TwitterUrls.GenerateCallingUrls(Parameters);
                // call the generated url
                using (WebClient client = new WebClient())
                {
                    TokenResponse = client.DownloadString(url);
                }
                // put the querystring in the response into a NVC
                NameValueCollection dict = HttpUtility.ParseQueryString(TokenResponse);
                return dict;
            }

            public string GetAccessToken(string ConsumerKey, string ConsumerSecret, string oauth_token, string oauth_verifier, NameValueCollection dict)
            {
                // see all comments from GenerateTokenUrl as they apply here as well
                string Nonce = GenerateNonce();
                string timestamp = GenerateTimeStamp();
                TwitterUrls TwitterUrls = new TwitterUrls("https://api.twitter.com/oauth/access_token");
                List<KeyValuePair<string, string>> Parameters = new List<KeyValuePair<string, string>>();

                Parameters.Add(new KeyValuePair<string, string>("oauth_consumer_key", ConsumerKey));
                Parameters.Add(new KeyValuePair<string, string>("oauth_nonce", Nonce));
                Parameters.Add(new KeyValuePair<string, string>("oauth_signature_method", "HMAC-SHA1"));
                Parameters.Add(new KeyValuePair<string, string>("oauth_timestamp", timestamp));
                Parameters.Add(new KeyValuePair<string, string>("oauth_token", oauth_token));
                Parameters.Add(new KeyValuePair<string, string>("oauth_verifier", oauth_verifier));
                Parameters.Add(new KeyValuePair<string, string>("oauth_version", OAuthVersion));
                string signature = TwitterUrls.GenerateSignature(ConsumerSecret, Parameters, dict["oauth_token_secret"]);
                Parameters.Insert(2, new KeyValuePair<string, string>("oauth_signature", signature));

                string url = TwitterUrls.GenerateCallingUrls(Parameters);
                string retVal = string.Empty;
                using (WebClient client = new WebClient())
                {
                    retVal = client.DownloadString(url);
                }
                return retVal;
            }

            public string GetTwitterUser(string ConsumerKey, string ConsumerSecret, NameValueCollection dict)
            {
                // see all comments from GenerateTokenUrl as they apply here as well
                string Nonce = GenerateNonce();
                string timestamp = GenerateTimeStamp();
                TwitterUrls TwitterUrls = new TwitterUrls("https://api.twitter.com/1.1/account/verify_credentials.json");
                List<KeyValuePair<string, string>> Parameters = new List<KeyValuePair<string, string>>();
                Parameters.Add(new KeyValuePair<string, string>("include_email", "true"));
                Parameters.Add(new KeyValuePair<string, string>("oauth_consumer_key", ConsumerKey));
                Parameters.Add(new KeyValuePair<string, string>("oauth_nonce", Nonce));
                Parameters.Add(new KeyValuePair<string, string>("oauth_signature_method", "HMAC-SHA1"));
                Parameters.Add(new KeyValuePair<string, string>("oauth_timestamp", timestamp));
                Parameters.Add(new KeyValuePair<string, string>("oauth_token", dict["oauth_token"]));
                Parameters.Add(new KeyValuePair<string, string>("oauth_version", OAuthVersion));
                string signature = TwitterUrls.GenerateSignature(ConsumerSecret, Parameters, dict["oauth_token_secret"]);
                Parameters.Insert(3, new KeyValuePair<string, string>("oauth_signature", signature));

                string retVal = string.Empty;

                string url = TwitterUrls.GenerateCallingUrls(Parameters);
                using (WebClient client = new WebClient())
                {
                    retVal = client.DownloadString(url);
                }
                return retVal;
            }
        }

        public class TwitterUrls
        {
            string _EndPointUrl = string.Empty;
            URLEncoder urlEncoder = new URLEncoder();
            public TwitterUrls(string EndPointUrl)
            {
                _EndPointUrl = EndPointUrl;
            }
            public string GenerateSignature(string ConsumerSecret, List<KeyValuePair<string, string>> Parameters, string TokenSecret = null)
            {

                string SignatureInner = string.Empty;
                // start the string to be included in the signature with "GET&" ... again just modeling this after the PHP translation
                string Signature = "GET&";
                // the first & is not urlencoded
                Signature += urlEncoder.UrlEncode(_EndPointUrl) + "&";
                // loop over the parameters and add them to the signatures string
                foreach (KeyValuePair<string, string> item in Parameters)
                {
                    if (item.Key == "oauth_callback")
                    {
                        // the callback url needs to be encoded twice, so do it first now
                        SignatureInner += item.Key + "=" + urlEncoder.UrlEncode(item.Value) + "&";
                    }
                    else
                    {
                        SignatureInner += item.Key + "=" + item.Value + "&";
                    }
                }
                // strip off the last "&"
                SignatureInner = SignatureInner.Substring(0, SignatureInner.Length - 1);
                // url encode the inner signature
                SignatureInner = urlEncoder.UrlEncode(SignatureInner);
                // add the inner signature to the signature
                Signature += SignatureInner;
                // url encode the whole thing and include the token secret as part of the key if it's passed in
                return urlEncoder.UrlEncode(new HashHMac().CreateHMacHash(ConsumerSecret + "&" + (!string.IsNullOrEmpty(TokenSecret) ? TokenSecret : ""), Signature));
            }

            public string GenerateCallingUrls(List<KeyValuePair<string, string>> Parameters)
            {
                // this method generates the actual url to call, see comments from above method
                string url = _EndPointUrl + "?";
                foreach (KeyValuePair<string, string> item in Parameters)
                {
                    if (item.Key == "oauth_callback")
                    {
                        url += item.Key + "=" + urlEncoder.UrlEncode(item.Value) + "&";
                    }
                    else
                    {
                        url += item.Key + "=" + item.Value + "&";
                    }
                }
                url = url.Substring(0, url.Length - 1);
                return url;
            }
        }

        public class URLEncoder
        {
            public string UrlEncode(string url)
            {
                // found this method here: http://www.codeproject.com/Questions/832905/Is-there-any-equivalent-for-rawurlencode-in-Csharp
                Dictionary<string, string> toBeEncoded = new Dictionary<string, string>() { { "%", "%25" }, { "!", "%21" }, { "#", "%23" }, { " ", "%20" },
        { "$", "%24" }, { "&", "%26" }, { "'", "%27" }, { "(", "%28" }, { ")", "%29" }, { "*", "%2A" }, { "+", "%2B" }, { ",", "%2C" },
        { "/", "%2F" }, { ":", "%3A" }, { ";", "%3B" }, { "=", "%3D" }, { "?", "%3F" }, { "@", "%40" }, { "[", "%5B" }, { "]", "%5D" } };
                Regex replaceRegex = new Regex(@"[%!# $&'()*+,/:;=?@\[\]]");
                MatchEvaluator matchEval = match => toBeEncoded[match.Value];
                string encoded = replaceRegex.Replace(url, matchEval);
                return encoded;
            }
        }

        public class HashHMac
        {
            public string CreateHMacHash(string key, string message)
            {
                // found this solution here: http://stackoverflow.com/questions/8780261/hmc-sha1-hash-java-producing-different-hash-output-than-c-sharp
                HMAC hasher = new HMACSHA1(Encoding.UTF8.GetBytes(key));
                byte[] data = hasher.ComputeHash(Encoding.UTF8.GetBytes(message));
                return Convert.ToBase64String(data);
            }
        }

        public void postToTwitter()
        {
            string dbToken = System.Configuration.ConfigurationManager.AppSettings["AccessToken"];
            string dbSecret = System.Configuration.ConfigurationManager.AppSettings["AccessTokenSecret"];

            bool hasToken = false;
            var service = new TwitterService(_consumerKey, _consumerSecret);
            //service.AuthenticateWith(dbToken, dbSecret);

            if (Request["oauth_token"] == null)
            {
                OAuthRequestToken requestToken = service.GetRequestToken(Request.Url.AbsoluteUri);
                Response.Redirect(string.Format("http://twitter.com/oauth/authorize?oauth_token={0}", requestToken.Token));
            }
            else
            {
                string requestToken = Request["oauth_token"].ToString();
                string pin = Request["oauth_verifier"].ToString();
                // Using the values Twitter sent back, get an access token from Twitter
                var accessToken = service.GetAccessToken(new OAuthRequestToken { Token = requestToken }, pin);
                // Use that access token and send a tweet on the user's behalf
                if (accessToken != null && !string.IsNullOrEmpty(accessToken.Token) && !string.IsNullOrEmpty(accessToken.TokenSecret))
                {
                    service.AuthenticateWith(accessToken.Token, accessToken.TokenSecret);
                    uploadPhotosTwitter(ref service);
                }
            }
        }

        private void uploadPhotosTwitter(ref TwitterService tService)
        {
            Bitmap img = new Bitmap(Server.MapPath("~/Images/h-product-17.jpg"));
            if (img != null)
            {
                MemoryStream ms = new MemoryStream();
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                ms.Seek(0, SeekOrigin.Begin);
                Dictionary<string, Stream> images = new Dictionary<string, Stream> { { "mypicture", ms } };

                var result = tService.SendTweetWithMedia(new SendTweetWithMediaOptions { Status = "test", Images = images });
                if (result != null && result.Id > 0)
                {
                    Response.Redirect("https://twitter.com");
                }
                else
                {
                    Response.Write("fails to update status");
                }
            }
        }

        // PATH SECTION
        public ActionResult getpathtoken()
        {
            string code = this.Request.QueryString["code"];
            string authUrl = "https://partner.path.com/oauth2/access_token";

            var sign = "grant_type=authorization_code" + "&code=" + code + "&client_id=449d6b103a4a1bd33308a0ab89028897285fd863&client_secret=c63d7bada2dffa4225acc61ad20a311f5dec7199";
            
            HttpWebRequest webRequest = WebRequest.Create(authUrl + "?" + sign) as HttpWebRequest;
            webRequest.Method = "POST";

            //This "application/x-www-form-urlencoded"; line is important
            webRequest.ContentType = "application/x-www-form-urlencoded";

            webRequest.ContentLength = sign.Length;

            StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream());
            requestWriter.Write(sign);
            requestWriter.Close();

            StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream());

            var accessToken = responseReader.ReadToEnd().ToString();
            pathToken path = JsonConvert.DeserializeObject<pathToken>(accessToken);

            TempData["accessToken"] = path;
            return RedirectToAction("postPathPhoto");
            //return RedirectToAction(Session["imgpath"].ToString());
        }

        public ActionResult postPathPhoto()
        {
            pathToken dt = null;
            if (TempData["accessToken"] != null)
            {
                dt = (pathToken)TempData["accessToken"];
            }
            string accessToken = dt.access_token;
            Dictionary<string, string> LY = new Dictionary<string, string>();
            LY.Add("source_url", Session["imgpath"].ToString());
            LY.Add("private", "false");
            LY.Add("caption", Session["captionpath"].ToString());
            LY.Add("links", "http://pi.demobatavianet.com/");


            var json = new JavaScriptSerializer().Serialize(LY);

            var baseAddress = "https://partner.path.com/1/moment/photo";
            //string accessToken = "bb9a93d86fa0d0363fa41ea952369bc64967e30f";
            string baseurl = "https://partner.path.com/1/moment/photo";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseurl);
            request.Headers.Add("Authorization", "Bearer " + accessToken);
            request.ContentType = "application/json";
            request.Method = "POST";
            request.UserAgent = "curl/7.37.0";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string data = "";
                streamWriter.Write(json);
            }            
            var response = request.GetResponse();
            Session["imgpath"] = null;
            Session["captionpath"] = null;
            return Redirect(Session["urlpath"].ToString());
        }
    } }

