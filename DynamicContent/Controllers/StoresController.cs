﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DynamicContent.Controllers
{
    public class StoresController : Controller
    {
        //
        // GET: /Stores/
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        public ActionResult Index(string id)
        {
            ViewData["storesId"] = id;
            if(id== "allstores")
            {
                return View("~/Views/Stores/allstores.cshtml");
            }
            else
            {
                string idTenant = id.Split('-')[0];
                var img = db.Banner_tenant.FirstOrDefault(x => x.TenantID == idTenant && x.IsActive == true);

                if (img != null)
                {
                    ViewBag.imgBanner = img.ImageTenantPath;
                }
                else
                {
                    ViewBag.imgBanner = string.Empty;
                }
                return View();
            }
            
        }

    }
}
