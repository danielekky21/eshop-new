﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DynamicContent.Controllers
{
    public class CategoryController : Controller
    {
        //
        // GET: /Category/
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        public ActionResult Index(int ? id, string category, string submenu)
        {
            
            ViewData["category"] = category;
            ViewData["id-category"] = id;
            ViewData["flashsale-detail"] = new List<DynamicContent.Models.FlashHeaderDetail>();

            var day = DateTime.Now.DayOfWeek.ToString();
            var timeNow = DateTime.Now.TimeOfDay;
            var flashSaleData = db.FlashSaleHeaders.FirstOrDefault(b => b.FlashSaleDay == day && b.IsActive == true && b.FlashSaleTimeBegin <= timeNow && b.FlashSaleTimeEnd >= timeNow);
            ViewData["flashsale-header"] = flashSaleData;
            if (flashSaleData != null)
            {
                var flashSaleDetailData = db.FlashHeaderDetails.Where(b => b.FlashSaleHeaderID == flashSaleData.FlashSaleHeaderID && b.isActive == true && b.qty > 0);
                ViewData["flashsale-detail"] = flashSaleDetailData.ToList();
            }
            if (id.HasValue && category != "")
            {
                return View("~/Views/Category/category-detail.cshtml");
            }
            else if(category != "")
            {
                return View("~/Views/Category/category.cshtml");
            }else
            {
                return View("~/Views/Pages/home.cshtml");
            }
        }

        //public ActionResult WhatsNew()
        //{
        //    return View("~/Views/Category/whatsnew.cshtml");
        //}
    }
}
