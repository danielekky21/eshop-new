﻿using DynamicContent.Helper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Mail;
using System.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;
using DynamicContent.Models;
using DynamicContent.Areas.SysManager.Helper;

namespace DynamicContent.Controllers
{
    public class CustomerController : Controller
    {
        //
        // GET: /Customer/
        public string baseURL
        {
            get
            {
                return string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            }
        }
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        DynamicContent.Models.EmailModel objemail = new DynamicContent.Models.EmailModel();
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        private string GmailID = System.Configuration.ConfigurationManager.AppSettings["GmailID"];
        private string GmailPass = System.Configuration.ConfigurationManager.AppSettings["GmailPass"];

        public ActionResult Index()
        {
            return null;
        }

        public ActionResult AddToCart(int id)
        {
            var s = new List<int>();
            if (HttpContext.Session["product"] != null)
            {
                s = HttpContext.Session["product"] as List<int>;
            }
            s.Add(id);
            HttpContext.Session["product"] = s;
            return null;
        }

        public ActionResult AddToCartDetail(string id, string qty, string size)
        {
            var day = DateTime.Now.DayOfWeek.ToString();
            var timeNow = DateTime.Now.TimeOfDay;
            var flashSaleData = db.FlashSaleHeaders.FirstOrDefault(x => x.FlashSaleDay == day && x.IsActive == true && x.FlashSaleTimeBegin <= timeNow && x.FlashSaleTimeEnd >= timeNow);

            string[] p = { id, qty, size };
            var joinP = string.Join("|", p);
            var prodDet = new List<string>();
            string result = "";
            if (_sessionFront.userIdLogin != 0)
            {
                var prods = obj.GetProductByColor(int.Parse(id));
                if (flashSaleData != null)
                {
                    var flashSaleDetailData = db.FlashHeaderDetails.FirstOrDefault(x => x.FlashSaleHeaderID == flashSaleData.FlashSaleHeaderID && x.tbl_product.ID == prods.product && x.isActive == true && x.qty > 0);
                    if (flashSaleDetailData != null)
                    {
                        if (Session[prods.product.ToString()] == null)
                        {
                            Session[prods.product.ToString()] = flashSaleDetailData.qty;
                        }
                        var currentCart = obj.GetMyCart(_sessionFront.userIdLogin);
                        var currentProds = currentCart.FirstOrDefault(x => x.product == prods.product);
                        if (currentProds != null)
                        {
                            Session[prods.product.ToString()] = (int)Session[prods.product.ToString()] - currentProds.qty;
                        }
                        Session[prods.product.ToString()] = (int)Session[prods.product.ToString()] - int.Parse(qty);
                        if ((int)Session[prods.product.ToString()] < 0)
                        {
                            Session[prods.product.ToString()] = flashSaleDetailData.qty;
                            return Json(new { success = true, responseText = "Qty 0" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        Session[prods.product.ToString()] = null;
                    }
                }
                var prod = obj.GetExistCart(_sessionFront.userIdLogin, size, Convert.ToInt32(p[0]));
                if (prod.Count() > 0)
                {
                    //if (Session[prods.product.ToString()] != null)
                    //{
                    //    Session[prods.product.ToString()] = (int)Session[prods.product.ToString()] - prod.FirstOrDefault().qty;
                    //    if ((int)Session[prods.product.ToString()] < 0)
                    //    {
                    //        return Json(new { success = true, responseText = "Qty 0" }, JsonRequestBehavior.AllowGet);
                    //    }
                    //}
                    var item = obj.GetExistCart(_sessionFront.userIdLogin, size, Convert.ToInt32(p[0])).FirstOrDefault();
                    var newQty = item.qty + Convert.ToInt32(qty);
                    item.qty = newQty;
                    obj.changeQtyReservation(item);
                    Session["productDetail"] = null;
                }
                else
                {
                    if (HttpContext.Session["productDetail"] != null)
                    {
                        prodDet = HttpContext.Session["productDetail"] as List<string>;
                        int exist = prodDet.Where(x => x.Split('|')[0] == p[0] && x.Split('|')[2] == p[2]).Count();
                        var itemToRemove = prodDet.SingleOrDefault(x => x.Split('|')[0] == p[0] && x.Split('|')[2] == p[2]);
                        if (exist > 0)
                        {
                            prodDet.Remove(itemToRemove);
                            var split = itemToRemove.Split('|').ToList<string>();
                            var addQty = Convert.ToInt32(p[1]) + Convert.ToInt32(split[1]);
                            string[] addP = { id, addQty.ToString(), size };
                            var joinPro = string.Join("|", addP);
                            prodDet.Add(joinPro);
                        }
                        else
                        {
                            prodDet.Add(joinP);
                        }
                    }
                    else
                    {
                        prodDet.Add(joinP);
                    }
                    HttpContext.Session["productDetail"] = prodDet;
                }
            }
            else
            {
                //if (HttpContext.Session["productDetail"] != null)
                //{
                //    prodDet = HttpContext.Session["productDetail"] as List<string>;
                //    int exist = prodDet.Where(x => x.Split('|')[0] == p[0] && x.Split('|')[2] == p[2]).Count();
                //    var itemToRemove = prodDet.SingleOrDefault(x => x.Split('|')[0] == p[0] && x.Split('|')[2] == p[2]);
                //    if (exist > 0)
                //    {
                //        prodDet.Remove(itemToRemove);
                //        var split = itemToRemove.Split('|').ToList<string>();
                //        var addQty = Convert.ToInt32(p[1]) + Convert.ToInt32(split[1]);
                //        string[] addP = { id, addQty.ToString(), size };
                //        var joinPro = string.Join("|", addP);
                //        prodDet.Add(joinPro);
                //    }
                //    else
                //    {
                //        prodDet.Add(joinP);
                //    }
                //}
                //else
                //{
                //    prodDet.Add(joinP);
                //}
                //HttpContext.Session["productDetail"] = prodDet;
                //HttpContext.Session["check"] = result;

                return Json(new { success = true, responseText = "Not Loggin in" }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        public ActionResult RemoveMycart(int id)
        {
            var del = (from r in db.tbl_cart
                       where r.ID == id
                       select r).FirstOrDefault();
            db.tbl_cart.Remove(del);
            db.SaveChanges();

            return null;
        }

        public ActionResult AddToWishlistFromCart(int id, int itemID, int color)
        {
            string[] p = { id.ToString(), color.ToString() };
            var joinP = string.Join("|", p);

            var s = new List<string>();
            if (HttpContext.Session["wishlist"] != null)
            {
                s = HttpContext.Session["wishlist"] as List<string>;
            }
            s.Add(joinP);

            HttpContext.Session["wishlist"] = s;
            if (_sessionFront.userIdLogin != 0)
            {
                var del = (from r in db.tbl_cart
                           where r.ID == itemID
                           select r).FirstOrDefault();
                db.tbl_cart.Remove(del);
                db.SaveChanges();
            }
            return null;
        }

        public ActionResult AddToWishlist(int id, int color)
        {
            string[] p = { id.ToString(), color.ToString() };
            var joinP = string.Join("|", p);

            var s = new List<string>();
            if (HttpContext.Session["wishlist"] != null)
            {
                s = HttpContext.Session["wishlist"] as List<string>;
            }
            s.Add(joinP);

            HttpContext.Session["wishlist"] = s;

            return null;
        }

        public ActionResult wishlist()
        {
            if (_sessionFront.userIdLogin != null)
                obj.CheckSessionId(_sessionFront.userIdLogin);
            if (_sessionFront.userIdLogin == 0)
            {
                return RedirectToAction("Login");
            }
            else
            {
                TempData["wishlist"] = "active";
                var wishlist = new List<_object.ProductPi>();

                var s = new List<string>();
                if (HttpContext.Session["wishlist"] != null)
                {
                    s = HttpContext.Session["wishlist"] as List<string>;
                }

                var allProd = obj.GetProductPi();
                foreach (var r in s)
                {
                    var split = r.Split('|').ToList<string>();
                    var p = allProd.Where(x => x.id == Convert.ToInt32(split[0])).FirstOrDefault();
                    var exist = obj.GetExistWishlist(Convert.ToInt32(split[0]), Convert.ToInt32(split[1]), _sessionFront.userIdLogin).Count();
                    if (exist == 0)
                    {
                        wishlist.Add(p);
                        DynamicContent.Models.tbl_wishlist w = new DynamicContent.Models.tbl_wishlist();
                        w.user_register = _sessionFront.userIdLogin;
                        w.product = p.id;
                        w.color = Convert.ToInt32(split[1]);
                        w.created_date = DateTime.Now;
                        db.tbl_wishlist.Add(w);
                        db.SaveChanges();
                    }

                    Session["wishlist"] = null;
                }

                ViewData["wishlist"] = wishlist;

                return View("~/Views/Pages/wishlist.cshtml");
            }
        }

        public ActionResult removeWishlist(int id)
        {
            obj.RemoveWishlist(id);
            //db.tbl_wishlist.

            return null;
        }

        public int LoadCart()
        {
            var s = new List<int>();
            int mycart = 0;
            if (HttpContext.Session["product"] != null)
            {
                s = HttpContext.Session["product"] as List<int>;
            }
            if (_sessionFront.userIdLogin != 0)
            {
                mycart = obj.GetMyCart(_sessionFront.userIdLogin).Count();
            }
            return s.Count() + mycart;
        }

        public virtual JsonResult LoadCartDetail()
        {
            var s = new List<string>();
            int mycart = 0;
            if (HttpContext.Session["productDetail"] != null)
            {
                s = HttpContext.Session["productDetail"] as List<string>;
            }
            if (_sessionFront.userIdLogin != 0)
            {
                mycart = obj.GetMyCart(_sessionFront.userIdLogin).Count();
            }
            var data = s.Count() + mycart;
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadItem()
        {
            var product = new List<_object.ProductPi>();

            var s = new List<string>();
            if (HttpContext.Session["productDetail"] != null)
            {
                s = HttpContext.Session["productDetail"] as List<string>;
            }
            var href = "";
            if (_sessionFront.userIdLogin == 0)
            {
                href = "/customer/Login?to=MyCart";
            }
            else
            {
                href = "/customer/MyCart";
            }
            TempData["href"] = href;
            ViewData["check"] = HttpContext.Session["check"];
            ViewData["productDetail"] = s;
            ViewBag.IsFlashSale = false;
            ViewBag.FlashSaleHeaderData = new DynamicContent.Models.FlashSaleHeader();
            ViewBag.FlashSaleDetail = new DynamicContent.Models.FlashHeaderDetail();
            var day = DateTime.Now.DayOfWeek.ToString();
            var timeNow = DateTime.Now.TimeOfDay;
            var flashSaleData = db.FlashSaleHeaders.FirstOrDefault(x => x.FlashSaleDay == day && x.IsActive == true && x.FlashSaleTimeBegin <= timeNow && x.FlashSaleTimeEnd >= timeNow);
            if (flashSaleData != null)
            {
                var flashSaleDetailData = db.FlashHeaderDetails.Where(x => x.FlashSaleHeaderID == flashSaleData.FlashSaleHeaderID && x.isActive == true && x.qty > 0).ToList();
                ViewBag.IsFlashSale = true;
                ViewBag.FlashSaleDetail = flashSaleDetailData;
            }
            return PartialView();
        }

        public ActionResult myOrder()
        {
            if (_sessionFront.userIdLogin != null)
                obj.CheckSessionId(_sessionFront.userIdLogin);
            if (_sessionFront.userIdLogin == 0)
            {
                return RedirectToAction("Login");
            }
            else
            {
                TempData["myorder"] = "active";
                return View("~/Views/Pages/myReservation.cshtml");
            }
        }

        public ActionResult Login(string msg)
        {
            if (_sessionFront.userIdLogin == 0)
            {
                if (msg == "nolog")
                {
                    TempData["msgNotLogin"] = "Please login before you continue shopping";
                }
                string param = this.Request.QueryString["to"];
                HttpContext.Session["fromLogin"] = param;

                return View("~/Views/Pages/login.cshtml");
            }
            else
            {
                return RedirectToAction("Myaccount");
            }
        }


        public ActionResult forgotpassword()
        {
            string id = this.Request.QueryString["id"];
            if (string.IsNullOrEmpty(id))
            {
                return View("~/Views/Pages/forgotpassword.cshtml");
            }
            else
            {
                var user = obj.GetUserByHashCodeFront(id);
                if (user != null)
                {
                    ViewData["id"] = id;
                    return View("~/Views/Pages/forgotpasswordconfirmation.cshtml");
                }
                else
                {
                    TempData["msg"] = "Link is expire or doesn't exist";
                    return RedirectToAction("forgotpassword");
                }
            }

        }

        [HttpPost]
        public ActionResult forgotpasswordprocess(string email)
        {
            var u = obj.GetUserLogin(email);
            if (u != null)
            {
                u.modified_date = DateTime.Now;
                obj.EditUserRegister(u);

                u = obj.GetUserLogin(email); //get curent modified date
                var ukey = CustomEncryption.HashStringSHA1(u.email + " at " + u.modified_date.ToString()); //generate unique key

                var mailTemplate = (from m in db.MailMaster
                                    where m.MailDesc == "ForgotPassword"
                                    select m).FirstOrDefault();

                string content = mailTemplate.MailBodyTemplate
                    .Replace("@email", u.email)
                    .Replace("@ukey", ukey)
                    .Replace("@baseURL", baseURL)
                    ;
                //GMailer.GmailUsername = GmailID;
                //GMailer.GmailPassword = GmailPass;
                //GMailer mailer = new GMailer();
                //mailer.ToEmail = u.email;
                //mailer.Subject = mailTemplate.MailSubjectTemplate;
                //mailer.Body = content;
                //mailer.IsHtml = true;
                //mailer.Send();
                Task.Run(() => { MainModel.SendEmail(u.email, mailTemplate.MailSubjectTemplate, content); }).Wait();
                TempData["msgVerification"] = "Verification email has been sent";
                return RedirectToAction("Login");
            }
            else
            {
                TempData["msg"] = "Email not found";
                return RedirectToAction("forgotpassword");
            }
        }

        [HttpPost]
        public ActionResult ForgotPasswordChange(string id, string pass, string cpass)
        {
            var user = obj.GetUserByHashCodeFront(id);
            if (user != null)
            {
                if (!string.IsNullOrEmpty(pass)) //&& pass.Length >= 8 && pass == cpass)
                {
                    user.password = Generator.toPassword(pass);

                    obj.changePassword(user);

                    //_sessionFront.firstName = user.firstName;
                    //_sessionFront.lastName = user.lastName;
                    //_sessionFront.userIdLogin = user.ID;

                    TempData["msgSuccessChange"] = "Password successfully changed!";
                    return RedirectToAction("Login");
                    //return RedirectToAction("Myaccount");
                }
                else
                {
                    TempData["msg"] = "Wrong password";
                    return RedirectToAction("forgotpassword");
                }
            }
            else
            {
                TempData["msg"] = "Link is expire or doesn't exist";
                return RedirectToAction("forgotpassword");
            }
        }

        public ActionResult changePassword()
        {
            if (_sessionFront.userIdLogin == 0)
            {
                return RedirectToAction("Login");
            }
            else
            {
                TempData["setting"] = "active";
                return View("~/Views/Pages/changePassword.cshtml");
            }
        }



        public ActionResult changeProfile()
        {
            if (_sessionFront.userIdLogin != null)
                obj.CheckSessionId(_sessionFront.userIdLogin);
            if (_sessionFront.userIdLogin == 0)
            {
                return RedirectToAction("Login");
            }
            else
            {
                TempData["setting"] = "active";
                return View("~/Views/Pages/changeProfile.cshtml");
            }
        }

        public int CheckLogin()
        {
            return obj.CheckLogin(_sessionFront.userIdLogin);
        }

        [HttpPost]
        public ActionResult changePasswordProccess(_object.UserRegister data)
        {
            if (_sessionFront.userIdLogin != null)
                obj.CheckSessionId(_sessionFront.userIdLogin);
            var user = obj.GetUserLogin(data.email);
            if (!string.IsNullOrEmpty(data.pass))
            {
                user.password = Generator.toPassword(data.pass);
            }

            obj.changePassword(user);

            TempData["msg"] = "success: Success change password " + data.firstname + " " + data.lastname;
            return RedirectToAction("changePassword");
        }

        public ActionResult Myaccount()
        {
            if (_sessionFront.userIdLogin != null)
                obj.CheckSessionId(_sessionFront.userIdLogin);
            if ((_sessionFront.userIdLogin) == 0)
            {
                return RedirectToAction("Login");
            }
            else
            {
                TempData["myaccount"] = "active";
                return View("~/Views/Pages/myaccount.cshtml");
            }

        }

        [HttpPost]
        public string CheckEmailRegist(string email)
        {
            string retval = "";
            var rs = obj.GetUserLogin(email);
            if (rs != null)
            {
                retval = "false";
            }
            else
            {
                retval = "true";
            }

            return retval;
        }

        [HttpPost]
        public ActionResult RegisterProcess(_object.UserRegister data)
        {
            var token = obj.generateToken();
            var user = new DynamicContent.Models.tbl_user_register();
            user.firstName = data.firstname;
            user.lastName = data.lastname;
            user.email = data.email;
            user.phone = data.phone;
            user.address = data.address;
            user.city = data.city;
            user.zipcode = data.zip;
            string dob = data.dob + " " + data.mob + " " + data.yob;
            user.dob = dob;
            user.gender = data.gender;
            user.password = Generator.toPassword(data.pass);
            user.regist_date = DateTime.Now;
            user.isValidate = false;
            user.Token = token;
            obj.UserRegister(user);

            //if user check subscribe checkbox, set subscribe.
            if (data.subscribe != null)
            {
                var subs = new DynamicContent.Models.M_Subscribers();
                subs.name = data.email.Split('@')[0];
                subs.email = data.email;
                subs.type = 1;
                subs.subscribe_by = data.email;
                subs.subscribe_date = DateTime.Now;
                db.M_Subscribers.Add(subs);
                db.SaveChanges();
            }
            var mailTemplate = (from m in db.MailMaster
                                where m.MailDesc == "Email Verification"
                                select m).FirstOrDefault();
            string url = baseURL + "customer/VerifiedEmail?token=" + token + "&email=" + user.email;
            string content = mailTemplate.MailBodyTemplate
                .Replace("@name", user.firstName)
                .Replace("@url", url);
            //.Replace("@baseURL", "http://pi.demobatavianet.com/")    
            ;

            //GMailer.GmailUsername = GmailID;
            //GMailer.GmailPassword = GmailPass;
            //GMailer mailer = new GMailer();
            //mailer.ToEmail = user.email;
            //mailer.Subject = mailTemplate.MailSubjectTemplate;
            //mailer.Body = content;
            //mailer.IsHtml = true;
            //mailer.Send();
            Task.Run(() => { MainModel.SendEmail(user.email, mailTemplate.MailSubjectTemplate, content); }).Wait();
            var u = obj.GetUserLogin(user.email);
            _sessionFront.userIdLogin = u.ID;
            _sessionFront.firstName = u.firstName;
            _sessionFront.lastName = u.lastName;
            TempData["msgSuccess"] = "success: Please Check Your Email To Validate";
            if (HttpContext.Session["fromLogin"] != null)
            {
                string fromLogin = HttpContext.Session["fromLogin"].ToString();
                return RedirectToAction(fromLogin);
            }
            else
            {
                return RedirectToAction("Myaccount");
            }
        }

        [HttpPost]
        public ActionResult changeProfileProcess(_object.UserRegister data)
        {
            if (_sessionFront.userIdLogin != null)
                obj.CheckSessionId(_sessionFront.userIdLogin);

            var user = obj.GetUserLogin(data.email);
            user.firstName = data.firstname;
            user.lastName = data.lastname;
            user.phone = data.phone;
            user.address = data.address;
            user.city = data.city;
            user.zipcode = data.zip;
            string dob = data.dob + " " + data.mob + " " + data.yob;
            user.dob = dob;
            user.gender = data.gender;
            obj.EditUserRegister(user);
            TempData["msg"] = "success: Successfully update user";

            return RedirectToAction("changeProfile");

        }

        //[HttpGet]
        //public JsonResult checkEmailRegist(string email)
        //{
        //    var user = obj.GetUserLogin(email);
        //    return Json(user, JsonRequestBehavior.AllowGet);

        //}
        public ActionResult Redirect()
        {
            return View();
        }
        public ActionResult VerifiedEmail(string token, string email)
        {
            if (obj.checkToken(token, email))
            {
                var user = obj.GetUserLogin(email);
                user.isValidate = true;
                obj.EditUserRegister(user);
                _sessionFront.firstName = user.firstName;
                _sessionFront.lastName = user.lastName;
                _sessionFront.userIdLogin = user.ID;

                string sessionId = System.Web.HttpContext.Current.Session.SessionID;
                _sessionFront.sessionId = sessionId;

                obj.UpdateSessionId(user.email, sessionId);
                return RedirectToAction("redirect", "customer");

            }
            else
            {
                return RedirectToAction("login", "customer");
            }

        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult LoginProcess(string email, string password)
        {
            var user = obj.GetUserLoginWithValidate(email);
            if (user != null)
            {
                var userlogin = obj.CheckUserLogin(email, Generator.toPassword(password));
                if (userlogin != null)
                {
                    _sessionFront.firstName = userlogin.firstName;
                    _sessionFront.lastName = userlogin.lastName;
                    _sessionFront.userIdLogin = userlogin.ID;

                    string sessionId = System.Web.HttpContext.Current.Session.SessionID;
                    _sessionFront.sessionId = sessionId;

                    obj.UpdateSessionId(userlogin.email, sessionId);

                    //checking mycart item//
                    var product = new List<_object.ProductPi>();
                    var s = new List<string>();
                    if (HttpContext.Session["productDetail"] != null)
                    {
                        s = HttpContext.Session["productDetail"] as List<string>;
                    }

                    var allProd = obj.GetProductPi();

                    foreach (var r in s)
                    {
                        var x = r.Split('|').ToList<string>();
                        var colorId = Convert.ToInt32(x[0]);
                        var qty = Convert.ToInt32(x[1]);
                        var sz = x[2];
                        var szArr = sz.Split(',').ToList<string>();
                        var countSz = szArr.Count();
                        var prodId = obj.GetProductByColor(colorId);
                        var p = allProd.Where(pd => pd.id == prodId.product).FirstOrDefault();
                        var exist = obj.GetExistCart(_sessionFront.userIdLogin, sz, Convert.ToInt32(x[0])).Count();
                        if (exist > 0)
                        {
                            var prod = obj.GetExistCart(_sessionFront.userIdLogin, sz, Convert.ToInt32(x[0])).FirstOrDefault();
                            var newQty = prod.qty + qty;
                            prod.qty = newQty;
                            obj.changeQtyReservation(prod);
                            Session["productDetail"] = null;
                        }
                        else
                        {
                            product.Add(p);
                            DynamicContent.Models.tbl_cart cart = new DynamicContent.Models.tbl_cart();
                            cart.product = Convert.ToInt32(prodId.product);
                            cart.user_register = _sessionFront.userIdLogin;
                            cart.qty = qty * countSz;
                            cart.size = sz;
                            cart.color = colorId;
                            cart.created_date = DateTime.Now;
                            cart.reserved = false;
                            obj.AddToCart(cart);
                            Session["productDetail"] = null;
                        }
                    }
                    if (HttpContext.Session["fromLogin"] != null)
                    {
                        string fromLogin = HttpContext.Session["fromLogin"].ToString();
                        return RedirectToAction(fromLogin);
                    }
                    else
                    {
                        return RedirectToAction("Myaccount");
                    }

                }
                else
                {
                    TempData["msg"] = "error: Login failed, email and password doesn't match! ";
                    return RedirectToAction("MyAccount");
                }
            }
            else
                return RedirectToAction("MyAccount");

        }
        public ActionResult Logout()
        {
            Session.Clear();

            return RedirectToAction("Login");
        }
        public ActionResult MyCart()
        {
            if (_sessionFront.userIdLogin != null)
                obj.CheckSessionId(_sessionFront.userIdLogin);

            if (_sessionFront.userIdLogin == 0)
            {
                return RedirectToAction("Login");
            }
            else
            {
                var product = new List<_object.ProductPi>();

                var s = new List<string>();
                if (HttpContext.Session["productDetail"] != null)
                {
                    s = HttpContext.Session["productDetail"] as List<string>;
                }

                var allProd = obj.GetProductPi();

                foreach (var r in s)
                {
                    var x = r.Split('|').ToList<string>();
                    var colorId = Convert.ToInt32(x[0]);
                    var qty = Convert.ToInt32(x[1]);
                    var sz = x[2];
                    var szArr = sz.Split(',').ToList<string>();
                    var countSz = szArr.Count();
                    var prodId = obj.GetProductByColor(colorId);
                    var p = allProd.Where(pd => pd.id == prodId.product).FirstOrDefault();
                    product.Add(p);
                    DynamicContent.Models.tbl_cart cart = new DynamicContent.Models.tbl_cart();
                    cart.product = Convert.ToInt32(prodId.product);
                    cart.user_register = _sessionFront.userIdLogin;
                    cart.qty = qty * countSz;
                    cart.size = sz;
                    cart.color = colorId;
                    cart.created_date = DateTime.Now;
                    cart.reserved = false;
                    obj.AddToCart(cart);

                    Session["productDetail"] = null;
                }
                ViewBag.IsFlashSale = false;
                ViewBag.FlashSaleHeaderData = new DynamicContent.Models.FlashSaleHeader();
                ViewBag.FlashSaleDetail = new DynamicContent.Models.FlashHeaderDetail();
                var day = DateTime.Now.DayOfWeek.ToString();
                var timeNow = DateTime.Now.TimeOfDay;
                var flashSaleData = db.FlashSaleHeaders.FirstOrDefault(x => x.FlashSaleDay == day && x.IsActive == true && x.FlashSaleTimeBegin <= timeNow && x.FlashSaleTimeEnd >= timeNow);
                if (flashSaleData != null)
                {
                    var flashSaleDetailData = db.FlashHeaderDetails.Where(x => x.FlashSaleHeaderID == flashSaleData.FlashSaleHeaderID && x.isActive == true).ToList();
                    ViewBag.IsFlashSale = true;
                    ViewBag.FlashSaleDetail = flashSaleDetailData;
                }
                //ViewData["productDetail"] = s;

                return View("~/Views/Pages/mycart.cshtml");
            }
        }
        [HttpPost]
        public ActionResult CartConfirmation(DynamicContent.Models.tbl_cart data)
        {
            if (_sessionFront.userIdLogin != null)
                obj.CheckSessionId(_sessionFront.userIdLogin);


            var mycart = obj.GetMyCart(_sessionFront.userIdLogin).Count();
            for (int x = 0; x < mycart; x++)
            {
                //if (Session[mycart[x].product.ToString()])
                //{

                //}
                IList<string> cId = (Request.Form["cartId"]).Split(',').ToList<string>();
                IList<string> cqty = (Request.Form["qty"]).Split(',').ToList<string>();
                IList<string> csize = (Request.Form["size"]).Split(',').ToList<string>();
                var size = "";
                if (csize[x].Contains('.'))
                {
                    size = csize[x].Replace('.', ',');
                }
                else
                {
                    size = csize[x];
                }
                ViewBag.IsFlashSale = false;
                ViewBag.FlashSaleHeaderData = new DynamicContent.Models.FlashSaleHeader();
                ViewBag.FlashSaleDetail = new DynamicContent.Models.FlashHeaderDetail();
                int id = Convert.ToInt32(cId[x]);
                int qty = Convert.ToInt32(cqty[x]);
                var cart = (from r in db.tbl_cart
                            where r.ID == id
                            select r).FirstOrDefault();
                var day = DateTime.Now.DayOfWeek.ToString();
                var timeNow = DateTime.Now.TimeOfDay;
                var flashSaleData = db.FlashSaleHeaders.FirstOrDefault(a => a.FlashSaleDay == day && a.IsActive == true && a.FlashSaleTimeBegin <= timeNow && a.FlashSaleTimeEnd >= timeNow);
                if (flashSaleData != null)
                {
                    var flashSaleDetailData = db.FlashHeaderDetails.Where(a => a.FlashSaleHeaderID == flashSaleData.FlashSaleHeaderID && a.isActive == true && a.qty > 0).ToList();
                    //if (true)
                    //{

                    //}
                    ViewBag.IsFlashSale = true;
                    ViewBag.FlashSaleDetail = flashSaleDetailData;


                    var datas = flashSaleDetailData.FirstOrDefault(a => a.ProductID == cart.product);
                    if (datas != null)
                    {
                        if (Convert.ToInt32(cqty[x]) > datas.qty)
                        {
                            TempData["msgQTY"] = "<script>alert('Quantity not allowed');</script>";
                            return View("~/Views/Pages/mycart.cshtml");

                        }
                    }

                }
                IList<string> ccolor = (Request.Form["color"]).Split(',').ToList<string>();

                //Response.Write(csize[x]+ "<br/>");



                cart.qty = qty;
                cart.size = size;
                cart.color = Convert.ToInt32(ccolor[x]);
                db.SaveChanges();
            }
            return View("~/Views/Pages/cartconfirmation.cshtml");
        }
        public ActionResult SubsMail(string currentPath, string subsMail)
        {
            var subs = new DynamicContent.Models.M_Subscribers();
            subs.name = subsMail;
            subs.email = subsMail;
            subs.subscribe_date = DateTime.Now;
            subs.subscribe_by = subsMail;
            subs.type = 1;
            db.M_Subscribers.Add(subs);
            db.SaveChanges();
            return View("~/Views/Pages/home.cshtml");
        }
        public ActionResult Reservation()
        {
            if (_sessionFront.userIdLogin != null)
                obj.CheckSessionId(_sessionFront.userIdLogin);

            var mycart = obj.GetMyCart(_sessionFront.userIdLogin).Count();
            var user = obj.GetUserActive(_sessionFront.userIdLogin);
            var listProd = "";
            decimal? totalprice = 0m;
            decimal valPrice = 0m;
            IList<string> getId = (Request.Form["cartId"]).Split(',').ToList<string>();
            string[] i = getId.ToArray();
            var prodId = i.GroupBy(p => p);
            var tId = new List<int>();
            var listprodTenant = "";
            DynamicContent.Models.tbl_ReservationParent res = new DynamicContent.Models.tbl_ReservationParent();
            res.ReservationDate = DateTime.Now;
            res.CustID = _sessionFront.userIdLogin;
            res.ReservationStatus = true;
            obj.ReservationParent(res);
            var resId = res.ID;
            string resCode = "#" + DateTime.Now.ToString("yyyyMMdd") + "-" + resId;
            var success = (from r in db.tbl_ReservationParent
                           where r.ID == resId
                           select r).FirstOrDefault();
            success.ReservationCode = resCode;
            db.SaveChanges();
            for (int x = 0; x < mycart; x++)
            {
                IList<string> cId = (Request.Form["cartId"]).Split(',').ToList<string>();
                int id = Convert.ToInt32(cId[x]);
                var cart = (from r in db.tbl_cart
                            where r.ID == id
                            select r).FirstOrDefault();
                cart.reserved = true;
                db.SaveChanges();
                var p = obj.GetAllProduct(cart.product).FirstOrDefault();
                decimal price;
                var d = Decimal.TryParse(p.price, out price);
                DynamicContent.Models.tbl_reservation rv = new DynamicContent.Models.tbl_reservation();
                rv.parentReservation = resId;
                rv.tenant = p.idtenant;
                rv.product = Convert.ToInt32(cart.product);
                rv.qty = Convert.ToInt32(cart.qty);
                rv.size = cart.size;
                rv.color = cart.color;
                rv.total_price = (cart.qty * price).ToString();
                obj.AddReservation(rv);
                var summary = new DynamicContent.Models.Summary();
                var images = obj.GetOneImgByColor(Convert.ToInt32(cart.color));
                var product = obj.GetAllProduct(cart.product).FirstOrDefault();
                var color = obj.GetProductByColor(Convert.ToInt32(cart.color));
                var tenant = obj.GetTenant(product.idtenant).FirstOrDefault();
                var tenantName = obj.GetTenant(product.idtenant).FirstOrDefault();
                tId.Add(tenant.id);
                var day = DateTime.Now.DayOfWeek.ToString();
                var timeNow = DateTime.Now.TimeOfDay;
                var flashSaleData = db.FlashSaleHeaders.FirstOrDefault(b => b.FlashSaleDay == day && b.IsActive == true && b.FlashSaleTimeBegin <= timeNow && b.FlashSaleTimeEnd >= timeNow);
                if (flashSaleData != null)
                {
                    var flashSaleDetailData = db.FlashHeaderDetails.FirstOrDefault(b => b.FlashSaleHeaderID == flashSaleData.FlashSaleHeaderID && b.isActive == true && b.tbl_product.ID == product.id && b.qty > 0);
                    if (flashSaleDetailData != null)
                    {
                        product.price = flashSaleDetailData.price_new;
                        obj.FlashDealReservation(flashSaleDetailData.tbl_product.ID, rv.qty, flashSaleDetailData.FlashSaleHeaderID.GetValueOrDefault());
                    }
                    summary.SaleType = "Flash Sale";
                }
                else
                {
                    summary.SaleType = "Normal";
                }
                summary.ProductID = Convert.ToInt32(cart.product);
                summary.Qty = Convert.ToInt32(cart.qty);
                summary.CreatedDate = DateTime.Now;
                summary.CreatedBy = Session_.Username;
                db.Summaries.Add(summary);
                db.SaveChanges();
                if (product.price != "price upon request")
                {
                    valPrice = Convert.ToDecimal(product.price);
                }
                else
                {
                    valPrice = 0;
                }

                totalprice += valPrice * cart.qty;
                listProd +=
                    "<tr>" +
                    "<td width=\"400\" style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;\">" +
                    "<div align=\"left\" width=\"200\" style=\"float:left; height:150px; border:1px solid #b1b1b1;padding:5px; margin-right:15px;\">" +
                    "<span style=\"height: 100%;display: inline-block;vertical-align: middle;\"></span>" +
                    //"<img src=" + baseURL + images.images + " style=\"width:100px; vertical-align: middle;\"/></div>" +
                    "<img align=\"left\" src=" + baseURL + images.images + " width=\"200\" style=\"width:100px; vertical-align: middle; margin-right:10px;\"/></div>" +
                    //"<img align=\"left\" src=http://pi.demobatavianet.com/" + images.images + " width=\"200\" style=\"width:100px; vertical-align: middle; margin-right:10px;\"/></div>" +
                    "<div align=\"left\" style=\"float:left;\">" +
                    "<table cellpadding=\"10\">" +
                    "<p style=\"font-weight:bold;\">" + tenantName.name.ToUpper() + "</p>" +
                    "<p>" + product.name + "<br />SIZE : " + cart.size.ToUpper() + "</p>" +
                    "<div bgcolor=" + color.color + " style=\"background-color:" + color.color + "; width:30px; height:25px;\"><table bgcolor=" + color.color + " width=\"10%\" border=\"0\">&nbsp;</table></div>" +
                    "</table>" +
                    "</div>" +
                    "</td>" +
                    "<td style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;text-align:center;\"><p>" + obj.priceCheck(product.price) + "</p></td>" +
                    "<td style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;text-align:center;\"><p>" + cart.qty + "</p></td>" +
                    "<td style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;text-align:right;\">" +
                    //" <p>" + tenant.name + "<br />" + tenant.locationname + "<br />Telephone :<br />" + tenant.phone + "</p>" +
                    "<p>" + tenant.name + "<br />" + tenant.locationname + "</p>" +
                    "</td>" +
                    "</tr>";
            }
            var mailTemplate = (from m in db.MailMaster
                                where m.MailDesc == "ReservationForUser"
                                select m).FirstOrDefault();

            string content = mailTemplate.MailBodyTemplate
                .Replace("@firstName", user.firstName)
                .Replace("@listProd", listProd)
                .Replace("@totalprice", Convert.ToDecimal(totalprice).ToString("N0"))
                .Replace("@baseURL", baseURL)
            //.Replace("@baseURL", "http://pi.demobatavianet.com/")    
            ;

            //GMailer.GmailUsername = GmailID;
            //GMailer.GmailPassword = GmailPass;
            //GMailer mailer = new GMailer();
            //mailer.ToEmail = user.email;

            //mailer.Subject = mailTemplate.MailSubjectTemplate.Replace("@reservationCode", resCode);
            //mailer.Body = content;
            //mailer.IsHtml = true;
            //mailer.Send();
            Task.Run(() => { MainModel.SendEmail(user.email, mailTemplate.MailSubjectTemplate.Replace("@reservationCode", resCode), content); }).Wait();
            var mailToTenant = (from r in db.tbl_reservation
                                where r.parentReservation == resId
                                select new
                                {
                                    tenant = r.tenant
                                }).ToList().Distinct();
            int ct = 0;
            foreach (var mt in mailToTenant)
            {
                var rs = (from r in db.tbl_reservation
                          where r.parentReservation == resId && r.tenant == mt.tenant
                          select r).ToList();
                foreach (var p in rs)
                {
                    var images = obj.GetOneImgByColor(Convert.ToInt32(p.color));
                    var product = obj.GetAllProduct(p.product).FirstOrDefault();
                    var tenantName = obj.GetTenant(Convert.ToInt32(mt.tenant)).FirstOrDefault();
                    var color = obj.GetProductByColor(Convert.ToInt32(p.color));
                    var day = DateTime.Now.DayOfWeek.ToString();
                    var timeNow = DateTime.Now.TimeOfDay;
                    var flashSaleData = db.FlashSaleHeaders.FirstOrDefault(b => b.FlashSaleDay == day && b.IsActive == true && b.FlashSaleTimeBegin <= timeNow && b.FlashSaleTimeEnd >= timeNow);
                    if (flashSaleData != null)
                    {
                        var flashSaleDetailData = db.FlashHeaderDetails.FirstOrDefault(b => b.FlashSaleHeaderID == flashSaleData.FlashSaleHeaderID && b.isActive == true && b.tbl_product.ID == product.id && b.qty >= 0);
                        if (flashSaleDetailData != null)
                        {
                            product.price = flashSaleDetailData.price_new;
                            listprodTenant +=
                             "<tr>" +
                             "<td width=\"400\" style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;\">" +
                             //"<div style=\"float:left; height:150px; border:1px solid #b1b1b1;padding:5px; margin-right:15px;\"><span style=\"height: 100%;display: inline-block;vertical-align: middle;\"></span><img src=" + baseURL + images.images + " style=\"width:100px; vertical-align: middle;\"/></div>" +
                             //"<div align=\"left\" width=\"200\" style=\"float:left; height:150px; border:1px solid #b1b1b1;padding:5px; margin-right:15px;\"><span style=\"height: 100%;display: inline-block;vertical-align: middle;\"></span><img align=\"left\" src=http://pi.demobatavianet.com/" + images.images + " width=\"200\" style=\"width:100px; vertical-align: middle; margin-right:10px;\"/></div>" +

                             "<div align=\"left\" width=\"200\" style=\"float:left; height:150px; border:1px solid #b1b1b1;padding:5px; margin-right:15px;\"><span style=\"height: 100%;display: inline-block;vertical-align: middle;\"></span><img align=\"left\" src=" + baseURL + images.images + " width=\"200\" style=\"width:100px; vertical-align: middle; margin-right:10px;\"/></div>" +
                             "<div align=\"left\" style=\"float:left;\">" +
                             "<table cellpadding=\"10\">" +
                             "<p style=\"font-weight:bold;\">" + tenantName.name.ToUpper() + "</p>" +
                             "<p>" + product.name + "<br />SIZE : " + p.size.ToUpper() + "</p>" +
                             "<p>Flash Deal Price : IDR " + product.price + "</p>" +
                             "<div bgcolor=" + color.color + " style=\"background-color:" + color.color + "; width:30px; height:25px;\"><table bgcolor=" + color.color + " width=\"10%\" border=\"0\">&nbsp;</table></div>" +
                             "</table>" +
                             "</div>" +
                             "</td>" +
                             "<td style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;text-align:center;\"><p>" + p.qty + "</p></td>" +
                             "</tr>";
                        }
                        else
                        {
                            listprodTenant +=
                              "<tr>" +
                              "<td width=\"400\" style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;\">" +
                              //"<div style=\"float:left; height:150px; border:1px solid #b1b1b1;padding:5px; margin-right:15px;\"><span style=\"height: 100%;display: inline-block;vertical-align: middle;\"></span><img src=" + baseURL + images.images + " style=\"width:100px; vertical-align: middle;\"/></div>" +
                              //"<div align=\"left\" width=\"200\" style=\"float:left; height:150px; border:1px solid #b1b1b1;padding:5px; margin-right:15px;\"><span style=\"height: 100%;display: inline-block;vertical-align: middle;\"></span><img align=\"left\" src=http://pi.demobatavianet.com/" + images.images + " width=\"200\" style=\"width:100px; vertical-align: middle; margin-right:10px;\"/></div>" +

                              "<div align=\"left\" width=\"200\" style=\"float:left; height:150px; border:1px solid #b1b1b1;padding:5px; margin-right:15px;\"><span style=\"height: 100%;display: inline-block;vertical-align: middle;\"></span><img align=\"left\" src=" + baseURL + images.images + " width=\"200\" style=\"width:100px; vertical-align: middle; margin-right:10px;\"/></div>" +
                              "<div align=\"left\" style=\"float:left;\">" +
                              "<table cellpadding=\"10\">" +
                              "<p style=\"font-weight:bold;\">" + tenantName.name.ToUpper() + "</p>" +
                              "<p>" + product.name + "<br />SIZE : " + p.size.ToUpper() + "</p>" +
                              "<div bgcolor=" + color.color + " style=\"background-color:" + color.color + "; width:30px; height:25px;\"><table bgcolor=" + color.color + " width=\"10%\" border=\"0\">&nbsp;</table></div>" +
                              "</table>" +
                              "</div>" +
                              "</td>" +
                              "<td style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;text-align:center;\"><p>" + p.qty + "</p></td>" +
                              "</tr>";
                        }
                    }
                    else
                    {
                        listprodTenant +=
                          "<tr>" +
                          "<td width=\"400\" style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;\">" +
                          //"<div style=\"float:left; height:150px; border:1px solid #b1b1b1;padding:5px; margin-right:15px;\"><span style=\"height: 100%;display: inline-block;vertical-align: middle;\"></span><img src=" + baseURL + images.images + " style=\"width:100px; vertical-align: middle;\"/></div>" +
                          //"<div align=\"left\" width=\"200\" style=\"float:left; height:150px; border:1px solid #b1b1b1;padding:5px; margin-right:15px;\"><span style=\"height: 100%;display: inline-block;vertical-align: middle;\"></span><img align=\"left\" src=http://pi.demobatavianet.com/" + images.images + " width=\"200\" style=\"width:100px; vertical-align: middle; margin-right:10px;\"/></div>" +

                          "<div align=\"left\" width=\"200\" style=\"float:left; height:150px; border:1px solid #b1b1b1;padding:5px; margin-right:15px;\"><span style=\"height: 100%;display: inline-block;vertical-align: middle;\"></span><img align=\"left\" src=" + baseURL + images.images + " width=\"200\" style=\"width:100px; vertical-align: middle; margin-right:10px;\"/></div>" +
                          "<div align=\"left\" style=\"float:left;\">" +
                          "<table cellpadding=\"10\">" +
                          "<p style=\"font-weight:bold;\">" + tenantName.name.ToUpper() + "</p>" +
                          "<p>" + product.name + "<br />SIZE : " + p.size.ToUpper() + "</p>" +
                          "<div bgcolor=" + color.color + " style=\"background-color:" + color.color + "; width:30px; height:25px;\"><table bgcolor=" + color.color + " width=\"10%\" border=\"0\">&nbsp;</table></div>" +
                          "</table>" +
                          "</div>" +
                          "</td>" +
                          "<td style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;text-align:center;\"><p>" + p.qty + "</p></td>" +
                          "</tr>";
                    }
                }
                var mailTenant = (from x in db.M_User_Tenant where x.tenant == mt.tenant select new { emailT = x.email }).ToList();
                var listmailTenant = new List<string>();
                string emailforTenant = "";
                foreach (var m in mailTenant)
                {
                    listmailTenant.Add(m.emailT);
                }

                emailforTenant = string.Join(",", listmailTenant.ToArray());
                if (emailforTenant == "")
                {
                    emailforTenant = "toni.cahyana@plazaindonesia.com";
                }

                var mailTemplateTenant = (from m in db.MailMaster
                                          where m.MailDesc == "ReservationForTenant"
                                          select m).FirstOrDefault();
                string tenantContent = mailTemplateTenant.MailBodyTemplate
                    .Replace("@baseURL", baseURL)
                    //.Replace("@baseURL", "http://pi.demobatavianet.com/")     
                    .Replace("@userEmail", user.email)
                    .Replace("@userFirstName", user.firstName)
                    .Replace("@userLastName", user.lastName)
                    //.Replace("@userAddress", user.address)
                    //.Replace("@userDob", user.dob)
                    .Replace("@userGender", user.gender)
                    .Replace("@userPhone", user.phone)
                    .Replace("@listprodTenant", listprodTenant)
                    ;
                //GMailer.GmailUsername = GmailID;
                //GMailer.GmailPassword = GmailPass;
                GMailer mailerTenant = new GMailer();
                mailerTenant.ToEmail = emailforTenant;

                mailerTenant.Subject = mailTemplateTenant.MailSubjectTemplate.Replace("@reservationCode", resCode);
                mailerTenant.Body = tenantContent;
                //mailerTenant.IsHtml = true;
                //mailerTenant.Send();
                Task.Run(() => { MainModel.SendEmail(emailforTenant, mailerTenant.Subject, tenantContent); }).Wait();
                listprodTenant = "";
                ct++;
            }

            return View("~/Views/Pages/reservation.cshtml");
        }

        public ActionResult cancelReservation(int idr)
        {
            if (_sessionFront.userIdLogin != null)
                obj.CheckSessionId(_sessionFront.userIdLogin);
            var reservation = db.tbl_reservation.Where(x => x.ID == idr).FirstOrDefault();
            reservation.status = 2;
            db.SaveChanges();
            var parent = db.tbl_ReservationParent.Where(x => x.ID == reservation.parentReservation).FirstOrDefault();

            //var mycart = obj.GetMyCart(_sessionFront.userIdLogin).Count();
            var user = obj.GetUserActive(_sessionFront.userIdLogin);
            var listProd = "";
            decimal? totalprice = 0m;
            decimal valPrice = 0m;
            //IList<string> getId = (Request.Form["cartId"]).Split(',').ToList<string>();
            //string[] i = getId.ToArray();
            //var tId = new List<int>();
            var listprodTenant = "";
            var p = obj.GetAllProduct(reservation.product).FirstOrDefault();
            decimal price;
            var d = Decimal.TryParse(p.price, out price);
            var images = obj.GetOneImgByColor(Convert.ToInt32(reservation.color));
            //var product = obj.GetAllProduct(cart.product).FirstOrDefault();
            var color = obj.GetProductByColor(Convert.ToInt32(reservation.color));
            var tenant = obj.GetTenant(Convert.ToInt32(reservation.tenant)).FirstOrDefault();
            var tenantName = obj.GetTenant(Convert.ToInt32(reservation.tenant)).FirstOrDefault();
            //tId.Add(tenant.id);
            if (p.price != "price upon request")
            {
                valPrice = Convert.ToDecimal(p.price);
            }
            else
            {
                valPrice = 0;
            }
            totalprice += valPrice * reservation.qty;
            listProd +=
                "<tr>" +
                "<td width=\"400\" style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;\">" +
                "<div align=\"left\" width=\"200\" style=\"float:left; height:150px; border:1px solid #b1b1b1;padding:5px; margin-right:15px;\">" +
                "<span style=\"height: 100%;display: inline-block;vertical-align: middle;\"></span>" +
                //"<img src=" + baseURL + images.images + " style=\"width:100px; vertical-align: middle;\"/></div>" +
                "<img align=\"left\" src=" + baseURL + images.images + " width=\"200\" style=\"width:100px; vertical-align: middle; margin-right:10px;\"/></div>" +
                //"<img align=\"left\" src=http://pi.demobatavianet.com/" + images.images + " width=\"200\" style=\"width:100px; vertical-align: middle; margin-right:10px;\"/></div>" +
                "<div align=\"left\" style=\"float:left;\">" +
                "<p style=\"font-weight:bold;\">" + tenantName.name.ToUpper() + "</p>" +
                "<p>" + p.name + "<br />SIZE : " + reservation.size.ToUpper() + "</p>" +
                "<div style=\"background-color:" + color.color + "; width:30px; height:25px;border:1px solid #b1b1b1;\"></div>" +
                "</div>" +
                "</td>" +
                "<td style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;text-align:center;\">" + obj.priceCheck(p.price) + "</td>" +
                "<td style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;text-align:center;\">" + reservation.qty + "</td>" +
                "<td style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;text-align:right;\">" +
                //" <p>" + tenant.name + "<br />" + tenant.locationname + "<br />Telephone :<br />" + tenant.phone + "</p>" +
                " <p>" + tenant.name + "<br />" + tenant.locationname + "</p>" +
                "</td>" +
                "</tr>";
            //}
            var mailTemplate = (from m in db.MailMaster
                                where m.MailDesc == "CancelReservationForUser"
                                select m).FirstOrDefault();

            string content = mailTemplate.MailBodyTemplate
                .Replace("@firstName", user.firstName)
                .Replace("@listProd", listProd)
                .Replace("@totalprice", Convert.ToDecimal(totalprice).ToString("N0"))
                .Replace("@baseURL", baseURL)
            //.Replace("@baseURL", "http://pi.demobatavianet.com/")    
            ;

            GMailer.GmailUsername = GmailID;
            GMailer.GmailPassword = GmailPass;
            GMailer mailer = new GMailer();
            mailer.ToEmail = user.email;
            //mailer.ToEmail = "ranimuliati@gmail.com";
            //mailer.Subject = mailTemplate.MailSubjectTemplate.Replace("@reservationCode", parent.ReservationCode);
            //mailer.Body = content;
            //mailer.IsHtml = true;
            //mailer.Send();
            Task.Run(() => { MainModel.SendEmail(user.email, mailer.Subject, content); }).Wait();

            listprodTenant +=
            "<tr>" +
            "<td width=\"400\" style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;\">" +
            //"<div style=\"float:left; height:150px; border:1px solid #b1b1b1;padding:5px; margin-right:15px;\"><span style=\"height: 100%;display: inline-block;vertical-align: middle;\"></span><img src=" + baseURL + images.images + " style=\"width:100px; vertical-align: middle;\"/></div>" +
            //"<div align=\"left\" width=\"200\" style=\"float:left; height:150px; border:1px solid #b1b1b1;padding:5px; margin-right:15px;\"><span style=\"height: 100%;display: inline-block;vertical-align: middle;\"></span><img align=\"left\" src=http://pi.demobatavianet.com/" + images.images + " width=\"200\" style=\"width:100px; vertical-align: middle; margin-right:10px;\"/></div>" +
            "<div align=\"left\" width=\"200\" style=\"float:left; height:150px; border:1px solid #b1b1b1;padding:5px; margin-right:15px;\"><span style=\"height: 100%;display: inline-block;vertical-align: middle;\"></span><img align=\"left\" src=" + baseURL + images.images + " width=\"200\" style=\"width:100px; vertical-align: middle; margin-right:10px;\"/></div>" +
            "<div align=\"left\" style=\"float:left;\">" +
            "<p style=\"font-weight:bold;\">" + tenantName.name.ToUpper() + "</p>" +
            "<p>" + p.name + "<br />SIZE : " + reservation.size.ToUpper() + "</p>" +
            "<div style=\"background-color:" + color.color + "; width:30px; height:25px;border:1px solid #b1b1b1;\"></div>" +
            "</div>" +
            "</td>" +
            "<td style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;text-align:center;\">" + reservation.qty + "</td>" +
            "</tr>";
            var mailTenant = (from x in db.M_User_Tenant where x.tenant == reservation.tenant select new { emailT = x.email }).ToList();
            var listmailTenant = new List<string>();
            string emailforTenant = "";
            foreach (var m in mailTenant)
            {
                listmailTenant.Add(m.emailT);
            }

            emailforTenant = string.Join(",", listmailTenant.ToArray());
            if (emailforTenant == "")
            {
                emailforTenant = "toni.cahyana@plazaindonesia.com";
            }

            var mailTemplateTenant = (from m in db.MailMaster
                                      where m.MailDesc == "CancelReservationForTenant"
                                      select m).FirstOrDefault();
            string tenantContent = mailTemplateTenant.MailBodyTemplate
                .Replace("@baseURL", baseURL)
                //.Replace("@baseURL", "http://pi.demobatavianet.com/")     
                .Replace("@userEmail", user.email)
                .Replace("@userFirstName", user.firstName)
                .Replace("@userLastName", user.lastName)
                //.Replace("@userAddress", user.address)
                //.Replace("@userDob", user.dob)
                .Replace("@userGender", user.gender)
                .Replace("@userPhone", user.phone)
                .Replace("@listprodTenant", listprodTenant)
                ;

            GMailer.GmailUsername = GmailID;
            GMailer.GmailPassword = GmailPass;
            GMailer mailerTenant = new GMailer();
            mailerTenant.ToEmail = emailforTenant;
            //mailerTenant.ToEmail = "ranimuliati@gmail.com";
            mailerTenant.Subject = mailTemplateTenant.MailSubjectTemplate.Replace("@reservationCode", parent.ReservationCode);
            //mailerTenant.Body = tenantContent;
            //mailerTenant.IsHtml = true;
            //mailerTenant.Send();
            Task.Run(() => { MainModel.SendEmail(emailforTenant, mailerTenant.Subject, tenantContent); }).Wait();

            return null;
        }

        public ActionResult tryEmail()
        {
            string content = "<html>" +
                    "<head>" +
                    "<title>My Reservations</title>" +
                    "</head>" +
                    "<body>" +
                    "<div style=\"background-color:#bca061; height:10px;\"></div>" +
                    "<div style=\"text-align:center;\"><img src=\"http://pi.demobatavianet.com/Images/logo.jpg\" style=\"width:150px;\"/></div>" +
                    "<div style=\"width:100%; margin:0 auto;\">" +
                    "<p>Dear Rani,</p>" +
                    "<p>Thank you for making Reservation with Plaza Indonesia eMall. We hope you enjoy reserving with us. We have received your enquiry and our value Tenant will follow up your order.Below is the detail of your order. If you have any further queries please do not hesitate to contact us at <a href='mailto:info@plazaindonesia.co.id'>info@plazaindonesia.co.id</a></p>" +
                    "<table style=\"width:100%;font-size: 13px; padding: 3px; border-collapse: collapse; border-spacing: 0;\">" +
                    "<thead>" +
                    "<tr>" +
                    "<th style=\"padding:10px;text-transform:uppercase;background-color:#000000; color: #FFFFFF;text-align: center;\">product detail</th>" +
                    "<th style =\"padding:10px;text-transform:uppercase;background-color:#000000; color: #FFFFFF;text-align: center;\">price</th>" +
                    "<th style =\"padding:10px;text-transform:uppercase;background-color:#000000; color: #FFFFFF;text-align: center;\">quantity</th>" +
                    "<th style =\"padding:10px;text-transform:uppercase;background-color:#000000; color: #FFFFFF;text-align: center;\">location</th>" +
                    "</tr>" +
                    "</thead>" +
                    "<tbody>" +
                    "<tr>" +
                    "<td style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;\">" +
                    "<div style=\"float:left; height:150px; border:1px solid #b1b1b1;padding:5px; margin-right:15px;\"><span style=\"height: 100%;display: inline-block;vertical-align: middle;\"></span><img src=\"http://pi.demobatavianet.com/Public/Images/product/rv-dress-4.jpg\" style=\"width:100px; vertical-align: middle;\"/></div>" +
                    "<div style=\"float:left;\">" +
                    "<p style=\"font-weight:bold;\">HERMES</p>" +
                    "<p>WOMAN BLACK FLAT SHOES<br />SIZE 40</p>" +
                    "<div style=\"background-color:#000; width:30px; height:25px;border:1px solid #b1b1b1;\"></div>" +
                    "</div>" +
                    "</td>" +
                    "<td style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;text-align:center;\">IDR 2.000.0000</td>" +
                    "<td style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;text-align:center;\">1</td>" +
                    "<td style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;text-align:right;\">" +
                    " <p>Stella McCartney<br />LEVEL 1<br />Telephone :<br />(021) 2992 4268</p>" +
                    "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td colspan=\"3\" style=\"background-color:#bc955a; color:#fff;text-transform:uppercase; padding:10px; text-align:right;\">price estimation</td>" +
                    "<td style=\"background-color:#bc955a; color:#fff;text-transform:uppercase; padding:10px; text-align:right;\">idr 10.500.000</td>" +
                    "</tr>" +
                    "</tbody>" +
                    "</table>" +
                    "<p>For more information please email to info@plazaindonesia.co.id.  We are looking forward to hear from you</p>" +
                    "</div>" +
                    "<div style=\"background-color:#f6f6f6; text-align:center;padding:20px; margin-top:50px;\">" +
                    "<a href='https://www.facebook.com/PlazaIndonesia/' target='_blank'><img src=\"http://pi.demobatavianet.com/Images/fb.png\" style=\"width:40px;margin-right:10px;\"/></a>" +
                    "<a href='https://www.twitter.com/plazaindonesia/' target='_blank'><img src=\"http://pi.demobatavianet.com/Images/twitter.png\" style=\"width:40px;margin-right:10px;\"/></a>" +
                    "<a href='https://www.instagram.com/plaza_indonesia/' target='_blank'><img src=\"http://pi.demobatavianet.com/Images/instagram.png\" style=\"width:40px;margin-right:10px;\"/></a>" +
                    "<a href='' target='_blank'><img src=\"http://pi.demobatavianet.com/Images/pinterest.png\" style=\"width:40px;\"/></a>" +
                    "<p style=\"text-transform:uppercase;\">For further information, please contact:</p>" +
                    "<p>PT Plaza Indonesia Realty Tbk.The Plaza, 10th Floor Jl.M.H.Thamrin Kav. 28 - 30 Jakarta 10350 <br/>Phone.: +6221 2992 0000 | Phone.: +6221 2992 0234 <br/>Concierge <br/>Telp: +62 21 2992 0000 ext: 52525 <br/>Terms & Conditions</p> " +
                    "</div>" +
                    "<p class=\"center\">Copyright &copy; 2016 Plaza Indonesia All Rights Reserved</p>" +
                    "</body>" +
                    "</html>";
            GMailer.GmailUsername = "emallplazaindonesia@gmail.com";
            GMailer.GmailPassword = "3m4LlpL4z4ind0n3Sia";
            GMailer mailer = new GMailer();
            mailer.ToEmail = "ranimuliati@gmail.com";
            mailer.Subject = "[eMall Plaza Indonesia]My Reservations";
            mailer.Body = content;
            mailer.IsHtml = true;
            mailer.Send();
            Console.WriteLine("Sent");
            return PartialView();
        }

        public ActionResult tryEmailTenant()
        {
            string content = "<html>" +
                    "<head>" +
                    "<title>New Reservations</title>" +
                    "</head>" +
                    "<body>" +
                    "<div style=\"background-color:#bca061; height:10px;\"></div>" +
                    "<div style=\"text-align:center;\"><img src=\"http://pi.demobatavianet.com/Images/logo.jpg\" style=\"width:150px;\"/></div>" +
                    "<div style=\"width:100%; margin:0 auto;\">" +
                    "<p>Dear Tenant,</p>" +
                    "<p>This is some reservation from <a href='mailto:info@plazaindonesia.co.id'>info@plazaindonesia.co.id</a>, <br/> Please follow up the order soon.</p>" +
                    "<table style=\"width:100%;font-size: 13px; padding: 3px; border-collapse: collapse; border-spacing: 0;\">" +
                    "<thead>" +
                    "<tr>" +
                    "<th style=\"padding:10px;text-transform:uppercase;background-color:#000000; color: #FFFFFF;text-align: center;\">product detail</th>" +
                    "<th style =\"padding:10px;text-transform:uppercase;background-color:#000000; color: #FFFFFF;text-align: center;\">quantity</th>" +
                    "</tr>" +
                    "</thead>" +
                    "<tbody>" +
                    "<tr>" +
                    "<td style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;\">" +
                    "<div style=\"float:left; height:150px; border:1px solid #b1b1b1;padding:5px; margin-right:15px;\"><span style=\"height: 100%;display: inline-block;vertical-align: middle;\"></span><img src=\"http://pi.demobatavianet.com/Public/Images/product/rv-dress-4.jpg\" style=\"width:100px; vertical-align: middle;\"/></div>" +
                    "<div style=\"float:left;\">" +
                    "<p style=\"font-weight:bold;\">HERMES</p>" +
                    "<p>WOMAN BLACK FLAT SHOES<br />SIZE 40</p>" +
                    "<div style=\"background-color:#000; width:30px; height:25px;border:1px solid #b1b1b1;\"></div>" +
                    "</div>" +
                    "</td>" +
                    "<td style=\"border-bottom: 1px solid #D1D1D1; padding: 10px;text-align:right;\">1</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td colspan=\"2\" style=\"background-color:#bc955a; color:#fff;text-transform:uppercase; padding:10px; text-align:right;\"></td>" +
                    "</tr>" +
                    "</tbody>" +
                    "</table>" +
                    "<p>For more information please email to info@plazaindonesia.co.id.  We are looking forward to hear from you</p><p>Thank You and keep reserving with Plaza Indonesia eMall.</p>" +
                    "</div>" +
                   "<div style=\"background-color:#f6f6f6; text-align:center;padding:20px; margin-top:50px;\">" +
                    "<a href='https://www.facebook.com/PlazaIndonesia/' target='_blank'><img src='http://pi.demobatavianet.com/Images/fb.png' style=\"width:40px;margin-right:10px;\"/></a>" +
                    "<a href='https://www.twitter.com/plazaindonesia/' target='_blank'><img src='http://pi.demobatavianet.com/Images/twitter.png' style=\"width:40px;margin-right:10px;\"/></a>" +
                    "<a href='https://www.instagram.com/plaza_indonesia/' target='_blank'><img src='http://pi.demobatavianet.com/Images/instagram.png' style=\"width:40px;margin-right:10px;\"/></a>" +
                    "<a href='' target='_blank'><img src='http://pi.demobatavianet.com/Images/pinterest.png' style=\"width:40px;\"/></a>" +
                    "<p style=\"text-transform:uppercase;\">For further information, please contact:</p>" +
                    "<p>PT Plaza Indonesia Realty Tbk.The Plaza, 10th Floor Jl.M.H.Thamrin Kav. 28 - 30 Jakarta 10350 <br/>Phone.: +6221 2992 0000 | Phone.: +6221 2992 0234 <br/>Concierge <br/>Telp: +62 21 2992 0000 ext: 52525 <br/>Terms & Conditions</p> " +
                    "</div>" +
                    "<p class=\"center\">Copyright &copy; 2016 Plaza Indonesia All Rights Reserved</p>" +
                    "</body>" +
                    "</html>";
            GMailer.GmailUsername = "emallplazaindonesia@gmail.com";
            GMailer.GmailPassword = "3m4LlpL4z4ind0n3Sia";
            GMailer mailer = new GMailer();
            mailer.ToEmail = "ranimuliati@gmail.com, himalayahijab@gmail.com";
            mailer.Subject = "[eMall Plaza Indonesia]New Reservations";
            mailer.Body = content;
            mailer.IsHtml = true;
            mailer.Send();
            Console.WriteLine("Sent");
            return PartialView();
        }

        [HttpPost]
        public ActionResult SubmitNewsletter(string email)
        {
            if (objemail.CheckSubscribers(email, 1) == null)
            {
                var subs = new DynamicContent.Models.M_Subscribers();

                subs.name = email.Split('@')[0];
                subs.email = email;
                subs.type = 1;
                subs.subscribe_by = email;
                subs.subscribe_date = DateTime.Now;
                db.M_Subscribers.Add(subs);
                db.SaveChanges();
            }
            return Redirect("/");
        }

        public ActionResult tnc()
        {
            return View("~/Views/Pages/tnc.cshtml");
        }
    }
}
