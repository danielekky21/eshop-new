google.maps.event.addDomListener(window, 'load', function() {

    var zoom

    if(window.innerWidth >= 1220){
        zoom = 5
    }else{
        zoom = 4
    }

    /*--- Initiate Map ---*/
    function initMap(){
        var map = new google.maps.Map(document.getElementById('map-canvas'), {
            center: new google.maps.LatLng(-1.473170, 119.595981),
            zoom: zoom,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            scrollwheel: false
        })

        setMarkers(map) // set markers
    }

    /* Array of Map Location */
    var locations = [
        ['Meulaboh',4.115011, 96.300047],
        ['Medan',3.598198, 98.669032],
        ['Pekanbaru',0.504619, 101.453137],
        ['Batam',1.132996, 104.045494],
        ['Palembang',-2.963770, 104.741772],
        ['Jabodetabek',-6.218054, 106.840160],
        ['Bandung',-6.877960, 107.593520],
        ['Tasikmalaya',-7.229153, 108.189471],
        ['Purwokerto',-7.429128, 109.248885],
        ['Yogyakarta',-7.811965, 110.373772],
        ['Semarang',-6.976414, 110.417603],
        ['Malang',-7.973920, 112.616222],
        ['Surabaya',-7.287999, 112.739426],
        ['Jember',-8.161186, 113.708065],
        ['Denpasar',-8.639188, 115.203968],
        ['Mataram',-8.587611, 116.117870],
        ['Pontianak',-0.051332, 109.345921],
        ['Balikpapan',-1.270926, 116.854503],
        ['Palu',-0.904374, 119.863526],
        ['Makassar',-5.140297, 119.420629],
        ['Jayapura',-2.508945, 140.633131]
    ]
    var markerIcon = {
        url: 'assets/img/map-marker.png',
        // This marker is 20 pixels wide by 32 pixels high.
        size: new google.maps.Size(22, 22),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(11, 11)
    };

    /* Assign markers to map */
    function setMarkers(map){
        for (var i = 0; i < locations.length; i++) {
            var location = locations[i];
            var marker = new google.maps.Marker({
                position: {lat: location[1], lng: location[2]},
                map: map,
                icon: markerIcon,
                title: location[0]
            })
        }
    }

    initMap()
});
