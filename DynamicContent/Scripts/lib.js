function windowWidth() {
    if ($(window).width() > 991) {
        $(window).scroll(function () {
            var a = $(window).scrollTop();
            if (a >= 115) {
                $('ul.head').addClass("sticky");
                $('body').addClass("top48");
            } else {
                $('ul.head').removeClass("sticky");
                $('body').removeClass("top48");
            }
        });
    }
    if ($(window).width() < 991) {
        $(window).scroll(function () {
            var b = $(window).scrollTop();
            if (b >= 115) {
                $('ul.head').removeClass("sticky");
                $('body').removeClass("top48");
            }
        });
    }
}
function createEvent() {
    $(".menu > ul > li .click-sub").click(function (event) {
        if ($(window).width() <= 991) {
            $(this).parent().children("ul").show().addClass('open');
            // var plus = $(this);
            // setTimeout(function() {
            //     plus.addClass('open');
            // }, 100);
            $('.menu-dropdown-icon').addClass('open');
            $('.menu > ul').addClass('open');
            //$('icon-close').addClass('open');
            //$('.show-on-mobile').addClass('open');
        }
    });
}
$(document).ready(function () {
    $('.click-search').click(function () {
        $('.form-search').addClass('open');
    });


    $('#dropdown-search').click(function (e) {
        //if ($('.box-search').css("display")== "block") {
        //    $('.box-search').css("display", "none")
        //    return;
        //}
        //if ($('.box-search').css("display") == "none") {
        //    $('.box-search').css("display", "block")
        //    return;
        //}
        $('.box-search').slideToggle(400);

    });
    $('html').click(function (e) {
        if (!$(e.target).parents().is('.click-search') && !$(e.target).is('.form-search')) {
            $('.form-search').removeClass('open');
        }
    });

    windowWidth();

    $(window).on('resize', function () {
        windowWidth();
    });

    //$('.menu > ul > li:has( > ul)').addClass('menu-dropdown-icon');

    //$('.menu > ul > li > ul:not(:has(ul))').addClass('normal-sub');

    //$(".menu > ul").before("<a href=\"#\" class=\"menu-mobile\">Navigation</a>");

    //$(".menu > ul > li").mouseenter(function (e) {
    //    if ($(window).width() > 991) {
    //        $(this).children("ul").fadeToggle(150).css("display","block");
    //        e.preventDefault();
    //    }
    //});
    //$(".menu > ul > li").mouseleave(function (e) {
    //   if ($(window).width() > 991) {
    //        $(this).children("ul").css("display", "none");
    //        e.preventDefault();
    //    }
    //});

    $(".menu > ul > li").mouseenter(function (e) {
        if ($(window).width() > 991) {
            $(this).children("ul").fadeToggle(150).css("display", "block");
            e.preventDefault();
        }
    });
    $(".menu > ul > li").mouseleave(function (e) {
        if ($(window).width() > 991) {
            $(this).children("ul").css("display", "none");
            e.preventDefault();
        }
    });

    //Mobile Dropdown//
    // $(".menu > ul > li").click(function (event) {
    //     if ($(window).width() <= 991) {
    //         //$(this).children("ul").fadeToggle(150).addClass('open');
    //         //$(this).toggleClass('active');
    //         $(this).children("ul").fadeIn(150).addClass('open');
    //         $(this).addClass('open');
    //         $('.show-on-mobile').addClass('open');
    //     }
    // });
    createEvent();

    $(".icon-back").click(function (event) {
        $('.menu > ul > li > ul').removeClass('open');
        $(".menu > ul > li").unbind('click');
        $('.menu > ul > li').removeClass('open');
        $('.menu > ul').removeClass('open');
        setTimeout(function () {
            createEvent();
            $('.menu > ul > li > ul').hide();
        }, 100);
    });

    $(".menu-mobile").click(function (e) {
        $(".menu-container").addClass('show-on-mobile');
        $(".bg-overlay").addClass('open');
        $(".close-menu").addClass('open');
        $("body").addClass('no-scroll');
        e.preventDefault();
    });
    $(".close-menu").click(function () {
        $(".menu-container").removeClass('show-on-mobile');
        $(".bg-overlay").removeClass('open');
        $(".close-menu").removeClass('open');
        $("body").removeClass('no-scroll');
        $('.menu > ul > li > ul').removeClass('open');
        $(".menu > ul > li").unbind('click');
        $('.menu > ul > li').removeClass('open');
        $('.menu > ul').removeClass('open');
        setTimeout(function () {
            createEvent();
            $('.menu > ul > li > ul').hide();
        }, 100);
    });

    $(".only-number").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $('.icon-wish').each(function () {
        $(this).click(function (event) {
            event.preventDefault();
            if (!$(this).parents('.l-product').find('.icon-wish').hasClass('active')) {
                $(this).parents('.l-product').find('.icon-wish').addClass('active');
            } else {
                $(this).parents('.l-product').find('.icon-wish').removeClass('active');
            }
        });
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 600) {
            $('.back-to-top').fadeIn();
        } else {
            $('.back-to-top').fadeOut();
        }
    });

    $('.back-to-top').click(function () {
        $('html, body').animate({ scrollTop: 0 }, 800);
        return false;
    });

});