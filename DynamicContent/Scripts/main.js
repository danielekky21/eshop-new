/*
   Commonwealth Life Official Site
   Copyright (C) 2012.  PT Commonwealth Life │ PT Commonwealth Life terdaftar dan diawasi oleh Otoritas Jasa Keuangan
   Frontend Developed on Dec 2015 by POTEam.id
   @himmarama & budisetiawan
 */

/*=== Initiate Selectpicker Plugin ===*/
$('.selectpicker').each(function () {
    $(this).selectpicker();
})

/*=== Set height of Image on Half ===*/
function setHalfImageHeight() {
    $(".half-image .img-wrap").each(function () {
        var img = $(this).find('img');
        if (!$(this).parent().hasClass('half-same')) {
            var parentHeight = $(this).parent().find('.content').height() + 30

            if ($(window).width() >= 1220) {
                $(this).css('height', parentHeight)
            } else {
                $(this).css('height', 'auto')
            }
        }
    })
}

/*=== Sliders ===*/
/* Each sliders is executed using owl.js slider plugin. We just set the slider option as needed */
/*--- banner slider ---*/
$(".banner-slider .slide-inner").each(function () {
    var inner = $(this)
    var banner = $(this).parent()
    var dotArea = banner.find('.dotArea')

    inner.owlCarousel({
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        items: 1,
        loop: true,
        margin: 0,
        mouseDrag: false,
        pullDrag: false,
        autoplay: true,
        dotsContainer: dotArea
    })

    banner.find('.play-pause').click(function (e) {
        var button = $(this)
        if (button.hasClass('paused')) {
            inner.trigger('play.owl.autoplay')
        } else {
            inner.trigger('stop.owl.autoplay')
        }
        button.toggleClass('paused')
        e.preventDefault()
    })
})

/*--- profile slider ---*/
$(".profile-slider .slide-inner").each(function () {
    var inner = $(this)
    var slider = $(this).parent()
    var controlArea = slider.find('.controlArea')

    inner.owlCarousel({
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        items: 1,
        loop: true,
        margin: 0,
        dots: false,
        nav: true,
        navContainer: controlArea
    })

})

/*--- link slider ---*/
$(".link-slider .slide-inner").each(function () {
    var inner = $(this)
    var slider = $(this).parents('.link-slider')
    var controlArea = slider.find('.controlArea')

    inner.owlCarousel({
        responsive: {
            0: {
                items: 2
            },
            1220: {
                items: 8
            }
        },
        loop: false,
        margin: 10,
        dots: false,
        nav: true,
        navContainer: controlArea
    })

})

/* Load content of year in fund performance from tab-data folder */
$(".link-slider a").click(function (e) {
    var anchor = $(this)
    var target = anchor.attr('href')
    var location = 'tab-data/' + target + '.html'

    if (!anchor.hasClass('active')) {
        $('.tab-content').load(location)
        $('.link-slider a.active').removeClass('active')
        anchor.addClass('active')
    }

    e.preventDefault()
})

/*--- profile slider ---*/
$(".article-slider .slide-inner").each(function () {
    var inner = $(this)
    var slider = $(this).parent()
    var controlArea = slider.find('.controlArea')

    inner.owlCarousel({
        items: 1,
        loop: true,
        margin: 0,
        dots: false,
        nav: true,
        navContainer: controlArea
    })

})

/*--- insurance slider ---*/
$(".card-slider .slide-inner").each(function () {
    var inner = $(this)
    var slider = $(this).parent()
    var controlArea = slider.find('.controlArea')

    inner.owlCarousel({
        responsive: {
            0: {
                items: 1
            },
            1024: {
                items: 3
            }
        },
        items: 3,
        loop: true,
        margin: 0,
        dots: false,
        nav: true,
        navContainer: controlArea
    })

})

/*--- video slider ---*/
$(".video-slider .slide-inner").each(function () {
    var inner = $(this)
    var slider = $(this).parent()
    var controlArea = slider.find('.controlArea')

    inner.owlCarousel({
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            992: {
                items: 2,
                nav: true
            },
            1220: {
                items: 3,
                nav: false
            }
        },
        loop: true,
        margin: 0,
        dots: false,
        navContainer: controlArea
    })
})

/*--- Action List slider ---*/
$(".action-list-slider.quarter .slide-inner").each(function () {
    var inner = $(this)
    var slider = $(this).parent()
    var controlArea = slider.find('.controlArea')

    inner.owlCarousel({
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            768: {
                items: 2,
                nav: true
            },
            992: {
                items: 3,
                nav: true
            },
            1220: {
                items: 4,
                nav: false
            }
        },
        loop: true,
        margin: 0,
        dots: false,
        navContainer: controlArea
    })
})
$(".action-list-slider.third .slide-inner").each(function () {
    var inner = $(this)
    var slider = $(this).parent()
    var controlArea = slider.find('.controlArea')

    inner.owlCarousel({
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            1220: {
                items: 3,
                nav: false
            }
        },
        loop: true,
        margin: 0,
        dots: false,
        navContainer: controlArea
    })
})

/*=== File Browse ===*/
/* Set the text on custom file input as user input the file */
$('.file-browse').each(function () {
    var theFile = $(this).find('.input-file')
    var theText = $(this).find('.pseudo-text')

    theFile.on('change', function () {
        theText.html(theFile.val())
    })
})

/*=== Prevent Mobile only Accordion click on Desktop Screen ===*/
/* Mobile accordion on team page. In mobile they are accordion, in desktop they display as column. */
function checkMobileAccordion() {
    if ($(window).width() >= 992) {
        $('.hidding-accordion .panel-heading').each(function () {
            $(this).attr('data-toggle', '')
        })
    } else {
        $('.hidding-accordion .panel-heading').each(function () {
            $(this).attr('data-toggle', 'collapse')
        })
    }
}

/*=== Slide Menu For Mobile ===*/
$('body').on('click', '#hamburger, #arrow-right-slide', function () {
    //$('#navbar-right-slide').toggleClass('opened');
    $('body').toggleClass('to-left');

    if ($('body').hasClass('to-left')) {
        $('html').css({ 'height': '100%' });
        $('body').on('touchmove.bs', function (e) {
            //if (!$('#navbar-right-slide').has($(e.target)).length){
            e.preventDefault();
            //}
        });
    } else {
        $('html').css({ 'height': 'auto' });
        $('body').off('touchmove.bs');
    }
});

(function ($) {
    var IS_IOS = /iphone|ipad/i.test(navigator.userAgent);
    $.fn.nodoubletapzoom = function () {
        if (IS_IOS)
            $(this).bind('touchstart', function preventZoom(e) {
                var t2 = e.timeStamp
                  , t1 = $(this).data('lastTouch') || t2
                  , dt = t2 - t1
                  , fingers = e.originalEvent.touches.length;
                $(this).data('lastTouch', t2);
                if (!dt || dt > 500 || fingers > 1) return; // not double-tap

                e.preventDefault(); // double tap - prevent the zoom
                // also synthesize click events we just swallowed up
                $(this).trigger('click').trigger('click');
            });
    };
})(jQuery);

var ts_right;
$('#navbar-right-slide').bind('touchstart', function (e) {
    ts_right = e.originalEvent.touches[0].clientY;
});

$('#navbar-right-slide').bind('touchend', function (e) {
    var el = $(this);
    var te_right = e.originalEvent.changedTouches[0].clientY;
    var postTop = el.scrollTop();
    var navHeight;
    var navInnerHeight;
    var newPost;
    if (ts_right > te_right + 5) {
        // down;
        navHeight = el.height();
        navInnerHeight = el[0].scrollHeight;
        if (navInnerHeight > navHeight + 90) { // 90 = padding top, since jquery.height() not counting the padding
            newPost = postTop + (ts_right - te_right) + 100;
            el.animate({ scrollTop: newPost }, 200);
        }
    } else if (ts_right < te_right - 5) {
        // up
        newPost = postTop - (te_right - ts_right) - 100;
        el.animate({ scrollTop: newPost }, 200);
    }
});

/*=== Login e-service For Mobile ===*/
$('body').on('click', '#login-e-service, #arrow-e-service', function () {
    //$('#nav-e-service').toggleClass('opened');
    $('body').toggleClass('to-right');

    if ($('body').hasClass('to-right')) {
        $('html').css({ 'height': '100%' });
        $('body').on('touchmove.bs', function (e) {
            //if (!$('#nav-e-service').has($(e.target)).length){
            e.preventDefault();
            //}
        });
    } else {
        $('html').css({ 'height': 'auto' });
        $('body').off('touchmove.bs');
    }
});

var ts_left;
$('#nav-e-service').bind('touchstart', function (e) {
    ts_left = e.originalEvent.touches[0].clientY;
});

$('#nav-e-service').bind('touchend', function (e) {
    var el = $(this);
    var te_left = e.originalEvent.changedTouches[0].clientY;
    var postTop = el.scrollTop();
    var navHeight;
    var navInnerHeight;
    var newPost;
    if (ts_left > te_left + 5) {
        // down;
        navHeight = el.height();
        navInnerHeight = el[0].scrollHeight;
        if (navInnerHeight > navHeight + 70) { // 70 = padding top, since jquery.height() not counting the padding
            newPost = postTop + (ts_left - te_left);
            el.animate({ scrollTop: newPost }, 100);
        }
    } else if (ts_left < te_left - 5) {
        // up
        newPost = postTop - (te_left - ts_left);
        el.animate({ scrollTop: newPost }, 100);
    }
});

/*=== Init Google Map ===*/
/* Call function to load members marker in google map. The function is defined in custom.js */
$('#gmap').each(function () {
    initMemberMap()
})

/*=== ON READY ===*/
$(document).ready(function () {
    setHalfImageHeight() // set the half image height
    checkMobileAccordion() // check the accordion

    $('#navbar-right-slide').nodoubletapzoom();
    $('#nav-e-service').nodoubletapzoom();

    // Handling the hamburger menu on mobile
    $(document).click(function (event) {
        var rightNav = $('#navbar-right-slide');
        var loginNav = $('#nav-e-service');
        var clickover = $(event.target);
        var rightNav_opened = rightNav.hasClass('opened');
        var loginNav_opened = loginNav.hasClass('opened');

        if (rightNav_opened === true && !clickover.hasClass('right-nav-toggle') && !clickover.is(rightNav) && rightNav.find(clickover).length <= 0 && !clickover.is('#login-e-service')) {

            $('#hamburger').click();
        }
        if (loginNav_opened === true && !clickover.hasClass('login-nav-toggle') && !clickover.is(loginNav) && loginNav.find(clickover).length <= 0 && !clickover.is('#hamburger')) {
            $('#login-e-service').click();
        }
    });

    $('.quick-link').click(function (e) {
        var elem = $(this);
        var targetOffsetTop = $('.nav-bottom').offset().top;
        elem.prop('disabled', true);
        $('html, body').animate({ 'scrollTop': targetOffsetTop + 'px' }, 500, 'easeInOutCirc', function () {
            elem.prop('disabled', false);
        });
    });
    $('.site-index').click(function (e) {
        var elem = $(this);
        elem.prop('disabled', true);
        $('html, body').animate({ 'scrollTop': '0px' }, 1000, 'easeInOutCirc', function () {
            elem.prop('disabled', false);
        });
    });
   


    /*--- Social Media Grid ---*/
    /* Initiate social media grid using isotope. The contents here are dummies. */
    $('.social-media-grid').each(function () {
        var filterArea = $(this).find('.filter-control')
        var theGrid = $(this).find('.grid-container')
        var loadMoreButton = $(this).find('.load-more')

        /* init */
        theGrid.imagesLoaded(function () {
            theGrid.isotope({
                filter: '*',
                masonry: {
                    columnWidth: 360,
                    gutter: 54,
                    isFitWidth: true
                }
            })
        })

        /* filtering */
        filterArea.on('click', 'a', function (e) {

            if (!$(this).hasClass('active')) {
                var target = $(this).attr('href')

                theGrid.isotope({ filter: target })
                filterArea.find('.active').removeClass('active')
                $(this).addClass('active')
            }

            e.preventDefault()
        })

        /* load more items */
        loadMoreButton.click(function (e) {

            jQuery.ajax({
                type: "POST",
                url: $(this).data("url"),
                //data: ajax_data,
                cache: false,
                success: function (item) {
                    if (item.length > 0) {
                        var append = $(item).find(".socmed-feed");
                        theGrid.append(append).isotope('appended', append)
                    }
                }
            });
        })
    })
})

/* Function that need to be called again on window resize */
$(window).resize(function () {
    setHalfImageHeight()
    checkMobileAccordion()
})