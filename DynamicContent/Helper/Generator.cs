﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DynamicContent.Helper;

namespace DynamicContent.Helper
{
    public class Generator
    {
        //
        // GET: /Manager/Generator/

        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();

        public string GenerateID()
        {
            string temp = "PG-" + DateTime.Now.ToString("ddMMyyyy") + "-";
            var allpages = (from b in db.M_Pages 
                           select b).ToList();
            int num = allpages.Where(x => x.id.Contains(temp)).Count() + 1;
            string id = "";
            while (true)
            {
                id = temp + num.ToString().PadLeft(5, '0');
                if (allpages.Where(x => x.id == id).Count() == 0) break;
                num++;
            }
            return id;
        }

        public static string toPassword(string password)
        {
            return CustomEncryption.HashStringSHA1(password);
        }

    }
}
