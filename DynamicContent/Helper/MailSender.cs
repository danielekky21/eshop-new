﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using log4net;
using System.Net.Mail;
using System.Net;
using System.Configuration;

namespace DynamicContent.Helper
{
    public class MailSender
    {

        private static string mandrill_key = ConfigurationManager.AppSettings["mandrill_key"];
        private static readonly ILog _log = LogManager.GetLogger("");
        private string method = "mandrill";

        private string smtp_host, smtp_user, smtp_pass;
        private int smtp_port;

        #region data object

        //reference https://mandrillapp.com/api/docs/messages.JSON.html
        public class mail
        {
            public string key { get; set; }
            public mailMessage message { get; set; }
            public bool async { get; set; }
            public string ip_pool { get; set; }
            public string send_at { get; set; }

            public mail()
            {
                key = mandrill_key;
                async = false;
                ip_pool = "Main Pool";
                send_at = null;

                message = new mailMessage();
            }
        }

        public class mailMessage
        {
            public string html { get; set; }
            public string text { get; set; }
            public string subject { get; set; }
            public string from_email { get; set; }
            public string from_name { get; set; }
            public List<mailTo> to { get; set; }
            public Dictionary<string, string> headers { get; set; }
            public bool? important { get; set; }
            public bool? track_opens { get; set; }
            public bool? track_clicks { get; set; }
            public bool? auto_text { get; set; }
            public bool? auto_html { get; set; }
            public bool? inline_css { get; set; }
            public bool? url_strip_qs { get; set; }
            public bool? preserve_recipients { get; set; }
            public bool? view_content_link { get; set; }
            public string bcc_address { get; set; }
            public string tracking_domain { get; set; }
            public string signing_domain { get; set; }
            public string return_path_domain { get; set; }
            public bool? merge { get; set; }
            public string merge_language { get; set; }
            public List<global_merge_vars> global_merge_vars { get; set; }
            public List<merge_vars> merge_vars { get; set; }
            public List<string> tags { get; set; }
            public string subaccount { get; set; }
            public string google_analytics_domains { get; set; }
            public string google_analytics_campaign { get; set; }
            public Dictionary<string, string> metadata { get; set; }
            public List<recipient_metadata> recipient_metadata { get; set; }
            public List<attachments> attachments { get; set; }
            public List<attachments> images { get; set; }

            public mailMessage()
            {
                from_email = "noreply@plazaindonesia.co.id";
                from_name = "eMall Plaza Indonesia NoReply";
                important = false;
                merge = true;
                merge_language = "mailchimp";

                to = new List<mailTo>();
                this.global_merge_vars = new List<global_merge_vars>();
                this.merge_vars = new List<merge_vars>();
                tags = new List<string>();
                this.recipient_metadata = new List<recipient_metadata>();
                this.attachments = new List<attachments>();
                images = new List<attachments>();
                headers = new Dictionary<string, string>();
                metadata = new Dictionary<string, string>();
            }
        }

        public class mailTo
        {
            public string email { get; set; }
            public string name { get; set; }
            public string type { get; set; }

            public mailTo()
            {
                type = "to";
            }
        }

        public class global_merge_vars
        {
            public string name { get; set; }
            public string content { get; set; }
        }

        public class merge_vars
        {
            public string rcpt { get; set; }
            public List<global_merge_vars> vars { get; set; }

            public merge_vars()
            {
                vars = new List<global_merge_vars>();
            }
        }

        public class recipient_metadata
        {
            public string rcpt { get; set; }
            public Dictionary<string, string> values { get; set; }

            public recipient_metadata()
            {
                values = new Dictionary<string, string>();
            }
        }

        public class attachments
        {
            public string type { get; set; }
            public string name { get; set; }
            public string content { get; set; }
        }

        #endregion

        /// <summary>
        /// mail sender helper
        /// </summary>
        /// <param name="method">"maindrill" or "smtp"</param>
        public MailSender(string method)
        {
            if (method == "mandrill")
            {
                this.method = method;
            }
            else if (method == "smtp")
            {
                this.method = method;

                this.smtp_host = ConfigurationManager.AppSettings["GmailHost"];
                this.smtp_port = Convert.ToInt32(ConfigurationManager.AppSettings["GmailPort"]);
                this.smtp_user = ConfigurationManager.AppSettings["GmailID"];
                this.smtp_pass = ConfigurationManager.AppSettings["GmailPass"];
            }
        }

        public MailSender() { }

        public bool ping()
        {
            //if smtp, no need to ping
            if (method == "smtp") return true;

            var request = new RestRequest();
            var client = new RestClient();

            request.AddParameter("key", mandrill_key , ParameterType.GetOrPost);
            client.BaseUrl = new Uri("https://mandrillapp.com/api/1.0/users/ping2.json");

            var result = new JObject();
            try
            {
                string json = client.Post(request).Content;
                result = JObject.Parse(json);
            }
            catch (Exception e)
            {
                _log.Error("[MailSender] Error : Connection Problem");
                _log.Error(e.ToString());
            }

            if (result.GetValue("PING") != null)
                return true;
            else
            {
                _log.Error("[MailSender] Error [" + result.GetValue("code") + "] : " + result.GetValue("name"));
                return false;
            }
        }

        public bool sendMailTo(mail email)
        {
            if (ping())
            {
                if (method == "mandrill")
                {

                    var request = new RestRequest();
                    var client = new RestClient();

                    request.AddJsonBody(email);
                    client.BaseUrl = new Uri("https://mandrillapp.com/api/1.0/messages/send.json");

                    var result = new JObject();
                    try
                    {
                        string json = client.Post(request).Content;
                        if (json.StartsWith("[")) json = "{\"root\":" + json + "}";
                        result = JObject.Parse(json);
                    }
                    catch (Exception e)
                    {
                        _log.Error("[MailSender] Error : Connection Problem");
                        _log.Error(e.ToString());
                    }

                    var status = result.GetValue("status");
                    if (status == null || status.ToString() != "error")
                    {
                        foreach (var item in result.GetValue("root"))
                        {
                            _log.Info("[MailSender] Success : " + item.ToString());
                        }
                        return true;
                    }
                    else
                    {
                        _log.Error("[MailSender] Error [" + result.GetValue("code") + "] : " + result.GetValue("name"));
                        return false;
                    }
                }
                else if (method == "smtp")
                {
                    try
                    {
                        var message = new MailMessage();
                        message.From = new MailAddress(email.message.from_email, email.message.from_name);
                        foreach (var item in email.message.to)
                        {
                            var e = new MailAddress(item.email, item.name);
                            if (item.type == "to")
                                message.To.Add(e);
                            else if (item.type == "cc")
                                message.CC.Add(e);
                            else if (item.type == "bcc")
                                message.Bcc.Add(e);
                        }
                        message.Subject = email.message.subject;
                        message.Body = email.message.html;
                        message.IsBodyHtml = true;

                        var client = new SmtpClient();
                        client.Host = smtp_host;
                        client.Port = smtp_port;
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.EnableSsl = true;
                        client.UseDefaultCredentials = false;
                        client.Credentials = new System.Net.NetworkCredential(smtp_user, smtp_pass);

                        try
                        {
                            client.Send(message);

                            _log.Info("[MailSender] Success : " + message.To.First().Address);
                            return true;
                        }
                        catch (SmtpException e)
                        {
                            _log.Error("[MailSender] Error : Cannot Send Mail with SMTP");
                            _log.Error(e.ToString());
                        }

                    }
                    catch (Exception e)
                    {
                        _log.Error("[MailSender] Error : Cannot Setup SMTP");
                        _log.Error(e.ToString());
                    }
                }

                return false;
            }

            return false;
        }

        public bool sendSimpleMail(string to, string subject, string bodyText, string bodyHTML)
        {
            var email = new mail();

            email.message.to.Add(new mailTo() { email = to });
            email.message.subject = subject;
            email.message.text = bodyText;
            email.message.html = bodyHTML;

            return sendMailTo(email);
        }

        public bool sendSimpleMail(List<string> to, string subject, string bodyText, string bodyHTML)
        {
            var email = new mail();

            foreach (var item in to)
            {
                email.message.to.Add(new mailTo() { email = item });
            }

            email.message.subject = subject;
            email.message.text = bodyText;
            email.message.html = bodyHTML;

            return sendMailTo(email);
        }

        public bool sendMail(List<mailTo> address, string subject, string bodyText, string bodyHTML)
        {
            return sendMail(null, null, null, address, subject, bodyText, bodyHTML);
        }

        public bool sendMail(string fromEmail, string fromName, string replyTo, List<mailTo> address, string subject, string bodyText, string bodyHTML)
        {
            var email = new mail();

            if (!string.IsNullOrEmpty(fromEmail)) email.message.from_email = fromEmail;
            if (!string.IsNullOrEmpty(fromName)) email.message.from_name = fromName;
            if (!string.IsNullOrEmpty(replyTo)) email.message.headers.Add("Reply-To", replyTo);

            email.message.to = address;
            email.message.subject = subject;
            email.message.text = bodyText;
            email.message.html = bodyHTML;

            return sendMailTo(email);
        }

    }

    public class GMailer
    {
        public static string GmailUsername { get; set; }
        public static string GmailPassword { get; set; }
        public static string GmailHost { get; set; }
        public static int GmailPort { get; set; }
        public static bool GmailSSL { get; set; }

        public string ToEmail { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsHtml { get; set; }

        static GMailer()
        {
            GmailHost = ConfigurationManager.AppSettings["GmailHost"];
            GmailPort = Convert.ToInt32(ConfigurationManager.AppSettings["GmailPort"]);
            GmailSSL = Convert.ToBoolean(ConfigurationManager.AppSettings["GmailSSL"]);
            GmailUsername = ConfigurationManager.AppSettings["GmailID"];
            GmailPassword = ConfigurationManager.AppSettings["GmailPass"];
        }

        public void Send()
        {
            SmtpClient smtp = new SmtpClient();
            smtp.Host = ConfigurationManager.AppSettings["GmailHost"];
            smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["GmailPort"]);
            smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["GmailSSL"]);
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(GmailUsername, GmailPassword);

            using (var message = new MailMessage(GmailUsername, ToEmail))
            {
                message.Subject = Subject;
                message.Body = Body;
                message.IsBodyHtml = IsHtml;
                smtp.Send(message);
            }
        }
    }
}
