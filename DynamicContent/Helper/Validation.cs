﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.Globalization;

namespace DynamicContent.Helper
{
    public class Validation : Controller
    {
        public static string to_string_notempty(string data)
        {
            if (!string.IsNullOrWhiteSpace(data))
            {
                return data;
            }
            throw new SystemException("validation error : Empty");
        }

        public static string to_string_simple(string data, bool allowEmtpy)
        {
            if (!string.IsNullOrWhiteSpace(data) || allowEmtpy)
            {
                if (string.IsNullOrEmpty(data))
                {
                    return data;
                }
                else
                {
                    var match = Regex.Match(data, "(?:(?![\\w\\s.,:;!?'\"€¥£¢$\\(\\)\\&-]).)+");
                    if (match.Length == 0)
                    {
                        return data;
                    }
                    throw new SystemException("validation error : Cannot contains [" + match.ToString() + "]");
                }
            }
            throw new SystemException("validation error : Empty");
        }

        public static string to_string_simple(string data, bool allowEmpty, int min, int max)
        {
            string new_data = to_string_simple(data, allowEmpty);
            if (allowEmpty && (new_data.Length >= min && new_data.Length <= max))
            {
                return new_data;
            }
            throw new SystemException("validation error : Must be between " + min + " and " + max);
        }

        public static DateTime to_date(string data, bool allowEmpty)
        {
            DateTime t;
            if (!string.IsNullOrWhiteSpace(data) || allowEmpty)
            {
                if (DateTime.TryParse(data, out t))
                {
                    return t;
                }
                return DateTime.Now;
            }
            throw new SystemException("validation error : Empty");
        }

        public static bool to_bool(string data)
        {
            bool t;
            if (bool.TryParse(data, out t))
            {
                return t;
            }
            else
            {
                return false;
            }
        }

        public static int to_int(string data)
        {
            int t;
            if (int.TryParse(data, out t))
            {
                return t;
            }
            else
            {
                return 0;
            }
        }

        public static decimal to_decimal(string data)
        {
            decimal t;
            if (decimal.TryParse(data, out t))
            {
                return t;
            }
            else
            {
                return 0;
            }
        }

        public static string to_email(string data, bool allowEmpty)
        {
            if (!string.IsNullOrWhiteSpace(data) || allowEmpty)
            {
                if (string.IsNullOrEmpty(data) || Regex.IsMatch(data, "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$", RegexOptions.IgnoreCase))
                {
                    return data;
                }
                throw new SystemException("validation error : Wrong email format");
            }
            throw new SystemException("validation error : Empty");
        }

        public static string to_url(string data, bool allowEmpty)
        {
            if (!string.IsNullOrWhiteSpace(data) || allowEmpty)
            {
                if (string.IsNullOrWhiteSpace(data))
                {
                    return string.Empty;
                }
                else
                {
                    try
                    {
                        var uri = new UriBuilder(data).Uri;

                        return uri.AbsoluteUri;
                    }
                    catch { }
                }
                throw new SystemException("validation error : Url not valid");
            }
            throw new SystemException("validation error : Empty");
        }

        public static List<string> to_list(JToken data, bool allowEmpty)
        {
            var result = new List<string>();
            if (data != null || allowEmpty)
            {
                if (data != null)
                {
                    if (data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            result.Add((string)item);
                        }
                    }
                    else
                    {
                        try
                        {
                            result.Add((string)data);
                        }
                        catch
                        {
                            result.Add("");
                        }
                    }
                }
                return result;
            }
            throw new SystemException("validation error : Empty");
        }

        public static Dictionary<string, string> to_dictionary(JToken data, bool allowEmpty)
        {
            var result = new Dictionary<string, string>();
            if (data != null || allowEmpty)
            {
                if (data != null)
                {
                    if (data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            //result.Add((string)item);
                        }
                    }
                    else
                    {
                        try
                        {
                            //result.Add((string)data);
                        }
                        catch
                        {
                            //result.Add("");
                        }
                    }
                }
                return result;
            }
            throw new SystemException("validation error : Empty");
        }

        public static string to_string_in_list(string data, List<string> list)
        {
            if (list.Contains(data))
            {
                return data;
            }
            throw new SystemException("validation error : Data not available");
        }

        public static int to_int_in_list(int data, List<int> list)
        {
            if (list.Contains(data))
            {
                return data;
            }
            throw new SystemException("validation error : Data not available");
        }
    }
}
