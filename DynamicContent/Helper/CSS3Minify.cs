﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Optimization;

#region NOTICE
// Yahoo.Yui.Compressor.YUICompressor class and its dependent classes were downloaded from http://yuicompressor.codeplex.com please read 
// license and copyright notice mentioned below. For more detail visit http://yuicompressor.codeplex.com/license
//
// For more detail on CSS3Minify class visit http://www.dotnetexpertguide.com
#endregion

namespace DynamicContent.Helper
{
    public class CSS3Minify : IBundleTransform
    {
        public void Process(BundleContext context, BundleResponse bundle)
        {
            string strAllCSS = string.Empty;
            foreach (var obj in bundle.Files)
            {
                StreamReader srCSSFile = new StreamReader(HttpContext.Current.Server.MapPath(obj.IncludedVirtualPath));
                strAllCSS += srCSSFile.ReadToEnd();
                strAllCSS += "\n";
                srCSSFile.Close();
            }

            if (strAllCSS == string.Empty)
                bundle.Content = string.Empty;
            else
                bundle.Content = Yahoo.Yui.Compressor.YUICompressor.Compress(strAllCSS, -1, true);

            bundle.ContentType = "text/css";
        }
    }
}

#region License
//Copyright (c) 2008 - 2011, Pure Krome
//All rights reserved.

//Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

//* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

//* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation 
//and/or other materials provided with the distribution.

//* Neither the name of World Domination Technologies nor the names of its contributors may be used to endorse or promote products derived from this 
//software without specific prior written permission.

//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
//THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
//BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
//GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
//LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
#endregion

namespace Yahoo.Yui.Compressor
{
    public static class YUICompressor
    {
        public static string Compress(string css, int columnWidth)
        {
            return Compress(css, columnWidth, true);
        }

        public static string Compress(string css, int columnWidth, bool removeComments)
        {
            if (string.IsNullOrEmpty(css))
            {
                throw new ArgumentNullException("css");
            }

            int totalLen = css.Length;
            int startIndex = 0;
            var comments = new ArrayList();
            var preservedTokens = new ArrayList();
            int max;


            if (removeComments)
            {
                while ((startIndex = css.IndexOf(@"/*", startIndex, StringComparison.OrdinalIgnoreCase)) >= 0)
                {
                    int endIndex = css.IndexOf(@"*/", startIndex + 2, StringComparison.OrdinalIgnoreCase);
                    if (endIndex < 0)
                    {
                        endIndex = totalLen;
                    }

                    // Note: java substring-length param = end index - 2 (which is end index - (startindex + 2))
                    string token = css.Substring(startIndex + 2, endIndex - (startIndex + 2));

                    comments.Add(token);

                    string newResult = css.Replace(startIndex + 2, endIndex,
                                                   "___YUICSSMIN_PRESERVE_CANDIDATE_COMMENT_" + (comments.Count - 1) +
                                                   "___");

                    //var newResult = css.Substring(startIndex + 2, endIndex - (startIndex + 2)) + "___YUICSSMIN_PRESERVE_CANDIDATE_COMMENT_" +
                    //                (comments.Count - 1) + "___" + css.Substring(endIndex + 1);

                    startIndex += 2;
                    css = newResult;
                }
            }

            // Preserve strings so their content doesn't get accidently minified
            var stringBuilder = new StringBuilder();
            var pattern = new Regex("(\"([^\\\\\"]|\\\\.|\\\\)*\")|(\'([^\\\\\']|\\\\.|\\\\)*\')");
            Match match = pattern.Match(css);
            int index = 0;
            while (match.Success)
            {
                string text = match.Groups[0].Value;
                if (!string.IsNullOrEmpty(text))
                {
                    string token = match.Value;
                    char quote = token[0];

                    // Java code: token.substring(1, token.length() -1) .. but that's ...
                    //            token.substring(start index, end index) .. while .NET it's length for the 2nd arg.
                    token = token.Substring(1, token.Length - 2);

                    // Maybe the string contains a comment-like substring?
                    // one, maybe more? put'em back then.
                    if (token.IndexOf("___YUICSSMIN_PRESERVE_CANDIDATE_COMMENT_") >= 0)
                    {
                        max = comments.Count;
                        for (int i = 0; i < max; i += 1)
                        {
                            token = token.Replace("___YUICSSMIN_PRESERVE_CANDIDATE_COMMENT_" + i + "___",
                                                  comments[i].ToString());
                        }
                    }

                    // Minify alpha opacity in filter strings.
                    token = token.RegexReplace("(?i)progid:DXImageTransform.Microsoft.Alpha\\(Opacity=",
                                               "alpha(opacity=");

                    preservedTokens.Add(token);
                    string preserver = quote + "___YUICSSMIN_PRESERVED_TOKEN_" + (preservedTokens.Count - 1) + "___" +
                                       quote;

                    index = match.AppendReplacement(stringBuilder, css, preserver, index);
                    match = match.NextMatch();
                }
            }
            stringBuilder.AppendTail(css, index);
            css = stringBuilder.ToString();

            // Strings are safe, now wrestle the comments.
            max = comments.Count;
            for (int i = 0; i < max; i += 1)
            {
                string token = comments[i].ToString();
                string placeholder = "___YUICSSMIN_PRESERVE_CANDIDATE_COMMENT_" + i + "___";

                // ! in the first position of the comment means preserve
                // so push to the preserved tokens while stripping the !
                if (token.StartsWith("!"))
                {
                    preservedTokens.Add(token);
                    css = css.Replace(placeholder, "___YUICSSMIN_PRESERVED_TOKEN_" + (preservedTokens.Count - 1) + "___");
                    continue;
                }

                // \ in the last position looks like hack for Mac/IE5
                // shorten that to /*\*/ and the next one to /**/
                if (token.EndsWith("\\"))
                {
                    preservedTokens.Add("\\");
                    css = css.Replace(placeholder, "___YUICSSMIN_PRESERVED_TOKEN_" + (preservedTokens.Count - 1) + "___");
                    i = i + 1; // attn: advancing the loop.
                    preservedTokens.Add(string.Empty);
                    css = css.Replace("___YUICSSMIN_PRESERVE_CANDIDATE_COMMENT_" + i + "___",
                                      "___YUICSSMIN_PRESERVED_TOKEN_" + (preservedTokens.Count - 1) + "___");
                    continue;
                }

                // keep empty comments after child selectors (IE7 hack)
                // e.g. html >/**/ body
                if (token.Length == 0)
                {
                    startIndex = css.IndexOf(placeholder);
                    if (startIndex > 2)
                    {
                        if (css[startIndex - 3] == '>')
                        {
                            preservedTokens.Add(string.Empty);
                            css = css.Replace(placeholder,
                                              "___YUICSSMIN_PRESERVED_TOKEN_" + (preservedTokens.Count - 1) + "___");
                        }
                    }
                }

                // In all other cases kill the comment.
                css = css.Replace("/*" + placeholder + "*/", string.Empty);
            }

            // Normalize all whitespace strings to single spaces. Easier to work with that way.
            css = css.RegexReplace("\\s+", " ");

            // Remove the spaces before the things that should not have spaces before them.
            // But, be careful not to turn "p :link {...}" into "p:link{...}"
            // Swap out any pseudo-class colons with the token, and then swap back.
            stringBuilder = new StringBuilder();
            pattern = new Regex("(^|\\})(([^\\{:])+:)+([^\\{]*\\{)");
            match = pattern.Match(css);
            index = 0;
            while (match.Success)
            {
                string text = match.Value;
                text = text.Replace(":", "___YUICSSMIN_PSEUDOCLASSCOLON___");
                text = text.Replace("\\\\", "\\\\\\\\");
                text = text.Replace("\\$", "\\\\\\$");
                index = match.AppendReplacement(stringBuilder, css, text, index);
                match = match.NextMatch();
            }
            stringBuilder.AppendTail(css, index);
            css = stringBuilder.ToString();

            // Remove spaces before the things that should not have spaces before them.
            css = css.RegexReplace("\\s+([!{};:>+\\(\\)\\],])", "$1");

            // Bring back the colon.
            css = css.RegexReplace("___YUICSSMIN_PSEUDOCLASSCOLON___", ":");

            // Retain space for special IE6 cases.
            css = css.RegexReplace(":first\\-(line|letter)(\\{|,)", ":first-$1 $2");

            // no space after the end of a preserved comment.
            css = css.RegexReplace("\\*/ ", "*/");

            // If there is a @charset, then only allow one, and push to the top of the file.
            css = css.RegexReplace("^(.*)(@charset \"[^\"]*\";)", "$2$1");
            css = css.RegexReplace("^(\\s*@charset [^;]+;\\s*)+", "$1");

            // Put the space back in some cases, to support stuff like
            // @media screen and (-webkit-min-device-pixel-ratio:0){
            css = css.RegexReplace("\\band\\(", "and (");

            // Remove the spaces after the things that should not have spaces after them.
            css = css.RegexReplace("([!{}:;>+\\(\\[,])\\s+", "$1");

            // remove unnecessary semicolons.
            css = css.RegexReplace(";+}", "}");

            // Replace 0(px,em,%) with 0.
            css = css.RegexReplace("([\\s:])(0)(px|em|%|in|cm|mm|pc|pt|ex)", "$1$2");

            // Replace 0 0 0 0; with 0.
            css = css.RegexReplace(":0 0 0 0(;|})", ":0$1");
            css = css.RegexReplace(":0 0 0(;|})", ":0$1");
            css = css.RegexReplace(":0 0(;|})", ":0$1");

            // Replace background-position:0; with background-position:0 0;
            // same for transform-origin
            stringBuilder = new StringBuilder();
            pattern =
                new Regex(
                    "(?i)(background-position|transform-origin|webkit-transform-origin|moz-transform-origin|o-transform-origin|ms-transform-origin):0(;|})");
            match = pattern.Match(css);
            index = 0;
            while (match.Success)
            {
                index = match.AppendReplacement(stringBuilder, css,
                                                match.Groups[1].Value.ToLowerInvariant() + ":0 0" + match.Groups[2],
                                                index);
                match = match.NextMatch();
            }
            stringBuilder.AppendTail(css, index);
            css = stringBuilder.ToString();

            // Replace 0.6 to .6, but only when preceded by : or a white-space.
            css = css.RegexReplace("(:|\\s)0+\\.(\\d+)", "$1.$2");

            // Shorten colors from rgb(51,102,153) to #336699
            // This makes it more likely that it'll get further compressed in the next step.
            stringBuilder = new StringBuilder();
            pattern = new Regex("rgb\\s*\\(\\s*([0-9,\\s]+)\\s*\\)");
            match = pattern.Match(css);
            index = 0;
            int value;
            while (match.Success)
            {
                string[] rgbcolors = match.Groups[1].Value.Split(',');
                var hexcolor = new StringBuilder("#");
                foreach (string rgbColour in rgbcolors)
                {
                    if (!Int32.TryParse(rgbColour, out value))
                    {
                        value = 0;
                    }

                    if (value < 16)
                    {
                        hexcolor.Append("0");
                    }
                    hexcolor.Append(value.ToHexString().ToLowerInvariant());
                }

                index = match.AppendReplacement(stringBuilder, css, hexcolor.ToString(), index);
                match = match.NextMatch();
            }
            stringBuilder.AppendTail(css, index);
            css = stringBuilder.ToString();


            // Shorten colors from #AABBCC to #ABC. Note that we want to make sure
            // the color is not preceded by either ", " or =. Indeed, the property
            //     filter: chroma(color="#FFFFFF");
            // would become
            //     filter: chroma(color="#FFF");
            // which makes the filter break in IE.
            stringBuilder = new StringBuilder();
            pattern =
                new Regex(
                    "([^\"'=\\s])(\\s*)#([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])");
            match = pattern.Match(css);
            index = 0;
            while (match.Success)
            {
                // Test for AABBCC pattern.
                if (match.Groups[3].Value.EqualsIgnoreCase(match.Groups[4].Value) &&
                    match.Groups[5].Value.EqualsIgnoreCase(match.Groups[6].Value) &&
                    match.Groups[7].Value.EqualsIgnoreCase(match.Groups[8].Value))
                {
                    string replacement = String.Concat(match.Groups[1].Value, match.Groups[2].Value, "#",
                                                       match.Groups[3].Value, match.Groups[5].Value,
                                                       match.Groups[7].Value);
                    index = match.AppendReplacement(stringBuilder, css, replacement, index);
                }
                else
                {
                    index = match.AppendReplacement(stringBuilder, css, match.Value, index);
                }

                match = match.NextMatch();
            }
            stringBuilder.AppendTail(css, index);
            css = stringBuilder.ToString();

            // border: none -> border:0
            stringBuilder = new StringBuilder();
            pattern =
                new Regex("(?i)(border|border-top|border-right|border-bottom|border-right|outline|background):none(;|})");
            match = pattern.Match(css);
            index = 0;
            while (match.Success)
            {
                string replacement = match.Groups[1].Value.ToLowerInvariant() + ":0" + match.Groups[2].Value;
                index = match.AppendReplacement(stringBuilder, css, replacement, index);
                match = match.NextMatch();
            }
            stringBuilder.AppendTail(css, index);
            css = stringBuilder.ToString();

            // Shorter opacity IE filter.
            css = css.RegexReplace("(?i)progid:DXImageTransform.Microsoft.Alpha\\(Opacity=", "alpha(opacity=");

            // Remove empty rules.
            css = css.RegexReplace("[^\\}\\{/;]+\\{\\}", string.Empty);

            if (columnWidth >= 0)
            {
                // Some source control tools don't like it when files containing lines longer
                // than, say 8000 characters, are checked in. The linebreak option is used in
                // that case to split long lines after a specific column.
                int i = 0;
                int linestartpos = 0;
                stringBuilder = new StringBuilder(css);
                while (i < stringBuilder.Length)
                {
                    char c = stringBuilder[i++];
                    if (c == '}' && i - linestartpos > columnWidth)
                    {
                        stringBuilder.Insert(i, '\n');
                        linestartpos = i;
                    }
                }

                css = stringBuilder.ToString();
            }

            // Replace multiple semi-colons in a row by a single one.
            // See SF bug #1980989.
            css = css.RegexReplace(";;+", ";");

            // Restore preserved comments and strings.
            max = preservedTokens.Count;
            for (int i = 0; i < max; i++)
            {
                css = css.Replace("___YUICSSMIN_PRESERVED_TOKEN_" + i + "___", preservedTokens[i].ToString());
            }

            // Trim the final string (for any leading or trailing white spaces).
            css = css.Trim();

            // Write the output...
            return css;
        }
    }
}

namespace Yahoo.Yui.Compressor
{
    public static class Extensions
    {
        public static int AppendReplacement(this Capture capture,
                                            StringBuilder value,
                                            string input,
                                            string replacement,
                                            int index)
        {
            string preceding = input.Substring(index,
                                               capture.Index - index);

            value.Append(preceding);
            value.Append(replacement);

            return capture.Index + capture.Length;
        }

        public static void AppendTail(this StringBuilder value,
                                      string input,
                                      int index)
        {
            value.Append(input.Substring(index));
        }

        public static string RegexReplace(this string input,
                                          string pattern,
                                          string replacement)
        {
            return Regex.Replace(input, pattern, replacement);
        }

        public static string RegexReplace(this string input,
                                          string pattern,
                                          string replacement,
                                          RegexOptions options)
        {
            return Regex.Replace(input,
                                 pattern,
                                 replacement,
                                 options);
        }

        public static string Fill(this string format,
                                  params object[] args)
        {
            return String.Format(CultureInfo.InvariantCulture,
                                 format,
                                 args);
        }

        public static string RemoveRange(this string input,
                                         int startIndex,
                                         int endIndex)
        {
            return input.Remove(startIndex,
                                endIndex - startIndex);
        }

        public static bool EqualsIgnoreCase(this string left,
                                            string right)
        {
            return String.Compare(left,
                                  right,
                                  StringComparison.OrdinalIgnoreCase) == 0;
        }

        // NOTE: To check out some decimal -> Hex converstions,
        //       goto http://www.openstrike.co.uk/cgi-bin/decimalhex.cgi
        public static string ToHexString(this int value)
        {
            return value.ToString("X");
        }

        public static string ToPluralString(this int value)
        {
            return value == 1 ? string.Empty : "s";
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> value)
        {
            return value == null ||
                   value.Count() <= 0
                       ? true
                       : false;
        }

        public static IList<T> ToListIfNotNullOrEmpty<T>(this IList<T> value)
        {
            return value.IsNullOrEmpty() ? null : value;
        }

        public static string Replace(this string value, int startIndex, int endIndex, string newContent)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException("value");
            }

            // Chop the string into two parts, the before and then the after.
            string before = value.Substring(0, startIndex);
            string after = value.Substring(endIndex);
            return before + newContent + after;
        }
    }
}