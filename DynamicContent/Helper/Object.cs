﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DynamicContent.Helper
{
    public class TreeNode : IEnumerable<TreeNode>
    {
        private readonly List<TreeNode> _children = new List<TreeNode>();

        public readonly string id;
        public readonly Dictionary<string, string> values;
        public TreeNode Parent { get; private set; }

        public TreeNode(string id, Dictionary<string, string> values)
        {
            this.id = id;
            this.values = values;
        }

        public TreeNode GetChild(string id)
        {
            return this._children.Where(x=> x.id == id).FirstOrDefault();
        }

        public TreeNode GetItem(string id)
        {
            var looked = new List<string>();
            var node = this;
            var lastnode = node;
            if (node != null)
            {
                while (true)
                {
                    lastnode = node;
                    var nodes = node._children;
                    if (nodes.Count() > 0)
                    {
                        node = nodes.Where(x => !looked.Contains(x.id)).FirstOrDefault();
                        if (node != null) lastnode = node;
                    }
                    else
                    {
                        node = null;
                    }

                    if (node != null && !looked.Contains(node.id))
                    {
                        if (node.id == id) return node;
                        looked.Add(node.id);
                    }
                    else if (node == null)
                    {
                        node = lastnode.Parent;
                        if (this.Parent == null ? node == null : node.id != this.Parent.id) break;
                    }
                }

                return null;
            }
            else
            {
                return null;
            }
        }

        public TreeNode SearchValue(string key, string value)
        {
            return null;
        }

        public void Add(TreeNode item)
        {
            if (item.Parent != null)
            {
                var current = item.Parent._children.Where(x => x.id == id).FirstOrDefault();
                item.Parent._children.Remove(current);
            }

            item.Parent = this;
            this._children.Add(item);
        }

        public IEnumerator<TreeNode> GetEnumerator()
        {
            return this._children.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public int Count
        {
            get { return this._children.Count; }
        }
    }

    public class _object : Controller
    {
        public class Slider
        {
            public int id { get; set; }
            public string image { get; set; }
            public string name { get; set; }
            public bool? active { get; set; }
            public int? sort { get; set; }
            public string url { get; set; }
            public string Type { get; set; }
        }
        public class FlashSaleHeader
        {
            public Guid id { get; set; }
            public string Day { get; set; }
            public string Start { get; set; }
            public string End { get; set; }
            public bool? Active { get; set; }
            public string Name { get; set; }
        }
        public class TopPicks
        {
            public int TopPicksID { get; set; }
            public int? sort { get; set; }
            public string ProductName { get; set; }
            public string CreatedDate { get; set; }
            public string CreateBy { get; set; }
        }
        public class NowOpen
        {
            public int id { get; set; }
            public string image { get; set; }
            public string name { get; set; }
            public string description { get; set; }
            public bool? active { get; set; }
            public int? sort { get; set; }
        }
        public class ReservationHeader
        {
            public string ReservatonCode { get; set; }
            public string ReservationDate { get; set; }
            public string CustomerName { get; set; }
            public string CustomerEmail { get; set; }
            public string ShopName { get; set; }
            public string FUStatus { get; set; }
            public string FUName { get; set; }
            public string FUNote { get; set; }

        }
        public class Submenu
        {
            public int id { get; set; }
            public string name { get; set; }
            public string images { get; set; }
        }

        public class SubmenuCategory
        {
            public int id { get; set; }
            public string name { get; set; }
            public string parent { get; set; }
        }
        public class TenantBanner
        {
            public int id { get; set; }
            public string TenantName { get; set; }
            public string status { get; set; }
        }
        public class Brand
        {
            public int id { get; set; }
            public string name { get; set; }
        }

        public class BrandWithParam
        {
            public int id { get; set; }
            public string name { get; set; }
            public int subid { get; set; }
            public string subname { get; set; }
            public string parent { get; set; }
        }

        public class Tenant
        {
            public int id { get; set; }
            public string name { get; set; }
            public string images { get; set; }
            public string phone { get; set; }
            public string description { get; set; }
            public int locationid { get; set; }
            public string locationname { get; set; }
            public string reservation_email { get; set; }
            public string category { get; set; }
            public string brand { get; set; }
            public string tenant_category { get; set; }
            public string tenant_subcategory { get; set; }
        }
        
        public class TenantByCategory
        {
            public long id { get; set; }
            public string name { get; set; }
        }

        public class ProductCategory 
        {
            public int id { get; set; }
            public string name { get; set; }
        }

        public class ProductType
        {
            public long id { get; set; }
            public string typeValue { get; set; }
            public string typeDesc { get; set; }
        }

        
        public class TenantCategory
        {
            public int id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
        }

        public class ProductPi
        {
            public int id { get; set; }
            public string name { get; set; }
            public string images { get; set; }
            public string price { get; set; }
            public string size { get; set; }
            public string description { get; set; }
            public string size_fit { get; set; } 
            public string gender { get; set; }
            public string category { get; set; }
            public int categoryid { get; set; }
            //public string brand { get; set; }
            public int tenant { get; set; }
            public bool? isHomepage { get; set; }
            public int? isOrder { get; set; }
            public DateTime  created_date { get; set; }
        }

        public class ProductImg
        {
            public int id { get; set; }
            public string images { get; set; }
            public int? color { get; set; } 
        }

        public class ProductBySubmenu 
        {
            public int id { get; set; }
            public string name { get; set; }
            public string images { get; set; }
            public string price { get; set; }
            public string gender { get; set; }
            public string category { get; set; }
            public string categoryKey { get; set; }
            public long categoryid { get; set; }
            public int tenantid { get; set; }
            public string brand { get; set; }
            public string size { get; set; }
            public int? tenant_category { get; set; }
            public int? tenant_subcategory { get; set; }
            public int idsubmenu { get; set; }
            public string submenu { get; set; }
            public string flashSalePrice { get; set; }
        }

        public class ProductColor
        {
            public int id { get; set; }
            public int product { get; set; }
            public string color { get; set; }
            public string description { get; set; }
            public int stock { get; set; }
        }
        
        public class lovedProduct
        {
            public int id { get; set; }
            public int wishlistID { get; set; }
            public DateTime created_date { get; set; }
            public DateTime prod_created_date { get; set; }
            public string name { get; set; }
            public string tenant { get; set; }
            public bool status { get; set; }
        }

        public class productUploadByBrand
        {
            public int id { get; set; }
            public string name { get; set; }
            public long tenant { get; set; }
            public string tenantName { get; set; } 
            public DateTime? created_date { get; set; }
        }


        public class viewedProduct
        {
            public int id { get; set; }
            public int viewID { get; set; } 
            public DateTime? created_date { get; set; }
            public DateTime? prod_created_date { get; set; }
            public string name { get; set; }
            public string tenant { get; set; }
            public bool status { get; set; }
        }

        public class reservedProduct
        {
            public int id { get; set; }
            public int parentReservation { get; set; } 
            public DateTime? created_date { get; set; }
            public DateTime? prod_created_date { get; set; }
            public string name { get; set; }
            public string tenant { get; set; }
            public int qty { get; set; }
            public bool status { get; set; }
        }

        public class AllProduct
        {
            public int id { get; set; }
            public string name { get; set; }
            public string images { get; set; }
            public string price { get; set; }
            public string size { get; set; }
            public string description { get; set; }
            public string size_fit { get; set; }
            public string gender { get; set; }
            public long idcategory { get; set; }
            public string category { get; set; }
            public int idsubmenu { get; set; }
            public string submenu { get; set; }
            public int idbrand { get; set; }
            public string brand { get; set; }
            public int idtenant { get; set; }
            public bool status { get; set; }
            public string tenant { get; set; }
            public int? tenant_category { get; set; }
            public int? tenant_subcategory { get; set; }
            public DateTime modified_date { get; set; }
            public DateTime? approved_date { get; set; }
            public int? approval_status { get; set; }
            public string approval_message { get; set; }
            public bool isHomepage { get; set; }
            public string flashSalePrice { get; set; }
        }

        public class Menu
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class UserRegister
        {
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string email { get; set; }
            public string phone { get; set; }
            public string address { get; set; }
            public string city { get; set; }
            public string zip { get; set; }
            public string dob { get; set; }
            public string mob { get; set; }
            public string yob { get; set; }
            public string gender { get; set; }
            public string pass { get; set; }
            public string subscribe { get; set; }
            public DateTime regist_date { get; set; }
        }

        public class UserAdd
        {
            public string username { get; set; }
            public string email { get; set; }
            public string pass { get; set; }
            public string cpass { get; set; }
            public string role { get; set; }
        }

        public class RoleAccess
        {
            public int id { get; set; }
            public string role_id { get; set; }
            public int priv_id { get; set; }
            public string pages_id { get; set; }
            public string pages_name { get; set; }
            public string type { get; set; }
            public string desc { get; set; }
            public bool active { get; set; }
            public int sort { get; set; }
        }

        public class UpdateRoleAccess
        {
            public string roleid { get; set; }
            public List<string> allow { get; set; }
        }

        //public class SearchList
        //{
        //    public List<ProductPi> ProductList { get; set; };
        //    public List<TenantList> ListItems { get; set; };
        //}

        #region objectPages
        public class Text
        {
            public string title_ind { get; set; }
            public string title_eng { get; set; }
            public string icon { get; set; }
            public string icon_mobile { get; set; }
            public string desc_ind { get; set; }
            public string desc_eng { get; set; }
            //public List<string> file { get; set; }
            public string file1 { get; set; }
            public string file2 { get; set; }
            public string link { get; set; }

            public Text() { }
            public Text(string json)
            {
                string current = "";
                if (json != null)
                {
                    JObject jObject = JObject.Parse(json);
                    try
                    {
                        current = "title_ind";
                        title_ind = Validation.to_string_simple((string)jObject["title_ind"], false);
                        current = "title_eng";
                        title_eng = Validation.to_string_simple((string)jObject["title_eng"], false);
                        icon = (string)jObject["icon"];
                        icon_mobile = (string)jObject["icon_mobile"];
                        desc_ind = (string)jObject["desc_ind"];
                        desc_eng = (string)jObject["desc_eng"];
                        //file = Validation.to_list(jObject["file"], true);
                        file1 = (string)jObject["file1"];
                        file2 = (string)jObject["file2"];

                        link = Validation.to_url((string)jObject["link"], true);
                    }
                    catch (Exception e)
                    {
                        string es = e.Message;
                        if (es.Contains("validation error : "))
                        {
                            throw new SystemException(es + " [" + current + "]");
                        }
                        else
                        {
                            throw e;
                        }
                    }
                }
            }
        }

        public class TextAlamat
        {
            public string title_ind { get; set; }
            public string title_eng { get; set; }
            public string type_ind { get; set; }
            public string type_eng { get; set; }
            public string address_ind { get; set; }
            public string address_eng { get; set; }
            public string phone_ind { get; set; }
            public string phone_eng { get; set; }

            public TextAlamat() { }
            public TextAlamat(string json)
            {
                string current = "";
                if (json != null)
                {
                    JObject jObject = JObject.Parse(json);
                    try
                    {
                        current = "title_ind";
                        title_ind = Validation.to_string_simple((string)jObject["title_ind"], false);
                        current = "title_eng";
                        title_eng = Validation.to_string_simple((string)jObject["title_eng"], false);
                        type_ind = (string)jObject["type_ind"];
                        type_eng = (string)jObject["type_eng"];
                        address_ind = (string)jObject["address_ind"];
                        address_eng = (string)jObject["address_eng"];
                        phone_ind = (string)jObject["phone_ind"];
                        phone_eng = (string)jObject["phone_eng"];
                    }
                    catch (Exception e)
                    {
                        string es = e.Message;
                        if (es.Contains("validation error : "))
                        {
                            throw new SystemException(es + " [" + current + "]");
                        }
                        else
                        {
                            throw e;
                        }
                    }
                }
            }
        }

        public class TextTitle
        {
            public string title_ind { get; set; }
            public string title_eng { get; set; }

            public TextTitle() { }
            public TextTitle(string json)
            {
                string current = "";
                if (json != null)
                {
                    JObject jObject = JObject.Parse(json);
                    try
                    {
                        current = "title_ind";
                        title_ind = Validation.to_string_simple((string)jObject["title_ind"], false);
                        current = "title_eng";
                        title_eng = Validation.to_string_simple((string)jObject["title_eng"], false);
                    }
                    catch (Exception e)
                    {
                        string es = e.Message;
                        if (es.Contains("validation error : "))
                        {
                            throw new SystemException(es + " [" + current + "]");
                        }
                        else
                        {
                            throw e;
                        }
                    }
                }
            }
        }

        
        public class TextItem
        {
            public string title_ind { get; set; }
            public string title_eng { get; set; }
            public string subtitle_ind { get; set; }
            public string subtitle_eng { get; set; }
            public string image { get; set; }
            public string hover_ind { get; set; }
            public string hover_eng { get; set; }
            public string desc_ind { get; set; }
            public string desc_eng { get; set; }
            //public List<string> file { get; set; }
            public string file1 { get; set; }
            public string file2 { get; set; }
            public string link { get; set; }

            public TextItem() { }
            public TextItem(string json)
            {
                string current = "";
                if (json != null)
                {
                    JObject jObject = JObject.Parse(json);
                    try
                    {
                        current = "title_ind";
                        title_ind = Validation.to_string_simple((string)jObject["title_ind"], false);
                        current = "title_eng";
                        title_eng = Validation.to_string_simple((string)jObject["title_eng"], false);
                        desc_ind = (string)jObject["desc_ind"];
                        desc_eng = (string)jObject["desc_eng"];
                        image = (string)jObject["image"];
                        current = "subtitle_eng";
                        subtitle_eng = Validation.to_string_simple((string)jObject["subtitle_eng"], true);
                        current = "subtitle_ind";
                        subtitle_ind = Validation.to_string_simple((string)jObject["subtitle_ind"], true);
                        image = (string)jObject["image"];
                        current = "hover_ind";
                        hover_ind = Validation.to_string_simple((string)jObject["hover_ind"], true);
                        current = "hover_eng";
                        hover_eng = Validation.to_string_simple((string)jObject["hover_eng"], true);
                        //file = Validation.to_list(jObject["file"], true);
                        file1 = (string)jObject["file1"];
                        file2 = (string)jObject["file2"];
                        link = (string)jObject["link"];
                    }
                    catch (Exception e)
                    {
                        string es = e.Message;
                        if (es.Contains("validation error : "))
                        {
                            throw new SystemException(es + " [" + current + "]");
                        }
                        else
                        {
                            throw e;
                        }
                    }
                }
            }
        }

        public class TextItemImg
        {
            public string title_ind { get; set; }
            public string title_eng { get; set; }
            public string subtitle_ind { get; set; }
            public string subtitle_eng { get; set; }
            public string image { get; set; }
            public string small_image { get; set; }
            public string hover_ind { get; set; }
            public string hover_eng { get; set; }
            public string teaser_ind { get; set; }
            public string teaser_eng { get; set; }
            public string desc_ind { get; set; }
            public string desc_eng { get; set; }
            //public List<string> file { get; set; }
            public string file1 { get; set; }
            public string file2 { get; set; }
            public string link { get; set; }

            public TextItemImg() { }
            public TextItemImg(string json)
            {
                string current = "";
                if (json != null)
                {
                    JObject jObject = JObject.Parse(json);
                    try
                    {
                        current = "title_ind";
                        title_ind = Validation.to_string_simple((string)jObject["title_ind"], false);
                        current = "title_eng";
                        title_eng = Validation.to_string_simple((string)jObject["title_eng"], false);
                        teaser_ind = (string)jObject["teaser_ind"];
                        teaser_eng = (string)jObject["teaser_eng"];
                        desc_ind = (string)jObject["desc_ind"];
                        desc_eng = (string)jObject["desc_eng"];
                        image = (string)jObject["image"];
                        small_image = (string)jObject["small_image"];
                        current = "subtitle_eng";
                        subtitle_eng = Validation.to_string_simple((string)jObject["subtitle_eng"], true);
                        current = "subtitle_ind";
                        subtitle_ind = Validation.to_string_simple((string)jObject["subtitle_ind"], true);
                        current = "hover_ind";
                        hover_ind = Validation.to_string_simple((string)jObject["hover_ind"], true);
                        current = "hover_eng";
                        hover_eng = Validation.to_string_simple((string)jObject["hover_eng"], true);
                        //file = Validation.to_list(jObject["file"], true);
                        file1 = (string)jObject["file1"];
                        file2 = (string)jObject["file2"];
                        link = Validation.to_url((string)jObject["link"], true);
                    }
                    catch (Exception e)
                    {
                        string es = e.Message;
                        if (es.Contains("validation error : "))
                        {
                            throw new SystemException(es + " [" + current + "]");
                        }
                        else
                        {
                            throw e;
                        }
                    }
                }
            }
        }

        public class TextMeta
        {
            public string fb_title_ind { get; set; }
            public string fb_title_eng { get; set; }
            public string fb_url { get; set; }
            public string fb_image { get; set; }
            public string fb_desc_ind { get; set; }
            public string fb_desc_eng { get; set; }

            public string meta_desc_ind { get; set; }
            public string meta_desc_eng { get; set; }
            public string meta_key_ind { get; set; }
            public string meta_key_eng { get; set; }
            public string title_ind { get; set; }
            public string title_eng { get; set; }
            public string subtitle_ind { get; set; }
            public string subtitle_eng { get; set; }
            public string desc_ind { get; set; }
            public string desc_eng { get; set; }
            public string icon { get; set; }
            public string icon_mobile { get; set; }
            public string image { get; set; }
            public string inner_image { get; set; }
            public string banner { get; set; }
            public string banner_mobile { get; set; }
            public string btitle_ind { get; set; }
            public string btitle_eng { get; set; }
            public string bdesc_ind { get; set; }
            public string bdesc_eng { get; set; }
            //public List<string> file { get; set; }
            public string file1 { get; set; }
            public string file2 { get; set; }
            public string links { get; set; }

            public TextMeta() { }
            public TextMeta(string json)
            {
                string current = "";
                if (json != null)
                {
                    JObject jObject = JObject.Parse(json);
                    try
                    {
                        fb_title_ind = (string)jObject["fb_title_ind"];
                        fb_title_eng = (string)jObject["fb_title_eng"];
                        fb_url = (string)jObject["fb_url"];
                        fb_image = (string)jObject["fb_image"];
                        fb_desc_ind = (string)jObject["fb_desc_ind"];
                        fb_desc_eng = (string)jObject["fb_desc_eng"];

                        meta_desc_ind = (string)jObject["meta_desc_ind"];
                        meta_desc_eng = (string)jObject["meta_desc_eng"];
                        meta_key_ind = (string)jObject["meta_key_ind"];
                        meta_key_eng = (string)jObject["meta_key_eng"];

                        title_ind = (string)jObject["title_ind"];
                        title_eng = (string)jObject["title_eng"];
                        current = "subtitle_eng";
                        subtitle_eng = Validation.to_string_simple((string)jObject["subtitle_eng"], true);
                        current = "subtitle_ind";
                        subtitle_ind = Validation.to_string_simple((string)jObject["subtitle_ind"], true);
                        desc_ind = (string)jObject["desc_ind"];
                        desc_eng = (string)jObject["desc_eng"];
                        icon = (string)jObject["icon"];
                        icon_mobile = (string)jObject["icon_mobile"];
                        image = (string)jObject["image"];
                        inner_image = (string)jObject["inner_image"];
                        banner = (string)jObject["banner"];
                        banner_mobile = (string)jObject["banner_mobile"];
                        current = "btitle_ind";
                        btitle_ind = Validation.to_string_simple((string)jObject["btitle_ind"], true);
                        current = "btitle_eng";
                        btitle_eng = Validation.to_string_simple((string)jObject["btitle_eng"], true);
                        bdesc_ind = (string)jObject["bdesc_ind"];
                        bdesc_eng = (string)jObject["bdesc_eng"];
                        //file = Validation.to_list(jObject["file"], true);
                        file1 = (string)jObject["file1"];
                        file2 = (string)jObject["file2"];
                        links = Validation.to_url((string)jObject["links"], true);
                    }
                    catch (Exception e)
                    {
                        string es = e.Message;
                        if (es.Contains("validation error : "))
                        {
                            throw new SystemException(es + " [" + current + "]");
                        }
                        else
                        {
                            throw e;
                        }
                    }
                }
            }
        }

        public class TextMetaItem
        {
            public string fb_title_ind { get; set; }
            public string fb_title_eng { get; set; }
            public string fb_url { get; set; }
            public string fb_image { get; set; }
            public string fb_desc_ind { get; set; }
            public string fb_desc_eng { get; set; }

            public string meta_desc_ind { get; set; }
            public string meta_desc_eng { get; set; }
            public string meta_key_ind { get; set; }
            public string meta_key_eng { get; set; }
            public string title_ind { get; set; }
            public string title_eng { get; set; }
            public string subtitle_ind { get; set; }
            public string subtitle_eng { get; set; }
            public string image { get; set; }
            public string inner_image { get; set; }
            public string hover_ind { get; set; }
            public string hover_eng { get; set; }
            public string desc_ind { get; set; }
            public string desc_eng { get; set; }
            //public List<string> file { get; set; }
            public string file1 { get; set; }
            public string file2 { get; set; }

            public TextMetaItem() { }
            public TextMetaItem(string json)
            {
                string current = "";
                if (json != null)
                {
                    JObject jObject = JObject.Parse(json);
                    try
                    {
                        fb_title_ind = (string)jObject["fb_title_ind"];
                        fb_title_eng = (string)jObject["fb_title_eng"];
                        fb_url = (string)jObject["fb_url"];
                        fb_image = (string)jObject["fb_image"];
                        fb_desc_ind = (string)jObject["fb_desc_ind"];
                        fb_desc_eng = (string)jObject["fb_desc_eng"];

                        meta_desc_ind = (string)jObject["meta_desc_ind"];
                        meta_desc_eng = (string)jObject["meta_desc_eng"];
                        meta_key_ind = (string)jObject["meta_key_ind"];
                        meta_key_eng = (string)jObject["meta_key_eng"];
                        current = "title_ind";
                        title_ind = Validation.to_string_simple((string)jObject["title_ind"], false);
                        current = "title_eng";
                        title_eng = Validation.to_string_simple((string)jObject["title_eng"], false);
                        image = (string)jObject["image"];
                        inner_image = (string)jObject["inner_image"];
                        current = "subtitle_eng";
                        subtitle_eng = Validation.to_string_simple((string)jObject["subtitle_eng"], true);
                        current = "subtitle_ind";
                        subtitle_ind = Validation.to_string_simple((string)jObject["subtitle_ind"], true);
                        image = (string)jObject["image"];
                        current = "hover_ind";
                        hover_ind = Validation.to_string_simple((string)jObject["hover_ind"], true);
                        current = "hover_eng";
                        hover_eng = Validation.to_string_simple((string)jObject["hover_eng"], true);
                        current = "desc_ind";
                        desc_ind = Validation.to_string_notempty((string)jObject["desc_ind"]);
                        current = "desc_eng";
                        desc_eng = Validation.to_string_notempty((string)jObject["desc_eng"]);
                        //file = Validation.to_list(jObject["file"], true);
                        file1 = (string)jObject["file1"];
                        file2 = (string)jObject["file2"];
                    }
                    catch (Exception e)
                    {
                        string es = e.Message;
                        if (es.Contains("validation error : "))
                        {
                            throw new SystemException(es + " [" + current + "]");
                        }
                        else
                        {
                            throw e;
                        }
                    }
                }
            }
        }

        public class TextMetaNews
        {
            public string fb_title_ind { get; set; }
            public string fb_title_eng { get; set; }
            public string fb_url { get; set; }
            public string fb_image { get; set; }
            public string fb_desc_ind { get; set; }
            public string fb_desc_eng { get; set; }

            public string meta_desc_ind { get; set; }
            public string meta_desc_eng { get; set; }
            public string title_ind { get; set; }
            public string title_eng { get; set; }
            public DateTime date { get; set; }
            public string desc_ind { get; set; }
            public string desc_eng { get; set; }
            public string image { get; set; }
            public string small_image { get; set; }

            public TextMetaNews() { }
            public TextMetaNews(string json)
            {
                string current = "";
                if (json != null)
                {
                    JObject jObject = JObject.Parse(json);
                    try
                    {
                        fb_title_ind = (string)jObject["fb_title_ind"];
                        fb_title_eng = (string)jObject["fb_title_eng"];
                        fb_url = (string)jObject["fb_url"];
                        fb_image = (string)jObject["fb_image"];
                        fb_desc_ind = (string)jObject["fb_desc_ind"];
                        fb_desc_eng = (string)jObject["fb_desc_eng"];

                        meta_desc_ind = (string)jObject["meta_desc_ind"];
                        meta_desc_eng = (string)jObject["meta_desc_eng"];
                        current = "title_ind";
                        title_ind = Validation.to_string_simple((string)jObject["title_ind"], false);
                        current = "title_eng";
                        title_eng = Validation.to_string_simple((string)jObject["title_eng"], false);
                        date = Validation.to_date((string)jObject["date"], false);
                        desc_ind = (string)jObject["desc_ind"];
                        desc_eng = (string)jObject["desc_eng"];
                        image = (string)jObject["image"];
                        small_image = (string)jObject["small_image"];
                    }
                    catch (Exception e)
                    {
                        string es = e.Message;
                        if (es.Contains("validation error : "))
                        {
                            throw new SystemException(es + " [" + current + "]");
                        }
                        else
                        {
                            throw e;
                        }
                    }
                }
            }
        }
        
        public class TextMetaDesc
        {
            public string fb_title_ind { get; set; }
            public string fb_title_eng { get; set; }
            public string fb_url { get; set; }
            public string fb_image { get; set; }
            public string fb_desc_ind { get; set; }
            public string fb_desc_eng { get; set; }

            public string meta_desc_ind { get; set; }
            public string meta_desc_eng { get; set; }
            public string meta_key_ind { get; set; }
            public string meta_key_eng { get; set; }

            public string banner { get; set; }
            public string banner_mobile { get; set; }
            public string btitle_ind { get; set; }
            public string btitle_eng { get; set; }
            public string bdesc_ind { get; set; }
            public string bdesc_eng { get; set; }

            public string title_ind { get; set; }
            public string title_eng { get; set; }
            public string subtitle_ind { get; set; }
            public string subtitle_eng { get; set; }
            public string topdesc_ind { get; set; }
            public string topdesc_eng { get; set; }
            public string middesc_ind { get; set; }
            public string middesc_eng { get; set; }
            public string botdesc_ind { get; set; }
            public string botdesc_eng { get; set; }
            public string icon { get; set; }
            public string icon_mobile { get; set;} 
            public string image { get; set; }
            public string inner_image { get; set; }
            public string imgdesc_ind { get; set; }
            public string imgdesc_eng { get; set; }
            public string file1 { get; set; }
            public string file2 { get; set; }

            public TextMetaDesc() { }
            public TextMetaDesc(string json)
            {
                string current = "";
                if (json != null)
                {
                    JObject jObject = JObject.Parse(json);
                    try
                    {
                        fb_title_ind = (string)jObject["fb_title_ind"];
                        fb_title_eng = (string)jObject["fb_title_eng"];
                        fb_url = (string)jObject["fb_url"];
                        fb_image = (string)jObject["fb_image"];
                        fb_desc_ind = (string)jObject["fb_desc_ind"];
                        fb_desc_eng = (string)jObject["fb_desc_eng"];

                        meta_desc_ind = (string)jObject["meta_desc_ind"];
                        meta_desc_eng = (string)jObject["meta_desc_eng"];
                        meta_key_ind = (string)jObject["meta_key_ind"];
                        meta_key_eng = (string)jObject["meta_key_eng"];

                        title_ind = (string)jObject["title_ind"];
                        title_eng = (string)jObject["title_eng"];
                        subtitle_ind = (string)jObject["subtitle_ind"];
                        subtitle_eng = (string)jObject["subtitle_eng"];
                        topdesc_ind = (string)jObject["topdesc_ind"];
                        topdesc_eng = (string)jObject["topdesc_eng"];
                        middesc_ind = (string)jObject["middesc_ind"];
                        middesc_eng = (string)jObject["middesc_eng"];
                        botdesc_ind = (string)jObject["botdesc_ind"];
                        botdesc_eng = (string)jObject["botdesc_eng"];
                        icon = (string)jObject["icon"];
                        icon_mobile = (string)jObject["icon_mobile"];
                        image = (string)jObject["image"];
                        inner_image = (string)jObject["inner_image"];
                        imgdesc_ind = (string)jObject["imgdesc_ind"];
                        imgdesc_eng = (string)jObject["imgdesc_eng"];

                        banner = (string)jObject["banner"];
                        banner_mobile = (string)jObject["banner_mobile"];
                        current = "btitle_ind";
                        btitle_ind = Validation.to_string_simple((string)jObject["btitle_ind"], true);
                        current = "btitle_eng";
                        btitle_eng = Validation.to_string_simple((string)jObject["btitle_eng"], true);
                        bdesc_ind = (string)jObject["bdesc_ind"];
                        bdesc_eng = (string)jObject["bdesc_eng"];
                        file1 = (string)jObject["file1"];
                        file2 = (string)jObject["file2"];
                    }
                    catch (Exception e)
                    {
                        string es = e.Message;
                        if (es.Contains("validation error : "))
                        {
                            throw new SystemException(es + " [" + current + "]");
                        }
                        else
                        {
                            throw e;
                        }
                    }
                }
            }
        }

        public class TextMetaVisi
        {
            public string fb_title_ind { get; set; }
            public string fb_title_eng { get; set; }
            public string fb_url { get; set; }
            public string fb_image { get; set; }
            public string fb_desc_ind { get; set; }
            public string fb_desc_eng { get; set; }

            public string meta_desc_ind { get; set; }
            public string meta_desc_eng { get; set; }
            public string meta_key_ind { get; set; }
            public string meta_key_eng { get; set; }

            public string title_ind { get; set; }
            public string title_eng { get; set; }
            public string banner { get; set; }
            public string banner_mobile { get; set; }
            public string btitle_ind { get; set; }
            public string btitle_eng { get; set; }
            public string bdesc_ind { get; set; }
            public string bdesc_eng { get; set; }
            public string ltitle_ind { get; set; }
            public string ltitle_eng { get; set; }
            public string limage { get; set; }
            public string ldesc_ind { get; set; }
            public string ldesc_eng { get; set; }
            public string rtitle_ind { get; set; }
            public string rtitle_eng { get; set; }
            public string rimage { get; set; }
            public string rdesc_ind { get; set; }
            public string rdesc_eng { get; set; }
            public string intitle_ind { get; set; }
            public string intitle_eng { get; set; }
            public string inner_image { get; set; } 
            public string indesc_ind { get; set; }
            public string indesc_eng { get; set; }
            public string middesc_ind { get; set; }
            public string middesc_eng { get; set; }
            public string botdesc_ind { get; set; }
            public string botdesc_eng { get; set; }

            public TextMetaVisi() { }
            public TextMetaVisi(string json)
            {
                string current = "";
                if (json != null)
                {
                    JObject jObject = JObject.Parse(json);
                    try
                    {
                        fb_title_ind = (string)jObject["fb_title_ind"];
                        fb_title_eng = (string)jObject["fb_title_eng"];
                        fb_url = (string)jObject["fb_url"];
                        fb_image = (string)jObject["fb_image"];
                        fb_desc_ind = (string)jObject["fb_desc_ind"];
                        fb_desc_eng = (string)jObject["fb_desc_eng"];

                        meta_desc_ind = (string)jObject["meta_desc_ind"];
                        meta_desc_eng = (string)jObject["meta_desc_eng"];
                        meta_key_ind = (string)jObject["meta_key_ind"];
                        meta_key_eng = (string)jObject["meta_key_eng"];
                        current = "title_ind";
                        title_ind = Validation.to_string_simple((string)jObject["title_ind"], false);
                        current = "title_eng";
                        title_eng = Validation.to_string_simple((string)jObject["title_eng"], false);
                        ltitle_eng = (string)jObject["ltitle_eng"];
                        ltitle_ind = (string)jObject["ltitle_ind"];
                        limage = (string)jObject["limage"];
                        ldesc_ind = (string)jObject["ldesc_ind"];
                        ldesc_eng = (string)jObject["ldesc_eng"];
                        rtitle_ind = (string)jObject["rtitle_ind"];
                        rtitle_eng = (string)jObject["rtitle_eng"];
                        rimage = (string)jObject["rimage"];
                        rdesc_ind = (string)jObject["rdesc_ind"];
                        rdesc_eng = (string)jObject["rdesc_eng"];
                        intitle_ind = (string)jObject["intitle_ind"];
                        intitle_eng = (string)jObject["intitle_eng"];
                        inner_image = (string)jObject["inner_image"];
                        indesc_ind = (string)jObject["indesc_ind"];
                        indesc_eng = (string)jObject["indesc_eng"];
                        middesc_ind = (string)jObject["middesc_ind"];
                        middesc_eng = (string)jObject["middesc_eng"];
                        botdesc_ind = (string)jObject["botdesc_ind"];
                        botdesc_eng = (string)jObject["botdesc_eng"];

                        banner = (string)jObject["banner"];
                        banner_mobile = (string)jObject["banner_mobile"];
                        current = "btitle_ind";
                        btitle_ind = Validation.to_string_simple((string)jObject["btitle_ind"], true);
                        current = "btitle_eng";
                        btitle_eng = Validation.to_string_simple((string)jObject["btitle_eng"], true);
                        bdesc_ind = (string)jObject["bdesc_ind"];
                        bdesc_eng = (string)jObject["bdesc_eng"];
                    }
                    catch (Exception e)
                    {
                        string es = e.Message;
                        if (es.Contains("validation error : "))
                        {
                            throw new SystemException(es + " [" + current + "]");
                        }
                        else
                        {
                            throw e;
                        }
                    }
                }
            }
        }

        public class Product
        {
            public string meta_desc_ind { get; set; }
            public string meta_desc_eng { get; set; }
            public string meta_key_ind { get; set; }
            public string meta_key_eng { get; set; }
            public string title_ind { get; set; }
            public string title_eng { get; set; }
            public string subtitle_ind { get; set; }
            public string subtitle_eng { get; set; }
            public string hover_ind { get; set; }
            public string hover_eng { get; set; }
            public string desc_ind { get; set; }
            public string desc_eng { get; set; }
            public string image { get; set; }
            public string image2 { get; set; }
            public string banner { get; set; }

            public Product() { }
            public Product(string json)
            {
                string current = "";
                if (json != null)
                {
                    JObject jObject = JObject.Parse(json);
                    try
                    {
                        meta_desc_ind = (string)jObject["meta_desc_ind"];
                        meta_desc_eng = (string)jObject["meta_desc_eng"];
                        meta_key_ind = (string)jObject["meta_key_ind"];
                        meta_key_eng = (string)jObject["meta_key_eng"];
                        current = "title_ind";
                        title_ind = Validation.to_string_simple((string)jObject["title_ind"], false);
                        current = "title_eng";
                        title_eng = Validation.to_string_simple((string)jObject["title_eng"], false);
                        current = "subtitle_eng";
                        subtitle_eng = Validation.to_string_simple((string)jObject["subtitle_eng"], true);
                        current = "subtitle_ind";
                        subtitle_ind = Validation.to_string_simple((string)jObject["subtitle_ind"], true);
                        current = "hover_ind";
                        hover_ind = Validation.to_string_simple((string)jObject["hover_ind"], true);
                        current = "hover_eng";
                        hover_eng = Validation.to_string_simple((string)jObject["hover_eng"], true);
                        desc_ind = (string)jObject["desc_ind"];
                        desc_eng = (string)jObject["desc_eng"];
                        image = (string)jObject["image"];
                        image2 = (string)jObject["image2"];
                        banner = (string)jObject["banner"];
                    }
                    catch (Exception e)
                    {
                        string es = e.Message;
                        if (es.Contains("validation error : "))
                        {
                            throw new SystemException(es + " [" + current + "]");
                        }
                        else
                        {
                            throw e;
                        }
                    }
                }
            }
        }

        public class News
        {
            public string meta_desc_ind { get; set; }
            public string meta_desc_eng { get; set; }
            public string meta_key_ind { get; set; }
            public string meta_key_eng { get; set; }
            public string title_ind { get; set; }
            public string title_eng { get; set; }
            public string short_desc_ind { get; set; }
            public string short_desc_eng { get; set; }
            public string desc_ind { get; set; }
            public string desc_eng { get; set; }
            public string banner { get; set; }
            public string image { get; set; } //icon thumbnails
            public DateTime date { get; set; }

            public News() { }
            public News(string json)
            {
                string current = "";
                if (json != null)
                {
                    JObject jObject = JObject.Parse(json);
                    try
                    {
                        meta_desc_ind = (string)jObject["meta_desc_ind"];
                        meta_desc_eng = (string)jObject["meta_desc_eng"];
                        meta_key_ind = (string)jObject["meta_key_ind"];
                        meta_key_eng = (string)jObject["meta_key_eng"];
                        current = "title_ind";
                        title_ind = Validation.to_string_notempty((string)jObject["title_ind"]);
                        current = "title_eng";
                        title_eng = Validation.to_string_notempty((string)jObject["title_eng"]);
                        current = "short_desc_ind";
                        short_desc_ind = Validation.to_string_notempty((string)jObject["short_desc_ind"]);
                        current = "short_desc_eng";
                        short_desc_eng = Validation.to_string_notempty((string)jObject["short_desc_eng"]);
                        current = "desc_ind";
                        desc_ind = Validation.to_string_notempty((string)jObject["desc_ind"]);
                        current = "desc_eng";
                        desc_eng = Validation.to_string_notempty((string)jObject["desc_eng"]);
                        banner = (string)jObject["banner"];
                        image = (string)jObject["image"];
                        current = "date";
                        date = Validation.to_date((string)jObject["date"], false);
                    }
                    catch (Exception e)
                    {
                        string es = e.Message;
                        if (es.Contains("validation error : "))
                        {
                            throw new SystemException(es + " [" + current + "]");
                        }
                        else
                        {
                            throw e;
                        }
                    }
                }
            }
        }

        public class Event
        {
            public string title_ind { get; set; }
            public string title_eng { get; set; }
            public string desc_ind { get; set; }
            public string desc_eng { get; set; }
            public DateTime date { get; set; }

            public Event() { }
            public Event(string json)
            {
                string current = "";
                if (json != null)
                {
                    JObject jObject = JObject.Parse(json);
                    try
                    {
                        current = "title_ind";
                        title_ind = Validation.to_string_notempty((string)jObject["title_ind"]);
                        current = "title_eng";
                        title_eng = Validation.to_string_notempty((string)jObject["title_eng"]);
                        current = "desc_ind";
                        desc_ind = Validation.to_string_notempty((string)jObject["desc_ind"]);
                        current = "desc_eng";
                        desc_eng = Validation.to_string_notempty((string)jObject["desc_eng"]);
                        current = "date";
                        date = Validation.to_date((string)jObject["date"], false);
                    }
                    catch (Exception e)
                    {
                        string es = e.Message;
                        if (es.Contains("validation error : "))
                        {
                            throw new SystemException(es + " [" + current + "]");
                        }
                        else
                        {
                            throw e;
                        }
                    }
                }
            }
        }

        

        public class Video
        {
            public string title_ind { get; set; }
            public string title_eng { get; set; }
            public string image { get; set; }
            public string video { get; set; }

            public Video() { }
            public Video(string json)
            {
                string current = "";
                if (json != null)
                {
                    JObject jObject = JObject.Parse(json);
                    try
                    {
                        current = "title_ind";
                        title_ind = Validation.to_string_simple((string)jObject["title_ind"], false);
                        current = "title_eng";
                        title_eng = Validation.to_string_simple((string)jObject["title_eng"], false);
                        image = (string)jObject["image"];
                        video = (string)jObject["video"];
                    }
                    catch (Exception e)
                    {
                        string es = e.Message;
                        if (es.Contains("validation error : "))
                        {
                            throw new SystemException(es + " [" + current + "]");
                        }
                        else
                        {
                            throw e;
                        }
                    }
                }
            }
        }

        public class Link
        {
            public string title_ind { get; set; }
            public string title_eng { get; set; }
            public string image_ind { get; set; }
            public string image_eng { get; set; }
            public string hover_ind { get; set; }
            public string hover_eng { get; set; }
            public string link { get; set; }

            public Link() { }
            public Link(string json)
            {
                string current = "";
                if (json != null)
                {
                    JObject jObject = JObject.Parse(json);
                    try
                    {
                        current = "title_ind";
                        title_ind = Validation.to_string_simple((string)jObject["title_ind"], true);
                        current = "title_eng";
                        title_eng = Validation.to_string_simple((string)jObject["title_eng"], true);
                        image_ind = (string)jObject["image_ind"];
                        image_eng = (string)jObject["image_eng"];
                        hover_ind = (string)jObject["hover_ind"];
                        hover_eng = (string)jObject["hover_eng"];
                        current = "link";
                        link = Validation.to_url((string)jObject["link"], true);
                    }
                    catch (Exception e)
                    {   
                        string es = e.Message;
                        if (es.Contains("validation error : "))
                        {
                            throw new SystemException(es + " [" + current + "]");
                        }
                        else
                        {
                            throw e;
                        }
                    }
                }
            }
        }

        public static string postToJson(Dictionary<string, string> data)
        {
            string t;
            string result = null;
            string json = JsonConvert.SerializeObject(data);
            try
            {
                if (data.TryGetValue("type", out t))
                {
                    switch (t)
                    {
                        case "text":
                            result = JsonConvert.SerializeObject(new Text(json));
                            break;
                        case "textAlamat":
                            result = JsonConvert.SerializeObject(new TextAlamat(json));
                            break;
                        case "textTitle":
                            result = JsonConvert.SerializeObject(new TextItem(json));
                            break;
                        case "textItem":
                            result = JsonConvert.SerializeObject(new TextItem(json));
                            break;
                        case "textItemImg":
                            result = JsonConvert.SerializeObject(new TextItemImg(json));
                            break;
                        case "product":
                            result = JsonConvert.SerializeObject(new Product(json));
                            break;
                        case "textMeta":
                            result = JsonConvert.SerializeObject(new TextMeta(json));
                            break;
                        case "textMetaItem":
                            result = JsonConvert.SerializeObject(new TextMetaItem(json));
                            break;
                        case "textMetaNews":
                            result = JsonConvert.SerializeObject(new TextMetaNews(json));
                            break;
                        case "textMetaDesc":
                            result = JsonConvert.SerializeObject(new TextMetaDesc(json));
                            break;
                        case "textMetaVisi":
                            result = JsonConvert.SerializeObject(new TextMetaVisi(json));
                            break;
                        case "event":
                            result = JsonConvert.SerializeObject(new Event(json));
                            break;
                        case "news":
                            result = JsonConvert.SerializeObject(new News(json));
                            break;
                        case "link":
                            result = JsonConvert.SerializeObject(new Link(json));
                            break;
                        case "video":
                            result = JsonConvert.SerializeObject(new Video(json));
                            break;
                        default :
                            throw new SystemException("validation error : Type not available");
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        #endregion
    }

    public class FacebookLoginModel
    {
        public string uid { get; set; }
        public string accessToken { get; set; }
    }

    public class FacebookLocation
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Picture
    {
        public PicureData data { get; set; }
    }

    public class PicureData
    {
        public string url { get; set; }
        public bool is_silhouette { get; set; }
    }

    public class FacebookUserModel
        {
            public string id { get; set; }
            public string email { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string gender { get; set; }
            public string locale { get; set; }
            public string link { get; set; }
            public string user_birthday { get; set; }
            public int timezone { get; set; }        
            public FacebookLocation location { get; set; }
            public Picture picture { get; set; }
        }
    public class TwitterUserModel
    {
        public long id { get; set; }
        public string id_str { get; set; }
        public string name { get; set; }
        public string screen_name { get; set; }
        public string location { get; set; }
        public string description { get; set; }
        public string profile_background_color { get; set; }
        public string profile_background_image_url { get; set; }
        public string profile_background_image_url_https { get; set; }
        public string profile_image_url { get; set; }
        public string profile_image_url_https { get; set; }
        public string email { get; set; }
    }
    public class pathToken
    {
        public string access_token { get; set; }
        public string user_id { get; set; }
    }
}

    
