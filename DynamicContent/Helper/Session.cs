﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using log4net;

namespace DynamicContent.Helper
{
    public class _sessionFront : Controller 
    {
        //
        // GET: /Session/

        public static string firstName
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["firstName"] != null)
                    return System.Web.HttpContext.Current.Session["firstName"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["firstName"] = value; }
        }

        public static string lastName
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["lastName"] != null)
                    return System.Web.HttpContext.Current.Session["lastName"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["lastName"] = value; }
        }

        public static int userIdLogin
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["userIdLogin"] != null)
                    return Convert.ToInt32(System.Web.HttpContext.Current.Session["userIdLogin"]);
                else return 0;
            }
            set { System.Web.HttpContext.Current.Session["userIdLogin"] = value; }
        }
        public static string sessionId
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["sessionId"] != null)
                    return System.Web.HttpContext.Current.Session["sessionId"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["sessionId"] = value; }
        }
    }
}
