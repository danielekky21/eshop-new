﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using DynamicContent.Models;


namespace DynamicContent.Areas.TenantManager.Models
{
    [Table("M_User_Tenant")]
    public class UserTenant
    {
        [Key]
        public string username { get; set; }
        public string password { get; set; }
        public string role_id { get; set; }
        public string created_by { get; set; }
        public DateTime created_date { get; set; }
        public string modified_by { get; set; }
        public DateTime modified_date { get; set; }
        public int pass_counter { get; set; }
        public DateTime? change_pass_date { get; set; }
        public string email { get; set; }
        public int tenant { get; set; }
        public int brand { get; set; }
        public string session_id { get; set; }
    }

    public class UserTenantModel
    {
        private readonly DynamicContent.Models.Entities db;
        public UserTenantModel()
        {
            db = new DynamicContent.Models.Entities();
        }
        public M_User_Tenant getUserByUsername(string username)
        {
            var rs = from ut in db.M_User_Tenant
                     where ut.username.Equals(username)
                     select ut;
            return rs.FirstOrDefault();
        }

        public void updateUserTenantSessionId(M_User_Tenant ut, string sessionId)
        {
            ut.session_id = sessionId;
            db.SaveChanges();
        }
    }
}