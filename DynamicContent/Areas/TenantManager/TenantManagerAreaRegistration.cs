﻿using System.Web.Mvc;

namespace DynamicContent.Areas.TenantManager
{
    public class TenantManagerAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TenantManager";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TenantManagerDefault",
                "TenantManager/{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            
        }
    }
}
