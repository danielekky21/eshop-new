﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DynamicContent.Areas.TenantManager.ViewModels;
using DynamicContent.Areas.TenantManager.Helper;
using System.IO;
using DynamicContent.Helper;
using System.Configuration;
using System.Text.RegularExpressions;
using DynamicContent.Models;

namespace DynamicContent.Areas.TenantManager.Controllers
{
    public class ProductController : AuthenticatedController
    {
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        public ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult AddProduct()
        {
            ViewData["TenantImageSize"] = ConfigurationManager.AppSettings["TenantImageSize"];
            ViewData["TenantFileAllowed"] = ConfigurationManager.AppSettings["TenantFileAllowed"];
            ViewData["TenantFileAllowedMsg"] = ConfigurationManager.AppSettings["TenantFileAllowedMsg"];
            var tenantId = Convert.ToInt32(Session_.TenantId);
            ProductViewModel model = new ProductViewModel();
            model.tenantId = tenantId;
            var tenant = obj.GetTenant(Convert.ToInt32(Session_.TenantId)).FirstOrDefault();

            var tenantCategory = (from a in db.tbl_TenantCategory
                                  where (db.map_TenantCategory.Where(x => x.TenantID == tenantId).Select(x => x.CategoryID).Contains(a.ID))
                                  select a);
            
            var productType = (from ty in db.TypeMasters
                               join b in db.tbl_TenantCategory on ty.TypeCategory equals b.ID
                               join c in db.tbl_submenu on ty.TypeSubCategory equals c.ID
                               select ty);

            var sub = (from a in db.tbl_submenu
                       where (db.map_TenantCategory.Where(x => x.TenantID == tenantId).Select(x => x.SubCategoryID).Contains(a.ID))
                       select a);
            //string subid;
            //if(sub != null)
            //{
            //    subid = sub.FirstOrDefault().ID.ToString();
            //}
            //else
            //{
            //    subid = "NULL";
            //}

            //List<TypeMaster> tm = new List<TypeMaster>();
            //var cat = tenant.tenant_category.Split(',');
            //var subcat = tenant.tenant_subcategory.Split(',');
            //var list3 = subcat.Except(cat).ToList();
            //foreach (var item in cat)
            //{
            //   long items = long.Parse(item);
            //   var category = db.TypeMasters.Where(x => x.TypeCategory == items).ToList();
            //   foreach (var cate in category)
            //   {
            //       tm.Add(cate);
            //   }    
            //}
            //foreach (var item in list3)
            //{
                
            //    long items = long.Parse(item);
            //    var subcate = db.TypeMasters.Where(x => x.TypeSubCategory == items).ToList();
            //    foreach (var subc in subcate)
            //    {
            //        tm.Add(subc);
            //    } 
            //}
            var type = db.TypeMasters.Where(x => x.TypeCategory == tenantCategory.FirstOrDefault().ID && x.TypeSubCategory == sub.FirstOrDefault().ID);
           
            //foreach (var item in tenantCategory)
            //{
               
            //}

            ViewData["ProductCategory"] = tenantCategory.Select(x => new SelectListItem { Text = x.name, Value = x.ID.ToString() });
            ViewData["ProductSubcategory"] = sub.Where(x=>x.parent == tenantCategory.FirstOrDefault().code).Select(x => new SelectListItem { Text = x.name, Value = x.ID.ToString() });
            ViewData["ProductType"] = type.Where(x => x.isActive == "Y").Select(x => new SelectListItem { Text = x.TypeDesc, Value = x.ID.ToString() });
           
            var priceSelect = new List<SelectListItem>();
            priceSelect.Add(new SelectListItem() { Value = "price upon request", Text = "Price upon Request", Selected=true });
            priceSelect.Add(new SelectListItem() { Value = "input", Text = "Input Price", Selected = true });
            ViewData["PriceSelection"] = priceSelect;

            var adultSz = db.SizeMasters.Where(x => x.SizeCategory == "1").OrderBy(x=>x.SizeIdx);
            var adultClothes = adultSz.Where(x => x.SizeGroup == "1").Select(x=>x.SizeDesc).ToArray();
            var adultShoes = adultSz.Where(x => x.SizeGroup == "2").Select(x => x.SizeDesc).ToArray();
            var adultSizes = new List<string>();
            
            adultSizes.Add(String.Join(",",adultClothes));
            adultSizes.Add(String.Join(",", adultShoes));
            ViewData["adultSizes"] = adultSizes;


            var kidSz = db.SizeMasters.Where(x => x.SizeCategory == "2").OrderBy(x=>x.SizeIdx);
            var kidClothes = kidSz.Where(x => x.SizeGroup == "1").Select(x => x.SizeDesc).ToArray();
            var kidShoes = kidSz.Where(x => x.SizeGroup == "2").Select(x => x.SizeDesc).ToArray();
            var kidSize = new List<string>();
            kidSize.Add(String.Join(",", kidClothes));
            kidSize.Add(String.Join(",", kidShoes));
            ViewData["kidSize"] = kidSize;

            var statusSelect = new List<SelectListItem>();
            statusSelect.Add(new SelectListItem() { Value = "true", Text = "Active", Selected = true });
            statusSelect.Add(new SelectListItem() { Value = "false", Text = "Inactive", Selected = true });
            ViewData["StatusSelection"] = statusSelect;

            return View(model);
        }
        
        [ValidateInput(false)]
        [HttpPost]
        public virtual ActionResult AddProduct(ProductViewModel product,string submitBtn = "AddProduct")
        {
            ViewData["TenantImageSize"] = ConfigurationManager.AppSettings["TenantImageSize"];
            ViewData["TenantFileAllowed"] = ConfigurationManager.AppSettings["TenantFileAllowed"];
            ViewData["TenantFileAllowedMsg"] = ConfigurationManager.AppSettings["TenantFileAllowedMsg"];
            product.tenantId = Convert.ToInt32(Session_.TenantId);
            var tenant = obj.GetTenant(Convert.ToInt32(Session_.TenantId)).FirstOrDefault();
            var tc = obj.GetTenantCategory(tenant.tenant_category);

            var tenantId = Convert.ToInt32(Session_.TenantId);
            var tenantCategory = (from a in db.tbl_TenantCategory
                                  where (db.map_TenantCategory.Where(x => x.TenantID == tenantId).Select(x => x.CategoryID).Contains(a.ID))
                                  select a);

            var productType = (from ty in db.TypeMasters
                               join b in db.tbl_TenantCategory on ty.TypeCategory equals b.ID
                               join c in db.tbl_submenu on ty.TypeSubCategory equals c.ID
                               select ty);

            var sub = (from a in db.tbl_submenu
                       where (db.map_TenantCategory.Where(x => x.TenantID == tenantId).Select(x => x.SubCategoryID).Contains(a.ID))
                       select a);


            ViewData["ProductCategory"] = tenantCategory.Select(x => new SelectListItem { Text = x.name, Value = x.ID.ToString() });
            ViewData["ProductSubcategory"] = sub.Where(x => x.parent == tenantCategory.FirstOrDefault().code).Select(x => new SelectListItem { Text = x.name, Value = x.ID.ToString() });
            ViewData["ProductType"] = db.TypeMasters.Where(x => x.TypeCategory == tenantCategory.FirstOrDefault().ID && x.TypeSubCategory == sub.FirstOrDefault().ID && x.isActive == "Y").Select(x => new SelectListItem { Text = x.TypeValue, Value = x.ID.ToString() });

            var priceSelect = new List<SelectListItem>();
            priceSelect.Add(new SelectListItem() { Value = "price upon request", Text = "Price upon Request", Selected = true });
            priceSelect.Add(new SelectListItem() { Value = "input", Text = "Input Price", Selected = true });
            ViewData["PriceSelection"] = priceSelect;

            var adultSz = db.SizeMasters.Where(x => x.SizeCategory == "1").OrderBy(x=>x.SizeIdx);
            var adultClothes = adultSz.Where(x => x.SizeGroup == "1").Select(x => x.SizeDesc).ToArray();
            var adultShoes = adultSz.Where(x => x.SizeGroup == "2").Select(x => x.SizeDesc).ToArray();
            var adultSizes = new List<string>();

            adultSizes.Add(String.Join(",", adultClothes));
            adultSizes.Add(String.Join(",", adultShoes));
            ViewData["adultSizes"] = adultSizes;


            var kidSz = db.SizeMasters.Where(x => x.SizeCategory == "2").OrderBy(x=>x.SizeIdx);
            var kidClothes = kidSz.Where(x => x.SizeGroup == "1").Select(x => x.SizeDesc).ToArray();
            var kidShoes = kidSz.Where(x => x.SizeGroup == "2").Select(x => x.SizeDesc).ToArray();
            var kidSize = new List<string>();
            kidSize.Add(String.Join(",", kidClothes));
            kidSize.Add(String.Join(",", kidShoes));
            ViewData["kidSize"] = kidSize;

            var statusSelect = new List<SelectListItem>();
            statusSelect.Add(new SelectListItem() { Value = "true", Text = "Active", Selected = true });
            statusSelect.Add(new SelectListItem() { Value = "false", Text = "Inactive", Selected = true });
            ViewData["StatusSelection"] = statusSelect;

            //if (product.variant != null)
            //{
            //    foreach (var p in product.variant)
            //    {
            //        if (p.images != null)
            //        {
            //            foreach (var img in p.images)
            //            {
            //                if (img == null)
            //                {
            //                    ModelState.AddModelError("Invalid Input", "Color " + p.color + " has one or more images without file");
            //                }
            //            }
            //        }

            //    }
            //}


            if (!ModelState.IsValid)
            {
                return View(product);
            }

           

            DynamicContent.Models.tbl_product_tmp pd = new DynamicContent.Models.tbl_product_tmp();
            pd.tenant = product.tenantId;
            pd.tenant_category = product.productCategory;
            pd.tenant_subcategory = product.productSubcategory;
            pd.category = product.productType;
            pd.gender = product.productGender;
            pd.name = product.productName;
            pd.price = (product.productPriceSelection == "price upon request") ? product.productPriceSelection : product.productPrice;
            pd.size = String.Join(",",product.availableSizes.ToArray());
            pd.description = product.description;
            pd.size_fit = product.sizeFit;
            pd.status = product.status;
            pd.created_by = Session_.Username;
            pd.created_date = DateTime.Now;
            pd.modified_by = Session_.Username;
            pd.modified_date = DateTime.Now;

            if(submitBtn == "AddProduct")
            {
                pd.approval_status = (int)Constant.approvalStatus.draft;
            }
            else
            {
                pd.approval_status = (int)Constant.approvalStatus.waiting;
            }
            db.tbl_product_tmp.Add(pd);

            db.SaveChanges();
            if (product.variant.Count > 0)
            {
                foreach(var p in product.variant)
                {
                    DynamicContent.Models.tbl_product_color_tmp pc = new DynamicContent.Models.tbl_product_color_tmp();
                    pc.product = pd.ID;
                    pc.color = (String.IsNullOrEmpty(p.color) ? "" : p.color);
                    db.tbl_product_color_tmp.Add(pc);
                    db.SaveChanges();
                    foreach (var img in p.images)
                    {
                        if (img == null) continue;
                        
                        var filename = Regex.Replace(pd.name.Replace(' ', '_'), "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled) + Guid.NewGuid() + Path.GetExtension(img.FileName);
                        var pth = Path.Combine(Server.MapPath("~/Public/"), filename);
                        img.SaveAs(pth);
                        DynamicContent.Models.tbl_product_image_tmp pi = new DynamicContent.Models.tbl_product_image_tmp();
                        pi.images = "/Public/"+HttpUtility.UrlEncode(filename);
                        pi.product= pd.ID;
                        pi.color = pc.ID;
                        db.tbl_product_image_tmp.Add(pi);

                        db.SaveChanges();
                    }

                }
            }
            return RedirectToAction("Index", "Product", new { area = "TenantManager" });
        }
        
        public virtual ActionResult EditProduct(int id)
        {
            ViewData["TenantImageSize"] = ConfigurationManager.AppSettings["TenantImageSize"];
            ViewData["TenantFileAllowed"] = ConfigurationManager.AppSettings["TenantFileAllowed"];
            ViewData["TenantFileAllowedMsg"] = ConfigurationManager.AppSettings["TenantFileAllowedMsg"];
            var prds = (from p in db.tbl_product_tmp
                        where p.ID == id
                        select p);

            var product = ((prds != null && prds.Count() > 0) ? prds.FirstOrDefault() : null);
            product.approval_status = (int)Constant.approvalStatus.draft;
            product.approved = false;
            product.approved_date = DateTime.Now;
            db.SaveChanges();
            ViewData["Product"] = product;

            var productVM = new ProductViewModel();
            if (product != null)
            {
                productVM.productCategory = product.tenant_category;
                productVM.productSubcategory = product.tenant_subcategory;
                productVM.productGender = product.gender;
                productVM.productName = product.name;
                productVM.productBrand = product.brand;
                productVM.productPrice = product.price;
                productVM.productPriceSelection = (product.price == "price upon request") ? "price upon request" : "input";
                productVM.productType = product.category;
                productVM.description = product.description;
                productVM.sizeFit = product.size_fit;
                productVM.status = product.status;
                productVM.availableSizes = (!String.IsNullOrEmpty(product.size)) ? product.size.Split(',').ToList() : null;
                productVM.approvalStatus = product.approval_status;
            }

            var pv = obj.GetProductColorTmp(id).ToList();
            productVM.variant = new List<Variant>();

            if (pv != null)
            {
                for(int i = 0; i < pv.Count(); i++)
                {
                    var vari = new Variant() { id = pv[i].ID, color = pv[i].color };
                    productVM.variant.Add(vari);
                }
            }


            var tenant = obj.GetTenant(Convert.ToInt32(Session_.TenantId)).FirstOrDefault();
            var tc = obj.GetTenantCategory(tenant.tenant_category);
            var tenantId = Convert.ToInt32(Session_.TenantId);
            var tenantCategory = (from a in db.tbl_TenantCategory
                                  where (db.map_TenantCategory.Where(x => x.TenantID == tenantId).Select(x => x.CategoryID).Contains(a.ID))
                                  select a);

             var productType = (from ty in db.TypeMasters
                               join b in db.tbl_TenantCategory on ty.TypeCategory equals b.ID
                               join c in db.tbl_submenu on ty.TypeSubCategory equals c.ID
                               select ty);

            var sub = (from a in db.tbl_submenu
                       where (db.map_TenantCategory.Where(x => x.TenantID == tenantId).Select(x => x.SubCategoryID).Contains(a.ID))
                       select a);


            ViewData["ProductCategory"] = tenantCategory.Select(x => new SelectListItem { Text = x.name, Value = x.ID.ToString(), Selected = (x.ID == product.tenant_category) });
            ViewData["ProductSubcategory"] = sub.Where(x => x.parent == tenantCategory.Where(y=>y.ID == product.tenant_category).FirstOrDefault().code).Select(x => new SelectListItem { Text = x.name, Value = x.ID.ToString(), Selected = (x.ID == product.tenant_subcategory) });
            //ViewData["ProductType"] = db.TypeMasters.Where(x => x.TypeCategory == product.tenant_category && x.TypeSubCategory ==product.tenant_subcategory  && x.isActive == "Y").Select(x => new SelectListItem { Text = x.TypeValue, Value = x.ID.ToString(), Selected = (x.ID == product.category) });
            ViewData["ProductType"] = db.TypeMasters.Where(x => x.TypeCategory == product.tenant_category && x.TypeSubCategory ==product.tenant_subcategory  && x.isActive == "Y").Select(x => new SelectListItem { Text = x.TypeDesc, Value = x.ID.ToString(), Selected = (x.ID == product.category) });

            var priceSelect = new List<SelectListItem>();
            priceSelect.Add(new SelectListItem() { Value = "price upon request", Text = "Price upon Request", Selected = true });
            priceSelect.Add(new SelectListItem() { Value = "input", Text = "Input Price", Selected = true });
            ViewData["PriceSelection"] = priceSelect;

            var adultSz = db.SizeMasters.Where(x => x.SizeCategory == "1").OrderBy(x=>x.SizeIdx);
            var adultClothes = adultSz.Where(x => x.SizeGroup == "1").Select(x => x.SizeDesc).ToArray();
            var adultShoes = adultSz.Where(x => x.SizeGroup == "2").Select(x => x.SizeDesc).ToArray();
            
            if(adultSz != null && adultSz.Count() >0)
            {
                var adultSizes = new List<string>();
                adultSizes.Add(String.Join(",", adultClothes));
                adultSizes.Add(String.Join(",", adultShoes));
                ViewData["adultSizes"] = adultSizes;
            }
            


            var kidSz = db.SizeMasters.Where(x => x.SizeCategory == "2").OrderBy(x=>x.SizeIdx);
            var kidClothes = kidSz.Where(x => x.SizeGroup == "1").Select(x => x.SizeDesc).ToArray();
            var kidShoes = kidSz.Where(x => x.SizeGroup == "2").Select(x => x.SizeDesc).ToArray();
            if(kidSz != null && kidSz.Count() > 0)
            {
                var kidSize = new List<string>();
                kidSize.Add(String.Join(",", kidClothes));
                kidSize.Add(String.Join(",", kidShoes));
                ViewData["kidSize"] = kidSize;
            }

            var statusSelect = new List<SelectListItem>();
            statusSelect.Add(new SelectListItem() { Value = "true", Text = "Active", Selected = true });
            statusSelect.Add(new SelectListItem() { Value = "false", Text = "Inactive", Selected = true });
            ViewData["StatusSelection"] = statusSelect;

            return View("~/Areas/TenantManager/Views/Product/AddProduct.cshtml", productVM);
        }

        [ValidateInput(false)]
        [HttpPost]
        public virtual ActionResult EditProduct(ProductViewModel vm, string submitBtn = "AddProduct")
        {
            ViewData["TenantImageSize"] = ConfigurationManager.AppSettings["TenantImageSize"];
            ViewData["TenantFileAllowed"] = ConfigurationManager.AppSettings["TenantFileAllowed"];
            ViewData["TenantFileAllowedMsg"] = ConfigurationManager.AppSettings["TenantFileAllowedMsg"];
            var tenant = obj.GetTenant(Convert.ToInt32(Session_.TenantId)).FirstOrDefault();
            var prds = from p in db.tbl_product_tmp
                       where p.ID == vm.ID
                       select p;
            var product = ((prds != null && prds.Count() > 0) ? prds.FirstOrDefault() : null);

            var tc = obj.GetTenantCategory(tenant.tenant_category);
            var tenantId = Convert.ToInt32(Session_.TenantId);
            var tenantCategory = (from a in db.tbl_TenantCategory
                                  where (db.map_TenantCategory.Where(x => x.TenantID == tenantId).Select(x => x.CategoryID).Contains(a.ID))
                                  select a);

            var productType = (from ty in db.TypeMasters
                               join b in db.tbl_TenantCategory on ty.TypeCategory equals b.ID
                               join c in db.tbl_submenu on ty.TypeSubCategory equals c.ID
                               select ty);

            var sub = (from a in db.tbl_submenu
                       where (db.map_TenantCategory.Where(x => x.TenantID == tenantId).Select(x => x.SubCategoryID).Contains(a.ID))
                       select a);
            var type = db.TypeMasters.Where(x => x.TypeCategory == tenantCategory.FirstOrDefault().ID && x.TypeSubCategory == sub.FirstOrDefault().ID);

            ViewData["ProductCategory"] = tenantCategory.Select(x => new SelectListItem { Text = x.name, Value = x.ID.ToString(), Selected = (x.ID == product.tenant_category) });
            ViewData["ProductSubcategory"] = sub.Where(x => x.parent == tenantCategory.Where(y => y.ID == product.tenant_category).FirstOrDefault().code).Select(x => new SelectListItem { Text = x.name, Value = x.ID.ToString(), Selected = (x.ID == product.tenant_subcategory) });
            ViewData["ProductType"] = db.TypeMasters.Where(x => x.TypeCategory == product.tenant_category && x.TypeSubCategory == product.tenant_subcategory && x.isActive == "Y").Select(x => new SelectListItem { Text = x.TypeValue, Value = x.ID.ToString(), Selected = (x.ID == product.category) });

            var priceSelect = new List<SelectListItem>();
            priceSelect.Add(new SelectListItem() { Value = "price upon request", Text = "Price upon Request", Selected = true });
            priceSelect.Add(new SelectListItem() { Value = "input", Text = "Input Price", Selected = true });
            ViewData["PriceSelection"] = priceSelect;

            var adultSz = db.SizeMasters.Where(x => x.SizeCategory == "1").OrderBy(x=>x.SizeIdx);
            var adultClothes = adultSz.Where(x => x.SizeGroup == "1").Select(x => x.SizeDesc).ToArray();
            var adultShoes = adultSz.Where(x => x.SizeGroup == "2").Select(x => x.SizeDesc).ToArray();
            var adultSizes = new List<string>();

            adultSizes.Add(String.Join(",", adultClothes));
            adultSizes.Add(String.Join(",", adultShoes));
            ViewData["adultSizes"] = adultSizes;


            var kidSz = db.SizeMasters.Where(x => x.SizeCategory == "2").OrderBy(x=>x.SizeIdx);
            var kidClothes = kidSz.Where(x => x.SizeGroup == "1").Select(x => x.SizeDesc).ToArray();
            var kidShoes = kidSz.Where(x => x.SizeGroup == "2").Select(x => x.SizeDesc).ToArray();
            var kidSize = new List<string>();
            kidSize.Add(String.Join(",", kidClothes));
            kidSize.Add(String.Join(",", kidShoes));
            ViewData["kidSize"] = kidSize;

            var statusSelect = new List<SelectListItem>();
            statusSelect.Add(new SelectListItem() { Value = "true", Text = "Active", Selected = true });
            statusSelect.Add(new SelectListItem() { Value = "false", Text = "Inactive", Selected = true });
            ViewData["StatusSelection"] = statusSelect;

            //if (vm.variant != null)
            //{
            //    foreach (var p in vm.variant)
            //    {
            //        if(p.images != null)
            //        {
            //            foreach (var img in p.images)
            //            {
            //                if (img == null)
            //                {
            //                    ModelState.AddModelError("Invalid Input", "Color " + p.color + " has one or more images without file");

            //                }
            //            }
            //        }
            //    }
            //}

            if (!ModelState.IsValid)
            {
                return View("~/Areas/TenantManager/Views/Product/AddProduct.cshtml", vm);
            }

            /* EDIT Product */
            product.tenant_category = vm.productCategory;
            product.tenant_subcategory = vm.productSubcategory;
            product.category = vm.productType;
            product.gender = vm.productGender;
            product.name = vm.productName;
            product.price = (vm.productPriceSelection.ToLower() == "input") ? vm.productPrice : vm.productPriceSelection;
            product.size = String.Join(",",vm.availableSizes);
            product.description = vm.description;
            product.size_fit = vm.sizeFit;
            product.status = vm.status;
            if (submitBtn == "AddProduct")
            {
                product.approval_status = (int)Constant.approvalStatus.draft;
            }
            else
            {
                product.approval_status = (int)Constant.approvalStatus.waiting;
            }

            /*EDIT COLOR*/
            if (vm.variant.Count > 0)
            {
                foreach (var v in vm.variant)
                {
                    var pc = (from pclr in db.tbl_product_color_tmp
                              where pclr.ID == v.id
                              select pclr).FirstOrDefault();
                    if(pc != null)
                    {
                        pc.color = (String.IsNullOrEmpty(v.color) || v.nocolor) ? "" :v.color;
                        var pi = (from pimg in db.tbl_product_image_tmp
                                    where pimg.color == pc.ID
                                    select pimg).ToList();
                        var images = (v.images != null) ? v.images.ToList() : null;
                        var inem = 0;
                        if (images != null)
                        {
                            for (inem = 0; inem < pi.Count(); inem++)
                            {
                                if (images[inem] != null)
                                {
                                    var filename = Regex.Replace(product.name.Replace(' ', '_'), "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled) + Guid.NewGuid() + Path.GetExtension(images[inem].FileName);
                                    var pth = Path.Combine(Server.MapPath("~/Public/"), filename);
                                    images[inem].SaveAs(pth);
                                    pi[inem].images = "/Public/" + HttpUtility.UrlEncode(filename);
                                    pi[inem].product = product.ID;
                                    pi[inem].color = pc.ID;
                                }
                            }



                            for (var tomat = inem; tomat < images.Count(); tomat++)
                            {
                                if(images[tomat] != null)
                                {
                                    var filename = Regex.Replace(product.name.Replace(' ', '_'), "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled) + Path.GetExtension(images[tomat].FileName);
                                    var pth = Path.Combine(Server.MapPath("~/Public/"), filename);
                                    images[tomat].SaveAs(pth);
                                    DynamicContent.Models.tbl_product_image_tmp pinem = new DynamicContent.Models.tbl_product_image_tmp();
                                    pinem.images = "/Public/" + HttpUtility.UrlEncode(filename);
                                    pinem.product = product.ID;
                                    pinem.color = pc.ID;
                                    db.tbl_product_image_tmp.Add(pinem);
                                }
                            }
                        }
                    }
                    else
                    {
                        var o = new DynamicContent.Models.tbl_product_color_tmp
                        {
                            color = (String.IsNullOrEmpty(v.color) || v.nocolor) ? "" : v.color,
                            product = product.ID
                        };
                        db.tbl_product_color_tmp.Add(o);
                        db.SaveChanges();
                        var images = (v.images != null) ? v.images.ToList() : null;
                        if (images != null)
                        {
                            for (var tomat = 0; tomat < images.Count(); tomat++)
                            {
                                if (images[tomat] == null) continue;
                                var filename = Regex.Replace(product.name.Replace(' ', '_'), "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled) + Guid.NewGuid() + Path.GetExtension(images[tomat].FileName);
                                var pth = Path.Combine(Server.MapPath("~/Public/"), filename);
                                images[tomat].SaveAs(pth);
                                DynamicContent.Models.tbl_product_image_tmp pinem = new DynamicContent.Models.tbl_product_image_tmp();
                                pinem.images = "/Public/" + HttpUtility.UrlEncode(filename);
                                pinem.product = product.ID;
                                pinem.color = o.ID;
                                db.tbl_product_image_tmp.Add(pinem);
                            }
                        }
                    }
                    
                }
            }

            var prdLive = (from pro in db.tbl_product
                           where pro.ID == product.ID
                           select pro).FirstOrDefault();
            if(prdLive != null && !vm.status)
            {
                prdLive.status = product.status;
            }

            db.SaveChanges();

            return RedirectToAction("Index", "Product", new { area = "TenantManager" });
        }

        public virtual ActionResult SubmitApp(int id)
        {
            var prd = (from p in db.tbl_product_tmp
                       where p.ID == id
                       select p).First();

            prd.approval_status = (int)Constant.approvalStatus.waiting;
            prd.modified_date = DateTime.Now;

            db.SaveChanges();
            return RedirectToAction("Index", "Product", new { area = "TenantManager" });
        }

        public virtual ActionResult DeleteProduct(int id)
        {
            var product = new DynamicContent.Models.tbl_product_tmp() { ID = id };

            var pimg = (from pi in db.tbl_product_image_tmp
                        join pcr in db.tbl_product_color on pi.color equals pcr.ID
                        where pcr.product == id
                        select pi);

            db.tbl_product_image_tmp.RemoveRange(pimg);

            db.tbl_product_color_tmp.RemoveRange(db.tbl_product_color_tmp.Where(x => x.product == id));

            db.tbl_product_tmp.Attach(product);
            db.tbl_product_tmp.Remove(product);

            db.SaveChanges();


            return RedirectToAction("Index", "Product", new { area = "TenantManager" });
        }

        public JsonResult ProductListDataHandler(DTParameters dtParam)
        {
            try
            {
                var products = obj.GetAllProductTmpByTenant2(Convert.ToInt32(Session_.TenantId));
                //var products = obj.GetAllProductTmpByTenant(Convert.ToInt32(Session_.TenantId));
                List<String> columnSearch = new List<string>();

                foreach (var col in dtParam.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                var data = new ResultSet().GetResult(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, products.ToList(), columnSearch);
                int count = new ResultSet().Count(dtParam.Search.Value, products.ToList(), columnSearch);
                var result = new DTResult<DynamicContent.Helper._object.AllProduct>
                {
                    draw = dtParam.Draw,
                    data = data,
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);
            }
            catch(Exception ex)
            {
                return Json(new { error = ex.Message } );
            }
        }

        [HttpGet]
        public JsonResult DeleteVariant(int id)
        {
            try
            {
                var product_images = db.tbl_product_image_tmp.Where(x => x.color == id);
                foreach(var img in product_images)
                {
                    System.IO.File.Delete(Server.MapPath("~" + img.images));
                }
                db.tbl_product_image_tmp.RemoveRange(product_images);
                db.tbl_product_color_tmp.RemoveRange(db.tbl_product_color_tmp.Where(x => x.ID == id));
                db.SaveChanges();
                return Json(new { success= true, message="Color Deleted"}, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            
        }

        [HttpGet]
        public JsonResult DeleteImage(int id)
        {
            try
            {
                var product_images = db.tbl_product_image_tmp.Where(x => x.ID == id);
                foreach (var img in product_images)
                {
                    System.IO.File.Delete(Server.MapPath("~" + img.images));
                }
                db.tbl_product_image_tmp.RemoveRange(product_images);
                db.SaveChanges();
                return Json(new { success = true, message = "Image Deleted" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public JsonResult GetSubcategory(string parent)
        {
            var tenantId = Convert.ToInt32(Session_.TenantId);
            if (String.IsNullOrEmpty(parent)) return null;
            var sub = (from a in db.tbl_submenu
                       where (db.map_TenantCategory.Where(x => x.TenantID == tenantId).Select(x => x.SubCategoryID).Contains(a.ID))
                       select a);
            var i = obj.GetTenantCategory(parent).FirstOrDefault();
            var item = sub.Where(x => x.parent ==  i.code) ;
            return Json(item, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetType(string cat, string sub)
        {
            var tenantId = Convert.ToInt32(Session_.TenantId);
            if (String.IsNullOrEmpty(cat)) return null;
            var catId = Convert.ToInt32(cat);
            if (!String.IsNullOrEmpty(sub))
            {
                var subId = Convert.ToInt32(sub);
                var type = db.TypeMasters.Where(x => x.TypeCategory == catId && x.TypeSubCategory == subId && x.isActive == "Y");
                return Json(type, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var type = db.TypeMasters.Where(x => x.TypeCategory == catId && x.isActive == "Y");
                return Json(type, JsonRequestBehavior.AllowGet);
            }
            
        }
        public virtual ActionResult AddTenantBanner()
        {
            var tenantId = Convert.ToInt32(Session_.TenantId);
            var currentBanner = db.Banner_tenant.FirstOrDefault(x => x.TenantID == tenantId.ToString());
            if (currentBanner != null)
            {
                ViewData.Model = currentBanner;
            }
            else
            {
                ViewData.Model = new Banner_tenant();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddTenantBanner(HttpPostedFileBase imageBanner)
        {
            var tenantId = Convert.ToInt32(Session_.TenantId);
            var currentBanner = db.Banner_tenant.FirstOrDefault(x => x.TenantID == tenantId.ToString());
            if (imageBanner == null)
            {
                return View();
            }
            if (currentBanner != null)
            {
                //update
                var uploadPath = "/Public/";
                var filename = Guid.NewGuid() + Path.GetExtension(imageBanner.FileName);
                var pth = Path.Combine(Server.MapPath("~/Public/"), filename);
                imageBanner.SaveAs(pth);
                Banner_tenant img = new Banner_tenant();
                currentBanner.imageName = filename;
                currentBanner.IsActive = false;
                currentBanner.ImageTenantPath = uploadPath + filename;
                currentBanner.UpdatedDate = DateTime.Now;
                currentBanner.UpdatedBy = Session_.Username;
                db.SaveChanges();
            }
            else {
                var uploadPath = "/Public/";
                var filename =  Guid.NewGuid() + Path.GetExtension(imageBanner.FileName);
                var pth = Path.Combine(Server.MapPath("~/Public/"), filename);
                imageBanner.SaveAs(pth);
                Banner_tenant img = new Banner_tenant();
                img.IsActive = false;
                img.TenantID = tenantId.ToString();
                img.imageName = filename;
                img.ImageTenantPath = uploadPath + filename;
                img.CreatedDate = DateTime.Now;
                img.CreatedBy = Session_.Username;
                db.Banner_tenant.Add(img);
                db.SaveChanges();
            }
           
            return RedirectToAction("AddTenantBanner","Product", new { area = "TenantManager" });
        }
    }
}