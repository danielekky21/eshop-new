﻿using DynamicContent.Areas.TenantManager.Helper;
using DynamicContent.Areas.TenantManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace DynamicContent.Areas.TenantManager.Controllers
{
    [AuthorizeTenant]
    public class HomeController : AuthenticatedController
    {
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        public ActionResult Index()
        {

            var tenantId = Convert.ToInt32(Session_.TenantId);
            var reservations = (from rp in db.tbl_ReservationParent
                               join u in db.tbl_user_register on rp.CustID equals u.ID
                               where (from r in db.tbl_reservation
                                      join t in db.tbl_tenant on r.tenant equals t.ID
                                      where r.parentReservation == rp.ID && t.ID == tenantId
                                      select t.ID).Count() > 0 && rp.FollowUp != "Y"
                                orderby rp.ReservationDate descending
                               select new ReservationViewModel
                               {
                                   id = rp.ID,
                                   reservationCode = rp.ReservationCode,
                                   reservedDate = rp.ReservationDate,
                                   userEmail = u.email,
                                   userId = u.ID,
                                   userName = u.firstName + " " + u.lastName,
                                   userPhone = u.phone
                               }).Take(5).ToList();

            for(var i = 0; i <reservations.Count();i++)
            {
                var id = reservations[i].id;
                var products = (from rp in db.tbl_reservation
                                join p in db.tbl_product on rp.product equals p.ID
                                join pc in db.tbl_product_color on rp.color equals pc.ID
                                join t in db.tbl_tenant on p.tenant equals t.ID
                                where rp.parentReservation == id && rp.tenant == tenantId
                                select new productReserved
                                {
                                    productId = p.ID,
                                    productName = p.name,
                                    productPrice = p.price,
                                    quantity = rp.qty,
                                    size = rp.size,
                                    tenantId = t.ID,
                                    tenantName = t.name,
                                    totalPrice = rp.total_price,
                                    colorCode = pc.color,
                                    status = rp.status
                                });

                reservations[i].products = new List<productReserved>();
                reservations[i].products.AddRange(products.ToList());
            }

            var tenant = db.M_User_Tenant.Where(x => x.username == Session_.Username).FirstOrDefault();

            ViewData["tenantInfo"] = tenant;
            ViewData["reservation"] = reservations;
            return View();
        }
    }
}