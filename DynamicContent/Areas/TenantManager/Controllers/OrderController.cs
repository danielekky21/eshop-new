﻿using DynamicContent.Areas.TenantManager.Helper;
using DynamicContent.Areas.TenantManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace DynamicContent.Areas.TenantManager.Controllers
{
    public class OrderController : AuthenticatedController
    {
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();

        public ActionResult Index()
        {
            return View();
        }
        [ValidateInput(false)]
        [HttpPost]
        public virtual ActionResult EditFollowup(ReservationViewModel vm, string submitBtn = "AddProduct")
        {
            var res = (from p in db.tbl_ReservationParent
                       where p.ID == vm.id
                       select p).FirstOrDefault();
            res.FollowUp = "Y";
            res.FollowUpName = Session_.Username;
            res.FollowUpDate = DateTime.Now;
            res.FollowUpNote = vm.followUpNote;

            db.SaveChanges();

            return RedirectToAction("Index", "Home", new { area = "TenantManager" });
        }
        public ActionResult Followup(int id)
        {
            //var res = (from p in db.tbl_ReservationParent
            //           where p.ID == id
            //           select p).FirstOrDefault();
            //res.FollowUp = "Y";
            //res.FollowUpName = Session_.Username;
            //res.FollowUpDate = DateTime.Now;

            //db.SaveChanges();

            //return RedirectToAction("Index", "Home", new { area = "TenantManager" });
            var reservation = (from rp in db.tbl_ReservationParent
                               join u in db.tbl_user_register on rp.CustID equals u.ID
                               where rp.ID == id
                               select new ReservationViewModel
                               {
                                   id = rp.ID,
                                   reservationCode = rp.ReservationCode,
                                   reservedDate = rp.ReservationDate,
                                   userEmail = u.email,
                                   userAddress = u.address,
                                   userCity = u.city,
                                   userGender = u.gender,
                                   userPhone = u.phone,
                                   reserved = rp.ReservationStatus,
                                   userId = u.ID,
                                   userName = u.firstName + " " + u.lastName,
                                   followUp = rp.FollowUp,
                                   followUpName = rp.FollowUpName,
                                   followUpNote = rp.FollowUpNote,
                                   followUpDate = rp.FollowUpDate
                               }).First();

            var tenantId = Convert.ToInt32(Session_.TenantId);
            var products = (from rp in db.tbl_reservation
                            join p in db.tbl_product on rp.product equals p.ID
                            join pc in db.tbl_product_color on rp.color equals pc.ID
                            join t in db.tbl_tenant on p.tenant equals t.ID
                            where rp.parentReservation == id && rp.tenant == tenantId
                            select new productReserved
                            {
                                productId = p.ID,
                                productName = p.name,
                                productPrice = p.price,
                                quantity = rp.qty,
                                size = rp.size,
                                tenantId = t.ID,
                                tenantName = t.name,
                                totalPrice = rp.total_price,
                                colorCode = pc.color,
                                colorId = pc.ID,
                                status = rp.status
                            });
            var images = (from img in db.tbl_product_image
                          where img.color == products.First().colorId
                          select new productImage
                          {
                              imageId = img.ID,
                              imagePath = img.images
                          });
            reservation.products = new List<productReserved>();
            reservation.products.AddRange(products.ToList());

            return View(reservation);
        }

        public ActionResult View(int id)
        {
            var reservation = (from rp in db.tbl_ReservationParent
                               join u in db.tbl_user_register on rp.CustID equals u.ID
                               where rp.ID == id
                               select new ReservationViewModel
                               {
                                   id = rp.ID,
                                   reservationCode = rp.ReservationCode,
                                   reservedDate = rp.ReservationDate,
                                   userEmail = u.email,
                                   userAddress = u.address,
                                   userCity = u.city,
                                   userGender = u.gender,
                                   userPhone = u.phone,
                                   reserved = rp.ReservationStatus,
                                   userId = u.ID,
                                   userName = u.firstName + " " + u.lastName,
                                   followUp = rp.FollowUp,
                                   followUpName = rp.FollowUpName,
                                   followUpDate = rp.FollowUpDate,
                                   followUpNote = rp.FollowUpNote
                               }).First();

            var tenantId = Convert.ToInt32(Session_.TenantId);
            var products = (from rp in db.tbl_reservation
                           join p in db.tbl_product on rp.product equals p.ID
                           join pc in db.tbl_product_color on rp.color equals pc.ID
                           join t in db.tbl_tenant on p.tenant equals t.ID
                           where rp.parentReservation == id && rp.tenant == tenantId
                           select new productReserved
                           {
                               productId = p.ID,
                               productName = p.name,
                               productPrice = p.price,
                               quantity = rp.qty,
                               size = rp.size,
                               tenantId = t.ID,
                               tenantName = t.name,
                               totalPrice = rp.total_price,
                               colorCode = pc.color,
                               colorId = pc.ID,
                               status = rp.status
                           });
            var images = (from img in db.tbl_product_image
                         where img.color == products.First().colorId
                         select new productImage
                         {
                             imageId = img.ID,
                             imagePath = img.images
                         });
            reservation.products = new List<productReserved>();
            reservation.products.AddRange(products.ToList());

            return View(reservation);
        }

        public JsonResult OrderListDataHandler(DTParameters dtParam)
        {
            try
            {
                var tenantId = Convert.ToInt32(Session_.TenantId);
                var reservations = from rp in db.tbl_ReservationParent
                                   join u in db.tbl_user_register on rp.CustID equals u.ID
                                   where (from r in db.tbl_reservation
                                          join t in db.tbl_tenant on r.tenant equals t.ID
                                          where r.parentReservation == rp.ID && t.ID == tenantId
                                          select t.ID).Count() > 0
                                   select new ReservationViewModel
                                   {
                                       id = rp.ID,
                                       reservationCode = rp.ReservationCode,
                                       reservedDate = rp.ReservationDate,
                                       userEmail =u.email,
                                       userId = u.ID,
                                       userName = u.firstName + " " + u.lastName,
                                       userPhone = u.phone,
                                       followUpName = rp.FollowUpName
                                   };

                List <String> columnSearch = new List<string>();

                foreach (var col in dtParam.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                var data = new ResultSet().GetResultReservation(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, reservations.ToList(), columnSearch);
                int count = new ResultSet().Count(dtParam.Search.Value, reservations.ToList(), columnSearch);
                var result = new DTResult<ReservationViewModel>
                {
                    draw = dtParam.Draw,
                    data = data,
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }
    }
}