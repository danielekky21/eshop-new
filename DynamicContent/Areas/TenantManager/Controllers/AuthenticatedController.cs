﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DynamicContent.Areas.TenantManager.Helper;


namespace DynamicContent.Areas.TenantManager.Controllers
{
    [AuthorizeTenant]
    public class AuthenticatedController : Controller
    {
        public AuthenticatedController()
        {
            //Testing purpose only
            //Session_.TenantId = "1";
        }
    }
}