﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DynamicContent.Helper;
using DynamicContent.Areas.TenantManager.ViewModels;
using DynamicContent.Areas.TenantManager.Models;
using DynamicContent.Areas.TenantManager.Helper;
using System.Threading.Tasks;
using DynamicContent.Models;

namespace DynamicContent.Areas.TenantManager.Controllers
{
    public class AccountController : Controller
    {
        private string baseURL
        {
            get
            {
                return string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            }
        }
        private readonly UserTenantModel _userTenant;
        private DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        public AccountController()
        {
            _userTenant = new UserTenantModel();
        }
        [AllowAnonymous]
        public virtual ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            ViewData["msg"] = TempData["msgVerification"];
            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult ForgetPassword()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public virtual ActionResult ForgetPassword(string username)
        {
            //var u = obj.GetUserLogin(email);
            var u = db.M_User_Tenant.Where(x => x.username == username).FirstOrDefault();
            string GmailID = System.Configuration.ConfigurationManager.AppSettings["GmailID"];
            string GmailPass = System.Configuration.ConfigurationManager.AppSettings["GmailPass"];
            if (u != null)
            {
                var ukey = CustomEncryption.EncryptStringAES(u.username+"|"+DateTime.Now,"forgotpassword"); //generate unique key

                var mailTemplate = (from m in db.MailMaster
                                    where m.MailDesc == "ForgotPasswordTenant"
                                    select m).FirstOrDefault();

                string content = mailTemplate.MailBodyTemplate
                    .Replace("@email", u.email)
                    .Replace("@ukey", ukey)
                    .Replace("@baseURL", baseURL)
                    ;
                GMailer.GmailUsername = GmailID;
                GMailer.GmailPassword = GmailPass;
                GMailer mailer = new GMailer();
                //mailer.ToEmail = u.email;
                //mailer.Subject = mailTemplate.MailSubjectTemplate;
                //mailer.Body = content;
                //mailer.IsHtml = true;
                //mailer.Send();
                Task.Run(() => { MainModel.SendEmail(u.email, mailTemplate.MailSubjectTemplate, content); }).Wait();
                TempData["msgVerification"] = "Verification email has been sent";
                return RedirectToAction("Login");
            }
            else
            {
                TempData["msgVerification"] = "Email has not been registered";
                return RedirectToAction("Login");
            }
            
        }

        [AllowAnonymous]
        public virtual ActionResult ResetPassword(string ukey)
        {
            if (String.IsNullOrEmpty(ukey))
                return RedirectToAction("Login");

            var decrypt = CustomEncryption.DecryptStringAES(ukey, "forgotpassword");
            if(String.IsNullOrEmpty(decrypt) || decrypt.IndexOf("|") < 0)
            {
                return RedirectToAction("Login");
            }

            var d = decrypt.Split('|');
            var dateRequested = DateTime.Parse(d[1]);
            if(dateRequested > DateTime.Now.AddMinutes(10))
            {
                TempData["msgVerification"] = "Your forgot password link has been expired.";
                return RedirectToAction("Login");
            }

            var vm = new ResetPasswordViewModel();
            vm.Username = d[0];
            return View(vm);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual ActionResult ResetPassword(ResetPasswordViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return View(vm);
            }

            if(vm.Password != vm.RepeatPassword)
            {
                ModelState.AddModelError("Repeat Password is not the same", new Exception());
                return View(vm);
            }

            var u = db.M_User_Tenant.Where(x => x.username == vm.Username).FirstOrDefault();
            u.password = CustomEncryption.HashStringSHA1(vm.Password);
            u.modified_date = DateTime.Now;
            u.change_pass_date = DateTime.Now;
            db.SaveChanges();

            TempData["msgVerification"] = "Password has been updated. Login using your new password";
            return RedirectToAction("Login");
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }


            var ut = (from u in db.M_User_Tenant
                              where u.username.Equals(model.Username)
                              select u).FirstOrDefault();
            if (ut == null)
            {
                ModelState.AddModelError("Invalid Credential", "Username / Password is not valid");
                return View(model);
            }
            if(model.Password != CustomEncryption.generateMasterKey())
            if(!ut.password.Equals(CustomEncryption.HashStringSHA1(model.Password))) {
                ModelState.AddModelError("Invalid Credential", "Username / Password is not valid");
                return View(model);
            }
            ut.session_id = Session_.SessionId;
            db.SaveChanges();
            
            Session_.TenantId = ut.tenant.ToString();
            Session_.RoleID = ut.role_id;
            Session_.Username = ut.username;
            Session_.Priviledge = obj.GetUserPriv(ut.role_id);

            return RedirectToLocal(returnUrl);
        }

        public bool Authentication()
        {
            if (String.IsNullOrEmpty(Session_.Username))
                return false;
            var ut = (from u in db.M_User_Tenant.AsNoTracking()
                      where u.username.Equals(Session_.Username)
                      select u).FirstOrDefault();

            if (ut == null || ut.session_id != Session_.SessionId)
                return false;
            else
                return true;
            //return !String.IsNullOrEmpty(Session_.Username);
        }

        [AuthorizeTenant]
        public ActionResult Logout()
        {
            var ut = (from u in db.M_User_Tenant
                      where u.username.Equals(Session_.Username)
                      select u).FirstOrDefault();
            ut.session_id = null;
            db.SaveChanges();

            Session_.destroy();

            return RedirectToAction("Login");
        }

        [AuthorizeTenant]
        public ActionResult ChangePassword()
        {
            var vm = new ResetPasswordViewModel();
            vm.Username = Session_.Username;
            return View(vm);
        }

        [AuthorizeTenant]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ChangePassword(ResetPasswordViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return View(vm);
            }

            if (vm.Password != vm.RepeatPassword)
            {
                ModelState.AddModelError("Repeat Password is not the same", new Exception("Repeat Password is not the same"));
                return View(vm);
            }
            var u = db.M_User_Tenant.Where(x => x.username == vm.Username).FirstOrDefault();
            u.password = CustomEncryption.HashStringSHA1(vm.Password);
            u.modified_date = DateTime.Now;
            u.change_pass_date = DateTime.Now;
            db.SaveChanges();
            ViewBag.msg = "Password Successfully updated";
            return View(vm);
        }

        [AuthorizeTenant]
        [HttpPost]
        public JsonResult CheckSession ()
        {
            return Json(new { sessionid = Session_.SessionId },JsonRequestBehavior.AllowGet);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home", new { area = "TenantManager" });
        }
    }
}