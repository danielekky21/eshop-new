﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using OfficeOpenXml;
using System.Security.Cryptography.X509Certificates;
using System.Drawing;
using OfficeOpenXml.Style;
using OfficeOpenXml.Drawing.Chart;
using System.Web.Mvc;
using DynamicContent.Areas.TenantManager.ViewModels;
using DynamicContent.Areas.TenantManager.Helper;
using DynamicContent.Helper;
using System.Globalization;

namespace DynamicContent.Areas.TenantManager.Controllers
{
    public class ReportController : AuthenticatedController
    {
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MostLovedProducts()
        {
            //var w = obj.GetMostLovedProduct(Convert.ToInt32(Session_.TenantId));
            
            return View();
        }

        public JsonResult MostLovedProductListHandler(DTParameters dtParam)
        {

            try
            {
                var tenantId = Convert.ToInt32(Session_.TenantId);
                DateTime startDate = (String.IsNullOrEmpty(dtParam.startDate) ? DateTime.MinValue : DateTime.ParseExact(dtParam.startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture));
                DateTime endDate = (String.IsNullOrEmpty(dtParam.endDate) ? DateTime.MaxValue : (DateTime.ParseExact(dtParam.endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture)).AddHours(23).AddMinutes(59).AddSeconds(59));
                var pr = (from w in db.tbl_wishlist
                          join p in db.tbl_product on w.product equals p.ID
                          join t in db.tbl_tenant on p.tenant equals t.ID
                          where p.tenant == tenantId && ( startDate == DateTime.MinValue || w.created_date >= startDate) && (endDate == DateTime.MaxValue || w.created_date <= endDate)
                          select new
                          {
                              id = p.ID,
                              tenant = t.name,
                            wishlistID = w.ID,
                            created_date = p.created_date,
                            name = p.name,
                            status = p.status
                        } into x
                          group x by new { x.tenant,  x.name, x.status,x.created_date } into g
                          select new LovedProductViewModel
                          {
                              tenantName = g.Key.tenant,
                              productName = g.Key.name, 
                              loveCount = g.Count(),
                              status = g.Key.status,
                              createdDate = g.Key.created_date
                          }
                        );

                List<String> columnSearch = new List<string>();

                foreach (var col in dtParam.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                var data = new ResultSet().lovedProductGetResult(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, pr, columnSearch);
                int count = new ResultSet().lovedProductCount(dtParam.Search.Value, pr, columnSearch);
                var result = new DTResult<LovedProductViewModel>
                {
                    draw = dtParam.Draw,
                    data = data.ToList(),
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        public ActionResult MostReservedProducts()
        {
            return View();
        }

        public JsonResult MostReservedProductListHandler(DTParameters dtParam)
        {
            try
            {
                var tenantId = Convert.ToInt32(Session_.TenantId);
                DateTime startDate = (String.IsNullOrEmpty(dtParam.startDate) ? DateTime.MinValue : DateTime.ParseExact(dtParam.startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture));
                DateTime endDate = (String.IsNullOrEmpty(dtParam.endDate) ? DateTime.MaxValue : (DateTime.ParseExact(dtParam.endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture)).AddHours(23).AddMinutes(59).AddSeconds(59));
                var pr = (from r in db.tbl_reservation
                          join rp in db.tbl_ReservationParent on r.parentReservation equals rp.ID
                          join p in db.tbl_product on r.product equals p.ID
                          join t in db.tbl_tenant on p.tenant equals t.ID
                          where p.tenant == tenantId && (startDate == DateTime.MinValue || rp.ReservationDate >= startDate) && (endDate == DateTime.MaxValue || rp.ReservationDate <= endDate)
                          select new
                          {
                              id = r.product,
                              name = p.name,
                              createdDate = p.created_date,
                              tenant = t.name,
                              qty = r.qty,
                              status = p.status
                          } into x
                          group x by new { x.tenant, x.name, x.status, x.createdDate } into g
                          select new ReservedProductViewModel
                          {
                              tenantName = g.Key.tenant,
                              productName = g.Key.name,
                              quantityTotal = g.Sum(t => t.qty),
                              status = g.Key.status,
                              createdDate = g.Key.createdDate
                          }
                        );

                List<String> columnSearch = new List<string>();

                foreach (var col in dtParam.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                var data = new ResultSet().reservedProductGetResult(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, pr, columnSearch);
                int count = new ResultSet().reservedProductCount(dtParam.Search.Value, pr, columnSearch);
                var result = new DTResult<ReservedProductViewModel>
                {
                    draw = dtParam.Draw,
                    data = data.ToList(),
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        public JsonResult MostViewedProductListHandler(DTParameters dtParam)
        {
            try
            {
                var tenantId = Convert.ToInt32(Session_.TenantId);
                DateTime startDate = (String.IsNullOrEmpty(dtParam.startDate) ? DateTime.MinValue : DateTime.ParseExact(dtParam.startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture));
                DateTime endDate = (String.IsNullOrEmpty(dtParam.endDate) ? DateTime.MaxValue : (DateTime.ParseExact(dtParam.endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture)).AddHours(23).AddMinutes(59).AddSeconds(59));
                var pr = (from v in db.tbl_product_view
                          join p in db.tbl_product on v.product_id equals p.ID
                          join t in db.tbl_tenant on p.tenant equals t.ID
                          where p.tenant == tenantId && (startDate == DateTime.MinValue || v.created_date >= startDate) && (endDate == DateTime.MaxValue || v.created_date <= endDate)
                          select new
                          {
                              id = p.ID,
                              viewID = v.ID,
                              createdDate = p.created_date,
                              name = p.name,
                              tenant = t.name,
                              status = p.status
                          } into x
                          group x by new { x.tenant, x.name, x.status, x.createdDate } into g
                          select new ViewedProductViewModel
                          {
                              tenantName = g.Key.tenant,
                              productName = g.Key.name,
                              viewTotal = g.Count(),
                              status = g.Key.status,
                              createdDate = g.Key.createdDate
                          }
                        );

                List<String> columnSearch = new List<string>();

                foreach (var col in dtParam.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                var data = new ResultSet().viewedProductGetResult(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, pr, columnSearch);
                int count = new ResultSet().viewedProductCount(dtParam.Search.Value, pr, columnSearch);
                var result = new DTResult<ViewedProductViewModel>
                {
                    draw = dtParam.Draw,
                    data = data.ToList(),
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        public ActionResult MostViewedProducts()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ExportMostLoved()
        {
            var de = Request.Form["dateEnd"];
            var ds = Request.Form["dateStart"];
            var s = Request.Form["filter"];
            string status = "ALL STATUS";
            string rangedate = "ALL DATE";
            bool stt = true;
            DateTime dateStart;
            DateTime dateEnd;
            if (s != "")
            {
                stt = (s == "True") ? true : false;
                status = (s == "True") ? "ACTIVE" : "INACTIVE";
            }
            if (de != "" && ds != "")
            {
                rangedate = ds + " to " + de;
            }
            var fileName = "MostLovedProducts-"+DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") +".xlsx";
            var outputDir = "~/Public/Files/";
            //var file = new FileInfo(outputDir + fileName);
            var file = Path.Combine(Server.MapPath(outputDir), fileName);
            var logo = Path.Combine(Server.MapPath("~/Images/"), "little-logo.jpg");
            //Image piLogo = Image.FromFile(logo);
            using (var package = new ExcelPackage(new FileInfo(file)))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("MostLovedProducts-"+DateTime.Now.ToString("dd/MM/yyyy"));
                Image myImage = Image.FromFile(logo);
                var pic = worksheet.Drawings.AddPicture("Logo", myImage);
                pic.SetPosition(0, 0, 0, 0);
                worksheet.Cells["C2"].Value = "MOST LOVED PRODUCTS";
                worksheet.Cells["C2"].Style.Font.SetFromFont(new Font("Arial", 14));
                worksheet.Cells[5, 1].Value = "Status";
                worksheet.Cells[6, 1].Value = "Date";
                worksheet.Cells[5, 2].Value = status;
                worksheet.Cells[6, 2].Value = rangedate;
                worksheet.Cells[7, 1].Value = "No.";
                worksheet.Cells[7, 2].Value = "Brand";
                worksheet.Cells[7, 3].Value = "Product Name";
                worksheet.Cells[7, 4].Value = "Count";
                worksheet.Cells[7, 5].Value = "Date Added";
                worksheet.Cells[7, 6].Value = "Status";
                IEnumerable<_object.lovedProduct> w;
                w = obj.GetMostLovedProduct(Convert.ToInt32(Session_.TenantId));
                var i = 1;
                if (ds != "" && de != "" && s != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59).AddSeconds(59);
                    foreach (var item in w
                        .Where(p => (Convert.ToDateTime(p.created_date).Date >= dateStart.Date)
                        && (Convert.ToDateTime(p.created_date).Date <= dateEnd.Date) && p.status == stt
                        )
                        .GroupBy(p => p.id).Select(g => new {
                        prodID = g.Key,
                        count = g.Count(),
                        created_date = g.First().created_date,
                        prod_created_date = g.First().prod_created_date,
                        p_status = g.First().status
                    })
                    .OrderByDescending(y=>y.count))
                    {
                        var prod = obj.GetProductWishlist(item.prodID).FirstOrDefault();
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = prod.tenant.ToUpper();
                        worksheet.Cells[i + 7, 3].Value = prod.name.ToUpper();
                        worksheet.Cells[i + 7, 4].Value = item.count;
                        worksheet.Cells[i + 7, 5].Value = item.prod_created_date.ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 6].Value = (prod.status) ? "ACTIVE" : "INACTIVE";
                        i++;
                    }
                }
                else if (ds != "" && de != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59).AddSeconds(59);
                    foreach (var item in w
                        .Where(p => (Convert.ToDateTime(p.created_date).Date >= dateStart.Date)
                            && (Convert.ToDateTime(p.created_date).Date <= dateEnd.Date)
                            )
                        .GroupBy(p => p.id).Select(g => new {
                        prodID = g.Key,
                        count = g.Count(),
                        created_date = g.First().created_date,
                        prod_created_date = g.First().prod_created_date,
                        p_status = g.First().status
                    })
                    .OrderByDescending(y => y.count))
                    {
                        var prod = obj.GetProductWishlist(item.prodID).FirstOrDefault();
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = prod.tenant.ToUpper();
                        worksheet.Cells[i + 7, 3].Value = prod.name.ToUpper();
                        worksheet.Cells[i + 7, 4].Value = item.count;
                        worksheet.Cells[i + 7, 5].Value = item.prod_created_date.ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 6].Value = (prod.status) ? "ACTIVE" : "INACTIVE";
                        i++;
                    }
                }
                else
                {
                    if (s != "")
                    {
                        w = obj.GetMostLovedProduct(Convert.ToInt32(Session_.TenantId))
                        .Where(p => p.status == stt);
                    }
                    else
                    {
                        w = obj.GetMostLovedProduct(Convert.ToInt32(Session_.TenantId));
                    }
                    foreach (var item in w.GroupBy(p => p.id).Select(g => new {
                        prodID = g.Key,
                        count = g.Count(),
                        created_date = g.First().created_date,
                        prod_created_date = g.First().prod_created_date,
                        p_status = g.First().status
                    }).OrderByDescending(y => y.count))
                    {
                        var prod = obj.GetProductWishlist(item.prodID).FirstOrDefault();
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = prod.tenant.ToUpper();
                        worksheet.Cells[i + 7, 3].Value = prod.name.ToUpper();
                        worksheet.Cells[i + 7, 4].Value = item.count;
                        worksheet.Cells[i + 7, 5].Value = item.prod_created_date.ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 6].Value = (prod.status) ? "ACTIVE" : "INACTIVE";
                        i++;
                    }
                    
                }                
                using (var range = worksheet.Cells["A7:F7"])
                {
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                    range.Style.Border.Bottom.Color.SetColor(Color.Black);
                }
                worksheet.Column(1).AutoFit();
                worksheet.Column(2).AutoFit();
                worksheet.Column(3).AutoFit();
                worksheet.Column(4).AutoFit();

                var memoryStream = new MemoryStream(package.GetAsByteArray());
                //var fileName = string.Format("MyData-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", DateTime.UtcNow);                
                return base.File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);

            }
            return RedirectToAction("MostLovedProducts");
        }

        [HttpPost]
        public ActionResult ExportMostViewed()
        {

            var de = Request.Form["dateEnd"];
            var ds = Request.Form["dateStart"];
            var s = Request.Form["filter"];
            string status = "ALL STATUS";
            string rangedate = "ALL DATE";
            bool stt = true;
            DateTime dateStart;
            DateTime dateEnd;
            if (s != "")
            {
                stt = (s == "True") ? true : false;
                status = (s == "True") ? "ACTIVE" : "INACTIVE";
            }
            if(de != "" && ds != "")
            {
                rangedate = ds + " to " + de;
            }
            var fileName = "MostViewedProducts-" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".xlsx";
            var outputDir = "~/Public/Files/";
            //var file = new FileInfo(outputDir + fileName);
            var file = Path.Combine(Server.MapPath(outputDir), fileName);
            var logo = Path.Combine(Server.MapPath("~/Images/"), "little-logo.jpg");
            //Image piLogo = Image.FromFile(logo);
            using (var package = new ExcelPackage(new FileInfo(file)))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("MostViewedProducts-" + DateTime.Now.ToString("dd/MM/yyyy"));
                Image myImage = Image.FromFile(logo);
                var pic = worksheet.Drawings.AddPicture("Logo", myImage);
                pic.SetPosition(0, 0, 0, 0);
                worksheet.Cells["C2"].Value = "MOST VIEWED PRODUCTS";
                worksheet.Cells["C2"].Style.Font.SetFromFont(new Font("Arial", 14));
                worksheet.Cells[5, 1].Value = "Status";
                worksheet.Cells[6, 1].Value = "Date";
                worksheet.Cells[5, 2].Value = status;
                worksheet.Cells[6, 2].Value = rangedate;
                worksheet.Cells[7, 1].Value = "No";
                worksheet.Cells[7, 2].Value = "Brand";
                worksheet.Cells[7, 3].Value = "Product Name";
                worksheet.Cells[7, 4].Value = "Count";
                worksheet.Cells[7, 5].Value = "Date Added";
                worksheet.Cells[7, 6].Value = "Status";

                IEnumerable<_object.viewedProduct> v;
                var i = 1;
                v = obj.GetMostViewedProduct(Convert.ToInt32(Session_.TenantId));

                if (ds != "" && de != "" && s!= "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59).AddSeconds(59);
                    foreach (var item in v
                        .Where(p => (Convert.ToDateTime(p.created_date).Date >= dateStart.Date)
                    && (Convert.ToDateTime(p.created_date).Date <= dateEnd.Date) && p.status == stt)
                        .GroupBy(p => p.id).Select(g => new {
                        prodID = g.Key,
                        count = g.Count(),
                        created_date = g.First().created_date,
                        prod_created_date = g.First().prod_created_date,
                        status = g.First().status
                    })
                    .OrderByDescending(y => y.count))
                    {
                        var prod = obj.GetProductViewed(item.prodID).FirstOrDefault();
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = prod.tenant.ToUpper();
                        worksheet.Cells[i + 7, 3].Value = prod.name.ToUpper();
                        worksheet.Cells[i + 7, 4].Value = item.count;
                        worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(prod.created_date).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 6].Value = (prod.status) ? "ACTIVE" : "INACTIVE";
                        i++;
                    }
                }else if(ds != "" && de != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59).AddSeconds(59);
                    foreach (var item in v
                         .Where(p => (Convert.ToDateTime(p.created_date).Date >= dateStart.Date)
                    && (Convert.ToDateTime(p.created_date).Date <= dateEnd.Date)
                    )
                        .GroupBy(p => p.id).Select(g => new {
                        prodID = g.Key,
                        count = g.Count(),
                        created_date = g.First().created_date,
                        status = g.First().status
                    })                   
                    .OrderByDescending(y => y.count))
                    {
                        var prod = obj.GetProductViewed(item.prodID).FirstOrDefault();
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = prod.tenant.ToUpper();
                        worksheet.Cells[i + 7, 3].Value = prod.name.ToUpper();
                        worksheet.Cells[i + 7, 4].Value = item.count;
                        worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(prod.created_date).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 6].Value = (prod.status) ? "ACTIVE" : "INACTIVE";
                        i++;
                    }
                }
                else
                {
                    if (s != "")
                    {
                        v = obj.GetMostViewedProduct(Convert.ToInt32(Session_.TenantId))
                        .Where(p => p.status == stt);
                    }
                    else
                    {
                        v = obj.GetMostViewedProduct(Convert.ToInt32(Session_.TenantId));
                    }
                    foreach (var item in v.GroupBy(p => p.id).Select(g => new {
                        prodID = g.Key,
                        count = g.Count(),
                        created_date = g.First().created_date,
                        status = g.First().status
                    }).OrderByDescending(y => y.count))
                    {
                        var prod = obj.GetProductViewed(item.prodID).FirstOrDefault();
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = prod.tenant.ToUpper();
                        worksheet.Cells[i + 7, 3].Value = prod.name.ToUpper();
                        worksheet.Cells[i + 7, 4].Value = item.count;
                        worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(prod.created_date).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 6].Value = (prod.status) ? "ACTIVE" : "INACTIVE";
                        i++;
                    }                    
                }                
                using (var range = worksheet.Cells["A7:F7"])
                {
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                    range.Style.Border.Bottom.Color.SetColor(Color.Black);
                }
                worksheet.Column(1).AutoFit();
                worksheet.Column(2).AutoFit();
                worksheet.Column(3).AutoFit();
                worksheet.Column(4).AutoFit();

                //package.Save();
                var memoryStream = new MemoryStream(package.GetAsByteArray());
                //var fileName = string.Format("MyData-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", DateTime.UtcNow);                
                return base.File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
            }
            return RedirectToAction("MostViewedProducts");
        }

        [HttpPost]
        public ActionResult ExportMostReserved()
        {
            var de = Request.Form["dateEnd"];
            var ds = Request.Form["dateStart"];
            var s = Request.Form["filter"];
            string status = "ALL STATUS";
            string rangedate = "ALL DATE";
            bool stt = true;
            DateTime dateStart;
            DateTime dateEnd;
            if (s != "")
            {
                stt = (s == "True") ? true : false;
                status = (s == "True") ? "ACTIVE" : "INACTIVE";
            }
            if (de != "" && ds != "")
            {
                rangedate = ds + " to " + de;
            }
            var fileName = "MostReservedProducts-" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".xlsx";
            var outputDir = "~/Public/Files/";
            //var file = new FileInfo(outputDir + fileName);
            var file = Path.Combine(Server.MapPath(outputDir), fileName);
            var logo = Path.Combine(Server.MapPath("~/Images/"), "little-logo.jpg");
            //Image piLogo = Image.FromFile(logo);
            using (var package = new ExcelPackage(new FileInfo(file)))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("MostReservedProducts-" + DateTime.Now.ToString("dd/MM/yyyy"));
                Image myImage = Image.FromFile(logo);
                var pic = worksheet.Drawings.AddPicture("Logo", myImage);
                pic.SetPosition(0, 0, 0, 0);
                worksheet.Cells["C2"].Value = "MOST RESERVED PRODUCTS";
                worksheet.Cells["C2"].Style.Font.SetFromFont(new Font("Arial", 14));
                worksheet.Cells[5, 1].Value = "Status";
                worksheet.Cells[6, 1].Value = "Date";
                worksheet.Cells[5, 2].Value = status;
                worksheet.Cells[6, 2].Value = rangedate;
                worksheet.Cells[7, 1].Value = "No.";
                worksheet.Cells[7, 2].Value = "Brand";
                worksheet.Cells[7, 3].Value = "Product Name";
                worksheet.Cells[7, 4].Value = "Qty";
                worksheet.Cells[7, 5].Value = "Date Added";
                worksheet.Cells[7, 6].Value = "Status";

                IEnumerable<_object.reservedProduct> r;
                r = obj.GetMostReservedProduct(Convert.ToInt32(Session_.TenantId));

                var i = 1;

                if (ds != "" && de != "" && s != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59).AddSeconds(59);
                    foreach (var item in r
                         .Where(p => (Convert.ToDateTime(p.created_date).Date >= dateStart.Date)
                            && (Convert.ToDateTime(p.created_date).Date <= dateEnd.Date) && p.status == stt
                            )
                        .GroupBy(p => p.id).Select(g => new _object.reservedProduct
                    {
                        name = g.First().name,
                        tenant = g.First().tenant,
                        qty = g.Sum(c => c.qty),
                        created_date = g.First().created_date,
                        prod_created_date = g.First().prod_created_date,
                            status = g.First().status
                    })
                   .OrderByDescending(y => y.qty))
                    {
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = item.tenant.ToUpper();
                        worksheet.Cells[i + 7, 3].Value = item.name.ToUpper();
                        worksheet.Cells[i + 7, 4].Value = item.qty;
                        worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(item.prod_created_date).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 6].Value = (item.status) ? "ACTIVE" : "INACTIVE";
                        i++;
                    }                    
                }
                else if (ds != "" && de != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59).AddSeconds(59);
                    foreach (var item in r
                        .Where(p => (Convert.ToDateTime(p.created_date).Date >= dateStart.Date)
                        && (Convert.ToDateTime(p.created_date).Date <= dateEnd.Date))
                        .GroupBy(p => p.id).Select(g => new _object.reservedProduct
                    {
                        name = g.First().name,
                        tenant = g.First().tenant,
                        qty = g.Sum(c => c.qty),
                        created_date = g.First().created_date,
                        prod_created_date = g.First().prod_created_date,
                            status = g.First().status
                    })
                    .OrderByDescending(y => y.qty))
                    {
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = item.tenant.ToUpper();
                        worksheet.Cells[i + 7, 3].Value = item.name.ToUpper();
                        worksheet.Cells[i + 7, 4].Value = item.qty;
                        worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(item.prod_created_date).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 6].Value = (item.status) ? "ACTIVE" : "INACTIVE";
                        i++;
                    }
                }
                else
                {
                    if (s != "")
                    {
                        r = obj.GetMostReservedProduct(Convert.ToInt32(Session_.TenantId))
                        .Where(p => p.status == stt);
                    }
                    else
                    {
                        r = obj.GetMostReservedProduct(Convert.ToInt32(Session_.TenantId));
                    }
                    foreach (var item in r.GroupBy(p => p.id).Select(g => new _object.reservedProduct
                    {
                        name = g.First().name,
                        tenant = g.First().tenant,
                        qty = g.Sum(c => c.qty),
                        created_date = g.First().created_date,
                        prod_created_date = g.First().prod_created_date,
                        status = g.First().status
                    }).OrderByDescending(y => y.qty)
                    )
                    {
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = item.tenant.ToUpper();
                        worksheet.Cells[i + 7, 3].Value = item.name.ToUpper();
                        worksheet.Cells[i + 7, 4].Value = item.qty;
                        worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(item.prod_created_date).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 6].Value = (item.status) ? "ACTIVE" : "INACTIVE";
                        i++;
                    }                    
                }  
                using (var range = worksheet.Cells["A7:F7"])
                {
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                    range.Style.Border.Bottom.Color.SetColor(Color.Black);
                }
                worksheet.Column(1).AutoFit();
                worksheet.Column(2).AutoFit();
                worksheet.Column(3).AutoFit();
                worksheet.Column(4).AutoFit();

                var memoryStream = new MemoryStream(package.GetAsByteArray());
                //var fileName = string.Format("MyData-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", DateTime.UtcNow);                
                return base.File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);

            }
            return RedirectToAction("MostReservedProducts");
        }
    }
}