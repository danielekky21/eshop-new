﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DynamicContent.Areas.TenantManager.ViewModels
{
    public class ReservedProductViewModel
    {
        public string tenantName { get; set; }
        public string productName { get; set; }
        public int quantityTotal { get; set; }
        public bool status { get; set; }
        public DateTime createdDate { get; set; }
    }
}