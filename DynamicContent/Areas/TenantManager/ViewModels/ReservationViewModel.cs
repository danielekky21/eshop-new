﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DynamicContent.Areas.TenantManager.ViewModels
{
    public class ReservationViewModel
    {
        public int id { get; set; }
        public int userId { get; set; }
        public string userEmail { get; set; }
        public string userName { get; set; }
        public string userPhone { get; set; }
        public string userAddress { get; set; }
        public string userCity { get; set; }
        public string userGender { get; set; }
        public string followUp { get; set; }
        public string followUpName { get; set; }
        public string followUpNote { get; set; }
        public DateTime? followUpDate { get; set; }
        public bool? reserved { get; set; }
        public string reservationCode { get; set; }
        public DateTime? reservedDate { get; set; }
        public List<productReserved> products { get; set; }
    }

    public class productReserved
    {
        public int productId { get; set; }
        public string productName { get; set; }
        public string productPrice { get; set; }
        public string colorCode { get; set; }
        public int colorId { get; set; }
        public int tenantId { get; set; }
        public string tenantName { get; set; }
        public int quantity { get; set; }
        public string size { get; set; }
        public string totalPrice { get; set; }
        public int? status { get; set; }
    }
    
    public class productImage
    {
        public int imageId { get; set; }
        public string imagePath { get; set; }
    }
}