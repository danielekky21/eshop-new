﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DynamicContent.Areas.TenantManager.ViewModels
{
    public class LovedProductViewModel
    {
        public string tenantName { get; set; }
        public string productName { get; set; }
        public int loveCount { get; set; }
        public bool status { get; set; }
        public DateTime createdDate { get; set; }
    }
}