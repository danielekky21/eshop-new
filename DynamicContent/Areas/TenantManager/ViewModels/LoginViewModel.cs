﻿using System.ComponentModel.DataAnnotations;

namespace DynamicContent.Areas.TenantManager.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage ="Username cannot be empty")]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password cannot be empty")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}