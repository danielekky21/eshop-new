﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DynamicContent.Areas.TenantManager.Helper
{
    public class Session_
    {
        public static string SessionId
        {
            get
            {
                if (System.Web.HttpContext.Current.Session.SessionID != null)
                    return System.Web.HttpContext.Current.Session.SessionID;
                else return "";
            }
        }
        public static void destroy()
        {
            HttpContext.Current.Session.Abandon();
        }

        public static string TenantId
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["TenantId"] != null)
                    return System.Web.HttpContext.Current.Session["TenantId"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["TenantId"] = value; }
        }
        public static string RoleID
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["TenantRoleID"] != null)
                    return System.Web.HttpContext.Current.Session["TenantRoleID"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["TenantRoleID"] = value; }
        }

        public static string Username
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["TenantUsername"] != null)
                    return System.Web.HttpContext.Current.Session["TenantUsername"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["TenantUsername"] = value; }
        }

        public static List<int> Priviledge
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["TenantPriviledge"] == null)
                {
                    System.Web.HttpContext.Current.Session["TenantPriviledge"] = new List<int>();
                }
                return System.Web.HttpContext.Current.Session["TenantPriviledge"] as List<int>;
            }
            set { System.Web.HttpContext.Current.Session["TenantPriviledge"] = value; }
        }
    }
}