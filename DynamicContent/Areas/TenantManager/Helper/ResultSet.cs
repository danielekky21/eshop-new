﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Web.UI.WebControls;
using DynamicContent.Helper;
using DynamicContent.Areas.TenantManager.ViewModels;

namespace DynamicContent.Areas.TenantManager.Helper
{
    public class ResultSet
    {

        #region reports
        public IQueryable<LovedProductViewModel> lovedProductGetResult(string search, string sortOrder, int start, int length, IQueryable<LovedProductViewModel> dtResult, List<string> columnFilters)
        {
            return lovedProductFilterResult(search, dtResult, columnFilters).SortBy(sortOrder).Skip(start).Take(length);
        }

        public int lovedProductCount(string search, IQueryable<LovedProductViewModel> dtResult, List<string> columnFilters)
        {
            return lovedProductFilterResult(search, dtResult, columnFilters).Count();
        }

        private IQueryable<LovedProductViewModel> lovedProductFilterResult(string search, IQueryable<LovedProductViewModel> dtResult, List<string> columnFilters)
        {
            IQueryable<LovedProductViewModel> results = dtResult.AsQueryable();
            if (columnFilters.Count() >= 4 && columnFilters[3] != null)
            {
                var ci = bool.Parse(columnFilters[3].ToString());
                results = results.Where(p => p.status == ci);
            }


            return results;
        }

        public IQueryable<ReservedProductViewModel> reservedProductGetResult(string search, string sortOrder, int start, int length, IQueryable<ReservedProductViewModel> dtResult, List<string> columnFilters)
        {
            return reservedProductFilterResult(search, dtResult, columnFilters).SortBy(sortOrder).Skip(start).Take(length);
        }

        public int reservedProductCount(string search, IQueryable<ReservedProductViewModel> dtResult, List<string> columnFilters)
        {
            return reservedProductFilterResult(search, dtResult, columnFilters).Count();
        }

        private IQueryable<ReservedProductViewModel> reservedProductFilterResult(string search, IQueryable<ReservedProductViewModel> dtResult, List<string> columnFilters)
        {
            IQueryable<ReservedProductViewModel> results = dtResult.AsQueryable();
            if (columnFilters.Count() >= 4 && columnFilters[3] != null)
            {
                var ci = bool.Parse(columnFilters[3].ToString());
                results = results.Where(p => p.status == ci);
            }


            return results;
        }

        public IQueryable<ViewedProductViewModel> viewedProductGetResult(string search, string sortOrder, int start, int length, IQueryable<ViewedProductViewModel> dtResult, List<string> columnFilters)
        {
            return viewedProductFilterResult(search, dtResult, columnFilters).SortBy(sortOrder).Skip(start).Take(length);
        }

        public int viewedProductCount(string search, IQueryable<ViewedProductViewModel> dtResult, List<string> columnFilters)
        {
            return viewedProductFilterResult(search, dtResult, columnFilters).Count();
        }

        private IQueryable<ViewedProductViewModel> viewedProductFilterResult(string search, IQueryable<ViewedProductViewModel> dtResult, List<string> columnFilters)
        {
            IQueryable<ViewedProductViewModel> results = dtResult.AsQueryable();
            if (columnFilters.Count() >= 4 && columnFilters[3] != null)
            {
                var ci = bool.Parse(columnFilters[3].ToString());
                results = results.Where(p => p.status == ci);
            }


            return results;
        }
        #endregion

        public List<_object.AllProduct> GetResult(string search, string sortOrder, int start, int length, List<_object.AllProduct> dtResult, List<string> columnFilters)
        {
            return FilterResult(search, dtResult, columnFilters).SortBy(sortOrder).Skip(start).Take(length).ToList();
        }

        public int Count(string search, List<_object.AllProduct> dtResult, List<string> columnFilters)
        {
            return FilterResult(search, dtResult, columnFilters).Count();
        }

        private IQueryable<_object.AllProduct> FilterResult(string search, List<_object.AllProduct> dtResult, List<string> columnFilters)
        {
            IQueryable<_object.AllProduct> results = dtResult.AsQueryable();
            
            


            results = results.Where(p => (search == null || (p.name != null && p.name.ToLower().Contains(search.ToLower()))
                ));
            if(columnFilters.Count() >=4 && columnFilters[4] != null)
            {
                var ci = (columnFilters[4].ToString() == "1") ? true : false;
                results = results.Where(p => p.status == ci);
            }
            return results;
        }

        public List<ReservationViewModel> GetResultReservation(string search, string sortOrder, int start, int length, List<ReservationViewModel> dtResult, List<string> columnFilters)
        {
            return FilterResult(search, dtResult, columnFilters).SortBy(sortOrder).Skip(start).Take(length).ToList();
        }

        public int Count(string search, List<ReservationViewModel> dtResult, List<string> columnFilters)
        {
            return FilterResult(search, dtResult, columnFilters).Count();
        }

        private IQueryable<ReservationViewModel> FilterResult(string search, List<ReservationViewModel> dtResult, List<string> columnFilters)
        {
            IQueryable<ReservationViewModel> results = dtResult.AsQueryable();

            results = results.Where(p => (search == null || (p.userName != null && p.userName.ToLower().Contains(search.ToLower()))
                && (columnFilters[0] == null || (p.userName != null && p.userName.ToLower().Contains(columnFilters[0].ToLower())))
                ));

            return results;
        }
    }
}