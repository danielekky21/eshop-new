﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DynamicContent.Areas.TenantManager.Helper
{
    public class AuthorizeTenant : AuthorizeAttribute
    {
        DynamicContent.Areas.TenantManager.Controllers.AccountController account = new Areas.TenantManager.Controllers.AccountController();

        public AuthorizeTenant()
        {
        }
        
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            base.AuthorizeCore(httpContext);
            return account.Authentication();
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var errmsg = "";
            var request = filterContext.HttpContext.Request;
            
            if (!String.IsNullOrEmpty(Session_.Username) && !String.IsNullOrEmpty(Session_.SessionId))
                errmsg = "Someone is logged in using your credential. If this is not you, please change login again and change your password immediately.";
            if (request["X-Requested-With"] == "XMLHttpRequest" || (request.Headers != null && request.Headers["X-Requested-With"] == "XMLHttpRequest"))
            {
                var urlHelper = new UrlHelper(filterContext.RequestContext);
                filterContext.HttpContext.Response.StatusCode = 401;
                filterContext.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;
                filterContext.Result = new JsonResult
                {
                    Data = new
                    {
                        error = "NotAuthorized",
                        logonurl = urlHelper.Action("Logout", "Account", new { area = "TenantManager" }),
                        msg = errmsg
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary(
                    new
                    {
                        controller = "Account",
                        action = "Login",
                        area = "TenantManager",
                        //msg = HttpUtility.HtmlDecode(errmsg)
                    })
                );
            }
            //base.HandleUnauthorizedRequest(filterContext);
        }
        
    }
}