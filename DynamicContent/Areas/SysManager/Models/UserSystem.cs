﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using DynamicContent.Models;


namespace DynamicContent.Areas.SysManager.Models
{
    [Table("M_User_Tenant")]
    public class UserSystem
    {
        [Key]
        public string username { get; set; }
        public string password { get; set; }
        public string role_id { get; set; }
        public string created_by { get; set; }
        public DateTime created_date { get; set; }
        public string modified_by { get; set; }
        public DateTime modified_date { get; set; }
        public int pass_counter { get; set; }
        public DateTime? change_pass_date { get; set; }
        public string email { get; set; }
        public int tenant { get; set; }
        public int brand { get; set; }
    }

    public class UserSystemModel
    {
        private readonly Entities db;
        public UserSystemModel()
        {
            db = new Entities();
        }
        public UserSystem getUserByUsername(string username)
        {
            var rs = from ut in db.M_User_Tenant
                     where ut.username.Equals(username)
                     select new UserSystem
                     {
                         username = ut.username,
                         password = ut.password,
                         role_id = ut.role_id,
                         created_by = ut.created_by,
                         created_date = ut.created_date,
                         modified_by = ut.modified_by,
                         modified_date = ut.modified_date,
                         pass_counter = ut.pass_counter,
                         change_pass_date = ut.change_pass_date,
                         email = ut.email,
                         tenant = ut.tenant,
                         brand = ut.brand
                     };
            return rs.FirstOrDefault();
        }
    }
}