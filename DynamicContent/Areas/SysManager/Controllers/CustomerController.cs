﻿using DynamicContent.Areas.SysManager.Helper;
using DynamicContent.Areas.SysManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DynamicContent.Helper;

namespace DynamicContent.Areas.SysManager.Controllers
{
    public class CustomerController : AuthenticatedController
    {

        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult CustomerListDataHandler(DTParameters dtParam)
        {
            try
            {
                var customers = (from c in db.tbl_user_register
                                select new _object.UserRegister {
                                    firstname = c.firstName,
                                    lastname = c.lastName,
                                    address = c.address,
                                    city = c.city,
                                    dob = c.dob,
                                    email = c.email,
                                    gender = c.gender,
                                    phone = c.phone,
                                    zip = c.zipcode
                                });
                //var products = obj.GetAllProductTmpWaitingApproval();
                
                List < String > columnSearch = new List<string>();

                foreach (var col in dtParam.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                var data = new ResultSet().GetResult(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, customers.ToList(), columnSearch);
                int count = new ResultSet().Count(dtParam.Search.Value, customers.ToList(), columnSearch);
                var result = new DTResult<DynamicContent.Helper._object.UserRegister>
                {
                    draw = dtParam.Draw,
                    data = data,
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }
        
    }
}