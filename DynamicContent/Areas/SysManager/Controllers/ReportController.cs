﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using OfficeOpenXml;
using System.Security.Cryptography.X509Certificates;
using System.Drawing;
using OfficeOpenXml.Style;
using OfficeOpenXml.Drawing.Chart;
using System.Web.Mvc;
using DynamicContent.Areas.SysManager.ViewModels;
using DynamicContent.Areas.SysManager.Helper;
using DynamicContent.Helper;
using System.Globalization;


namespace DynamicContent.Areas.SysManager.Controllers
{
    public class ReportController : AuthenticatedController
    {
        //
        // GET: /SysManager/Report/
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SubscibersSignUp()
        {
            //var s = obj.GetUserRegister();
            //ViewData["user"] = s;
            
            return View();
        }
        public ActionResult ReservationHandler()
        {
            return View();
        }
        public JsonResult ReservationHeaderHandler(DTParameters dtParam)
        {
            try
            {
                DateTime startDate = (String.IsNullOrEmpty(dtParam.startDate) ? DateTime.MinValue : DateTime.ParseExact(dtParam.startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture));
                DateTime endDate = (String.IsNullOrEmpty(dtParam.endDate) ? DateTime.MaxValue : (DateTime.ParseExact(dtParam.endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture)).AddHours(23).AddMinutes(59).AddSeconds(59));
                var pr = (from u in db.vw_ReservationHeader
                          where (startDate == DateTime.MinValue || u.ReservationDate >= startDate) && (endDate == DateTime.MaxValue || u.ReservationDate <= endDate)
                          select new reservationHeaderModel
                          {
                              ReservatonCode = u.ReservationCode,
                              ReservationDate = u.ReservationDate.ToString(),
                              CustomerEmail = u.CustomerEmail,
                              CustomerName = u.CustomerName,
                              ShopName = u.ShopName,
                              FUStatus = u.FUStatus,
                              FUName  = u.FUName,
                              FUNote = u.FUNote
                          });

                List<String> columnSearch = new List<string>();

                foreach (var col in dtParam.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }
             
                var data = new ResultSet().reservationHeaderGetResult(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, pr, columnSearch);
                int count = new ResultSet().reservationHeaderCount(dtParam.Search.Value, pr, columnSearch);
                var result = new DTResult<reservationHeaderModel>
                {
                    draw = dtParam.Draw,
                    data = data.ToList(),
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }
        public JsonResult SubscriberListHandler(DTParameters dtParam)
        {
            try
            {
                DateTime startDate = (String.IsNullOrEmpty(dtParam.startDate) ? DateTime.MinValue : DateTime.ParseExact(dtParam.startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture));
                DateTime endDate = (String.IsNullOrEmpty(dtParam.endDate) ? DateTime.MaxValue : (DateTime.ParseExact(dtParam.endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture)).AddHours(23).AddMinutes(59).AddSeconds(59));
                var pr = (from u in db.tbl_user_register
                          where (startDate == DateTime.MinValue || u.regist_date >= startDate) && (endDate == DateTime.MaxValue || u.regist_date <= endDate)
                          select u into x
                          group x by System.Data.Entity.DbFunctions.TruncateTime(x.regist_date) into g
                          select new SubscriberReportViewModel
                          {
                              date = g.Key,
                              count = g.Count()
                          }
                        );

                List<String> columnSearch = new List<string>();

                foreach (var col in dtParam.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                var data = new ResultSet().subcriberReportGetResult(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, pr, columnSearch);
                int count = new ResultSet().subcriberReportCount(dtParam.Search.Value, pr, columnSearch);
                var result = new DTResult<SubscriberReportViewModel>
                {
                    draw = dtParam.Draw,
                    data = data.ToList(),
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        public ActionResult BrandItemsUpload()
        {
            //var u = obj.ItemsUploadedByBrand();
            //ViewData["upload"] = u;
            return View();
        }

        /*var rs = from p in db.tbl_product 
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     orderby p.created_date descending
                     select new _object.productUploadByBrand  
                     {
                         id = p.ID,
                         name = p.name,
                         tenant = t.ID, 
                         tenantName = t.name,
                         created_date = p.created_date
                     };*/

        public JsonResult BrandItemsUploadListHandler(DTParameters dtParam)
        {
            try
            {
                DateTime startDate = (String.IsNullOrEmpty(dtParam.startDate) ? DateTime.MinValue : DateTime.ParseExact(dtParam.startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture));
                DateTime endDate = (String.IsNullOrEmpty(dtParam.endDate) ? DateTime.MaxValue : (DateTime.ParseExact(dtParam.endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture)).AddHours(23).AddMinutes(59).AddSeconds(59));
                var pr = (from p in db.tbl_product
                          join t in db.tbl_tenant on p.tenant equals t.ID
                          where (startDate == DateTime.MinValue || p.created_date >= startDate) && (endDate == DateTime.MaxValue || p.created_date <= endDate)
                          select new 
                          {
                              id = p.ID,
                              name = p.name,
                              tenant = t.ID,
                              tenantName = t.name
                          } into x
                          group x by x.tenantName into g
                          select new TenantItemReportViewModel
                          {
                              tenantName = g.Key,
                              totalItems = g.Count()
                          }
                    );

                List<String> columnSearch = new List<string>();

                foreach (var col in dtParam.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                var data = new ResultSet().reportGetResult(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, pr, columnSearch);
                int count = new ResultSet().reportCount(dtParam.Search.Value, pr, columnSearch);
                var result = new DTResult<TenantItemReportViewModel>
                {
                    draw = dtParam.Draw,
                    data = data.ToList(),
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }
        
        public ActionResult CustomerRegistration()
        {
            //var c = from u in db.tbl_user_register
            //        where !(from o in db.tbl_ReservationParent
            //                select o.CustID)
            //                .Contains(u.ID)
            //        select u;
            //ViewData["customer"] = c;
            return View();
        }

        public JsonResult CustomerListHandler(DTParameters dtParam)
        {
            try
            {
                DateTime startDate = (String.IsNullOrEmpty(dtParam.startDate) ? DateTime.MinValue : DateTime.ParseExact(dtParam.startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture));
                DateTime endDate = (String.IsNullOrEmpty(dtParam.endDate) ? DateTime.MaxValue : (DateTime.ParseExact(dtParam.endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture)).AddHours(23).AddMinutes(59).AddSeconds(59));
                var pr = (from u in db.tbl_user_register
                          where  (startDate == DateTime.MinValue || u.regist_date >= startDate) && (endDate == DateTime.MaxValue || u.regist_date <= endDate)
                          select new CustomerReportViewModel
                          {
                              signupDate = u.regist_date,
                              customerName = u.firstName + " " + u.lastName,
                              dob = u.dob,// DateTime.ParseExact(u.dob,"d MMMM yyyy",CultureInfo.InvariantCulture),
                              email = u.email,
                              gender = u.gender,
                              phoneNumber = u.phone
                          }
                        );

                List<String> columnSearch = new List<string>();

                foreach (var col in dtParam.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                var data = new ResultSet().customerReportGetResult(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, pr, columnSearch);
                int count = new ResultSet().customerReportCount(dtParam.Search.Value, pr, columnSearch);
                var result = new DTResult<CustomerReportViewModel>
                {
                    draw = dtParam.Draw,
                    data = data.ToList(),
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        public ActionResult LeastViewedItems()
        {
            var v = obj.GetMostViewedProductSysManager();
            ViewData["viewed"] = v;
            return View();
        }

        public ActionResult MostViewedItems()
        {
            //var v = obj.GetMostViewedProductSysManager();
            //ViewData["viewed"] = v;
            return View();
        }

        public JsonResult MostViewedProductListHandler(DTParameters dtParam)
        {
            try
            {
                DateTime startDate = (String.IsNullOrEmpty(dtParam.startDate) ? DateTime.MinValue : DateTime.ParseExact(dtParam.startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture));
                DateTime endDate = (String.IsNullOrEmpty(dtParam.endDate) ? DateTime.MaxValue : (DateTime.ParseExact(dtParam.endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture)).AddHours(23).AddMinutes(59).AddSeconds(59));
                var pr = (from v in db.tbl_product_view
                          join p in db.tbl_product on v.product_id equals p.ID
                          join t in db.tbl_tenant on p.tenant equals t.ID
                          where (startDate == DateTime.MinValue || v.created_date >= startDate) && (endDate == DateTime.MaxValue || v.created_date <= endDate)
                          select new
                          {
                              id = p.ID,
                              viewID = v.ID,
                              createdDate = p.created_date,
                              name = p.name,
                              tenant = t.name,
                              status = p.status
                          } into x
                          group x by new { x.id, x.tenant, x.name, x.status, x.createdDate } into g
                          select new TenantManager.ViewModels.ViewedProductViewModel
                          {
                              tenantName = g.Key.tenant,
                              productName = g.Key.name,
                              viewTotal = g.Count(),
                              status = g.Key.status,
                              createdDate = g.Key.createdDate
                          }
                        );

                List<String> columnSearch = new List<string>();

                foreach (var col in dtParam.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                var data = new TenantManager.Helper.ResultSet().viewedProductGetResult(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, pr, columnSearch);
                int count = new TenantManager.Helper.ResultSet().viewedProductCount(dtParam.Search.Value, pr, columnSearch);
                var result = new DTResult<TenantManager.ViewModels.ViewedProductViewModel>
                {
                    draw = dtParam.Draw,
                    data = data.ToList(),
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        public ActionResult MostLovedItems()
        {
            //var w = obj.GetMostLovedProductSysManager();
            //ViewData["wishlist"] = w;
            return View();
        }

        public JsonResult MostLovedProductListHandler(DTParameters dtParam)
        {

            try
            {
                DateTime startDate = (String.IsNullOrEmpty(dtParam.startDate) ? DateTime.MinValue : DateTime.ParseExact(dtParam.startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture));
                DateTime endDate = (String.IsNullOrEmpty(dtParam.endDate) ? DateTime.MaxValue : (DateTime.ParseExact(dtParam.endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture)).AddHours(23).AddMinutes(59).AddSeconds(59));
                var pr = (from w in db.tbl_wishlist
                          join p in db.tbl_product on w.product equals p.ID
                          join t in db.tbl_tenant on p.tenant equals t.ID
                          where (startDate == DateTime.MinValue || w.created_date >= startDate) && (endDate == DateTime.MaxValue || w.created_date <= endDate)
                          select new
                          {
                              id = p.ID,
                              tenant = t.name,
                              wishlistID = w.ID,
                              created_date = p.created_date,
                              name = p.name,
                              status = p.status
                          } into x
                          group x by new { x.id, x.tenant, x.name, x.status, x.created_date } into g
                          select new TenantManager.ViewModels.LovedProductViewModel
                          {
                              tenantName = g.Key.tenant,
                              productName = g.Key.name,
                              loveCount = g.Count(),
                              status = g.Key.status,
                              createdDate = g.Key.created_date
                          }
                        );

                List<String> columnSearch = new List<string>();

                foreach (var col in dtParam.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                var data = new TenantManager.Helper.ResultSet().lovedProductGetResult(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, pr, columnSearch);
                int count = new TenantManager.Helper.ResultSet().lovedProductCount(dtParam.Search.Value, pr, columnSearch);
                var result = new DTResult<TenantManager.ViewModels.LovedProductViewModel>
                {
                    draw = dtParam.Draw,
                    data = data.ToList(),
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        public ActionResult MostReservedItems() 
        {
            return View();
        }

        public JsonResult MostReservedProductListHandler(DTParameters dtParam)
        {
            try
            {
                DateTime startDate = (String.IsNullOrEmpty(dtParam.startDate) ? DateTime.MinValue : DateTime.ParseExact(dtParam.startDate, "dd-MM-yyyy", CultureInfo.InvariantCulture));
                DateTime endDate = (String.IsNullOrEmpty(dtParam.endDate) ? DateTime.MaxValue : (DateTime.ParseExact(dtParam.endDate, "dd-MM-yyyy", CultureInfo.InvariantCulture)).AddHours(23).AddMinutes(59).AddSeconds(59));
                var pr = (from r in db.tbl_reservation
                          join rp in db.tbl_ReservationParent on r.parentReservation equals rp.ID
                          join p in db.tbl_product on r.product equals p.ID
                          join t in db.tbl_tenant on p.tenant equals t.ID
                          where (startDate == DateTime.MinValue || rp.ReservationDate >= startDate) && (endDate == DateTime.MaxValue || rp.ReservationDate <= endDate)
                          select new
                          {
                              id = r.product,
                              name = p.name,
                              createdDate = p.created_date,
                              tenant = t.name,
                              qty = r.qty,
                              status = p.status
                          } into x
                          group x by new { x.id, x.tenant, x.name, x.status, x.createdDate } into g
                          select new TenantManager.ViewModels.ReservedProductViewModel
                          {
                              tenantName = g.Key.tenant,
                              productName = g.Key.name,
                              quantityTotal = g.Sum(t => t.qty),
                              status = g.Key.status,
                              createdDate = g.Key.createdDate
                          }
                        );

                List<String> columnSearch = new List<string>();

                foreach (var col in dtParam.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                var data = new TenantManager.Helper.ResultSet().reservedProductGetResult(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, pr, columnSearch);
                int count = new TenantManager.Helper.ResultSet().reservedProductCount(dtParam.Search.Value, pr, columnSearch);
                var result = new DTResult<TenantManager.ViewModels.ReservedProductViewModel>
                {
                    draw = dtParam.Draw,
                    data = data.ToList(),
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        public ActionResult ExportSubscibersSignUp()
        {
            var de = Request.Form["dateEnd"];
            var ds = Request.Form["dateStart"];
            string rangedate = "ALL DATE";
            DateTime dateStart;
            DateTime dateEnd;
            if (de != "" && ds != "")
            {
                rangedate = ds + " to " + de;
            }
            var fileName = "SubscibersSignUp-" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".xls";
            var outputDir = "~/Public/Files/";
            //var file = new FileInfo(outputDir + fileName);
            var file = Path.Combine(Server.MapPath(outputDir), fileName);
            var logo = Path.Combine(Server.MapPath("~/Images/"), "little-logo.jpg");
            //Image piLogo = Image.FromFile(logo);
            using (var package = new ExcelPackage(new FileInfo(file)))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("SubscibersSignUp-" + DateTime.Now.ToString("dd/MM/yyyy"));
                Image myImage = Image.FromFile(logo);
                var pic = worksheet.Drawings.AddPicture("Logo", myImage);
                pic.SetPosition(0, 0, 0, 0);
                worksheet.Cells["C2"].Value = "SUBSCRIBER SIGN UP";
                worksheet.Cells["C2"].Style.Font.SetFromFont(new Font("Arial", 14));
                worksheet.Cells[6, 1].Value = "Date";
                worksheet.Cells[6, 2].Value = rangedate;
                worksheet.Cells[7, 1].Value = "No.";
                worksheet.Cells[7, 2].Value = "Date";
                worksheet.Cells[7, 3].Value = "Total Subsribers";

                IEnumerable<DynamicContent.Models.tbl_user_register> s;
                s = obj.GetUserRegister();
                var i = 1;

                if (ds != "" && de != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    foreach (var item in (s as IEnumerable<DynamicContent.Models.tbl_user_register>)
                                        .Where(p => (Convert.ToDateTime(p.regist_date).Date >= dateStart.Date)
                                        && (Convert.ToDateTime(p.regist_date).Date <= dateEnd.Date)
                                        )
                                        .OrderByDescending(y => y.regist_date)
                                        .GroupBy(x => Convert.ToDateTime(x.regist_date).ToString("dd-MM-yyyy"))
                                        .Select(group => new {
                                            date = group.Key,
                                            count = group.Count()
                                        }))
                    {
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = item.date;
                        worksheet.Cells[i + 7, 3].Value = item.count;
                        i++;
                    }
                }
                else
                {
                    foreach (var item in (s as IEnumerable<DynamicContent.Models.tbl_user_register>)
                                        .OrderByDescending(y => y.regist_date)
                                        .GroupBy(x => Convert.ToDateTime(x.regist_date).ToString("dd-MM-yyyy"))
                                        .Select(group => new {
                                            date = group.Key,
                                            count = group.Count()
                                        }))
                    {
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = item.date;
                        worksheet.Cells[i + 7, 3].Value = item.count;
                        i++;
                    }
                }
                
                
                
                using (var range = worksheet.Cells["A7:C7"])
                {
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                    range.Style.Border.Bottom.Color.SetColor(Color.Black);
                }
                worksheet.Column(1).AutoFit();
                worksheet.Column(2).AutoFit();
                worksheet.Column(3).AutoFit();

                var memoryStream = new MemoryStream(package.GetAsByteArray());
                //var fileName = string.Format("MyData-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", DateTime.UtcNow);                
                return base.File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);


            }
            return RedirectToAction("SubscibersSignUp");
        }
        public ActionResult ExportReservationHeader()
        {
            var de = Request.Form["dateEnd"];
            var ds = Request.Form["dateStart"];
            string rangedate = "ALL DATE";
            DateTime dateStart;
            DateTime dateEnd;
            var s = Request.Form["filter"];
        
            if (de != "" && ds != "")
            {
                rangedate = ds + " to " + de;
            }
            var fileName = "ReservationHeader-" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".xls";
            var outputDir = "~/Public/Files/";
            //var file = new FileInfo(outputDir + fileName);
            var file = Path.Combine(Server.MapPath(outputDir), fileName);
            var logo = Path.Combine(Server.MapPath("~/Images/"), "little-logo.jpg");
            //Image piLogo = Image.FromFile(logo);
            using (var package = new ExcelPackage(new FileInfo(file)))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Reservation-" + DateTime.Now.ToString("dd/MM/yyyy"));
                Image myImage = Image.FromFile(logo);
                var pic = worksheet.Drawings.AddPicture("Logo", myImage);
                pic.SetPosition(0, 0, 0, 0);
                worksheet.Cells["C2"].Value = "Reservation";
                worksheet.Cells["C2"].Style.Font.SetFromFont(new Font("Arial", 14));
                worksheet.Cells[6, 1].Value = "Date";
                worksheet.Cells[6, 2].Value = rangedate;
                worksheet.Cells[7, 1].Value = "No.";
                worksheet.Cells[7, 2].Value = "Reservation Code.";
                worksheet.Cells[7, 3].Value = "Reservation Date";
                worksheet.Cells[7, 4].Value = "Customer Name";
                worksheet.Cells[7, 5].Value = "Customer Email";
                worksheet.Cells[7, 6].Value = "Shop Name";
                worksheet.Cells[7, 7].Value = "Follow up";
                worksheet.Cells[7, 8].Value = "Follow up Name";
                worksheet.Cells[7, 9].Value = "Follow up Note";


                IEnumerable<_object.ReservationHeader> u;
                u = obj.ItemReservationHeader();
                var i = 1;

                if (ds != "" && de != "" && s != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    foreach (var item in u.Where(p => (Convert.ToDateTime(p.ReservationDate).Date >= dateStart.Date)
                    && (Convert.ToDateTime(p.ReservationDate).Date <= dateEnd.Date && p.FUStatus == s)
                    ).OrderByDescending(y => y.ReservationDate))
                    {

                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = item.ReservatonCode;
                        worksheet.Cells[i + 7, 3].Value = Convert.ToDateTime(item.ReservationDate).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 4].Value = item.CustomerName;
                        worksheet.Cells[i + 7, 5].Value = item.CustomerEmail;
                        worksheet.Cells[i + 7, 6].Value = item.ShopName;
                        worksheet.Cells[i + 7, 7].Value = item.FUStatus;
                        worksheet.Cells[i + 7, 8].Value = item.FUName;
                        worksheet.Cells[i + 7, 9].Value = item.FUNote;
                        i++;
                    }


                }
                else
                if (ds != "" && de != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    foreach (var item in u.Where(p => (Convert.ToDateTime(p.ReservationDate).Date >= dateStart.Date)
                    && (Convert.ToDateTime(p.ReservationDate).Date <= dateEnd.Date)
                    ).OrderByDescending(y => y.ReservationDate))
                    {

                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = item.ReservatonCode;
                        worksheet.Cells[i + 7, 3].Value = Convert.ToDateTime(item.ReservationDate).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 4].Value = item.CustomerName;
                        worksheet.Cells[i + 7, 5].Value = item.CustomerEmail;
                        worksheet.Cells[i + 7, 6].Value = item.ShopName;
                        worksheet.Cells[i + 7, 7].Value = item.FUStatus;
                        worksheet.Cells[i + 7, 8].Value = item.FUName;
                        worksheet.Cells[i + 7, 9].Value = item.FUNote;
                        i++;
                    }
                }
                else
                {
                    if (s != "")
                    {
                        foreach (var item in u.Where(x => x.FUStatus == s).OrderByDescending(y => y.ReservationDate))
                        {
                            worksheet.Cells[i + 7, 1].Value = i;
                            worksheet.Cells[i + 7, 2].Value = item.ReservatonCode;
                            worksheet.Cells[i + 7, 3].Value = Convert.ToDateTime(item.ReservationDate).ToString("dd-MM-yyyy");
                            worksheet.Cells[i + 7, 4].Value = item.CustomerName;
                            worksheet.Cells[i + 7, 5].Value = item.CustomerEmail;
                            worksheet.Cells[i + 7, 6].Value = item.ShopName;
                            worksheet.Cells[i + 7, 7].Value = item.FUStatus;
                            worksheet.Cells[i + 7, 8].Value = item.FUName;
                            worksheet.Cells[i + 7, 9].Value = item.FUNote;
                            i++;
                        }
                    }
                    else
                    {
                        foreach (var item in u.OrderByDescending(y => y.ReservationDate))
                        {
                            worksheet.Cells[i + 7, 1].Value = i;
                            worksheet.Cells[i + 7, 2].Value = item.ReservatonCode;
                            worksheet.Cells[i + 7, 3].Value = Convert.ToDateTime(item.ReservationDate).ToString("dd-MM-yyyy");
                            worksheet.Cells[i + 7, 4].Value = item.CustomerName;
                            worksheet.Cells[i + 7, 5].Value = item.CustomerEmail;
                            worksheet.Cells[i + 7, 6].Value = item.ShopName;
                            worksheet.Cells[i + 7, 7].Value = item.FUStatus;
                            worksheet.Cells[i + 7, 8].Value = item.FUName;
                            worksheet.Cells[i + 7, 9].Value = item.FUNote;
                            i++;
                        }
                    }
                }
                
                    
                
                using (var range = worksheet.Cells["A7:I7"])
                {
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                    range.Style.Border.Bottom.Color.SetColor(Color.Black);
                }
                worksheet.Column(1).AutoFit();
                worksheet.Column(2).AutoFit();
                worksheet.Column(3).AutoFit();

                var memoryStream = new MemoryStream(package.GetAsByteArray());
                //var fileName = string.Format("MyData-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", DateTime.UtcNow);                
                return base.File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);

            }
            return RedirectToAction("ReservationHandler");
        }

        public ActionResult ExportBrandItemsUpload() 
        {
            var de = Request.Form["dateEnd"];
            var ds = Request.Form["dateStart"];
            string rangedate = "ALL DATE";
            DateTime dateStart;
            DateTime dateEnd;
            if (de != "" && ds != "")
            {
                rangedate = ds + " to " + de;
            }
            var fileName = "BrandItemsUpload-" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".xls";
            var outputDir = "~/Public/Files/";
            //var file = new FileInfo(outputDir + fileName);
            var file = Path.Combine(Server.MapPath(outputDir), fileName);
            var logo = Path.Combine(Server.MapPath("~/Images/"), "little-logo.jpg");
            //Image piLogo = Image.FromFile(logo);
            using (var package = new ExcelPackage(new FileInfo(file)))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("MostLovedProducts-" + DateTime.Now.ToString("dd/MM/yyyy"));
                Image myImage = Image.FromFile(logo);
                var pic = worksheet.Drawings.AddPicture("Logo", myImage);
                pic.SetPosition(0, 0, 0, 0);
                worksheet.Cells["C2"].Value = "ITEMS UPLOADED by BRAND";
                worksheet.Cells["C2"].Style.Font.SetFromFont(new Font("Arial", 14));
                worksheet.Cells[6, 1].Value = "Date";
                worksheet.Cells[6, 2].Value = rangedate;
                worksheet.Cells[7, 1].Value = "No.";
                worksheet.Cells[7, 2].Value = "Date Added";
                worksheet.Cells[7, 3].Value = "Brand";
                worksheet.Cells[7, 4].Value = "Count";

                IEnumerable<_object.productUploadByBrand> u;
                u = obj.ItemsUploadedByBrand();
                var i = 1;

                if (ds != "" && de != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    foreach (var item in u.GroupBy(p => p.tenant).Select(g => new
                    {
                        tenant = (int)g.Key,
                        count = g.Count(),
                        created_date = g.First().created_date
                    })
                    .Where(p => (Convert.ToDateTime(p.created_date).Date >= dateStart.Date)
                    && (Convert.ToDateTime(p.created_date).Date <= dateEnd.Date)
                    ).OrderByDescending(y=>y.created_date))
                    {
                        var tenant = obj.GetTenant(item.tenant).FirstOrDefault();
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = Convert.ToDateTime(item.created_date).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 3].Value = tenant.name.ToUpper();
                        worksheet.Cells[i + 7, 4].Value = item.count;
                        i++;
                    }
                }
                else
                {
                    foreach (var item in u.GroupBy(p => p.tenant).Select(g => new
                    {
                        tenant = (int)g.Key,
                        count = g.Count(),
                        created_date = g.First().created_date
                    }).OrderByDescending(y => y.created_date))
                    {
                        var tenant = obj.GetTenant(item.tenant).FirstOrDefault();
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = Convert.ToDateTime(item.created_date).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 3].Value = tenant.name.ToUpper();
                        worksheet.Cells[i + 7, 4].Value = item.count;
                        i++;
                    }
                }

                using (var range = worksheet.Cells["A7:D7"])
                {
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                    range.Style.Border.Bottom.Color.SetColor(Color.Black);
                }
                worksheet.Column(1).AutoFit();
                worksheet.Column(2).AutoFit();
                worksheet.Column(3).AutoFit();

                var memoryStream = new MemoryStream(package.GetAsByteArray());
                //var fileName = string.Format("MyData-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", DateTime.UtcNow);                
                return base.File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);

            }
            return RedirectToAction("BrandItemsUpload");
        }

        public ActionResult ExportCustomerRegistration()
        {
            var de = Request.Form["dateEnd"];
            var ds = Request.Form["dateStart"];
            string rangedate = "ALL DATE";
            DateTime dateStart;
            DateTime dateEnd;
            if (de != "" && ds != "")
            {
                rangedate = ds + " to " + de;
            }
            var fileName = "CustomerDatabaseRegistration-" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".xls";
            var outputDir = "~/Public/Files/";
            //var file = new FileInfo(outputDir + fileName);
            var file = Path.Combine(Server.MapPath(outputDir), fileName);
            var logo = Path.Combine(Server.MapPath("~/Images/"), "little-logo.jpg");
            //Image piLogo = Image.FromFile(logo);
            using (var package = new ExcelPackage(new FileInfo(file)))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("CustomerRegistration-" + DateTime.Now.ToString("dd/MM/yyyy"));
                Image myImage = Image.FromFile(logo);
                var pic = worksheet.Drawings.AddPicture("Logo", myImage);
                pic.SetPosition(0, 0, 0, 0);
                worksheet.Cells["C2"].Value = "CUSTOMER DATABASE REGISTRATION";
                worksheet.Cells["C2"].Style.Font.SetFromFont(new Font("Arial", 14));
                worksheet.Cells[6, 1].Value = "Date";
                worksheet.Cells[6, 2].Value = rangedate;
                worksheet.Cells[7, 1].Value = "No.";
                worksheet.Cells[7, 2].Value = "Sign Up Date";
                worksheet.Cells[7, 3].Value = "Customer Name";
                worksheet.Cells[7, 4].Value = "Phone Number";
                worksheet.Cells[7, 5].Value = "Email";
                worksheet.Cells[7, 6].Value = "Gender";
                worksheet.Cells[7, 7].Value = "Birthday";

                IEnumerable<DynamicContent.Models.tbl_user_register> c;
                var i = 1;
                if (ds != "" && de != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddDays(1);

                    var rs = (from u in db.tbl_user_register
                              select u);
                    
                    foreach (var item in (rs as IEnumerable<DynamicContent.Models.tbl_user_register>)
                    .Where(p => (Convert.ToDateTime(p.regist_date).Date >= dateStart.Date)
                    && (Convert.ToDateTime(p.regist_date).Date <= dateEnd.Date))
                    .OrderByDescending(y=>y.regist_date))
                    {
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = Convert.ToDateTime(item.regist_date).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 3].Value = item.firstName + " " + item.lastName;
                        worksheet.Cells[i + 7, 4].Value = item.phone;
                        worksheet.Cells[i + 7, 5].Value = item.email;
                        worksheet.Cells[i + 7, 6].Value = item.gender;
                        worksheet.Cells[i + 7, 7].Value = item.dob;
                        i++;
                    }
                }
                else
                {
                    c = from u in db.tbl_user_register
                                orderby u.regist_date descending
                        select u;
                    foreach (var item in c)
                    {
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = Convert.ToDateTime(item.regist_date).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 3].Value = item.firstName + " " + item.lastName;
                        worksheet.Cells[i + 7, 4].Value = item.phone;
                        worksheet.Cells[i + 7, 5].Value = item.email;
                        worksheet.Cells[i + 7, 6].Value = item.gender;
                        worksheet.Cells[i + 7, 7].Value = item.dob;
                        i++;
                    }
                }

                
                

                using (var range = worksheet.Cells["A7:G7"])
                {
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                    range.Style.Border.Bottom.Color.SetColor(Color.Black);
                }
                worksheet.Column(1).AutoFit();
                worksheet.Column(2).AutoFit();
                worksheet.Column(3).AutoFit();
                worksheet.Column(4).AutoFit();
                worksheet.Column(5).AutoFit();
                worksheet.Column(6).AutoFit();
                worksheet.Column(7).AutoFit();

                var memoryStream = new MemoryStream(package.GetAsByteArray());
                //var fileName = string.Format("MyData-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", DateTime.UtcNow);                
                return base.File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);


            }
            return RedirectToAction("CustomerRegistration");
        }

        public ActionResult ExportLeastViewedItems()
        {
            var de = Request.Form["dateEnd"];
            var ds = Request.Form["dateStart"];
            var s = Request.Form["filter"];
            string status = "ALL STATUS";
            string rangedate = "ALL DATE";
            bool stt = true;
            DateTime dateStart;
            DateTime dateEnd;
            if (s != "")
            {
                stt = (s == "True") ? true : false;
                status = (s == "True") ? "ACTIVE" : "INACTIVE";
            }
            if (de != "" && ds != "")
            {
                rangedate = ds + " to " + de;
            }
            var fileName = "LeastViewedItems-" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".xls";
            var outputDir = "~/Public/Files/";            
            var file = Path.Combine(Server.MapPath(outputDir), fileName);
            var logo = Path.Combine(Server.MapPath("~/Images/"), "little-logo.jpg");
            using (var package = new ExcelPackage(new FileInfo(file)))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("LeastViewedItems-" + DateTime.Now.ToString("dd/MM/yyyy"));
                Image myImage = Image.FromFile(logo);
                var pic = worksheet.Drawings.AddPicture("Logo", myImage);
                pic.SetPosition(0, 0, 0, 0);
                worksheet.Cells["C2"].Value = "LEAST VIEWED ITEMS";
                worksheet.Cells["C2"].Style.Font.SetFromFont(new Font("Arial", 14));
                worksheet.Cells[5, 1].Value = "Status";
                worksheet.Cells[6, 1].Value = "Date";
                worksheet.Cells[5, 2].Value = status;
                worksheet.Cells[6, 2].Value = rangedate;
                worksheet.Cells[7, 1].Value = "No.";
                worksheet.Cells[7, 2].Value = "Brand";
                worksheet.Cells[7, 3].Value = "Product Name";
                worksheet.Cells[7, 4].Value = "Count";
                worksheet.Cells[7, 5].Value = "Date Added";
                worksheet.Cells[7, 6].Value = "Status";

                IEnumerable<_object.viewedProduct> v;
                v = obj.GetMostViewedProductSysManager();               
                    
                var i = 1;
                if (ds != "" && de != "" && s != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59).AddSeconds(59);
                    foreach (var item in v
                        .Where(p => (Convert.ToDateTime(p.created_date).Date >= dateStart.Date)
                        && (Convert.ToDateTime(p.created_date).Date <= dateEnd.Date) && p.status == stt)
                        .GroupBy(p => p.id)
                    .Select(g => new
                    {
                        prodId = g.Key,
                        count = g.Count(),
                        created_date = g.First().created_date,
                        prod_created_date = g.First().prod_created_date,
                        p_status = g.First().status
                    })
                    .OrderBy(y => y.count))
                    {
                        //if (item.count < 10)
                        //{
                            var prod = obj.GetProductViewed(item.prodId).FirstOrDefault();
                            worksheet.Cells[i + 7, 1].Value = i;
                            worksheet.Cells[i + 7, 2].Value = prod.tenant.ToUpper();
                            worksheet.Cells[i + 7, 3].Value = prod.name.ToUpper();
                            worksheet.Cells[i + 7, 4].Value = item.count;
                            worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(item.prod_created_date).ToString("dd-MM-yyyy");
                            worksheet.Cells[i + 7, 6].Value = (prod.status) ? "ACTIVE" : "INACTIVE";
                            i++;
                        //}
                    }
                }
                else if (ds != "" && de != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59).AddSeconds(59);
                   
                    foreach (var item in v
                        .Where(p => (Convert.ToDateTime(p.created_date).Date >= dateStart.Date)
                        && ( Convert.ToDateTime(p.created_date).Date <= dateEnd.Date))                        
                        .GroupBy(p => p.id)
                        .Select(g => new
                        {
                            prodId = g.Key,
                            count = g.Count(),
                            created_date = g.First().created_date,
                            prod_created_date = g.First().prod_created_date,
                            p_status = g.First().status
                        })
                        .OrderBy(y => y.count))
                        {
                            //if (item.count < 10)
                            //{
                                var prod = obj.GetProductViewed(item.prodId).FirstOrDefault();
                                worksheet.Cells[i + 7, 1].Value = i;
                                worksheet.Cells[i + 7, 2].Value = prod.tenant.ToUpper();
                                worksheet.Cells[i + 7, 3].Value = prod.name.ToUpper();
                                worksheet.Cells[i + 7, 4].Value = item.count;
                                worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(item.prod_created_date).ToString("dd-MM-yyyy");
                                worksheet.Cells[i + 7, 6].Value = (prod.status) ? "ACTIVE" : "INACTIVE";
                                i++;
                            //}
                        }
                }
                else
                {
                    if (s != "")
                    {
                        v = obj.GetMostViewedProductSysManager()
                        .Where(p => p.status == stt);
                    }
                    else
                    {
                        v = obj.GetMostViewedProductSysManager();
                    }
                    foreach (var item in v.GroupBy(p => p.id)
                   .Select(g => new
                   {
                       prodId = g.Key,
                       count = g.Count(),
                       created_date = g.First().created_date,
                       prod_created_date = g.First().prod_created_date,
                       p_status = g.First().status
                   })
                   .OrderBy(y => y.count)
                   )
                    {
                        //if (item.count < 10)
                        //{
                            var prod = obj.GetProductViewed(item.prodId).FirstOrDefault();
                            worksheet.Cells[i + 7, 1].Value = i;
                            worksheet.Cells[i + 7, 2].Value = prod.tenant.ToUpper();
                            worksheet.Cells[i + 7, 3].Value = prod.name.ToUpper();
                            worksheet.Cells[i + 7, 4].Value = item.count;
                            worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(item.prod_created_date).ToString("dd-MM-yyyy");
                            worksheet.Cells[i + 7, 6].Value = (prod.status) ? "ACTIVE" : "INACTIVE";
                            i++;
                        //}
                    }
                }



                using (var range = worksheet.Cells["A7:F7"])
                {
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                    range.Style.Border.Bottom.Color.SetColor(Color.Black);
                }
                worksheet.Column(1).AutoFit();
                worksheet.Column(2).AutoFit();
                worksheet.Column(3).AutoFit();
                worksheet.Column(4).AutoFit();
                worksheet.Column(5).AutoFit();
                worksheet.Column(6).AutoFit();
                var memoryStream = new MemoryStream(package.GetAsByteArray());
                //var fileName = string.Format("MyData-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", DateTime.UtcNow);                
                return base.File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);

            }
            return RedirectToAction("LeastViewedItems");
        }

        public ActionResult ExportMostViewedItems()
        {
            var de = Request.Form["dateEnd"];
            var ds = Request.Form["dateStart"];
            var s = Request.Form["filter"];
            string status = "ALL STATUS";
            string rangedate = "ALL DATE";
            bool stt = true;
            DateTime dateStart;
            DateTime dateEnd;
            if (s != "")
            {
                stt = (s == "True") ? true : false;
                status = (s == "True") ? "ACTIVE" : "INACTIVE";
            }
            if (de != "" && ds != "")
            {
                rangedate = ds + " to " + de;
            }
            var fileName = "MostViewedItemsPI-" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".xls";
            var outputDir = "~/Public/Files/";
            var file = Path.Combine(Server.MapPath(outputDir), fileName);
            var logo = Path.Combine(Server.MapPath("~/Images/"), "little-logo.jpg");
            using (var package = new ExcelPackage(new FileInfo(file)))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("MostViewedItemsPI-" + DateTime.Now.ToString("dd/MM/yyyy"));
                Image myImage = Image.FromFile(logo);
                var pic = worksheet.Drawings.AddPicture("Logo", myImage);
                pic.SetPosition(0, 0, 0, 0);
                worksheet.Cells["C2"].Value = "MOST VIEWED ITEMS";
                worksheet.Cells["C2"].Style.Font.SetFromFont(new Font("Arial", 14));
                worksheet.Cells[5, 1].Value = "Status";
                worksheet.Cells[6, 1].Value = "Date";
                worksheet.Cells[5, 2].Value = status;
                worksheet.Cells[6, 2].Value = rangedate;
                worksheet.Cells[7, 1].Value = "No.";
                worksheet.Cells[7, 2].Value = "Brand";
                worksheet.Cells[7, 3].Value = "Product Name";
                worksheet.Cells[7, 4].Value = "Count";
                worksheet.Cells[7, 5].Value = "Date Added";
                worksheet.Cells[7, 6].Value = "Status";

                IEnumerable<_object.viewedProduct> v;
                v = obj.GetMostViewedProductSysManager();

                var i = 1;
                if (ds != "" && de != "" && s != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59).AddSeconds(59);
                    foreach (var item in v
                        .Where(p => (Convert.ToDateTime(p.created_date).Date >= dateStart.Date)
                        && (Convert.ToDateTime(p.created_date).Date <= dateEnd.Date) && p.status == stt)
                        .GroupBy(p => p.id)
                    .Select(g => new
                    {
                        prodId = g.Key,
                        count = g.Count(),
                        created_date = g.First().created_date,
                        prod_created_date = g.First().prod_created_date,
                        p_status = g.First().status
                    })
                    .OrderByDescending(y=>y.count))
                    {
                        //if (item.count > 10)
                        //{
                            var prod = obj.GetProductViewed(item.prodId).FirstOrDefault();
                            worksheet.Cells[i + 7, 1].Value = i;
                            worksheet.Cells[i + 7, 2].Value = prod.tenant.ToUpper();
                            worksheet.Cells[i + 7, 3].Value = prod.name.ToUpper();
                            worksheet.Cells[i + 7, 4].Value = item.count;
                            worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(item.prod_created_date).ToString("dd-MM-yyyy");
                            worksheet.Cells[i + 7, 6].Value = (prod.status) ? "ACTIVE" : "INACTIVE";
                            i++;
                        //}
                    }
                }
                else if (ds != "" && de != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59).AddSeconds(59);
                    foreach (var item in v
                        .Where(p => (Convert.ToDateTime(p.created_date).Date >= dateStart.Date)
                        && (Convert.ToDateTime(p.created_date).Date <= dateEnd.Date))
                        .GroupBy(p => p.id)
                    .Select(g => new
                    {
                        prodId = g.Key,
                        count = g.Count(),
                        created_date = g.First().created_date,
                        prod_created_date = g.First().prod_created_date,
                        p_status = g.First().status
                    })                    
                        .OrderByDescending(y => y.count))
                    {
                        //if (item.count > 10)
                        //{
                            var prod = obj.GetProductViewed(item.prodId).FirstOrDefault();
                            worksheet.Cells[i + 7, 1].Value = i;
                            worksheet.Cells[i + 7, 2].Value = prod.tenant.ToUpper();
                            worksheet.Cells[i + 7, 3].Value = prod.name.ToUpper();
                            worksheet.Cells[i + 7, 4].Value = item.count;
                            worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(item.prod_created_date).ToString("dd-MM-yyyy");
                            worksheet.Cells[i + 7, 6].Value = (prod.status) ? "ACTIVE" : "INACTIVE";
                            i++;
                        //}
                    }
                }
                else
                {
                    if (s != "")
                    {
                        v = obj.GetMostViewedProductSysManager()
                        .Where(p => p.status == stt);
                    }
                    else
                    {
                        v = obj.GetMostViewedProductSysManager();
                    }
                    foreach (var item in v.GroupBy(p => p.id)
                   .Select(g => new
                   {
                       prodId = g.Key,
                       count = g.Count(),
                       created_date = g.First().created_date,
                       prod_created_date = g.First().prod_created_date,
                       p_status = g.First().status
                   })
                   .OrderByDescending(y => y.count)
                   )
                    {
                        //if (item.count > 10)
                        //{
                            var prod = obj.GetProductViewed(item.prodId).FirstOrDefault();
                            worksheet.Cells[i + 7, 1].Value = i;
                            worksheet.Cells[i + 7, 2].Value = prod.tenant.ToUpper();
                            worksheet.Cells[i + 7, 3].Value = prod.name.ToUpper();
                            worksheet.Cells[i + 7, 4].Value = item.count;
                            worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(item.prod_created_date).ToString("dd-MM-yyyy");
                            worksheet.Cells[i + 7, 6].Value = (prod.status) ? "ACTIVE" : "INACTIVE";
                            i++;
                        //}
                    }
                }
                using (var range = worksheet.Cells["A7:F7"])
                {
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                    range.Style.Border.Bottom.Color.SetColor(Color.Black);
                }
                worksheet.Column(1).AutoFit();
                worksheet.Column(2).AutoFit();
                worksheet.Column(3).AutoFit();
                worksheet.Column(4).AutoFit();
                worksheet.Column(5).AutoFit();
                worksheet.Column(6).AutoFit();

                var memoryStream = new MemoryStream(package.GetAsByteArray());
                //var fileName = string.Format("MyData-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", DateTime.UtcNow);                
                return base.File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);

            }
            return RedirectToAction("MostViewedItems");
        }

        public ActionResult ExportMostLovedItems()
        {
            var de = Request.Form["dateEnd"];
            var ds = Request.Form["dateStart"];
            var s = Request.Form["filter"];
            string status = "ALL STATUS";
            string rangedate = "ALL DATE";
            bool stt = true;
            DateTime dateStart;
            DateTime dateEnd;
            if (s != "")
            {
                stt = (s == "True") ? true : false;
                status = (s == "True") ? "ACTIVE" : "INACTIVE";
            }
            if (de != "" && ds != "")
            {
                rangedate = ds + " to " + de;
            }
            var fileName = "MostLovedItemsPI-" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".xls";
            var outputDir = "~/Public/Files/";
            var file = Path.Combine(Server.MapPath(outputDir), fileName);
            var logo = Path.Combine(Server.MapPath("~/Images/"), "little-logo.jpg");
            using (var package = new ExcelPackage(new FileInfo(file)))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("MostLovedItemsPI-" + DateTime.Now.ToString("dd/MM/yyyy"));
                Image myImage = Image.FromFile(logo);
                var pic = worksheet.Drawings.AddPicture("Logo", myImage);
                pic.SetPosition(0, 0, 0, 0);
                worksheet.Cells["C2"].Value = "MOST LOVED ITEMS";
                worksheet.Cells["C2"].Style.Font.SetFromFont(new Font("Arial", 14));
                worksheet.Cells[5, 1].Value = "Status";
                worksheet.Cells[6, 1].Value = "Date";
                worksheet.Cells[5, 2].Value = status;
                worksheet.Cells[6, 2].Value = rangedate;
                worksheet.Cells[7, 1].Value = "No.";
                worksheet.Cells[7, 2].Value = "Brand";
                worksheet.Cells[7, 3].Value = "Product Name";
                worksheet.Cells[7, 4].Value = "Count";
                worksheet.Cells[7, 5].Value = "Date Added";
                worksheet.Cells[7, 6].Value = "Status";

                IEnumerable<_object.lovedProduct> w;
                w = obj.GetMostLovedProductSysManager();

                var i = 1;

                if (ds != "" && de != "" && s != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59).AddSeconds(59);
                    foreach (var item in w
                        .Where(p => (Convert.ToDateTime(p.created_date).Date >= dateStart.Date)
                        && (Convert.ToDateTime(p.created_date).Date <= dateEnd.Date) && p.status == stt)
                        .GroupBy(p => p.id).Select(g => new
                    {
                        prodId = g.Key,
                        count = g.Count(),
                        created_date = g.First().created_date,
                        prod_created_date = g.First().prod_created_date,
                        p_status = g.First().status
                    }).OrderByDescending(y => y.count))
                    {
                        var prod = obj.GetProductWishlist(item.prodId).FirstOrDefault();
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = prod.tenant.ToUpper();
                        worksheet.Cells[i + 7, 3].Value = prod.name.ToUpper();
                        worksheet.Cells[i + 7, 4].Value = item.count;
                        worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(item.prod_created_date).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 6].Value = (prod.status) ? "ACTIVE" : "INACTIVE";

                        i++;
                    }
                }
                else if (ds != "" && de != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59).AddSeconds(59);
                    foreach (var item in w
                        .Where(p => (Convert.ToDateTime(p.created_date).Date >= dateStart.Date)
                    && (Convert.ToDateTime(p.created_date).Date <= dateEnd.Date))
                        .GroupBy(p => p.id).Select(g => new
                    {
                        prodId = g.Key,
                        count = g.Count(),
                        created_date = g.First().created_date,
                        prod_created_date = g.First().prod_created_date,
                        p_status = g.First().status
                    }).OrderByDescending(y => y.count))
                    {
                        var prod = obj.GetProductWishlist(item.prodId).FirstOrDefault();
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = prod.tenant.ToUpper();
                        worksheet.Cells[i + 7, 3].Value = prod.name.ToUpper();
                        worksheet.Cells[i + 7, 4].Value = item.count;
                        worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(item.prod_created_date).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 6].Value = (prod.status) ? "ACTIVE" : "INACTIVE";

                        i++;
                    }
                }
                else
                {
                    if (s != "")
                    {
                        w = obj.GetMostLovedProductSysManager()
                        .Where(p => p.status == stt);
                    }
                    else
                    {
                        w = obj.GetMostLovedProductSysManager();
                    }

                    foreach (var item in w.GroupBy(p => p.id).Select(g => new
                    {
                        prodId = g.Key,
                        count = g.Count(),
                        created_date = g.First().created_date,
                        prod_created_date = g.First().prod_created_date,
                        p_status = g.First().status
                    })
                    .OrderByDescending(y => y.count)
                    )
                    {
                        var prod = obj.GetProductWishlist(item.prodId).FirstOrDefault();
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = prod.tenant.ToUpper();
                        worksheet.Cells[i + 7, 3].Value = prod.name.ToUpper();
                        worksheet.Cells[i + 7, 4].Value = item.count;
                        worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(item.prod_created_date).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 6].Value = (prod.status) ? "ACTIVE" : "INACTIVE";

                        i++;
                    }
                    
                }
                using (var range = worksheet.Cells["A7:F7"])
                {
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                    range.Style.Border.Bottom.Color.SetColor(Color.Black);
                }

                worksheet.Column(1).AutoFit();
                worksheet.Column(2).AutoFit();
                worksheet.Column(3).AutoFit();
                worksheet.Column(4).AutoFit();
                worksheet.Column(5).AutoFit();
                worksheet.Column(6).AutoFit();
                var memoryStream = new MemoryStream(package.GetAsByteArray());
                //var fileName = string.Format("MyData-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", DateTime.UtcNow);                
                return base.File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);

            }
            return RedirectToAction("MostLovedItems");
        }

        public ActionResult ExportMostReservedItems()
        {
            var de = Request.Form["dateEnd"];
            var ds = Request.Form["dateStart"];
            var s = Request.Form["filter"];
            string status = "ALL STATUS";
            string rangedate = "ALL DATE";
            bool stt = true;
            DateTime dateStart;
            DateTime dateEnd;
            if (s != "")
            {
                stt = (s == "True") ? true : false;
                status = (s == "True") ? "ACTIVE" : "INACTIVE";
            }
            if (de != "" && ds != "")
            {
                rangedate = ds + " to " + de;
            }
            var fileName = "MostMostReservedItemsPI-" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".xls";
            var outputDir = "~/Public/Files/";
            var file = Path.Combine(Server.MapPath(outputDir), fileName);
            var logo = Path.Combine(Server.MapPath("~/Images/"), "little-logo.jpg");
            using (var package = new ExcelPackage(new FileInfo(file)))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("MostReservedItemsPI-" + DateTime.Now.ToString("dd/MM/yyyy"));
                Image myImage = Image.FromFile(logo);
                var pic = worksheet.Drawings.AddPicture("Logo", myImage);
                pic.SetPosition(0, 0, 0, 0);
                worksheet.Cells["C2"].Value = "MOST RESERVED ITEMS";
                worksheet.Cells["C2"].Style.Font.SetFromFont(new Font("Arial", 14));
                worksheet.Cells[5, 1].Value = "Status";
                worksheet.Cells[6, 1].Value = "Date";
                worksheet.Cells[5, 2].Value = status;
                worksheet.Cells[6, 2].Value = rangedate;
                worksheet.Cells[7, 1].Value = "No.";
                worksheet.Cells[7, 2].Value = "Brand";
                worksheet.Cells[7, 3].Value = "Product Name";
                worksheet.Cells[7, 4].Value = "Qty";
                worksheet.Cells[7, 5].Value = "Date Added";
                worksheet.Cells[7, 6].Value = "Status";

                IEnumerable<_object.reservedProduct> r;
                var i = 1;
                r = obj.GetMostReservedProductSysManager();
                if (ds != "" && de != "" && s != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59).AddSeconds(59);
                    foreach (var item in r
                        .Where(p => (Convert.ToDateTime(p.created_date).Date >= dateStart.Date)
                    && (Convert.ToDateTime(p.created_date).Date <= dateEnd.Date) && p.status == stt)
                        .GroupBy(p => p.id).Select(g => new _object.reservedProduct
                    {
                        name = g.First().name,
                        tenant = g.First().tenant,
                        qty = g.Sum(c => c.qty),
                        created_date = g.First().created_date,
                        prod_created_date = g.First().prod_created_date,
                        status = g.First().status
                    }).OrderByDescending(y => y.qty))
                    {
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = item.tenant.ToUpper();
                        worksheet.Cells[i + 7, 3].Value = item.name.ToUpper();
                        worksheet.Cells[i + 7, 4].Value = item.qty;
                        worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(item.prod_created_date).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 6].Value = (item.status) ? "ACTIVE" : "INACTIVE";

                        i++;
                    }
                }
                else if (ds != "" && de != "")
                {
                    dateStart = DateTime.ParseExact(ds, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    dateEnd = DateTime.ParseExact(de, "dd-MM-yyyy", CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59).AddSeconds(59);
                    foreach (var item in r
                        .Where(p => (Convert.ToDateTime(p.created_date).Date >= dateStart.Date)
                        && (Convert.ToDateTime(p.created_date).Date <= dateEnd.Date))
                        .GroupBy(p => p.id).Select(g => new _object.reservedProduct
                    {
                        name = g.First().name,
                        tenant = g.First().tenant,
                        qty = g.Sum(c => c.qty),
                        created_date = g.First().created_date,
                        prod_created_date = g.First().prod_created_date,
                        status = g.First().status
                    }).OrderByDescending(y => y.qty))
                    {
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = item.tenant.ToUpper();
                        worksheet.Cells[i + 7, 3].Value = item.name.ToUpper();
                        worksheet.Cells[i + 7, 4].Value = item.qty;
                        worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(item.prod_created_date).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 6].Value = (item.status) ? "ACTIVE" : "INACTIVE";

                        i++;
                    }
                }
                else
                {
                    if (s != "")
                    {
                        r = obj.GetMostReservedProductSysManager()
                        .Where(p => p.status == stt);
                    }
                    else
                    {
                        r = obj.GetMostReservedProductSysManager();
                    }
                    foreach (var item in r.GroupBy(p => p.id).Select(g => new _object.reservedProduct
                    {
                        name = g.First().name,
                        tenant = g.First().tenant,
                        qty = g.Sum(c => c.qty),
                        created_date = g.First().created_date,
                        prod_created_date = g.First().prod_created_date,
                        status = g.First().status
                    })
                    .OrderByDescending(y => y.qty)
                    )
                    {
                        worksheet.Cells[i + 7, 1].Value = i;
                        worksheet.Cells[i + 7, 2].Value = item.tenant.ToUpper();
                        worksheet.Cells[i + 7, 3].Value = item.name.ToUpper();
                        worksheet.Cells[i + 7, 4].Value = item.qty;
                        worksheet.Cells[i + 7, 5].Value = Convert.ToDateTime(item.prod_created_date).ToString("dd-MM-yyyy");
                        worksheet.Cells[i + 7, 6].Value = (item.status) ? "ACTIVE" : "INACTIVE";

                        i++;
                    }
                    
                } 
                using (var range = worksheet.Cells["A7:F7"])
                {
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                    range.Style.Border.Bottom.Color.SetColor(Color.Black);
                }

                worksheet.Column(1).AutoFit();
                worksheet.Column(2).AutoFit();
                worksheet.Column(3).AutoFit();
                worksheet.Column(4).AutoFit();
                worksheet.Column(5).AutoFit();
                worksheet.Column(6).AutoFit();
                var memoryStream = new MemoryStream(package.GetAsByteArray());
                //var fileName = string.Format("MyData-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", DateTime.UtcNow);                
                return base.File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);

            }
            return RedirectToAction("MostReservedItems");
        }
    }
}
