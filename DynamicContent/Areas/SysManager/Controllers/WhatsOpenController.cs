﻿using DynamicContent.Areas.SysManager.Helper;
using DynamicContent.Areas.SysManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DynamicContent.Helper;
using System.IO;

namespace DynamicContent.Areas.SysManager.Controllers
{
    public class WhatsOpenController : AuthenticatedController
    {
        
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        public ActionResult Index()
        {
            var p = obj.GetProductPi().OrderBy(x=>x.isOrder);
            ViewData["product"] = p;
            return View();
        }

        public ActionResult Sort(int id, int to)
        {
            var pAll = obj.GetProductPi().Count();
            int page = to/10;
            if (to % 10 == 0)
            {
                page = page - 1;
            }

            DynamicContent.Models.tbl_product prod = (from p in db.tbl_product where p.ID == id select p).FirstOrDefault();

            if (prod != null)
            {
                bool isMore = false;
                prod.modified_by = Session_.Username;
                prod.modified_date = DateTime.Now;
                isMore = to > prod.isOrder;
                prod.isOrder = to;
                db.Entry(prod).CurrentValues.SetValues(prod);
                db.SaveChanges();

                List<DynamicContent.Models.tbl_product> prods;
                if (isMore)
                {
                    prods = (from p in db.tbl_product orderby p.isOrder ascending, p.modified_date ascending select p).ToList();
                }
                else
                {
                    prods = (from p in db.tbl_product orderby p.isOrder ascending, p.modified_date descending select p).ToList();
                }

                var i = 1;
                foreach (var item in prods)
                {
                    item.isOrder = i;
                    i++;
                }

                db.SaveChanges();
                //TempData["msg"] = "success: Sorting Successfully";
            }
            else
            {
                //TempData["msg"] = "error: Sorting Error";
            }

            return RedirectToAction("Index", new { page = page});
        }
        public ActionResult ChangeActive(int id)
        {
            DynamicContent.Models.tbl_product prod = (from p in db.tbl_product where p.ID == id select p).FirstOrDefault();
            var to = prod.isOrder;
            int page = Convert.ToInt32(to) / 10;
            if (to % 10 == 0)
            {
                page = page - 1;
            }
            if (prod != null)
            {
                prod.isHomepage = !prod.isHomepage;
                prod.modified_by = Session_.Username;
                prod.modified_date = DateTime.Now;

                db.Entry(prod).CurrentValues.SetValues(prod);
                db.SaveChanges();

                //TempData["msg"] = "success: Successfully change active";
            }
            else
            {
                //TempData["msg"] = "error: Error change active";
            }
            return RedirectToAction("Index", new { page = page });

        }
        

        public virtual ActionResult Edit()
        {
            var sld = (from s in db.MailMaster where s.MailDesc == "HomepageTitle" select s);
            var text = ((sld != null && sld.Count() > 0) ? sld.FirstOrDefault() : null);

            ViewData["homeText"] = text;
            var textVM = new HomeTextViewModel();
            if (text != null)
            {
                textVM.ID = text.MailID;
                textVM.name = text.ProfileName;
            }
                return View("~/Areas/SysManager/Views/WhatsOpen/Edit.cshtml", textVM);
        }

        [ValidateInput(false)]
        [HttpPost]
        public virtual ActionResult Edit(HomeTextViewModel textVM)
        {
            var sld = (from s in db.MailMaster where s.MailDesc == "HomepageTitle" select s);
            var text = ((sld != null && sld.Count() > 0) ? sld.FirstOrDefault() : null);

            text.ProfileName = textVM.name;            
            db.SaveChanges();

            return RedirectToAction("Edit", "WhatsOpen", new { area = "SysManager" });
        }
        
    }
}
