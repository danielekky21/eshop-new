﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DynamicContent.Helper;
using DynamicContent.Areas.SysManager.ViewModels;
using DynamicContent.Areas.SysManager.Helper;

namespace DynamicContent.Areas.SysManager.Controllers
{
    public class FlashSaleController : Controller
    {
        //
        // GET: /SysManager/FlashSale/
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult FlashSaleDataHandler(DTParameters dtParam)
        {
            try
            {
                var flashSaleData = (from f in db.FlashSaleHeaders
                                     select new _object.FlashSaleHeader
                                     {
                                         id = f.FlashSaleHeaderID,
                                         Name = f.FlashSaleName,
                                         Day = f.FlashSaleDay,
                                         Start = f.FlashSaleTimeBegin.Value.ToString(),
                                         End = f.FlashSaleTimeEnd.ToString(),
                                         Active = f.IsActive

                                     });
            
            List<String> columnSearch = new List<string>();

            foreach (var col in dtParam.Columns)
            {
                columnSearch.Add(col.Search.Value);
            }

            var data = new ResultSet().GetFlashSaleHeaderResult(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, flashSaleData.ToList(), columnSearch);
            int count = new ResultSet().CountFlashHeader(dtParam.Search.Value, flashSaleData.ToList(), columnSearch);
            var result = new DTResult<DynamicContent.Helper._object.FlashSaleHeader>
            {
                draw = dtParam.Draw,
                data = data,
                recordsFiltered = count,
                recordsTotal = count
            };
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }
        public virtual ActionResult FlashSaleHeaderAddEdit(Guid ID)
        {
            var result = dbHelper.getFlashSaleHeaderByID(ID);
            if (result != null)
            {
                ViewData.Model = result;
          
            }
            else
            {
                ViewData.Model = new FlashSaleHeaderViewModel();
            }

            return View();
        }
        public virtual JsonResult FlashSaleSetEnableDisable(string id, bool val)
        {
            try
            {
                Guid flashsaleid = Guid.Parse(id);
                var data = db.FlashSaleHeaders.FirstOrDefault(x => x.FlashSaleHeaderID == flashsaleid);
                if (data != null)
                {
                    data.IsActive = val;
                    data.UpdatedBy = Session_.Username;
                    data.UpdatedDate = DateTime.Now;
                    db.SaveChanges();
                }
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                return Json("NotOK", JsonRequestBehavior.AllowGet);
            }
         
        }
        [HttpPost]
        public virtual ActionResult FlashSaleHeaderAddEdit(FlashSaleHeaderViewModel model)
        {
            if (model.FlashSaleHeaderID != Guid.Empty)
            {
                //update
                var data = db.FlashSaleHeaders.FirstOrDefault(x => x.FlashSaleHeaderID == model.FlashSaleHeaderID);
                if (data != null)
                {
                    data.FlashSaleName = model.FlashSaleName;
                    data.FlashSaleDay = model.FlashSaleDay;
                    data.FlashSaleTimeBegin = model.FlashSaleTimeBegin;
                    data.FlashSaleTimeEnd = model.FlashSaleTimeEnd;
                    data.UpdatedBy = Session_.Username;
                    data.UpdatedDate = DateTime.Now;
                    db.SaveChanges();

                }
            }
            else
            {
                //insert
                var ids = Guid.NewGuid();
                DynamicContent.Models.FlashSaleHeader newData = new DynamicContent.Models.FlashSaleHeader();
                newData.FlashSaleHeaderID = ids;
                if (!string.IsNullOrEmpty(model.FlashSaleName))
                {
                    newData.FlashSaleName = model.FlashSaleName;
                }
                if (!string.IsNullOrEmpty(model.FlashSaleDay))
                {
                    newData.FlashSaleDay = model.FlashSaleDay;
                }
                if (!string.IsNullOrEmpty(model.FlashSaleTimeBegin.ToString()))
                {
                    newData.FlashSaleTimeBegin = model.FlashSaleTimeBegin;
                }
                if (!string.IsNullOrEmpty(model.FlashSaleTimeEnd.ToString()))
                {
                    newData.FlashSaleTimeEnd = model.FlashSaleTimeEnd;
                }
                newData.CreatedBy = Session_.Username;
                newData.CreatedDate = DateTime.Now;
                newData.IsActive = true;
                db.FlashSaleHeaders.Add(newData);
                db.SaveChanges();
            }
            return RedirectToAction("Index", "FlashSale", new { area = "SysManager" });
        }
        public virtual ActionResult flashHeaderDetail(string id)
        {
            ViewBag.headerid = id;
            Guid detailid = Guid.Parse(id);
            IQueryable<DynamicContent.Models.FlashHeaderDetail> detail = db.FlashHeaderDetails.Where(x => x.FlashSaleHeaderID == detailid && x.isActive == true).OrderByDescending(x => x.CreatedDate);
            ViewData.Model = detail.ToList();
            return View();
        }
        public virtual ActionResult FlashSaleDetailDelete(int id, string headerid)
        {
            var data = db.FlashHeaderDetails.FirstOrDefault(x => x.FlashSaleDetailID == id);
            if (data != null)
            {
                data.isActive = false;
                data.UpdateDate = DateTime.Now;
                data.UpdatedBy = Session_.Username;
                db.SaveChanges();
            }
            return RedirectToAction("flashHeaderDetail", "FlashSale", new { area = "SysManager", id = headerid });
        }
        public virtual ActionResult FlashSaleDetailAddEdit(int ID,string headerid)
        {
            var result = dbHelper.GetFlashSaleDetailByID(ID);
            if (result != null)
            {

                ViewData.Model = result;
                result.FlashSaleHeaderID = Guid.Parse(headerid);
                ViewBag.prodName = result.tbl_product.name;
                var imgProd = db.tbl_product_image.FirstOrDefault(x => x.product == result.ProductID);
                ViewBag.imgprod = imgProd.images;
            }
            else
            {
                ViewBag.prodName = "";
                DynamicContent.Models.FlashHeaderDetail newData = new DynamicContent.Models.FlashHeaderDetail();
                newData.FlashSaleHeaderID = Guid.Parse(headerid);
                ViewData.Model = newData;
                ViewBag.imgprod = string.Empty;
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult FlashSaleDetailAddEdit(DynamicContent.Models.FlashHeaderDetail model)
        {
            if (model.FlashSaleDetailID != 0)
            {
                //update
                var data = db.FlashHeaderDetails.FirstOrDefault(x => x.FlashSaleHeaderID == model.FlashSaleHeaderID && x.FlashSaleDetailID == model.FlashSaleDetailID);
                if (data != null)
                {
                    data.ProductID = model.ProductID;
                    data.qty = model.qty;
                    data.price_new = model.price_new;
                    data.UpdateDate = DateTime.Now;
                    data.UpdatedBy = Session_.Username;
                    db.SaveChanges();

                }
            }
            else
            {
                //insert
                DynamicContent.Models.FlashHeaderDetail newData = new DynamicContent.Models.FlashHeaderDetail();
                newData.ProductID = model.ProductID;
                newData.FlashSaleHeaderID = model.FlashSaleHeaderID;
                newData.qty = model.qty;
                newData.price_new = model.price_new;
                newData.CreatedBy = Session_.Username;
                newData.CreatedDate = DateTime.Now;
                newData.isActive = true;
                db.FlashHeaderDetails.Add(newData);
                db.SaveChanges();

            }
            return RedirectToAction("flashHeaderDetail", "FlashSale", new { area = "SysManager" , id = model.FlashSaleHeaderID });
        }
        public virtual JsonResult AutocompleteName(string prefix)
        {
            var product = db.tbl_product.Where(x => x.name.StartsWith(prefix) && x.status == true && x.approved == true).Select(x => new AutoCompleteProductViewModel()
            {
                name = x.name,
                ID = x.ID
            });
            return Json(product.ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}
