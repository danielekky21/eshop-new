﻿using DynamicContent.Areas.SysManager.Helper;
using DynamicContent.Areas.SysManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DynamicContent.Helper;
using System.IO;
using System.Text.RegularExpressions;

namespace DynamicContent.Areas.SysManager.Controllers
{
    public class ApprovalController : AuthenticatedController
    {
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ProductListDataHandler(DTParameters dtParam)
        {
            try
            {
                //throw new Exception();
                var products = obj.GetAllProductTmpWaitingApproval2();
                
                List < String > columnSearch = new List<string>();

                foreach (var col in dtParam.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                var data = new ResultSet().GetResult(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, products.ToList(), columnSearch);
                int count = new ResultSet().Count(dtParam.Search.Value, products.ToList(), columnSearch);
                var result = new DTResult<DynamicContent.Helper._object.AllProduct>
                {
                    draw = dtParam.Draw,
                    data = data,
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        public virtual ActionResult Approve(int id)
        {
            var prdTmp = (from p in db.tbl_product_tmp
                        where p.ID == id
                        select p).First();
            prdTmp.approved = true;
            prdTmp.approval_status =(int) Constant.approvalStatus.approved;
            prdTmp.approved_by = "system";
            prdTmp.approved_date = DateTime.Now;
            prdTmp.modified_date = DateTime.Now;

            var prd = (from p in db.tbl_product
                       where p.ID == id
                       select p).FirstOrDefault();
            
            if (prd == null)
            {
                db.tbl_product.Add(new DynamicContent.Models.tbl_product()
                {
                    tenant_category = prdTmp.tenant_category,
                    tenant_subcategory = prdTmp.tenant_subcategory,
                    tenant = prdTmp.tenant,
                    status = prdTmp.status,
                    size_fit = prdTmp.size_fit,
                    size = prdTmp.size,
                    price = prdTmp.price,
                    name = prdTmp.name,
                    modified_date = prdTmp.modified_date,
                    modified_by = prdTmp.modified_by,
                    images = prdTmp.images,
                    ID = prdTmp.ID,
                    gender = prdTmp.gender,
                    approval_status = prdTmp.approval_status,
                    approved = prdTmp.approved,
                    approved_by = prdTmp.approved_by,
                    approved_date = prdTmp.approved_date,
                    brand = prdTmp.brand,
                    category = prdTmp.category,
                    created_by = prdTmp.created_by,
                    created_date = prdTmp.created_date,
                    description = prdTmp.description,
                    isHomepage = false,
                    isOrder = 0
                });
                
            }
            else
            {
                
                prd.tenant_category = prdTmp.tenant_category;
                prd.tenant_subcategory = prdTmp.tenant_subcategory;
                prd.tenant = prdTmp.tenant;
                prd.status = prdTmp.status;
                prd.size_fit = prdTmp.size_fit;
                prd.size = prdTmp.size;
                prd.price = prdTmp.price;
                prd.name = prdTmp.name;
                prd.modified_date = prdTmp.modified_date;
                prd.modified_by = prdTmp.modified_by;
                prd.images = prdTmp.images;
                prd.gender = prdTmp.gender;
                prd.approval_status = prdTmp.approval_status;
                prd.approved = prdTmp.approved;
                prd.approved_by = prdTmp.approved_by;
                prd.approved_date = prdTmp.approved_date;
                prd.brand = prdTmp.brand;
                prd.category = prdTmp.category;
                prd.created_by = prdTmp.created_by;
                prd.created_date = prdTmp.created_date;
                prd.description = prdTmp.description;
            }

            var pathApproved = "~/Public/Approved";
            var pathTenant = pathApproved + "/" + prdTmp.tenant;
            if (!Directory.Exists(Server.MapPath(pathApproved)))
            {
                Directory.CreateDirectory(Server.MapPath(pathApproved));
            }

            if (!Directory.Exists(Server.MapPath(pathTenant)))
            {
                Directory.CreateDirectory(Server.MapPath(pathTenant));
            }




            var prd_clr_tmp = (from pc in db.tbl_product_color_tmp
                          where pc.product == prdTmp.ID
                          select pc).ToList();

            
            
            
            for(int i= 0; i < prd_clr_tmp.Count();i++)
            {
                if(i == 0)
                {
                    db.tbl_product_color.RemoveRange(db.tbl_product_color.Where(x => x.product == id));
                }

                var pcTempId = prd_clr_tmp[i].ID;
                var prd_img_tmp = (from pc in db.tbl_product_image_tmp
                                  where pc.color == pcTempId
                                  select pc).ToList();

                var product_images = db.tbl_product_image.Where(x => x.color == pcTempId);
                foreach (var img in product_images)
                {
                    System.IO.File.Delete(Server.MapPath("~" + HttpUtility.UrlDecode(img.images)));
                }

                db.tbl_product_image.RemoveRange(product_images);
                for(int j= 0; j < prd_img_tmp.Count(); j++)
                {
                    var serverImgPath = Server.MapPath("~" + HttpUtility.UrlDecode(prd_img_tmp[j].images));
                    var fileName = Regex.Replace(prdTmp.name.Replace(' ', '_'), "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled) + Guid.NewGuid() + Path.GetExtension(serverImgPath);

                    var newImgPath = Server.MapPath(pathTenant) + "/" + fileName;
                    if(System.IO.File.Exists(serverImgPath))
                        System.IO.File.Copy(serverImgPath, newImgPath, true);

                    db.tbl_product_image.Add(new DynamicContent.Models.tbl_product_image()
                    {
                        color = prd_img_tmp[j].color,
                        ID = prd_img_tmp[j].ID,
                        images = pathTenant.Replace("~","") + "/" + HttpUtility.UrlEncode(fileName),
                        product = prd_img_tmp[j].product
                        
                    });
                }

                db.tbl_product_color.Add(new DynamicContent.Models.tbl_product_color() {
                        color = prd_clr_tmp[i].color,
                       description = prd_clr_tmp[i].description,
                       ID = prd_clr_tmp[i].ID,
                       ids = prd_clr_tmp[i].ids,
                       product = prd_clr_tmp[i].product,
                       stock = prd_clr_tmp[i].stock
                });
            }
            
            db.SaveChanges();


            return RedirectToAction("Index", "Approval", new { area = "SysManager" });
        }

        public virtual ActionResult Rejected(int id, string msg)
        {
            var prd = (from p in db.tbl_product_tmp
                       where p.ID == id
                       select p).First();
            prd.approved = true;
            prd.approval_status = (int)Constant.approvalStatus.rejected;
            prd.approved_by = "system";
            prd.approved_date = DateTime.Now;
            prd.modified_date = DateTime.Now;
            prd.approval_message = msg;

            db.SaveChanges();
            return RedirectToAction("Index", "Approval", new { area = "SysManager" });
        }

        public virtual ActionResult ViewProduct(int id)
        {
            var prds = (from p in db.tbl_product_tmp
                        where p.ID == id
                        select p);

            var product = ((prds != null && prds.Count() > 0) ? prds.FirstOrDefault() : null);
            ViewData["Product"] = product;

            var productVM = new ProductViewModel();
            if (product != null)
            {
                productVM.productCategory = product.tenant_category;
                productVM.productSubcategory = product.tenant_subcategory;
                productVM.productGender = product.gender;
                productVM.productName = product.name;
                productVM.productBrand = product.brand;
                productVM.productPrice = product.price;
                productVM.productPriceSelection = (product.price == "price upon request") ? "price upon request" : "input";
                productVM.productType = product.category;
                productVM.description = product.description;
                productVM.sizeFit = product.size_fit;
                productVM.status = product.status;
                productVM.availableSizes = (!String.IsNullOrEmpty(product.size)) ? product.size.Split(',').ToList() : null;
                //productVM.approvalStatus = product.approval_status;
            }

            var pv = obj.GetProductColorTmp(id).ToList();
            productVM.variant = new List<Variant>();

            if (pv != null)
            {
                for (int i = 0; i < pv.Count(); i++)
                {
                    var vari = new Variant() { id = pv[i].ID, color = pv[i].color };
                    productVM.variant.Add(vari);
                }
            }


            //var tenant = obj.GetTenant(Convert.ToInt32(Session_.TenantId)).FirstOrDefault();
            var tc = obj.GetTenantCategory(product.tenant_category.ToString());
            var tenantId = Convert.ToInt32(product.tenant);
            var tenantCategory = (from a in db.tbl_TenantCategory
                                  where (db.map_TenantCategory.Where(x => x.TenantID == tenantId).Select(x => x.CategoryID).Contains(a.ID))
                                  select a);

            var productType = (from ty in db.TypeMasters
                               join b in db.tbl_TenantCategory on ty.TypeCategory equals b.ID
                               join c in db.tbl_submenu on ty.TypeSubCategory equals c.ID
                               select ty);

            var sub = (from a in db.tbl_submenu
                       where (db.map_TenantCategory.Where(x => x.TenantID == tenantId).Select(x => x.SubCategoryID).Contains(a.ID))
                       select a);
            var type = db.TypeMasters.Where(x => x.TypeCategory == tenantCategory.FirstOrDefault().ID && x.TypeSubCategory == sub.FirstOrDefault().ID);

            ViewData["ProductCategory"] = tenantCategory.Select(x => new SelectListItem { Text = x.name, Value = x.ID.ToString(), Selected = (x.ID == product.tenant_category) });
            ViewData["ProductSubcategory"] = sub.Where(x => x.parent == tenantCategory.Where(y => y.ID == product.tenant_category).FirstOrDefault().code).Select(x => new SelectListItem { Text = x.name, Value = x.ID.ToString(), Selected = (x.ID == product.tenant_subcategory) });
            ViewData["ProductType"] = db.TypeMasters.Where(x => x.TypeCategory == product.tenant_category && x.TypeSubCategory == product.tenant_subcategory && x.isActive == "Y").Select(x => new SelectListItem { Text = x.TypeDesc, Value = x.ID.ToString(), Selected = (x.ID == product.category) });

            var priceSelect = new List<SelectListItem>();
            priceSelect.Add(new SelectListItem() { Value = "price upon request", Text = "Price upon Request", Selected = true });
            priceSelect.Add(new SelectListItem() { Value = "input", Text = "Input Price", Selected = true });
            ViewData["PriceSelection"] = priceSelect;


            var adultSz = db.SizeMasters.Where(x => x.SizeCategory == "1").OrderBy(x => x.SizeIdx);
            var adultClothes = adultSz.Where(x => x.SizeGroup == "1").Select(x => x.SizeDesc).ToArray();
            var adultShoes = adultSz.Where(x => x.SizeGroup == "2").Select(x => x.SizeDesc).ToArray();

            if (adultSz != null && adultSz.Count() > 0)
            {
                var adultSizes = new List<string>();
                adultSizes.Add(String.Join(",", adultClothes));
                adultSizes.Add(String.Join(",", adultShoes));
                ViewData["adultSizes"] = adultSizes;
            }

            var kidSz = db.SizeMasters.Where(x => x.SizeCategory == "2").OrderBy(x => x.SizeIdx);
            var kidClothes = kidSz.Where(x => x.SizeGroup == "1").Select(x => x.SizeDesc).ToArray();
            var kidShoes = kidSz.Where(x => x.SizeGroup == "2").Select(x => x.SizeDesc).ToArray();
            if (kidSz != null && kidSz.Count() > 0)
            {
                var kidSize = new List<string>();
                kidSize.Add(String.Join(",", kidClothes));
                kidSize.Add(String.Join(",", kidShoes));
                ViewData["kidSize"] = kidSize;
            }

            var statusSelect = new List<SelectListItem>();
            statusSelect.Add(new SelectListItem() { Value = "true", Text = "Active", Selected = true });
            statusSelect.Add(new SelectListItem() { Value = "false", Text = "Inactive", Selected = true });
            ViewData["StatusSelection"] = statusSelect;

            return View("~/Areas/SysManager/Views/Approval/ViewProduct.cshtml", productVM);
        }
    }
}