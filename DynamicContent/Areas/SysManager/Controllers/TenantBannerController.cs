﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DynamicContent.Areas.SysManager.Helper;
using DynamicContent.Helper;
using DynamicContent.Areas.SysManager.ViewModels;
using DynamicContent.Models;

namespace DynamicContent.Areas.SysManager.Controllers
{
    public class TenantBannerController : Controller
    {
        //
        // GET: /SysManager/TenantBanner/
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult TenantBannerHandler(DTParameters dtParam)
        {
            try
            {
                ResultSet rs = new ResultSet();
                var BannerTenantDataLinq = db.Banner_tenant.Where(x => x.IsActive == false);
                var BannerTenantData = new List<_object.TenantBanner>();
                foreach (var item in BannerTenantDataLinq)
                {
                    _object.TenantBanner banner = new _object.TenantBanner();
                    banner.id = item.ImgaeTenantID;
                    banner.TenantName = rs.getTenantNameByID(int.Parse(item.TenantID));
                    banner.status = item.IsActive.ToString();
                    BannerTenantData.Add(banner);
                }
                                    

                List<String> columnSearch = new List<string>();

                foreach (var col in dtParam.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                var data = new ResultSet().GetTenantBannerResult(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, BannerTenantData.ToList(), columnSearch);
                int count = new ResultSet().CountBannerTenant(dtParam.Search.Value, BannerTenantData.ToList(), columnSearch);
                var result = new DTResult<DynamicContent.Helper._object.TenantBanner>
                {
                    draw = dtParam.Draw,
                    data = data,
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result,JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }
        public ActionResult Approve(string id)
        {
            var tenantId = Convert.ToInt32(id);
            var currentBanner = db.Banner_tenant.FirstOrDefault(x => x.ImgaeTenantID == tenantId);
            if (currentBanner != null)
            {
                ViewData.Model = currentBanner;
            }
            else
            {
                ViewData.Model = new Banner_tenant();
            }
            return View();
        }
        [HttpPost]
        public ActionResult Approve(DynamicContent.Models.Banner_tenant model)
        {
            var currentData = db.Banner_tenant.FirstOrDefault(x => x.ImgaeTenantID == model.ImgaeTenantID);
            if (currentData != null)
            {
                currentData.IsActive = true;
                currentData.UpdatedDate = DateTime.Now;
                currentData.UpdatedBy = Session_.Username;
                db.SaveChanges();
            }
            return RedirectToAction("Index", "TenantBanner");
        }

    }
}
