﻿using DynamicContent.Areas.SysManager.Helper;
using DynamicContent.Areas.SysManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DynamicContent.Helper;
using System.IO;

namespace DynamicContent.Areas.SysManager.Controllers
{
    public class ShopNowController : AuthenticatedController
    {
        //
        // GET: /SysManager/Slider/
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        public ActionResult Index()
        {
            var s = obj.GetSlider2();
            ViewData["slider"] = s;
            return View();
        }

        public ActionResult Sort(int id, int to)
        {

            DynamicContent.Models.tbl_slider2 slider = (from s in db.tbl_slider2 where s.ID == id select s).FirstOrDefault();
            if (slider != null)
            {
                bool isMore = false;
                slider.modified_by = Session_.Username;
                slider.modified_date = DateTime.Now;
                isMore = to > slider.sort;
                slider.sort = to;
                db.Entry(slider).CurrentValues.SetValues(slider);
                db.SaveChanges();

                List<DynamicContent.Models.tbl_slider2> sliders;
                if (isMore)
                {
                    sliders = (from s in db. tbl_slider2 orderby s.sort ascending, s.modified_date ascending select s).ToList();
                }
                else
                {
                    sliders = (from s in db.tbl_slider2 orderby s.sort ascending, s.modified_date descending select s).ToList();
                }

                var i = 1;
                foreach (var item in sliders)
                {
                    item.sort = i;
                    i++;
                }

                db.SaveChanges();
                //TempData["msg"] = "success: Sorting Successfully";
            }
            else
            {
                //TempData["msg"] = "error: Sorting Error";
            }

            return RedirectToAction("Index");
        }
        public ActionResult ChangeActive(int id)
        {
            DynamicContent.Models.tbl_slider2 slider = (from s in db.tbl_slider2 where s.ID == id select s).FirstOrDefault();
            if (slider != null)
            {
                slider.active = !slider.active;
                slider.modified_by = Session_.Username;
                slider.modified_date = DateTime.Now;

                db.Entry(slider).CurrentValues.SetValues(slider);
                db.SaveChanges();

                //TempData["msg"] = "success: Successfully change active";
            }
            else
            {
                //TempData["msg"] = "error: Error change active";
            }
            return RedirectToAction("Index");

        }

        public virtual ActionResult AddSlider()
        {
            //SliderViewModel model = new SliderViewModel();
            return View();
        }

        [ValidateInput(false)]
        [HttpPost]
        public virtual ActionResult AddSlider(SliderViewModel slider)
        {
            DynamicContent.Models.tbl_slider2 sld = new DynamicContent.Models.tbl_slider2();
            sld.name = slider.name;
            sld.url = slider.url;
            sld.created_by = Session_.Username;
            sld.created_date = DateTime.Now;
            sld.modified_by = Session_.Username;
            sld.modified_date = DateTime.Now;
            var filename = slider.image.FileName.Replace(' ', '_') + Guid.NewGuid() + Path.GetExtension(slider.image.FileName);
            var pth = Path.Combine(Server.MapPath("~/Public/Images/sliding-banner/"), filename);
            slider.image.SaveAs(pth);
            sld.image = "/Public/Images/sliding-banner/" + filename;
            sld.active = true;
            db.tbl_slider2.Add(sld);
            db.SaveChanges();

            return RedirectToAction("Index", "ShopNow", new { area = "SysManager" });
        }

        public virtual ActionResult EditSlider(int id)
        {
            var sld = (from s in db.tbl_slider2 where s.ID == id select s);
            var slider = ((sld != null && sld.Count() > 0) ? sld.FirstOrDefault() : null);

            ViewData["slider"] = slider;
            var sliderVM = new SliderViewModel();
            if (slider != null)
            {
                sliderVM.ID = slider.ID;
                sliderVM.name = slider.name;
                sliderVM.url = slider.url;
            }
                return View("~/Areas/SysManager/Views/ShopNow/AddSlider.cshtml", sliderVM);
        }

        [ValidateInput(false)]
        [HttpPost]
        public virtual ActionResult EditSlider(SliderViewModel sliderVM)
        {
            var sld = (from s in db.tbl_slider2 where s.ID == sliderVM.ID select s);
            var slider = ((sld != null && sld.Count() > 0) ? sld.FirstOrDefault() : null);

            slider.name = sliderVM.name;
            slider.url = sliderVM.url;
            slider.created_by = Session_.Username;
            slider.created_date = DateTime.Now;
            slider.modified_by = Session_.Username;
            slider.modified_date = DateTime.Now;
            if(sliderVM.image != null)
            {
                var filename = sliderVM.image.FileName.Replace(' ', '_') + Guid.NewGuid() + Path.GetExtension(sliderVM.image.FileName);
                var pth = Path.Combine(Server.MapPath("~/Public/Images/sliding-banner/"), filename);
                sliderVM.image.SaveAs(pth);
                slider.image = "/Public/Images/sliding-banner/" + filename;
            }
            
            db.SaveChanges();

            return RedirectToAction("Index", "ShopNow", new { area = "SysManager" });
        }

        public virtual ActionResult DeleteSlider(int id)
        {
            //var slider = new DynamicContent.Models.tbl_slider2() { ID = id };

            db.tbl_slider2.RemoveRange(db.tbl_slider2.Where(x => x.ID == id));
            db.SaveChanges();

            return RedirectToAction("Index", "ShopNow", new { area = "SysManager" });
        }
    }
}
