﻿using DynamicContent.Areas.SysManager.Helper;
using DynamicContent.Areas.SysManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DynamicContent.Helper;

namespace DynamicContent.Areas.SysManager.Controllers
{
    public class SysUserController : AuthenticatedController
    {

        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        public ActionResult Index()
        {
            var usergroups = db.UserGroup.ToList();
            ViewData["UserGroup"] = usergroups;
            return View();
        }

        public ActionResult AddRole()
        {
            var menuItems = db.SysMenuItems.ToList();
            ViewData["menu"] = menuItems;
            return View();
        }

        [HttpPost]
        public ActionResult AddRole(RolePrivViewModel vm)
        {
            var menuItems = db.SysMenuItems.ToList();
            ViewData["menu"] = menuItems;

            if(String.IsNullOrEmpty(vm.RoleName))
                return View();

            var ug = new DynamicContent.Models.UserGroup();
            ug.GroupName = vm.RoleName;
            ug.CreateDate = DateTime.Now;
            ug.EditDate = DateTime.Now;
            ug.CreateBy = Session_.Username;
            ug.EditBy = Session_.Username;

            var userGroupNew = db.UserGroup.Add(ug);
            db.SaveChanges();
            if(userGroupNew != null && vm.MenuAllowed != null && vm.MenuAllowed.Count > 0)
            {
                var lt = new List<DynamicContent.Models.SysMenuAccess>();
                foreach(var m in vm.MenuAllowed)
                {
                    var o = Convert.ToInt32(m);
                    lt.Add(new DynamicContent.Models.SysMenuAccess()
                    {
                        GroupID = userGroupNew.GroupID,
                        CreateDate = DateTime.Now,
                        CreatedBy = Session_.Username,
                        MenuID = o
                    });
                }
                db.SysMenuAccesses.AddRange(lt);
                db.SaveChanges();
            }
            
            return RedirectToAction("Index","SysUser", new { area="SysManager" });
        }

        public ActionResult EditRole(int id)
        {
            var role = (from a in db.UserGroup
                        where a.GroupID == id
                        select new RolePrivViewModel
                        {
                            RoleId = a.GroupID,
                            RoleName = a.GroupName
                        }).FirstOrDefault();
            var accessMenus = db.SysMenuAccesses.Where(x => x.GroupID == id).Select(t=>t.MenuID.ToString()).ToList();
            role.MenuAllowed = accessMenus;
            var menuItems = db.SysMenuItems.ToList();
            ViewData["menu"] = menuItems;

            ViewData["usergroup"] = db.UserGroup.Where(x=>x.GroupID == id).FirstOrDefault();
            return View("~/Areas/SysManager/Views/SysUser/AddRole.cshtml", role);
        }

        [HttpPost]
        public ActionResult EditRole(RolePrivViewModel vm)
        {

            var role = (from a in db.UserGroup
                        where a.GroupID == vm.RoleId
                        select a).FirstOrDefault();
            role.GroupName = vm.RoleName;
            role.EditBy = Session_.Username;
            role.EditDate = DateTime.Now;
            db.SaveChanges();

            var accessMenus = db.SysMenuAccesses.RemoveRange(db.SysMenuAccesses.Where(x => x.GroupID == vm.RoleId));
            db.SaveChanges();

            if (vm.MenuAllowed != null && vm.MenuAllowed.Count > 0)
            {
                var lt = new List<DynamicContent.Models.SysMenuAccess>();
                foreach (var m in vm.MenuAllowed)
                {
                    var o = Convert.ToInt32(m);
                    lt.Add(new DynamicContent.Models.SysMenuAccess()
                    {
                        GroupID = vm.RoleId,
                        CreateDate = DateTime.Now,
                        CreatedBy = Session_.Username,
                        MenuID = o
                    });
                }
                db.SysMenuAccesses.AddRange(lt);
                db.SaveChanges();
            }
            var menuItems = db.SysMenuItems.ToList();
            ViewData["menu"] = menuItems;
            ViewData["usergroup"] = db.UserGroup.Where(x => x.GroupID == vm.RoleId).FirstOrDefault();

            return RedirectToAction("Index", "SysUser", new { area = "SysManager" });
        }

        public ActionResult DeleteRole(int id)
        {
            var role = new DynamicContent.Models.UserGroup { GroupID = id };

            var SysRoleAccess = (from s in db.SysMenuAccesses
                                 where s.MenuID == id
                                 select s);

            db.SysMenuAccesses.RemoveRange(SysRoleAccess);
            db.UserGroup.Attach(role);
            db.UserGroup.Remove(role);
            db.SaveChanges();

            return RedirectToAction("Index", "SysUser", new { area = "SysManager" });
        }

    }
}