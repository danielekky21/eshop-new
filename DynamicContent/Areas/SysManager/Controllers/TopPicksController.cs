﻿using DynamicContent.Areas.SysManager.Helper;
using DynamicContent.Areas.SysManager.ViewModels;
using DynamicContent.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DynamicContent.Areas.SysManager.Controllers
{
    public class TopPicksController : Controller
    {
        //
        // GET: /SysManager/TopPicks/
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult TopPicksDataHandler(DTParameters dtParam)
        {
            try
            {
                var TopPicksData = (from f in db.TopPicks
                                     select new _object.TopPicks
                                     {
                                         TopPicksID = f.TopPicksID,
                                         ProductName = f.ProductName,
                                         sort = f.sort,
                                         CreateBy = f.CreatedBy,
                                         CreatedDate = f.CreatedDate.ToString(),
                                     });

                List<String> columnSearch = new List<string>();

                foreach (var col in dtParam.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                var data = new ResultSet().GetTopPicksResult(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, TopPicksData.ToList(), columnSearch);
                int count = new ResultSet().CountTopPicksResult(dtParam.Search.Value, TopPicksData.ToList(), columnSearch);
                var result = new DTResult<DynamicContent.Helper._object.TopPicks>
                {
                    draw = dtParam.Draw,
                    data = data,
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }
        public ActionResult AddEditTopPicks(int id)
        {
            if (id != 0)
            {
                var toppickData = db.TopPicks.FirstOrDefault(x => x.TopPicksID == id);
                if (toppickData != null)
                {
                    ViewData.Model = toppickData;
                    var imgProd = db.tbl_product_image.FirstOrDefault(x => x.product == toppickData.ProductID);
                    ViewBag.imgprod = imgProd.images;
                }
            }
            else
            {

                ViewData.Model = new DynamicContent.Models.TopPick();
                ViewBag.imgprod = string.Empty;
            }
          
            return View();
        }
        [HttpPost]
        public ActionResult AddEditTopPicks(DynamicContent.Models.TopPick model)
        {
            if (model.TopPicksID != 0)
            {
                var toppickData = db.TopPicks.FirstOrDefault(x => x.TopPicksID == model.TopPicksID && x.IsActive == true);
                if (toppickData != null)
                {
                    //update
                    toppickData.ProductID = model.ProductID;
                    toppickData.ProductName = model.ProductName;
                    toppickData.sort = model.sort;
                    toppickData.UpdatedDate = DateTime.Now;
                    toppickData.CreatedBy = Session_.Username;
                    db.SaveChanges();
                }
            }
            else
            {
                DynamicContent.Models.TopPick newData = new DynamicContent.Models.TopPick();
                newData.ProductID = model.ProductID;
                newData.ProductName = model.ProductName;
                newData.sort = model.sort;
                newData.CreatedBy = Session_.Username;
                newData.CreatedDate = DateTime.Now;
                newData.IsActive = true;
                db.TopPicks.Add(newData);
                db.SaveChanges();
            }
            return RedirectToAction("Index", "TopPicks", new { area = "SysManager" });
        }
        [HttpPost]
        public virtual JsonResult AutocompleteName(string prefix)
        {
            var product = db.tbl_product.Where(x => x.name.StartsWith(prefix) && x.status == true && x.approved == true).Select(x => new AutoCompleteProductViewModel()
            {
                name = x.name,
                ID = x.ID
            });
            return Json(product.ToList(), JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult GetPictureByProduct(string id)
        {
            int prodid = int.Parse(id);
            var image = db.tbl_product_image.FirstOrDefault(x => x.product == prodid);
            return Json(image.images, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult RemoveTopPicks(int id)
        {
            var toppickData = db.TopPicks.FirstOrDefault(x => x.TopPicksID == id);
            toppickData.IsActive = false;
            db.SaveChanges();
            return View();
        }
    }
}
