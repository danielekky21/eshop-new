﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DynamicContent.Models;
using DynamicContent.Areas.SysManager.Helper;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web.Routing;
using DynamicContent.Areas.SysManager.ViewModels;

namespace DynamicContent.Areas.SysManager.Controllers
{
    public class DummyController : Controller
    {
    }
    [AuthorizeSys]
    public class AuthenticatedController : Controller
    {
        [ChildActionOnly]
        public ActionResult Controls() {
            var userGroup = Convert.ToInt32(Session_.RoleID);
            var db = new Entities();
            var menu = (from a in db.SysMenuAccesses
                        join i in db.SysMenuItems on a.MenuID equals i.id
                        where a.GroupID == userGroup
                        select i).ToList();
            // menu[0].name

            DataTable dtMenu = this.GetMenuData(0);
            ViewData["dtMenu"] = dtMenu;
            if (menu != null && menu.Count > 0)
                return View("~/Areas/SysManager/Views/Shared/_control.cshtml", menu);
            else
                return View("~/Areas/SysManager/Views/Shared/_control.cshtml");
        }

        public static void RenderPartial(string partialName)
        {
            //get a wrapper for the legacy WebForm context
            var httpCtx = new HttpContextWrapper(System.Web.HttpContext.Current);

            //create a mock route that points to the empty controller
            var rt = new RouteData();
            rt.Values.Add("controller", typeof(DummyController).Name);

            //create a controller context for the route and http context
            var ctx = new ControllerContext(
                new RequestContext(httpCtx, rt), new DummyController());

            //find the partial view using the viewengine
            var view = ViewEngines.Engines.FindPartialView(ctx, partialName).View;
            
            //create a view context and assign the model
            var vctx = new ViewContext(ctx, view,
                new ViewDataDictionary(),
                new TempDataDictionary(), httpCtx.Response.Output);
            
            //render the partial view
            view.Render(vctx, httpCtx.Response.Output);
        }

        private DataTable GetMenuData(int parentMenuId)
        {
            string query = "SELECT Distinct [MenuId], [ParentMenuID], [Title], [TitleDesc], [Url], [MenuIdx] FROM [vw_MenuList]";

            if (Session["UserID"] == null)
            {
                query = query + " WHERE UserID in ('Guest','') ";
            }
            else
            {
                query = query + " WHERE UserID in ('" + Session["UserID"] + "','') ";
            }
            query = query + " Order by MenuIdx";
            //string query = "SELECT [MenuId], [Title], [TitleDesc], [Url] FROM [MenuMaster] WHERE ParentMenuId = @ParentMenuId";

            string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
            //SqlConnection con = new SqlConnection(
            //        WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString);
            using (SqlConnection con = new SqlConnection(constr))
            {
                DataTable dt = new DataTable();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        //cmd.Parameters.AddWithValue("@ParentMenuId", parentMenuId);
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        sda.Fill(dt);
                    }
                }
                return dt;
            }
        }

    }
}