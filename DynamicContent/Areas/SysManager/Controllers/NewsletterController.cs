﻿using DynamicContent.Areas.SysManager.Helper;
using DynamicContent.Areas.SysManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DynamicContent.Helper;
using System.IO;

namespace DynamicContent.Areas.SysManager.Controllers
{
    public class NewsletterController : Controller
    {
        //
        // GET: /SysManager/Newsletter/
        private static MailSender _mail = new MailSender("smtp");
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();

        public string baseURL
        {
            get
            {
                return string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EmailCategory()
        {
            var c = db.M_EmailType.ToList();
            ViewData["category"] = c;
            return View();
        }
        
        public virtual ActionResult AddEmailCategory()
        {
            EmailCategoryViewModel model = new EmailCategoryViewModel();
            return View();
        }

        [ValidateInput(false)]
        [HttpPost]
        public virtual ActionResult AddEmailCategory(EmailCategoryViewModel category)
        {
            DynamicContent.Models.M_EmailType cat = new DynamicContent.Models.M_EmailType();
            cat.name = category.name;
            //cat.mail_to = category.mail_to;
            db.M_EmailType.Add(cat);
            db.SaveChanges();

            return RedirectToAction("EmailCategory", "Newsletter", new { area = "SysManager" });
        }

        public virtual ActionResult EditEmailCategory(int id)
        {
            var cat = (from s in db.M_EmailType where s.id == id select s);
            var category = ((cat != null && cat.Count() > 0) ? cat.FirstOrDefault() : null);

            ViewData["category"] = category;
            var categoryVM = new EmailCategoryViewModel();
            if (category != null)
            {
                categoryVM.id = category.id;
                categoryVM.name = category.name;
                //categoryVM.mail_to = category.mail_to;
            }
            return View("~/Areas/SysManager/Views/Newsletter/AddEmailCategory.cshtml", categoryVM);
        }

        [ValidateInput(false)]
        [HttpPost]
        public virtual ActionResult EditEmailCategory(EmailCategoryViewModel categoryVM)
        {
            var cat = (from s in db.M_EmailType where s.id == categoryVM.id select s);
            var category = ((cat != null && cat.Count() > 0) ? cat.FirstOrDefault() : null);
            
            category.name = categoryVM.name;
            category.mail_to = categoryVM.mail_to;

            db.SaveChanges();

            return RedirectToAction("EmailCategory", "Newsletter", new { area = "SysManager" });
        }

        public ActionResult Subscribers()
        {
            var c = obj.GetAllSubscribers(false);
            ViewData["subscribers"] = c;
            return View();
        }

        public virtual ActionResult AddSubscriber()
        {
            ViewData["emailType"] = obj.GetAllEmailType(false);
            return View();
        }

        [HttpPost]//, AuthorizeUser("Email Subscribers:insert")]
        public ActionResult AddSubscriber(DynamicContent.Models.M_Subscribers data)
        {
            string current = null;
            try
            {
                current = "name";
                Validation.to_string_simple(data.name, false);
                current = "email";
                Validation.to_email(data.email, false);
                current = "type";
                var type = obj.GetAllEmailType(false).Select(x => x.id).ToList();
                Validation.to_int_in_list(data.type, type);

                data.subscribe_by = Session_.Username;
                data.subscribe_date = DateTime.Now;

                var ins = obj.AddSubscribers(data);
                if(ins == 0)
                {
                    TempData["msg"] = "error: Subscribers already exist";
                }
                else
                {
                    TempData["msg"] = "success: Successfully add subscribers";
                }
                return RedirectToAction("Subscribers", "Newsletter", new { area = "SysManager" });
            }
            catch (Exception e)
            {
                string es = e.Message;
                if (es.Contains("validation error : "))
                {
                    TempData["msg"] = "error:" + es.ToString() + " [" + current + "]";
                    return RedirectToAction("AddSubscriber", "Newsletter", new { area = "SysManager" });
                }
                throw e;
            }
        }

        public ActionResult EditSubscriber(string id)
        {
            int idd = 0;
            if (int.TryParse(id, out idd))
            {
                var data = obj.GetSubscribers(idd);
                if (data != null && !data.del)
                {
                    ViewData["data"] = data;
                    ViewData["emailType"] = obj.GetAllEmailType(false);
                    return View("AddSubscriber");
                }
            }

            return RedirectToAction("Subscribers");
        }

        [HttpPost]//, AuthorizeUser("Email Subscribers:update")]
        public ActionResult EditSubscriber(DynamicContent.Models.M_Subscribers data)
        {
            string current = null;
            try
            {
                var subscriber = obj.GetSubscribers(data.id);
                if (subscriber != null && !subscriber.del)
                {
                    current = "name";
                    subscriber.name = Validation.to_string_simple(data.name, false);
                    current = "email";
                    subscriber.email = Validation.to_email(data.email, false);
                    current = "type";
                    var type = obj.GetAllEmailType(false).Select(x => x.id).ToList();
                    subscriber.type = Validation.to_int_in_list(data.type, type);

                    obj.UpdateSubscribers(subscriber);
                    TempData["msg"] = "success: Successfully update subscribers";
                }
            }
            catch (Exception e)
            {
                string es = e.Message;
                if (es.Contains("validation error : "))
                {
                    TempData["msg"] = "error:" + es.ToString() + " [" + current + "]";
                }
                throw e;
            }

            return RedirectToAction("Subscribers");
        }

        public ActionResult DeleteSubscriber(string id)
        {
            int idd = 0;
            if (int.TryParse(id, out idd))
            {
                var data = obj.GetSubscribers(idd);
                if (data != null && !data.del)
                {
                    obj.DeleteSubscribers(idd, Session_.Username);

                    TempData["msg"] = "success: Successfully delete subscribers";
                }
            }

            return RedirectToAction("Subscribers");
        }

        #region newsletter
        public ActionResult Newsletter()
        {
            ViewData["data"] = obj.GetAllNewsletter(false);
            return View();
        }

        public ActionResult AddNewsletter(string id)
        {
            var idd = 0;
            int.TryParse(id, out idd);

            ViewData["templates"] = obj.GetAllNewsletter(true);
            ViewData["template"] = obj.GetNewsletter(idd);
            return View();
        }

        [HttpPost, ValidateInput(false)]//, AuthorizeUser("Email Newsletter:insert")]
        public ActionResult AddNewsletter(DynamicContent.Models.M_Newsletter data)
        {
            string current = null;
            try
            {
                current = "title";
                Validation.to_string_simple(data.name, false);
                current = "content";
                Validation.to_string_notempty(data.content);
                current = "plain content";
                //data.plain_content = Validation.to_string_notempty(data.plain_content.Trim());
                data.plain_content = "";

                data.created_by = Session_.Username;
                data.created_date = DateTime.Now;
                data.modified_by = Session_.Username;
                data.modified_date = DateTime.Now;
                data.template = false;

                obj.AddNewsletter(data);
                TempData["msg"] = "success: Successfully add newsletter";
                return RedirectToAction("Newsletter");
            }
            catch (Exception e)
            {
                string es = e.Message;
                if (es.Contains("validation error : "))
                {
                    TempData["msg"] = "error:" + es.ToString() + " [" + current + "]";
                    return RedirectToAction("AddNewsletter");
                }
                throw e;
            }
        }

        public ActionResult EditNewsletter(string id)
        {
            int idd = 0;
            if (int.TryParse(id, out idd))
            {
                var data = obj.GetNewsletter(idd);
                if (data != null && !data.del && data.send_date == null)
                {
                    ViewData["data"] = data;

                    return View("AddNewsletter");
                }
            }

            return RedirectToAction("Newsletter");
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult EditNewsletter(DynamicContent.Models.M_Newsletter data)
        {
            string current = null;
            try
            {
                var newsletter = obj.GetNewsletter(data.id);
                if (newsletter != null && !newsletter.del && newsletter.send_date == null)
                {
                    current = "title";
                    newsletter.name = Validation.to_string_simple(data.name, false);
                    current = "content";
                    newsletter.content = Validation.to_string_notempty(data.content);
                    //current = "plain content";
                    //newsletter.plain_content = Validation.to_string_notempty(data.plain_content.Trim());

                    newsletter.modified_by = Session_.Username;
                    newsletter.modified_date = DateTime.Now;

                    obj.UpdateNewsletter(newsletter);
                    TempData["msg"] = "success: Successfully update newsletter";
                }
            }
            catch (Exception e)
            {
                string es = e.Message;
                if (es.Contains("validation error : "))
                {
                    TempData["msg"] = "error:" + es.ToString() + " [" + current + "]";
                    return RedirectToAction("Newsletter");
                }
                throw e;
            }

            return RedirectToAction("Newsletter");
        }

        
        public ActionResult DeleteNewsletter(string id)
        {
            int idd = 0;
            if (int.TryParse(id, out idd))
            {
                var data = obj.GetNewsletter(idd);
                if (data != null && !data.del && data.send_date == null)
                {
                    //obj.DeleteSubscribers(idd, _session.UserName);
                    obj.DeleteNewsletter(idd, Session_.Username);

                    TempData["msg"] = "success: Successfully delete newsletter";
                }
            }

            return RedirectToAction("Newsletter");
        }

        
        public ActionResult BlastNewsletter(string id)
        {
            int idd = 0;
            if (int.TryParse(id, out idd))
            {
                var data = obj.GetNewsletter(idd);
                if (data != null && !data.del && data.send_date == null)
                {
                    ViewData["data"] = data;
                    ViewData["emailType"] = obj.GetAllEmailType(false);
                    return View();
                }
            }

            return RedirectToAction("Newsletter");
        }

        [HttpPost]
        public ActionResult TestNewsletter(int id, string email)
        {
            if (id != null && email != null)
            {
                //try
                //{
                    var data = obj.GetNewsletter(id);
                    if (data != null && !data.del && data.send_date == null)
                    {
                        var emailList = new List<MailSender.mailTo>();

                        data.name = "[TEST] " + data.name;
                        string WebNewsletter = GenerateWebNewsletter(data);
                        var replacement = new Dictionary<string, string>() { { "title", data.name }, { "unsubscribe_link", "#" }, { "webnewsletter_link", WebNewsletter } };
                        string content = ParseNewsletter(data.content, replacement);
                        string plainContent = ParseNewsletter(data.plain_content, replacement);

                        foreach (var item in email.Split(' '))
                        {
                            var mailTo = new MailSender.mailTo();

                            mailTo.email = Validation.to_email(item, false);

                            emailList.Add(mailTo);
                        }

                        string fromEmail = obj.GetParamVal("newsletter_from_email");
                        string fromName = obj.GetParamVal("newsletter_from_name");

                        _mail.sendMail(fromEmail, fromName, null, emailList, data.name, plainContent, content);

                        TempData["msg"] = "success: Successfully sent test newsletter to [" + email + "]";
                        return RedirectToAction("Newsletter");
                    }
            }

            return RedirectToAction("Newsletter");
        }

        [HttpPost]
        public ActionResult BlastNewsletter(int id, int emailType)
        {
            if (id != null && emailType != null)
            {
                //try
                //{
                    var data = obj.GetNewsletter(id);
                    if (data != null && !data.del && data.send_date == null)
                    {
                        var emailList = obj.getAllSubscribersFromType(emailType);
                        var emailTo = new List<MailSender.mailTo>();
                        if (emailList.Count() > 0)
                        {
                            data.send_date = DateTime.Now;
                            data.send_to = string.Join(" ", emailList.Select(x => x.email).ToList());
                            
                            var type = obj.GetEmailType(emailType);

                            string fromEmail = obj.GetParamVal("newsletter_from_email");
                            string fromName = obj.GetParamVal("newsletter_from_name");
                            string replyTo = type != null ? type.mail_to : null;

                            obj.UpdateNewsletter(data);

                            string WebNewsletter = GenerateWebNewsletter(data);
                            var replacement = new Dictionary<string, string>() { { "title", data.name }, { "unsubscribe_link", "#" }, { "webnewsletter_link", WebNewsletter } };

                            foreach (var item in emailList)
                            {
                                var mailTo = new MailSender.mailTo();
                                mailTo.email = Validation.to_email(item.email, false);
                                emailTo.Add(mailTo);
                            }

                        string content = ParseNewsletter(data.content, replacement);
                        string plainContent = ParseNewsletter(data.plain_content, replacement);                        

                        _mail.sendMail(fromEmail, fromName, null, emailTo, data.name, plainContent, content);

                        TempData["msg"] = "success: Successfully blast newsletter";
                        }
                        else
                        {
                            throw new Exception("validation error : no subscriber available");
                        }
                    }
            }

            return RedirectToAction("Newsletter");
        }

        private string GenerateWebNewsletter(DynamicContent.Models.M_Newsletter data)
        {
            try
            {
                var replacement = new Dictionary<string, string>() { { "title", data.name }, { "unsubscribe_link", "#" }, { "webnewsletter_link", "#" } };

                string html = ParseNewsletter(data.content, replacement);
                string filepath = Server.MapPath("~/Public/Newsletter/");
                string filename = CustomEncryption.HashStringSHA1(DateTime.Now.ToString()) + ".html";
                string file = filepath + filename;

                using (StreamWriter sw = new StreamWriter(file, true))
                {
                    sw.Write(html);
                    sw.Close();
                }

                return baseURL + "/Public/Newsletter/" + filename;
            }
            catch (Exception e)
            {
                //_log.Error("[GenerateWebNewsletter] : " + e.ToString());
                return null;
            }
        }

        private string ParseNewsletter(string content, Dictionary<string, string> replacement)
        {
            content = "<html>" +
                "<head>" +
                "<title>{title}</title>" +
                "<style>" +
                "@font-face{" +
                    "font-family:\"frutiger\";" +
                    "src:url(\"" + baseURL + "fonts/FRUTIGER.eot?\") format(\"eot\")," +
                    "url(\"" + baseURL + "fonts/FRUTIGER.woff\") format(\"woff\")," +
                    "url(\"" + baseURL + "fonts/FRUTIGER.ttf\") format(\"truetype\")," +
                    "url(\"" + baseURL + "fonts/FRUTIGER.svg#FRUTIGER\") format(\"svg\");" +
                    "font-weight:normal;" +
                    "font-style:normal}" +
                "@font-face{" +
                    "font-family:\"frutigerblack\";" +
                    "src:url(\"" + baseURL + "fonts/FRUTIGERBLACK.eot?\") format(\"eot\")," +
                    "url(\"" + baseURL + "fonts/FRUTIGERBLACK.woff\") format(\"woff\")," +
                    "url(\"" + baseURL + "fonts/FRUTIGERBLACK.ttf\") format(\"truetype\")," +
                    "url(\"" + baseURL + "fonts/FRUTIGERBLACK.svg#FRUTIGERBLACK\") format(\"svg\");" +
                    "font-weight:normal;" +
                    "font-style:normal}" +
                "</style>" +
                "</head>" +
                "<body leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" style=\"\">" +
                content +
                "</body>" +
                "</html>";

            //replace local image to absolutepath
            content = content.Replace("src=\"/Public", "src=\"" + baseURL + "Public");

            foreach (var item in replacement)
            {
                content = content.Replace("{" + item.Key + "}", item.Value);
            }
            return content;
        }
        #endregion
    }
}
