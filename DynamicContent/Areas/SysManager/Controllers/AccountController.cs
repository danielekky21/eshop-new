﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DynamicContent.Helper;
using DynamicContent.Areas.SysManager.ViewModels;
using DynamicContent.Areas.SysManager.Models;
using DynamicContent.Areas.SysManager.Helper;
using System.Threading.Tasks;
using DynamicContent.Models;

namespace DynamicContent.Areas.SysManager.Controllers
{
    public class AccountController : Controller
    {
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        private readonly UserSystemModel _userSystem;
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        private string baseURL
        {
            get
            {
                return string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            }
        }
        public AccountController()
        {
            _userSystem = new UserSystemModel();
        }
        [AllowAnonymous]
        public virtual ActionResult Login(string returnUrl)
        {
            return Redirect("/SysManager/Login.aspx");
            //ViewBag.ReturnUrl = returnUrl;
            //return View();
        }

        [AllowAnonymous]
        public virtual ActionResult PageNotFound()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }


            var ut = (from u in db.UserMaster
                      where u.UserID.Equals(model.Username)
                      select u).FirstOrDefault();

            if (ut == null)
            {
                ModelState.AddModelError("Invalid Credential", "Username / Password is not valid");
                return View(model);
            }

            if (!ut.UserPassword.Equals(CustomEncryption.HashStringSHA1(model.Password))) {
                ModelState.AddModelError("Invalid Credential", "Username / Password is not valid");
                return View(model);
            }

            ut.session_id = Session_.SessionId;
            db.SaveChanges();

            Session_.RoleID = ut.GroupID.ToString();
            Session_.Username = ut.UserID;

            return RedirectToLocal(returnUrl);
        }

        [AllowAnonymous]
        public virtual ActionResult ForgetPassword()
        {
            return View();
        }


        [AllowAnonymous]
        [HttpPost]
        public virtual ActionResult ForgetPassword(string username)
        {
            var u = db.UserMaster.Where(x => x.UserID == username).FirstOrDefault();
            string GmailID = System.Configuration.ConfigurationManager.AppSettings["GmailID"];
            string GmailPass = System.Configuration.ConfigurationManager.AppSettings["GmailPass"];
            if (u != null)
            {
                var ukey = CustomEncryption.EncryptStringAES(u.UserID + "|" + DateTime.Now, "forgotpasswordSys"); //generate unique key

                var mailTemplate = (from m in db.MailMaster
                                    where m.MailDesc == "ForgotPasswordSys"
                                    select m).FirstOrDefault();

                string content = mailTemplate.MailBodyTemplate
                    .Replace("@email", u.Email)
                    .Replace("@ukey", HttpUtility.UrlEncode(ukey))
                    .Replace("@baseURL", baseURL)
                    ;
                GMailer.GmailUsername = GmailID;
                GMailer.GmailPassword = GmailPass;
                GMailer mailer = new GMailer();
                //mailer.ToEmail = u.Email;
                //mailer.Subject = mailTemplate.MailSubjectTemplate;
                //mailer.Body = content;
                //mailer.IsHtml = true;
                //mailer.Send();
                Task.Run(() => { MainModel.SendEmail(u.Email, mailTemplate.MailSubjectTemplate, content); }).Wait();
                ViewBag.msgVerification = "Verification email has been sent";
                return View();
            }
            else
            {
                ViewBag.msgVerification = "Email has not been registered";
                return View();
            }

        }
        private string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        [AllowAnonymous]
        public virtual ActionResult ResetPassword(string ukey)
        {
            var url = "/SysManager/Login.aspx?msg={0}";
            if (String.IsNullOrEmpty(ukey))
                return RedirectToAction("Login");
            var decrypt = "";
            try
            {
                decrypt = CustomEncryption.DecryptStringAES(ukey, "forgotpasswordSys");
            }
            catch (Exception ex)
            {
                url = String.Format(url, Base64Encode("Link is incorrect"));
                return Redirect(url); // RedirectToAction("Login", new { msg = "Link is incorrect" });
            }

            if (String.IsNullOrEmpty(decrypt) || decrypt.IndexOf("|") < 0)
            {
                url = String.Format(url, Base64Encode("Link is incorrect"));
                return Redirect(url);
            }

            var d = decrypt.Split('|');
            var dateRequested = DateTime.Parse(d[1]);
            if (dateRequested > DateTime.Now.AddMinutes(10))
            {
                url = String.Format(url, Base64Encode("Your forgot password link has been expired."));
                return Redirect(url); //RedirectToAction("Login",new { msg = "Your forgot password link has been expired." });
            }

            var vm = new ResetPasswordViewModel();
            vm.Username = d[0];
            return View(vm);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual ActionResult ResetPassword(ResetPasswordViewModel vm)
        {
            var url = "/SysManager/Login.aspx?msg={0}";
            if (!ModelState.IsValid)
            {
                return View(vm);
            }

            if (vm.Password != vm.RepeatPassword)
            {
                ModelState.AddModelError("Repeat Password is not the same", new Exception());
                return View(vm);
            }

            var u = db.UserMaster.Where(x => x.UserID == vm.Username).FirstOrDefault();
            u.UserPassword = CustomEncryption.HashStringSHA1(vm.Password);
            u.EditDate = DateTime.Now;
            u.EditBy = u.UserID;
            db.SaveChanges();

            url = String.Format(url, Base64Encode("Password has been updated. Login using your new password"));
            TempData["msgVerification"] = "Password has been updated. Login using your new password";
            return Redirect(url); //RedirectToAction("Login");
        }


        public bool Authentication(string controller, string action)
        {

            if (String.IsNullOrEmpty(Session_.Username) || String.IsNullOrEmpty(Session_.RoleID))
            {
                return !String.IsNullOrEmpty(Session_.Username);
            }

            var ut = (from u in db.UserMaster.AsNoTracking()
                      where u.UserID.Equals(Session_.Username)
                      select u).FirstOrDefault();
            if (ut == null || ut.session_id != Session_.SessionId)
            {
                return false;
            }
            var groupId = Convert.ToInt32(Session_.RoleID);
            var menuAccess = (from sa in db.SysMenuAccesses
                              join sm in db.SysMenuItems on sa.MenuID equals sm.id
                              where sa.GroupID == groupId && sm.controller == controller
                              select sa).FirstOrDefault();

            return (menuAccess != null || (controller.ToLower() == "account" && action.ToLower() == "changepassword") || (controller.ToLower() == "account" && action.ToLower() == "checksession"));// (menuAccess != null);

            //return !String.IsNullOrEmpty(Session_.Username);
        }

        public bool AuthenticationAspx()
        {
            if (String.IsNullOrEmpty(Session_.Username))
            {
                return !String.IsNullOrEmpty(Session_.Username);
            }

            var ut = (from u in db.UserMaster.AsNoTracking()
                      where u.UserID.Equals(Session_.Username)
                      select u).FirstOrDefault();
            return (ut != null && ut.session_id == Session_.SessionId);
            
        }

        public ActionResult Logout()
        {
            var ut = (from u in db.UserMaster.AsNoTracking()
                      where u.UserID.Equals(Session_.Username)
                      select u).FirstOrDefault();
            if(ut != null)
            {
                ut.session_id = null;
            }

            db.SaveChanges();

            Session_.destroy();
            
            return Redirect("/SysManager/Login.aspx");
        }

        [AuthorizeSys]
        public ActionResult ChangePassword()
        {
            var vm = new ResetPasswordViewModel();
            vm.Username = Session_.Username;
            return View(vm);
        }

        [AuthorizeSys]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ChangePassword(ResetPasswordViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return View(vm);
            }

            if (vm.Password != vm.RepeatPassword)
            {
                ModelState.AddModelError("Repeat Password is not the same", new Exception("Repeat Password is not the same"));
                return View(vm);
            }
            var u = db.UserMaster.Where(x => x.UserID == vm.Username).FirstOrDefault();
            u.UserPassword = CustomEncryption.HashStringSHA1(vm.Password);
            u.EditDate = DateTime.Now;
            //u = DateTime.Now;
            db.SaveChanges();
            ViewBag.msg = "Password Successfully updated";
            return View(vm);
        }

        [AuthorizeSys]
        [HttpPost]
        public JsonResult CheckSession()
        {
            return Json(new { sessionid = Session_.SessionId }, JsonRequestBehavior.AllowGet);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home", new { area = "SystemManager" });
        }
    }
}