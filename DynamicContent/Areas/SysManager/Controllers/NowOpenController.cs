﻿using DynamicContent.Areas.SysManager.Helper;
using DynamicContent.Areas.SysManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DynamicContent.Helper;
using System.IO;

namespace DynamicContent.Areas.SysManager.Controllers
{
    public class NowOpenController : AuthenticatedController
    {
        
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        public ActionResult Index()
        {
            var s = obj.GetNowopen();
            ViewData["nowopen"] = s;
            return View();
        }

        public ActionResult Sort(int id, int to)
        {

            DynamicContent.Models.NowOpen slider = (from s in db.NowOpen where s.ID == id select s).FirstOrDefault();
            if (slider != null)
            {
                bool isMore = false;
                slider.modified_by = Session_.Username;
                slider.modified_date = DateTime.Now;
                isMore = to > slider.sort;
                slider.sort = to;
                db.Entry(slider).CurrentValues.SetValues(slider);
                db.SaveChanges();

                List<DynamicContent.Models.NowOpen> sliders;
                if (isMore)
                {
                    sliders = (from s in db.NowOpen orderby s.sort ascending, s.modified_date ascending select s).ToList();
                }
                else
                {
                    sliders = (from s in db.NowOpen orderby s.sort ascending, s.modified_date descending select s).ToList();
                }

                var i = 1;
                foreach (var item in sliders)
                {
                    item.sort = i;
                    i++;
                }

                db.SaveChanges();
                //TempData["msg"] = "success: Sorting Successfully";
            }
            else
            {
                //TempData["msg"] = "error: Sorting Error";
            }

            return RedirectToAction("Index");
        }
        public ActionResult ChangeActive(int id)
        {
            DynamicContent.Models.NowOpen slider = (from s in db.NowOpen where s.ID == id select s).FirstOrDefault();
            if (slider != null)
            {
                slider.active = !slider.active;
                slider.modified_by = Session_.Username;
                slider.modified_date = DateTime.Now;

                db.Entry(slider).CurrentValues.SetValues(slider);
                db.SaveChanges();

                //TempData["msg"] = "success: Successfully change active";
            }
            else
            {
                //TempData["msg"] = "error: Error change active";
            }
            return RedirectToAction("Index");

        }

        public virtual ActionResult Add()
        {
            //NowOpenViewModel model = new NowOpenViewModel();
            return View();
        }

        [ValidateInput(false)]
        [HttpPost]
        public virtual ActionResult Add(NowOpenViewModel slider)
        {
            DynamicContent.Models.NowOpen sld = new DynamicContent.Models.NowOpen();
            sld.name = slider.name;
            sld.description = slider.description;
            sld.created_by = Session_.Username;
            sld.created_date = DateTime.Now;
            sld.modified_by = Session_.Username;
            sld.modified_date = DateTime.Now;
            var filename = slider.image.FileName.Replace(' ', '_') + Guid.NewGuid() + Path.GetExtension(slider.image.FileName);
            var pth = Path.Combine(Server.MapPath("~/Public/"), filename);
            slider.image.SaveAs(pth);
            sld.image = "/Public/" + filename;
            sld.active = true;
            db.NowOpen.Add(sld);
            db.SaveChanges();

            return RedirectToAction("Index", "NowOpen", new { area = "SysManager" });
        }

        public virtual ActionResult Edit(int id)
        {
            var sld = (from s in db.NowOpen where s.ID == id select s);
            var slider = ((sld != null && sld.Count() > 0) ? sld.FirstOrDefault() : null);

            ViewData["nowopen"] = slider;
            var sliderVM = new NowOpenViewModel();
            if (slider != null)
            {
                sliderVM.ID = slider.ID;
                sliderVM.name = slider.name;
                sliderVM.description = slider.description;
            }
                return View("~/Areas/SysManager/Views/NowOpen/Add.cshtml", sliderVM);
        }

        [ValidateInput(false)]
        [HttpPost]
        public virtual ActionResult Edit(NowOpenViewModel sliderVM)
        {
            var sld = (from s in db.NowOpen where s.ID == sliderVM.ID select s);
            var slider = ((sld != null && sld.Count() > 0) ? sld.FirstOrDefault() : null);

            slider.name = sliderVM.name;
            slider.description = sliderVM.description;
            slider.created_by = Session_.Username;
            slider.created_date = DateTime.Now;
            slider.modified_by = Session_.Username;
            slider.modified_date = DateTime.Now;
            if(sliderVM.image != null)
            {
                var filename = sliderVM.image.FileName.Replace(' ', '_') + Guid.NewGuid() + Path.GetExtension(sliderVM.image.FileName);
                var pth = Path.Combine(Server.MapPath("~/Public/"), filename);
                sliderVM.image.SaveAs(pth);
                slider.image = "/Public/" + filename;
            }
            
            db.SaveChanges();

            return RedirectToAction("Index", "NowOpen", new { area = "SysManager" });
        }

        public virtual ActionResult Delete(int id)
        {
            //var slider = new DynamicContent.Models.NowOpen() { ID = id };

            db.NowOpen.RemoveRange(db.NowOpen.Where(x => x.ID == id));
            db.SaveChanges();

            return RedirectToAction("Index", "NowOpen", new { area = "SysManager" });
        }
    }
}
