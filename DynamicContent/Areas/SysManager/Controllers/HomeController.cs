﻿using DynamicContent.Areas.SysManager.Helper;
using DynamicContent.Areas.SysManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DynamicContent.Helper;

namespace DynamicContent.Areas.SysManager.Controllers
{
    public class HomeController : AuthenticatedController
    {

        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
        public ActionResult Index()
        {
            return RedirectToActionPermanent("Dashboard");
        }

        public ActionResult Dashboard()
        {
            var needApproval = (from tp in db.tbl_product_tmp
                                join t in db.tbl_tenant on tp.tenant equals t.ID
                                where tp.approval_status == 1
                                select new DashboardViewModel
                                {
                                    tenantID = t.ID,
                                    tenantName = t.name,
                                    productID = tp.ID,
                                    productName = tp.name
                                }).ToList();
            int roleid = Convert.ToInt32(Session_.RoleID);
            var group = (from u in db.UserGroup
                         where u.GroupID == roleid
                         select u).FirstOrDefault();
            var userlogin = (from u in db.UserMaster
                         where u.UserID == Session_.Username
                         select u).FirstOrDefault();
            ViewData["needApproval"] = needApproval;
            ViewData["group"] = group;
            ViewData["userlogin"] = userlogin;
            return View();
        }

        public JsonResult ProductListDataHandler(DTParameters dtParam)
        {
            try
            {
                var products = obj.GetAllProductTmpWaitingApproval();

                List<String> columnSearch = new List<string>();

                foreach (var col in dtParam.Columns)
                {
                    columnSearch.Add(col.Search.Value);
                }

                var data = new ResultSet().GetResult(dtParam.Search.Value, dtParam.SortOrder, dtParam.Start, dtParam.Length, products.ToList(), columnSearch);
                int count = new ResultSet().Count(dtParam.Search.Value, products.ToList(), columnSearch);
                var result = new DTResult<DynamicContent.Helper._object.AllProduct>
                {
                    draw = dtParam.Draw,
                    data = data,
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        public virtual ActionResult Approve(int id)
        {
            var prdTmp = (from p in db.tbl_product_tmp
                          where p.ID == id
                          select p).First();
            prdTmp.approved = true;
            prdTmp.approval_status = (int)Constant.approvalStatus.approved;
            prdTmp.approved_by = "system";
            prdTmp.approved_date = DateTime.Now;
            prdTmp.modified_date = DateTime.Now;

            var prd = (from p in db.tbl_product
                       where p.ID == id
                       select p).FirstOrDefault();

            if (prd == null)
            {
                db.tbl_product.Add(new DynamicContent.Models.tbl_product()
                {
                    tenant_category = prdTmp.tenant_category,
                    tenant_subcategory = prdTmp.tenant_subcategory,
                    tenant = prdTmp.tenant,
                    status = prdTmp.status,
                    size_fit = prdTmp.size_fit,
                    size = prdTmp.size,
                    price = prdTmp.price,
                    name = prdTmp.name,
                    modified_date = prdTmp.modified_date,
                    modified_by = prdTmp.modified_by,
                    images = prdTmp.images,
                    ID = prdTmp.ID,
                    gender = prdTmp.gender,
                    approval_status = prdTmp.approval_status,
                    approved = prdTmp.approved,
                    approved_by = prdTmp.approved_by,
                    approved_date = prdTmp.approved_date,
                    brand = prdTmp.brand,
                    category = prdTmp.category,
                    created_by = prdTmp.created_by,
                    created_date = prdTmp.created_date,
                    description = prdTmp.description,
                });
            }
            else
            {

                prd.tenant_category = prdTmp.tenant_category;
                prd.tenant_subcategory = prdTmp.tenant_subcategory;
                prd.tenant = prdTmp.tenant;
                prd.status = prdTmp.status;
                prd.size_fit = prdTmp.size_fit;
                prd.size = prdTmp.size;
                prd.price = prdTmp.price;
                prd.name = prdTmp.name;
                prd.modified_date = prdTmp.modified_date;
                prd.modified_by = prdTmp.modified_by;
                prd.images = prdTmp.images;
                prd.gender = prdTmp.gender;
                prd.approval_status = prdTmp.approval_status;
                prd.approved = prdTmp.approved;
                prd.approved_by = prdTmp.approved_by;
                prd.approved_date = prdTmp.approved_date;
                prd.brand = prdTmp.brand;
                prd.category = prdTmp.category;
                prd.created_by = prdTmp.created_by;
                prd.created_date = prdTmp.created_date;
                prd.description = prdTmp.description;
            }

            var prd_clr_tmp = (from pc in db.tbl_product_color_tmp
                               where pc.product == prdTmp.ID
                               select pc).ToList();




            for (int i = 0; i < prd_clr_tmp.Count(); i++)
            {
                if (i == 0)
                {
                    db.tbl_product_color.RemoveRange(db.tbl_product_color.Where(x => x.product == id));
                }

                var pcTempId = prd_clr_tmp[i].ID;
                var prd_img_tmp = (from pc in db.tbl_product_image_tmp
                                   where pc.color == pcTempId
                                   select pc).ToList();

                db.tbl_product_image.RemoveRange(db.tbl_product_image.Where(x => x.color == pcTempId));
                for (int j = 0; j < prd_img_tmp.Count(); j++)
                {
                    db.tbl_product_image.Add(new DynamicContent.Models.tbl_product_image()
                    {
                        color = prd_img_tmp[j].color,
                        ID = prd_img_tmp[j].ID,
                        images = prd_img_tmp[j].images,
                        product = prd_img_tmp[j].product

                    });
                }

                db.tbl_product_color.Add(new DynamicContent.Models.tbl_product_color()
                {
                    color = prd_clr_tmp[i].color,
                    description = prd_clr_tmp[i].description,
                    ID = prd_clr_tmp[i].ID,
                    ids = prd_clr_tmp[i].ids,
                    product = prd_clr_tmp[i].product,
                    stock = prd_clr_tmp[i].stock
                });
            }

            db.SaveChanges();


            return RedirectToAction("Index", "Home", new { area = "SysManager" });
        }

        public virtual ActionResult Rejected(int id, string msg)
        {
            var prd = (from p in db.tbl_product_tmp
                       where p.ID == id
                       select p).First();
            prd.approved = true;
            prd.approval_status = (int)Constant.approvalStatus.rejected;
            prd.approved_by = "system";
            prd.approved_date = DateTime.Now;
            prd.modified_date = DateTime.Now;
            prd.approval_message = msg;

            db.SaveChanges();
            return RedirectToAction("Index", "Home", new { area = "SysManager" });
        }

    }
}