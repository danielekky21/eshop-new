﻿using System.Web.Mvc;

namespace DynamicContent.Areas.SysManager
{
    public class SysManagerAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SysManager";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SysManagerDefault",
                "SysManager/{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
