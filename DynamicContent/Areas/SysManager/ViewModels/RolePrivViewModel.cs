﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DynamicContent.Areas.SysManager.ViewModels
{
    public class RolePrivViewModel
    {
        public string RoleName { get; set; }
        public int RoleId { get; set; }
        public List<string> MenuAllowed { get; set; }
    }
    
}
