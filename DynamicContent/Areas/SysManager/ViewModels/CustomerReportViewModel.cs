﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DynamicContent.Areas.SysManager.ViewModels
{
    public class CustomerReportViewModel  
    {
        public DateTime? signupDate { get; set; }
        public string customerName { get; set; }
        public string phoneNumber { get; set; }
        public string email { get; set; }
        public string gender { get; set; }
        public string dob { get; set; }
    }
}
