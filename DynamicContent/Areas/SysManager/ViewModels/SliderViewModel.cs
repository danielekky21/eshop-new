﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace DynamicContent.Areas.SysManager.ViewModels
{
    public class SliderViewModel
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Title cannot be empty")]
        public string name { get; set; }
        [Required(ErrorMessage = "Image cannot be empty")]
        public HttpPostedFileBase image { get; set; }
        public string url { get; set; }
        public string Type { get; set; }
    }
}
