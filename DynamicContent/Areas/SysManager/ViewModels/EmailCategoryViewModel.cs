﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace DynamicContent.Areas.SysManager.ViewModels
{
    public class EmailCategoryViewModel
    {
        public int id { get; set; }
        [Required(ErrorMessage = "Title cannot be empty")]
        public string name { get; set; }
        [Required(ErrorMessage = "Email cannot be empty")]
        public string mail_to { get; set; }
    }
}
