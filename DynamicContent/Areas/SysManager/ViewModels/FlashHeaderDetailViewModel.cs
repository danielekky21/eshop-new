﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DynamicContent.Areas.SysManager.ViewModels
{
    public class FlashHeaderDetailViewModel
    {
        public int FlashSaleDetailID { get; set; }
        public Guid FlashSaleHeaderID { get; set; }
        public int? ProductID { get; set; }
        public int? qty { get; set; }
        public bool? isActive { get; set; }
        public string price_new { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}