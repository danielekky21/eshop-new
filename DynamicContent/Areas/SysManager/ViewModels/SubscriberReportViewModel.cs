﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DynamicContent.Areas.SysManager.ViewModels
{
    public class SubscriberReportViewModel  
    {
        public DateTime? date { get; set; }
        public int count { get; set; }
    }
    public class reservationHeaderModel
    {
        public string ReservatonCode { get; set; }
        public string ReservationDate { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string ShopName { get; set; }
        public string FUStatus { get; set; }
        public string FUName { get; set; }
        public string FUNote { get; set; }
    }
}
