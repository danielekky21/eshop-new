﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DynamicContent.Areas.SysManager.ViewModels
{
    public class FlashSaleHeaderViewModel
    {
        public Guid FlashSaleHeaderID { get; set; }
        public string FlashSaleDay { get; set; }
        public TimeSpan? FlashSaleTimeBegin { get; set; }
        public TimeSpan? FlashSaleTimeEnd { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string CreatedBy { get; set; }
        public string FlashSaleName { get; set; }
    }
}