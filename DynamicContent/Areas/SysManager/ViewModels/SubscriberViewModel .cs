﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace DynamicContent.Areas.SysManager.ViewModels
{
    public class SubscriberViewModel
    {
        public int id { get; set; }
        [Required(ErrorMessage = "Name cannot be empty")]
        public string name { get; set; }
        [Required(ErrorMessage = "Email cannot be empty")]
        public string email { get; set; }
        [Required(ErrorMessage = "Type cannot be empty")]
        public string type { get; set; }
    }
}
