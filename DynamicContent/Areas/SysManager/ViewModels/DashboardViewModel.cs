﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DynamicContent.Areas.SysManager.ViewModels
{
    public class DashboardViewModel  
    {
        public int tenantID { get; set; }
        public string tenantName { get; set; }
        public int productID { get; set; }
        public string productName { get; set; }
    }
}
