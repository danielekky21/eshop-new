﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace DynamicContent.Areas.SysManager.ViewModels
{
    public class NowOpenViewModel
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Title cannot be empty")]
        public string name { get; set; }
        public string description { get; set; }
        [Required(ErrorMessage = "Image cannot be empty")]
        public HttpPostedFileBase image { get; set; }
    }
}
