﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace DynamicContent.Areas.SysManager.ViewModels
{
    public class ProductViewModel
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Product Category cannot be empty")]
        public int? productCategory { get; set; }
        [Required(ErrorMessage = "Product Subcategory cannot be empty")]
        public int? productSubcategory { get; set; }
        [Required(ErrorMessage = "Product Type cannot be empty")]
        public int productType { get; set; }
        [Required(ErrorMessage = "Product Brand cannot be empty")]
        public int productBrand { get; set; }
        [Required(ErrorMessage = "Product Gender cannot be empty")]
        public string productGender { get; set; }
        [Required(ErrorMessage = "Product name cannot be empty")]
        public string productName { get; set; }
        [Required(ErrorMessage = "Price cannot be empty")]
        public string productPriceSelection { get; set; }
        public string productPrice { get; set; }
        [Required(ErrorMessage = "Size cannot be empty")]
        public List<string> availableSizes { get; set; }
        public int tenantId { get; set; }
        public string description { get; set; }
        public string sizeFit { get; set; }
        [Required(ErrorMessage = "A color is required")]
        public List<Variant> variant { get; set; }
        [Required(ErrorMessage = "Status cannot be empty")]
        public bool status { get; set; }
        public string statusDescription { get; set; }
    }

    public class Variant
    {
        public int id { get; set; }
        public string color { get; set; }
        public IEnumerable<HttpPostedFileBase> images { get; set; }
    }
    
}