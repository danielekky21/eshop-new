﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DynamicContent.Areas.SysManager.ViewModels
{
    public class TenantItemReportViewModel
    {
        public string tenantName { get; set; }
        public int totalItems { get; set; }
    }
}
