﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using DynamicContent.Helper;
using DynamicContent.Areas.SysManager.ViewModels;

namespace DynamicContent.Areas.SysManager.Helper
{
    public class ResultSet
    {

        public IQueryable<TenantItemReportViewModel> reportGetResult(string search, string sortOrder, int start, int length, IQueryable<TenantItemReportViewModel> dtResult, List<string> columnFilters)
        {
            return reportFilterResult(search, dtResult, columnFilters).SortBy(sortOrder).Skip(start).Take(length);
        }

        public int reportCount(string search, IQueryable<TenantItemReportViewModel> dtResult, List<string> columnFilters)
        {
            return reportFilterResult(search, dtResult, columnFilters).Count();
        }

        private IQueryable<TenantItemReportViewModel> reportFilterResult(string search, IQueryable<TenantItemReportViewModel> dtResult, List<string> columnFilters)
        {
            IQueryable<TenantItemReportViewModel> results = dtResult.AsQueryable();
            return results;
        }

        public IQueryable<SubscriberReportViewModel> subcriberReportGetResult(string search, string sortOrder, int start, int length, IQueryable<SubscriberReportViewModel> dtResult, List<string> columnFilters)
        {
            return subcriberReportFilterResult(search, dtResult, columnFilters).SortBy(sortOrder).Skip(start).Take(length);
        }
        public IQueryable<reservationHeaderModel> reservationHeaderGetResult(string search, string sortOrder, int start, int length, IQueryable<reservationHeaderModel> dtResult, List<string> columnFilters)
        {
            return reservationHeaderReportFilterResult(search, dtResult, columnFilters).SortBy(sortOrder).Skip(start).Take(length);
        }
        public int subcriberReportCount(string search, IQueryable<SubscriberReportViewModel> dtResult, List<string> columnFilters)
        {
            return subcriberReportFilterResult(search, dtResult, columnFilters).Count();
        }
        public int reservationHeaderCount(string search, IQueryable<reservationHeaderModel> dtResult, List<string> columnFilters)
        {
            return reservationHeaderReportFilterResult(search, dtResult, columnFilters).Count();
        }
        private IQueryable<SubscriberReportViewModel> subcriberReportFilterResult(string search, IQueryable<SubscriberReportViewModel> dtResult, List<string> columnFilters)
        {
            IQueryable<SubscriberReportViewModel> results = dtResult.AsQueryable();
            return results;
        }
        private IQueryable<reservationHeaderModel> reservationHeaderReportFilterResult(string search, IQueryable<reservationHeaderModel> dtResult, List<string> columnFilters)
        {
            IQueryable<reservationHeaderModel> results = dtResult.AsQueryable();
            if (columnFilters.Count() >= 4 && columnFilters[5] != null)
            {
                var ci = columnFilters[5].ToString();
                return results.Where(x => x.FUStatus == ci);
            }
            return results;
        }
        public string getTenantNameByID(int id)
        {
            DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
            DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();
            return db.tbl_tenant.FirstOrDefault(x => x.ID == id).name;
        }

        public IQueryable<CustomerReportViewModel> customerReportGetResult(string search, string sortOrder, int start, int length, IQueryable<CustomerReportViewModel> dtResult, List<string> columnFilters)
        {
            return customerReportFilterResult(search, dtResult, columnFilters).SortBy(sortOrder).Skip(start).Take(length);
        }

        public int customerReportCount(string search, IQueryable<CustomerReportViewModel> dtResult, List<string> columnFilters)
        {
            return customerReportFilterResult(search, dtResult, columnFilters).Count();
        }

        private IQueryable<CustomerReportViewModel> customerReportFilterResult(string search, IQueryable<CustomerReportViewModel> dtResult, List<string> columnFilters)
        {
            IQueryable<CustomerReportViewModel> results = dtResult.AsQueryable();
            results = results.Where(p => (search == null || (p.customerName != null && p.customerName.ToLower().Contains(search.ToLower()))
                ));
            return results;
        }



        public List<_object.AllProduct> GetResult(string search, string sortOrder, int start, int length, List<_object.AllProduct> dtResult, List<string> columnFilters)
        {
            return FilterResult(search, dtResult, columnFilters).SortBy(sortOrder).Skip(start).Take(length).ToList();
        }
        public List<_object.FlashSaleHeader> GetFlashSaleHeaderResult(string search, string sortOrder, int start, int length, List<_object.FlashSaleHeader> dtResult, List<string> columnFilters)
        {
            return FilterFlashSaleHeaderResult(search, dtResult, columnFilters).SortBy(sortOrder).Skip(start).Take(length).ToList();
        }
        public List<_object.TenantBanner> GetTenantBannerResult(string search, string sortOrder, int start, int length, List<_object.TenantBanner> dtResult, List<string> columnFilters)
        {
            return FilterTenantBannerResult(search,dtResult,columnFilters).SortBy(sortOrder).Skip(start).Take(length).ToList();
        }
        public List<_object.TopPicks> GetTopPicksResult(string search, string sortOrder, int start, int length, List<_object.TopPicks> dtResult, List<string> columnFilters)
        {
            return FilterTopPicksResult(search, dtResult, columnFilters).SortBy(sortOrder).Skip(start).Take(length).OrderBy(x => x.sort).ToList();
        }
        public int Count(string search, List<_object.AllProduct> dtResult, List<string> columnFilters)
        {
            return FilterResult(search, dtResult, columnFilters).Count();
        }
        public int CountFlashHeader(string search, List<_object.FlashSaleHeader> dtResult, List<string> columnFilters)
        {
            return FilterFlashSaleHeaderResult(search, dtResult, columnFilters).Count();
        }
        public int CountBannerTenant(string search, List<_object.TenantBanner> dtResult, List<string> columnFilters)
        {
            return FilterTenantBannerResult(search, dtResult, columnFilters).Count();
        }
        public int CountTopPicksResult(string search, List<_object.TopPicks> dtResult, List<string> columnFilters)
        {
            return FilterTopPicksResult(search, dtResult, columnFilters).Count();
        }
        private IQueryable<_object.AllProduct> FilterResult(string search, List<_object.AllProduct> dtResult, List<string> columnFilters)
        {
            IQueryable<_object.AllProduct> results = dtResult.AsQueryable();

            results = results.Where(p => (search == null || (p.name != null && p.name.ToLower().Contains(search.ToLower()))
                && (columnFilters[0] == null || (p.name != null && p.name.ToLower().Contains(columnFilters[0].ToLower())))
                ));

            if (columnFilters.Count() >= 2 && columnFilters[2] != null)
            {
                results = results.Where(p => p.idtenant == Convert.ToInt32(columnFilters[2].ToString()));
            }

            return results;
        }
        private IQueryable<_object.FlashSaleHeader> FilterFlashSaleHeaderResult(string search, List<_object.FlashSaleHeader> dtResult, List<string> columnFilters)
        {
            IQueryable<_object.FlashSaleHeader> results = dtResult.AsQueryable();
            results = results.Where(p => (search == null || (p.Name != null && p.Name.ToLower().Contains(search.ToLower()))
                && (columnFilters[0] == null || (p.Name != null && p.Name.ToLower().Contains(columnFilters[0].ToLower())))));
            if (columnFilters.Count() >= 2 && columnFilters[2] != null)
            {
                results = results.Where(p => p.id == Guid.Parse(columnFilters[2].ToString()));
            }

            return results;
        }
        private IQueryable<_object.TenantBanner> FilterTenantBannerResult(string search, List<_object.TenantBanner> dtResult, List<string> columnFilters)
        {
            IQueryable<_object.TenantBanner> results = dtResult.AsQueryable();
            results = results.Where(p => (search == null || (p.TenantName != null && p.TenantName.ToLower().Contains(search.ToLower()))
                && (columnFilters[0] == null || (p.TenantName != null && p.TenantName.ToLower().Contains(columnFilters[0].ToLower())))));
            if (columnFilters.Count() >= 2 && columnFilters[2] != null)
            {
                results = results.Where(p => p.id == int.Parse(columnFilters[2].ToString()));
            }
            return results;
        }
        private IQueryable<_object.TopPicks> FilterTopPicksResult(string search, List<_object.TopPicks> dtResult, List<string> columnFilters)
        {
            IQueryable<_object.TopPicks> results = dtResult.AsQueryable();
            results = results.Where(p => (search == null || (p.ProductName != null && p.ProductName.ToLower().Contains(search.ToLower()))
                && (columnFilters[0] == null || (p.ProductName != null && p.ProductName.ToLower().Contains(columnFilters[0].ToLower())))));
            if (columnFilters.Count() >= 2 && columnFilters[2] != null)
            {
                results = results.Where(p => p.TopPicksID == int.Parse(columnFilters[2].ToString()));
            }

            return results;
        }

        public List<_object.UserRegister> GetResult(string search, string sortOrder, int start, int length, List<_object.UserRegister> dtResult, List<string> columnFilters)
        {
            return FilterResult(search, dtResult, columnFilters).SortBy(sortOrder).Skip(start).Take(length).ToList();
        }

        public int Count(string search, List<_object.UserRegister> dtResult, List<string> columnFilters)
        {
            return FilterResult(search, dtResult, columnFilters).Count();
        }

        private IQueryable<_object.UserRegister> FilterResult(string search, List<_object.UserRegister> dtResult, List<string> columnFilters)
        {
            IQueryable<_object.UserRegister> results = dtResult.AsQueryable();

            results = results.Where(p => (search == null || (p.firstname != null && p.firstname.ToLower().Contains(search.ToLower()))
                && (columnFilters[0] == null || (p.firstname != null && p.firstname.ToLower().Contains(columnFilters[0].ToLower())))
                ));

            return results;
        }




    }
}