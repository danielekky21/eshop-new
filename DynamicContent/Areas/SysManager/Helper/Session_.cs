﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DynamicContent.Areas.SysManager.Helper
{
    public class Session_
    {
        public static string SessionId
        {
            get
            {
                if (System.Web.HttpContext.Current.Session.SessionID != null)
                    return System.Web.HttpContext.Current.Session.SessionID;
                else return "";
            }
        }
        public static void destroy()
        {
            HttpContext.Current.Session.Abandon();
        }
        public static string SystemId
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["SystemId"] != null)
                    return System.Web.HttpContext.Current.Session["SystemId"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["SystemId"] = value; }
        }
        public static string RoleID
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["RoleID"] != null)
                    return System.Web.HttpContext.Current.Session["RoleID"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["RoleID"] = value; }
        }

        public static string Username
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["Username"] != null)
                    return System.Web.HttpContext.Current.Session["Username"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["Username"] = value; }
        }

        public static List<int> Priviledge
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["Priviledge"] == null)
                {
                    System.Web.HttpContext.Current.Session["Priviledge"] = new List<int>();
                }
                return System.Web.HttpContext.Current.Session["Priviledge"] as List<int>;
            }
            set { System.Web.HttpContext.Current.Session["Priviledge"] = value; }
        }
    }
}