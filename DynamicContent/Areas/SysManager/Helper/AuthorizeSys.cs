﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DynamicContent.Areas.SysManager.Helper
{
    public class AuthorizeSys : AuthorizeAttribute
    {
        DynamicContent.Areas.SysManager.Controllers.AccountController account = new Areas.SysManager.Controllers.AccountController();

        public AuthorizeSys()
        {
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var rd = httpContext.Request.RequestContext.RouteData;
            
            if(rd.Route != null)
            {
                var controller = rd.GetRequiredString("controller");
                var action = rd.GetRequiredString("action");
                return account.Authentication(controller, action);
            }
            else if(httpContext.Request.RawUrl.Contains(".aspx"))
            {
                return account.AuthenticationAspx();
            }
            else
            {
                return false;
            }
            //return account.Authentication("","");
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var errmsg = "";
            var request = filterContext.HttpContext.Request;
            if (!String.IsNullOrEmpty(Session_.Username) && !String.IsNullOrEmpty(Session_.SessionId))
                errmsg = "Someone is logged in using your credential. If this is not you, please change login again and change your password immediately.";
            if (request["X-Requested-With"] == "XMLHttpRequest" || (request.Headers != null && request.Headers["X-Requested-With"] == "XMLHttpRequest"))
            {
                var urlHelper = new UrlHelper(filterContext.RequestContext);
                filterContext.HttpContext.Response.StatusCode = 401;
                filterContext.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;
                filterContext.Result = new JsonResult
                {
                    Data = new
                    {
                        error = "NotAuthorized",
                        logonurl = urlHelper.Action("Logout", "Account", new { area = "SysManager" }),
                        msg = errmsg
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                if (String.IsNullOrEmpty(Session_.Username))
                {
                    filterContext.Result = new RedirectToRouteResult(
                                    new RouteValueDictionary(
                                        new
                                        {
                                            controller = "Account",
                                            action = "Login",
                                            area = "SysManager"
                                        })
                                    );
                }
                else
                {
                    filterContext.Result = new RedirectToRouteResult(
                                    new RouteValueDictionary(
                                        new
                                        {
                                            controller = "Account",
                                            action = "PageNotFound",
                                            area = "SysManager"
                                        })
                                    );
                }
            }
            
           
        }
        
    }
}