﻿using DynamicContent.Areas.SysManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DynamicContent.Areas.SysManager.Helper
{
    public class dbHelper
    {

        public static FlashSaleHeaderViewModel getFlashSaleHeaderByID(Guid id)
        {
            DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
            return db.FlashSaleHeaders.Where(x => x.FlashSaleHeaderID == id).Select(p => new FlashSaleHeaderViewModel {
                FlashSaleHeaderID = p.FlashSaleHeaderID,
                FlashSaleDay = p.FlashSaleDay,
                FlashSaleTimeBegin = p.FlashSaleTimeBegin,
                FlashSaleTimeEnd = p.FlashSaleTimeEnd,
                FlashSaleName = p.FlashSaleName,
                IsActive = p.IsActive,
                UpdatedBy = p.UpdatedBy,
                UpdatedDate = p.UpdatedDate
            }).FirstOrDefault();
        }
        public static DynamicContent.Models.FlashHeaderDetail GetFlashSaleDetailByID(int ID)
        {
            DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
            return db.FlashHeaderDetails.FirstOrDefault(x => x.FlashSaleDetailID == ID && x.isActive == true);
        }
        public static DynamicContent.Models.TopPick GetTopPickByID(int ID)
        {
            DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
            return db.TopPicks.FirstOrDefault(x => x.TopPicksID == ID && x.IsActive == true);
        }
    }
}