﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DynamicContent.Filters
{
    public class LanguageFilter : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //set default language
            if (filterContext.HttpContext.Session["lang"] == null)
                filterContext.HttpContext.Session["lang"] = "ID";

            base.OnActionExecuting(filterContext);
        }

    }
}
