﻿using DynamicContent.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using System.Globalization;

namespace DynamicContent.Models
{
    public class PagesModel : Controller
    {
        //
        // GET: /Manager/Home/
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();

        private DynamicContent.Helper.Generator gen = new DynamicContent.Helper.Generator();
        
        //-------------SLIDER-------------
        public IEnumerable<_object.Slider> GetSlider()
        {
            var rs = from b in db.tbl_slider
                     orderby b.sort ascending
                     select new _object.Slider
                     {
                         id = b.ID,
                         name = b.name,
                         image = b.image,
                         active = b.active,
                         sort = b.sort,
                         url = b.url,
                         Type = b.Type
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.Slider> GetSlider2()
        {
            var rs = from b in db.tbl_slider2
                     orderby b.sort ascending
                     select new _object.Slider
                     {
                         id = b.ID,
                         name = b.name,
                         image = b.image,
                         active = b.active,
                         sort = b.sort,
                         url = b.url
                     };
            return rs.ToList();
        }

        public DynamicContent.Models.tbl_slider GetSliderById(int id)
        {
            var rs = from b in db.tbl_slider
                     where b.ID == id
                     select b;
            return rs.FirstOrDefault();
        }

        public DynamicContent.Models.tbl_slider2 GetSlider2ById(int id)
        {
            var rs = from b in db.tbl_slider2
                     where b.ID == id
                     select b;
            return rs.FirstOrDefault();
        }

        public IEnumerable<_object.NowOpen> GetNowopen()
        {
            var rs = from b in db.NowOpen
                     orderby b.sort ascending
                     select new _object.NowOpen
                     {
                         id = b.ID,
                         name = b.name,
                         description = b.description,
                         image = b.image,
                         active = b.active,
                         sort = b.sort
                     };
            return rs.ToList();
        }
        
        public DynamicContent.Models.NowOpen GetNowopenById(int id)
        {
            var rs = from b in db.NowOpen
                     where b.ID == id
                     select b;
            return rs.FirstOrDefault();
        }

        public void SliderEdit(DynamicContent.Models.tbl_slider data)
        {
            var currentSlider = GetSliderById(data.ID);
            if(currentSlider != null)
            {
                currentSlider.modified_by = data.modified_by;
                currentSlider.modified_date = DateTime.Now;
                currentSlider.name = data.name;
                currentSlider.image = data.image;
                db.Entry(data).CurrentValues.SetValues(currentSlider);
                db.SaveChanges();
            }
        }

        public void SliderAdd(DynamicContent.Models.tbl_slider data)
        {
            db.tbl_slider.Add(data);
            db.SaveChanges();
        }

        public void AddProductImage(DynamicContent.Models.tbl_product_image data)
        {
            db.tbl_product_image.Add(data);
            db.SaveChanges();
        }

        
        public DynamicContent.Models.tbl_slider SliderDelete(int id)
        {
            var d = GetSliderById(id);
            db.tbl_slider.Remove(d);
            db.SaveChanges();
            return d;
        }

        //-------------END SLIDER-------------
        //GetSubmenu
        public IEnumerable<_object.Submenu> GetSubmenu(string parent)
        {
            var rs = from b in db.tbl_submenu
                     where b.parent == parent
                     orderby b.created_date ascending
                     select new _object.Submenu
                     {
                         id = b.ID,
                         name = b.name,
                         images = b.images,
                     };
            return rs.ToList();
        }
       
        public DynamicContent.Models.tbl_submenu GetSubmenuCategory(int id)
        {
            var rs = from b in db.tbl_submenu
                     where b.ID == id
                     select b;

            return rs.FirstOrDefault();
        }

        //GetBrand PI
        public IEnumerable<_object.Brand> GetBrand()
        {
            var rs = from b in db.tbl_brand
                     orderby b.name ascending
                     select new _object.Brand
                     {
                         id = b.ID,
                         name = b.name
                     };
            return rs.ToList();
        }
        public IEnumerable<_object.Brand> GetTenantBrand(int[] id)
        {
            var rs = from b in db.tbl_brand
                     where id.Contains(b.ID)
                     orderby b.name ascending
                     select new _object.Brand
                     {
                         id = b.ID,
                         name = b.name
                     };
            return rs.ToList();
        }
        public IEnumerable<_object.BrandWithParam> GetBrandWithParam(int id)
        {
            var rs = from b in db.tbl_brand
                     join s in db.tbl_submenu on b.submenu_id equals s.ID
                     where b.ID == id
                     select new _object.BrandWithParam
                     {
                         id = b.ID,
                         name = b.name,
                         subid = s.ID,
                         subname = s.name,
                         parent = s.parent
                     };
            return rs.ToList();
        }
        
        public IEnumerable<_object.ProductCategory> GetProdCategory()
        {
            var rs = from b in db.tbl_category
                     orderby b.name ascending
                     select new _object.ProductCategory
                     {
                         id = b.ID,
                         name = b.name
                     };
            return rs.ToList();
        }

        public IEnumerable<DynamicContent.Models.tbl_tenant> GetListTenant(long[] id)
        {
            var rs = from t in db.tbl_tenant
                     where id.Contains(t.ID)
                     select t;
            return rs.ToList();
        }

        public IEnumerable<_object.TenantByCategory> ProdTenantByCategory(long[] id)
        {

            var rs = from b in db.tbl_tenant
                     where id.Contains(b.ID)
                     select new _object.TenantByCategory
                     {
                         id = b.ID,
                         name = b.name
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.ProductCategory> GetProdCategories(int[] id) {

            var rs = from b in db.tbl_category
                     where id.Contains(b.ID)
                     select new _object.ProductCategory
                     {
                         id = b.ID,
                         name = b.name
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.ProductType> GetProdType(long[] id)
        {

            var rs = from b in db.TypeMasters
                     where id.Contains(b.ID)
                     select new _object.ProductType
                     {
                         id = b.ID,
                         typeValue = b.TypeValue,
                         typeDesc = b.TypeDesc
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.ProductType> GetTypeDetail(string id)
        {
            var ids = id.Split(',');
            var typeId = Convert.ToInt32(ids[0]);
            var subTypeId = Convert.ToInt32(ids[1]);
            var rs = from b in db.TypeMasters
                     where b.TypeCategory == typeId && b.TypeSubCategory == subTypeId && b.isActive == "Y" && b.isShowMenu == "Y"
                     select new _object.ProductType
                     {
                         id = b.ID,
                         typeValue = b.TypeValue,
                         typeDesc = b.TypeDesc
                     };
            return rs.ToList();
        }

        

        public IEnumerable<_object.TenantCategory> GetTenantCategories(int[] id)
        {
            var rs = from b in db.tbl_TenantCategory
                     where id.Contains(b.ID)
                     select new _object.TenantCategory
                     {
                         id = b.ID,
                         name = b.name,
                         code = b.code
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.SubmenuCategory> GetTenantSubcategories(int[] id, string parent)
        {
            var rs = from b in db.tbl_submenu
                     where id.Contains(b.ID) && b.parent == parent
                     select new _object.SubmenuCategory
                     {
                         id = b.ID,
                         name = b.name,
                         parent = b.parent
                     };
            return rs.ToList();
        }
        //-------------PRODUCT-------------
        public IEnumerable<_object.ProductPi> GetProductPi()
        {
            var rs = from p in db.tbl_product
                     //join c in db.TypeMasters on p.category equals c.ID
                     join tm in db.TenantMaster on p.tenant equals tm.TenantID
                     where tm.isJoin == "Y" && p.status == true && p.approved == true
                     orderby p.created_date descending
                     select new _object.ProductPi
                     {
                         id = p.ID,
                         name = p.name,
                         images = p.images,
                         price = p.price,
                         size = p.size,
                         description = p.description,
                         size_fit = p.size_fit,
                         gender = p.gender,
                         //category = c.TypeDesc,
                         categoryid = p.category,
                         tenant = p.tenant,
                         isHomepage = p.isHomepage,
                         isOrder = p.isOrder,
                         created_date = p.created_date
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.ProductBySubmenu> ProductBySubmenu(int id)
        {
            var rs = from p in db.tbl_product
                     //join c in db.TypeMasters on p.category equals c.ID
                     join s in db.tbl_submenu on p.tenant_subcategory equals s.ID
                     join tm in db.TenantMaster on p.tenant equals tm.TenantID
                     where tm.isJoin == "Y" && p.status == true && p.approved == true && s.ID == id
                     orderby p.name ascending
                     select new _object.ProductBySubmenu
                     {
                         id = p.ID,
                         name = p.name,
                         images = p.images,
                         price = p.price,
                         gender = p.gender,
                         tenantid = p.tenant,
                         //category = c.TypeDesc,
                         //categoryKey = c.TypeValue,
                         //categoryid = c.ID,
                         categoryid = p.category,
                         size = p.size,
                         idsubmenu = s.ID,
                         submenu = s.name,
                         tenant_category = p.tenant_category,
                         tenant_subcategory = p.tenant_subcategory
                     };
            return rs.ToList();
        }        

        public IEnumerable<_object.AllProduct> GetAllProduct(int? id)
        {
            var rs = from p in db.tbl_product
                     //join c in db.TypeMasters on p.category equals c.ID
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     where p.status == true && p.ID == id
                     select new _object.AllProduct
                     {
                         id = p.ID,
                         name = p.name,
                         images = p.images,
                         price = p.price,
                         size = p.size,
                         description = p.description,
                         size_fit = p.size_fit,
                         gender = p.gender,
                         idcategory = p.category,
                         //category = c.TypeDesc,
                         idtenant = t.ID,
                         tenant = t.name,
                         status = p.status,
                         tenant_category = p.tenant_category,
                         tenant_subcategory = p.tenant_subcategory
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.AllProduct> GetAllProduct2(int? id)
        {
            var rs = from p in db.tbl_product
                         //join c in db.TypeMasters on p.category equals c.ID
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     where p.ID == id
                     select new _object.AllProduct
                     {
                         id = p.ID,
                         name = p.name,
                         images = p.images,
                         price = p.price,
                         size = p.size,
                         description = p.description,
                         size_fit = p.size_fit,
                         gender = p.gender,
                         idcategory = p.category,
                         //category = c.TypeDesc,
                         idtenant = t.ID,
                         tenant = t.name,
                         status = p.status,
                         tenant_category = p.tenant_category,
                         tenant_subcategory = p.tenant_subcategory
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.AllProduct> GetAllProductTmp(int? id)
        {
            var rs = from p in db.tbl_product_tmp
                     join c in db.tbl_submenu on p.tenant_subcategory equals c.ID
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     where p.ID == id
                     select new _object.AllProduct
                     {
                         id = p.ID,
                         name = p.name,
                         images = p.images,
                         price = p.price,
                         size = p.size,
                         description = p.description,
                         size_fit = p.size_fit,
                         gender = p.gender,
                         idcategory = c.ID,
                         category = c.name,
                         idtenant = t.ID,
                         tenant = t.name,
                         status = p.status,
                         tenant_category = p.tenant_category,
                         tenant_subcategory = p.tenant_subcategory
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.AllProduct> GetAllProductTmp2(int? id)
        {
            var rs = from p in db.tbl_product_tmp
                     //join c in db.tbl_submenu on p.tenant_subcategory equals c.ID
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     where p.ID == id
                     select new _object.AllProduct
                     {
                         id = p.ID,
                         name = p.name,
                         images = p.images,
                         price = p.price,
                         size = p.size,
                         description = p.description,
                         size_fit = p.size_fit,
                         gender = p.gender,
                         //idcategory = c.ID,
                         //category = c.name,
                         idtenant = t.ID,
                         tenant = t.name,
                         status = p.status,
                         tenant_category = p.tenant_category,
                         tenant_subcategory = p.tenant_subcategory
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.productUploadByBrand> ItemsUploadedByBrand()
        {
            var rs = from p in db.tbl_product 
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     orderby p.created_date descending
                     select new _object.productUploadByBrand  
                     {
                         id = p.ID,
                         name = p.name,
                         tenant = t.ID, 
                         tenantName = t.name,
                         created_date = p.created_date
                     };
            return rs.ToList();
        }
        public IEnumerable<_object.ReservationHeader> ReservationHeader()
        {
            var rs = from u in db.vw_ReservationHeader
                     orderby u.ReservationDate descending
                     select new _object.ReservationHeader
                     {
                         ReservatonCode = u.ReservationCode,
                         ReservationDate = u.ReservationDate.ToString(),
                         CustomerEmail = u.CustomerEmail,
                         CustomerName = u.CustomerName,
                         ShopName = u.ShopName,
                         FUStatus = u.FUStatus,
                         FUName = u.FUName,
                         FUNote = u.FUNote
                     };
            return rs.ToList();
        }
        public IEnumerable<_object.lovedProduct> GetMostLovedProduct(int id)
        {
            var rs = from w in db.tbl_wishlist
                     join p in db.tbl_product on w.product equals p.ID
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     where t.ID == id orderby w.created_date descending
                     select new _object.lovedProduct 
                     {
                        id = p.ID,
                        wishlistID = w.ID,
                        prod_created_date = p.created_date,
                        created_date = w.created_date,
                        name = p.name,
                        tenant = t.name,
                        status = p.status
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.lovedProduct> GetMostLovedProductSysManager()
        {
            var rs = from w in db.tbl_wishlist
                     join p in db.tbl_product on w.product equals p.ID
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     orderby w.created_date descending
                     select new _object.lovedProduct
                     {
                         id = p.ID,
                         wishlistID = w.ID,
                         created_date = w.created_date,
                         prod_created_date = p.created_date,
                         name = p.name,
                         tenant = t.name,
                         status = p.status
                     };
            return rs.ToList();
        }
        
        public IEnumerable<_object.viewedProduct> GetMostViewedProduct(int id)
        {
            var rs = from v in db.tbl_product_view
                     join p in db.tbl_product on v.product_id equals p.ID
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     where t.ID == id
                     select new _object.viewedProduct
                     {                         
                         id = p.ID,
                         viewID = v.ID,
                         created_date = v.created_date, 
                         prod_created_date = p.created_date,
                         name = p.name,
                         tenant = t.name,
                         status = p.status
                     };
            return rs.ToList();
        }
        
        public IEnumerable<_object.viewedProduct> GetMostViewedProductSysManager()
        {
            var rs = from v in db.tbl_product_view
                     join p in db.tbl_product on v.product_id equals p.ID
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     //orderby v.created_date descending
                     select new _object.viewedProduct
                     {
                         id = p.ID,
                         viewID = v.ID,
                         created_date = v.created_date,
                         prod_created_date = p.created_date,
                         name = p.name,
                         tenant = t.name,
                         status = p.status
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.reservedProduct> GetMostReservedProduct(int id)
        {
            var rs = from r in db.tbl_reservation
                     join pr in db.tbl_ReservationParent on r.parentReservation equals pr.ID
                     join p in db.tbl_product on r.product equals p.ID
                     join t in db.tbl_tenant on r.tenant equals t.ID
                     where t.ID == id 
                     select new _object.reservedProduct
                     {
                         id = r.product,
                         parentReservation = r.parentReservation,
                         created_date = pr.ReservationDate,
                         prod_created_date = p.created_date,
                         name = p.name,
                         tenant = t.name,
                         qty = r.qty,
                         status = p.status
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.reservedProduct> GetMostReservedProductSysManager()
        {
            var rs = from r in db.tbl_reservation
                     join pr in db.tbl_ReservationParent on r.parentReservation equals pr.ID
                     join p in db.tbl_product on r.product equals p.ID
                     join t in db.tbl_tenant on r.tenant equals t.ID
                     select new _object.reservedProduct
                     {
                         id = r.product,
                         parentReservation = r.parentReservation,
                         created_date = pr.ReservationDate,
                         prod_created_date = p.created_date,
                         name = p.name,
                         tenant = t.name,
                         qty = r.qty,
                         status = p.status
                     };
            return rs.ToList();
        }
        

        public IEnumerable<_object.lovedProduct> GetProductWishlist(int id)
        {
            var rs = from w in db.tbl_wishlist
                     join p in db.tbl_product on w.product equals p.ID
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     where w.product == id
                     
                     select new _object.lovedProduct
                     {
                         id = p.ID,
                         wishlistID = w.ID,
                         created_date = w.created_date,
                         name = p.name,
                         tenant = t.name,
                         status = p.status
                     };
            return rs.ToList();
        }
        
        public IEnumerable<_object.viewedProduct> GetProductViewed(int id)
        {
            var rs = from v in db.tbl_product_view
                     join p in db.tbl_product on v.product_id equals p.ID
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     where v.product_id == id
                     select new _object.viewedProduct
                     {
                         id = p.ID,
                         viewID = v.ID,
                         created_date = p.created_date,
                         name = p.name,
                         tenant = t.name,
                         status = p.status
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.AllProduct> GetAllProductByTenant(int id)
        {
            var rs = from p in db.tbl_product
                     join c in db.TypeMasters on p.category equals c.ID
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     where t.ID == id
                     select new _object.AllProduct
                     {
                         id = p.ID,
                         name = p.name,
                         images = p.images,
                         price = p.price,
                         size = p.size,
                         description = p.description,
                         size_fit = p.size_fit,
                         gender = p.gender,
                         idcategory = c.ID,
                         category = c.TypeDesc,
                         idtenant = t.ID,
                         tenant = t.name,
                         status = p.status,
                         tenant_category = p.tenant_category,
                         tenant_subcategory = p.tenant_subcategory
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.AllProduct> GetAllProductTmpByTenant(int id)
        {
            var rs = from p in db.tbl_product_tmp
                     join c in db.TypeMasters on p.category equals c.ID
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     where t.ID == id
                     select new _object.AllProduct
                     {
                         id = p.ID,
                         name = p.name,
                         images = p.images,
                         price = p.price,
                         size = p.size,
                         description = p.description,
                         size_fit = p.size_fit,
                         gender = p.gender,
                         idcategory = c.ID,
                         category = c.TypeDesc,
                         idtenant = t.ID,
                         tenant = t.name,
                         status = p.status,
                         tenant_category = p.tenant_category,
                         tenant_subcategory = p.tenant_subcategory,
                         modified_date = p.modified_date,
                         approval_status = p.approval_status,
                         approval_message= p.approval_message
                         
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.AllProduct> GetAllProductTmpByTenant2(int id)
        {
            var rs = from p in db.tbl_product_tmp
                     //join c in db.tbl_submenu on p.tenant_subcategory equals c.ID
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     where t.ID == id
                     select new _object.AllProduct
                     {
                         id = p.ID,
                         name = p.name,
                         images = p.images,
                         price = p.price,
                         size = p.size,
                         description = p.description,
                         size_fit = p.size_fit,
                         gender = p.gender,
                         //idcategory = c.ID,
                         //category = c.name,
                         idtenant = t.ID,
                         tenant = t.name,
                         status = p.status,
                         tenant_category = p.tenant_category,
                         tenant_subcategory = p.tenant_subcategory,
                         modified_date = p.modified_date,
                         approval_status = p.approval_status,
                         approval_message = p.approval_message

                     };
            return rs.ToList();
        }

        public IEnumerable<_object.AllProduct> GetAllProductTmpWaitingApproval()
        {
            var rs = from p in db.tbl_product_tmp
                     join c in db.tbl_submenu on p.tenant_subcategory equals c.ID
                     //join c in db.TypeMasters on p.category equals c.ID
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     where (p.approval_status == (int) Constant.approvalStatus.waiting)
                     select new _object.AllProduct
                     {
                         id = p.ID,
                         name = p.name,
                         images = p.images,
                         price = p.price,
                         size = p.size,
                         description = p.description,
                         size_fit = p.size_fit,
                         gender = p.gender,
                         idcategory = c.ID,
                         category = c.name,
                         idtenant = t.ID,
                         tenant = t.name,
                         status = p.status,
                         tenant_category = p.tenant_category,
                         tenant_subcategory = p.tenant_subcategory,
                         approval_status = p.approval_status,
                         modified_date = p.modified_date
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.AllProduct> GetAllProductTmpWaitingApproval2()
        {
            var rs = from p in db.tbl_product_tmp
                     //join c in db.tbl_submenu on p.tenant_subcategory equals c.ID
                     //join c in db.TypeMasters on p.category equals c.ID
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     where (p.approval_status == (int)Constant.approvalStatus.waiting)
                     select new _object.AllProduct
                     {
                         id = p.ID,
                         name = p.name,
                         images = p.images,
                         price = p.price,
                         size = p.size,
                         description = p.description,
                         size_fit = p.size_fit,
                         gender = p.gender,
                         //idcategory = c.ID,
                         //category = c.name,
                         idtenant = t.ID,
                         tenant = t.name,
                         status = p.status,
                         tenant_category = p.tenant_category,
                         tenant_subcategory = p.tenant_subcategory,
                         approval_status = p.approval_status,
                         modified_date = p.modified_date
                     };
            return rs.ToList();
        }
        public IEnumerable<_object.AllProduct> GetProductById(int id)
        {
            var rs = from p in db.tbl_product
                     join c in db.TypeMasters on p.category equals c.ID
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     where p.ID == id
                     select new _object.AllProduct
                     {
                         id = p.ID,
                         name = p.name,
                         images = p.images,
                         price = p.price,
                         size = p.size,
                         description = p.description,
                         size_fit = p.size_fit,
                         gender = p.gender,
                         idcategory = c.ID,
                         category = c.TypeDesc,
                         idtenant = t.ID,
                         tenant = t.name,
                         status = p.status,
                         tenant_category = p.tenant_category,
                         tenant_subcategory = p.tenant_subcategory
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.AllProduct> GetProductTmpById(int id)
        {
            var rs = from p in db.tbl_product_tmp
                     join c in db.TypeMasters on p.category equals c.ID
                     join t in db.tbl_tenant on p.tenant equals t.ID
                     where p.ID == id
                     select new _object.AllProduct
                     {
                         id = p.ID,
                         name = p.name,
                         images = p.images,
                         price = p.price,
                         size = p.size,
                         description = p.description,
                         size_fit = p.size_fit,
                         gender = p.gender,
                         idcategory = c.ID,
                         category = c.TypeDesc,
                         idtenant = t.ID,
                         tenant = t.name,
                         status = p.status,
                         tenant_category = p.tenant_category,
                         tenant_subcategory = p.tenant_subcategory
                     };
            return rs.ToList();
        }

        public IEnumerable<DynamicContent.Models.tbl_product> GetProductByTenant(int id)
        {
            var rs = from b in db.tbl_product
                     where b.tenant == id && b.status == true
                     orderby b.created_date descending
                     select b;
            return rs.ToList();
        }

        public string StatusProductCheck(bool stt)
        {
            string result = "";
            if (stt == false)
            {
                result = "Not Active";
            }
            else
            {
                result = "Active";
            }
            return result;
        }

        public DynamicContent.Models.tbl_submenu SubmenuByBrand(int id)
        {
            var rs = from s in db.tbl_submenu 
                     join b in db.tbl_brand on s.ID equals b.submenu_id
                     where b.ID == id
                     select s;
            return rs.FirstOrDefault();
        }

        //-------------END PRODUCT-------------

        //-----------TENANT------------

        public IEnumerable<_object.Tenant> GetTenant(int id)
        {
            var rs = from t in db.tbl_tenant
                     where t.ID == id
                     select new _object.Tenant
                     {
                         id = t.ID,
                         name = t.name,
                         images = t.image,
                         phone = t.phone,
                         description = t.description,
                         //locationid = t.location,
                         locationname = t.location,
                         reservation_email = t.reservation_email,
                         category = t.category,
                         //brand = t.brand,
                         tenant_category = t.tenant_category,
                         tenant_subcategory = t.tenant_subcategory
                     };
            return rs.ToList();
        }

        public DynamicContent.Models.M_User_Tenant GetTenantLogin(string id)
        {
            var rs = from b in db.M_User_Tenant
                     where b.username == id && b.del == false
                     select b;
            return rs.FirstOrDefault();
        }
        //-----------END TENANT------------
        public IEnumerable<DynamicContent.Models.tbl_wishlist> GetWishlist(int id)
        {
            var rs = from b in db.tbl_wishlist
                     where b.user_register == id
                     orderby b.created_date descending
                     select b;
            return rs.ToList();
        }

        public IEnumerable<DynamicContent.Models.tbl_wishlist> GetExistWishlist(int id, int color, int userID)
        {
            var rs = from b in db.tbl_wishlist
                     where b.user_register == userID && b.product == id && b.color == color
                     orderby b.created_date descending
                     select b;
            return rs.ToList();
        }
        
        public DynamicContent.Models.tbl_wishlist RemoveWishlist(int id)
        {
            var d = db.tbl_wishlist.SingleOrDefault(p => p.ID == id);
            db.tbl_wishlist.Remove(d);
            db.SaveChanges();

            return d;
        }

        public IEnumerable<DynamicContent.Models.tbl_cart> GetMyCart(int id)
        {
            var rs = from b in db.tbl_cart
                     where b.user_register == id && b.reserved == false
                     orderby b.created_date descending
                     select b;
            return rs.ToList();
        }

        public void changeQtyReservation(DynamicContent.Models.tbl_cart data)
        {
            var item = GetExistCart(data.user_register, data.size, data.color).FirstOrDefault();
            if (item != null)
            {
                item.qty = data.qty;
                db.Entry(data).CurrentValues.SetValues(item);
                db.SaveChanges();
            }
        }



        public IEnumerable<DynamicContent.Models.tbl_cart> GetExistCart(int? userID, string sz, int? color)
        {
            var rs = from b in db.tbl_cart
                     where b.user_register == userID && b.size == sz && b.color == color && b.reserved == false
                     orderby b.created_date descending
                     select b;
            return rs.ToList();
        }

        public IEnumerable<DynamicContent.Models.tbl_ReservationParent> GetMyCartReserved(int id)
        {
            var rs = from b in db.tbl_ReservationParent
                     where b.CustID == id
                     orderby b.ReservationDate descending
                     select b;
            return rs.ToList();
        }

        public IEnumerable<DynamicContent.Models.tbl_reservation> GetDetailReservation(int id)
        {
            var rs = from b in db.tbl_reservation
                     where b.parentReservation == id
                     orderby b.tenant ascending
                     select b;
            return rs.ToList();
        }

        public class MyEntity
        {
            public int MyEntityId { get; set; }
            public DateTime created_date { get; set; }
        }
        
        public IEnumerable<_object.Tenant> GetTenantNO()
        {
            var rs = from t in db.tbl_tenant
                     join tm in db.TenantMaster on t.ID equals tm.TenantID
                     where tm.isJoin == "Y" && tm.isShowMenu == "Y" 
                     select new _object.Tenant
                     {
                         id = t.ID,
                         name = t.name,
                         images = t.image,
                         phone = t.phone,
                         description = t.description,
                         //locationid = t.location,
                         locationname = t.location
                     };
            return rs.ToList();
        }

        public IEnumerable<string> getTenantFirstLetter()
        {
            var rs = (from p in db.tbl_tenant
                      let first = p.name.Substring(0, 1)
                      orderby first
                      select first).Distinct();
            return rs.ToList();
        }

        public void UserRegister(DynamicContent.Models.tbl_user_register user)
        {
            db.tbl_user_register.Add(user);
            db.SaveChanges();
        }

        public void ReservationParent(DynamicContent.Models.tbl_ReservationParent data)
        {
            db.tbl_ReservationParent.Add(data);
            db.SaveChanges();
        }
        
        public void AddReservation(DynamicContent.Models.tbl_reservation data)
        {
            db.tbl_reservation.Add(data);
            db.SaveChanges();
        }
        public void FlashDealReserv(int ID, int QTY, Guid FlashDealHeaderID)
        {
            var FlashDeal = db.FlashSaleHeaders.FirstOrDefault(x => x.FlashSaleHeaderID == FlashDealHeaderID);
            if (FlashDeal != null)
            {
                var flashDealDetail = FlashDeal.FlashHeaderDetails.FirstOrDefault(x => x.tbl_product.ID == ID);
                flashDealDetail.qty = flashDealDetail.qty - QTY;
                db.SaveChanges();
            }
        }
        public void AddToCart(DynamicContent.Models.tbl_cart data)
        {
            db.tbl_cart.Add(data);
            db.SaveChanges();
        }

        public string stringToURL(string title)
        {
            var replacedTitle = Regex.Replace(title, "(?:(?![a-z0-9]).)+", "_", RegexOptions.IgnoreCase);
            title = replacedTitle.ToString();
            replacedTitle = null;
            return title;
        }

        public string wordsUpper(string title)
        {
            var result = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(title);
            return result;
        }

        
        public string toIDR(string idr)
        {
            var result = Convert.ToDecimal(idr).ToString("N0").Replace(',', '.');
            return result;
        }
        
        public string priceCheck(string price)
        {
            var pAdd = "IDR ";
            var result = "";
            if (price != "price upon request")
            {
                result = pAdd + " " + toIDR(price);
            }
            else
            {
                result = price;
             }
            
            return result;
        }
    }
}
