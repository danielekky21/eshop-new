﻿using DynamicContent.Areas.TenantManager.Helper;
using DynamicContent.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DynamicContent.Models
{
    public class PrivModel : Controller
    {
        //
        // GET: /Manager/Home/
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();

        private DynamicContent.Helper.Generator gen = new DynamicContent.Helper.Generator();

        public List<int> GetUserPriv(string roleid)
        {
            return (from a in db.M_PrivRoleAccess where a.role_id == roleid && !a.del select a.priv_id).ToList();
        }
        


        public IEnumerable<DynamicContent.Models.M_PrivRole> getAllRole()
        {
            var rs = (from b in db.M_PrivRole
                      where b.del == false
                      select b).ToList();
            return rs;
        }

        public DynamicContent.Models.M_PrivRole GetRole(string roleId)
        {
            var rs = (from b in db.M_PrivRole
                      where b.role_id == roleId && b.del == false
                      select b).FirstOrDefault();
            return rs;
        }

        public IEnumerable<DynamicContent.Models.M_Priv> GetAllPriv()
        {
            var rs = (from b in db.M_Priv
                      select b).ToList();
            return rs;
        }

        public IEnumerable<DynamicContent.Models.M_PrivType> GetAllPrivType()
        {
            return (from b in db.M_PrivType orderby b.sort select b).ToList();
        }

        public void UserRoleDelete(string roleId, string by)
        {
            DynamicContent.Models.M_PrivRole role = GetRole(roleId);
            role.del = true;
            role.modified_by = by;
            role.modified_date = DateTime.Now;
            db.Entry(role).CurrentValues.SetValues(role);
            db.SaveChanges();
        }

        public void UpdateUserRole(string id, string desc, string by)
        {
            DynamicContent.Models.M_PrivRole role = GetRole(id);
            role.description = desc;
            role.modified_by = by;
            role.modified_date = DateTime.Now;
            db.Entry(role).CurrentValues.SetValues(role);
            db.SaveChanges();
        }

        public void AddUserRole(DynamicContent.Models.M_PrivRole role)
        {
            db.M_PrivRole.Add(role);
            db.SaveChanges();
        }

        public bool RoleExistFull(string RoleName)
        {
            var temp = (from b in db.M_PrivRole
                        where b.role_id == RoleName
                        select b).FirstOrDefault();
            bool rs = (temp != null ? true : false);
            return rs;
        }

        public bool RoleExist(string RoleName)
        {
            var temp = (from b in db.M_PrivRole
                        where b.role_id == RoleName && b.del == false
                        select b).FirstOrDefault();
            bool rs = (temp != null ? true : false);
            return rs;
        }
        


        public DynamicContent.Models.M_PrivRoleAccess GetRoleAccess(int Id)
        {
            var rs = (from b in db.M_PrivRoleAccess
                      where b.id == Id
                      select b).FirstOrDefault();
            return rs;
        }

        public void UpdateRoleAccess(_object.UpdateRoleAccess access, string by)
        {
            if (access.allow != null && !string.IsNullOrEmpty(access.roleid))
            {
                var allow = access.allow != null ? access.allow : new List<string>();

                foreach (var item in (from b in db.M_PrivRoleAccess where b.role_id == access.roleid select b).ToList())
                {
                    item.del = !allow.Contains(item.id.ToString());
                    item.assign_by = by;
                    item.assign_date = DateTime.Now;
                    db.Entry(item).CurrentValues.SetValues(item);
                    db.SaveChanges();
                }
            }
        }

        public bool PrivCheck(string id, string type, bool child_type_required)
        {
            var page = (from b in db.M_Pages where b.id == id select b).FirstOrDefault();
            var priv = (from b in db.M_Priv where b.pages_id == id && b.type == type select b).FirstOrDefault();

            if (priv == null)
            {
                var pages = (from b in db.M_Pages where b.id == id select b).FirstOrDefault();
                while (pages != null)// && pages.parent != "")
                {
                    priv = (from b in db.M_Priv where b.pages_id == pages.id && b.type == type select b).FirstOrDefault();
                    pages = (from b in db.M_Pages where b.id == pages.parent select b).FirstOrDefault();
                    if (priv != null) break;
                }
            }

            if (page != null && child_type_required ? string.IsNullOrEmpty(page.child_type) : false) priv = null;

            return (priv != null ? Session_.Priviledge.Contains(priv.priv_id) : false);
        }
    }
}
