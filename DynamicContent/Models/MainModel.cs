﻿using DynamicContent.Helper;
using DynamicContent.Areas.SysManager.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Configuration;
using System.Net;

namespace DynamicContent.Models
{
    public class MainModel : Controller
    {
        Models.PagesModel pages = new Models.PagesModel();
        Models.PrivModel priv = new Models.PrivModel();
        Models.UserModel user = new Models.UserModel();
        Models.ProductModel product = new Models.ProductModel();
        Models.EmailModel email = new Models.EmailModel();

        //-------------SLIDER-------------
        public IEnumerable<_object.Slider> GetSlider()
        {
            return pages.GetSlider();
        }

        public IEnumerable<_object.Slider> GetSlider2()
        {
            return pages.GetSlider2();
        }

        public DynamicContent.Models.tbl_slider GetSliderById(int id)
        {
            return pages.GetSliderById(id);
        }

        public DynamicContent.Models.tbl_slider2 GetSlider2ById(int id)
        {
            return pages.GetSlider2ById(id);
        }

        public IEnumerable<_object.NowOpen> GetNowopen()
        {
            return pages.GetNowopen();
        }

        public DynamicContent.Models.NowOpen GetNowopenById(int id)
        {
            return pages.GetNowopenById(id);
        }

        public void SliderEdit(DynamicContent.Models.tbl_slider data)
        {
            pages.SliderEdit(data);
        }

        public void SliderAdd(DynamicContent.Models.tbl_slider data)
        {
            pages.SliderAdd(data);
        }

        public void AddProductImage(DynamicContent.Models.tbl_product_image data)
        {
            pages.AddProductImage(data);
        }

        public void SliderDelete(int id)
        {
            pages.SliderDelete(id);
        }
        //-------------END SLIDER-------------

        //-------------NEWSLETTER-------------

        //subscriber
        public IEnumerable<EmailModel.Subsribers> GetAllSubscribers(bool del)
        {
            return email.GetAllSubscribers(del);
        }

        public IEnumerable<DynamicContent.Models.M_EmailType> GetAllEmailType(bool contact_only)
        {
            return email.GetAllEmailType(contact_only);
        }

        public int AddSubscribers(DynamicContent.Models.M_Subscribers data)
        {
            return email.AddSubscribers(data);
        }

        public DynamicContent.Models.M_Subscribers GetSubscribers(int id)
        {
            return email.GetSubscribers(id);
        }

        public void UpdateSubscribers(DynamicContent.Models.M_Subscribers data)
        {
            email.UpdateSubscribers(data);
        }

        public void DeleteSubscribers(int id, string by)
        {
            email.DeleteSubscribers(id, by);
        }

        public IEnumerable<DynamicContent.Models.M_Newsletter> GetAllNewsletter(bool template)
        {
            return email.GetAllNewsletter(template);
        }

        public DynamicContent.Models.M_Newsletter GetNewsletter(int id)
        {
            return email.GetNewsletter(id);
        }

        public int AddNewsletter(DynamicContent.Models.M_Newsletter data)
        {
            return email.AddNewsletter(data);
        }

        public void UpdateNewsletter(DynamicContent.Models.M_Newsletter data)
        {
            email.UpdateNewsletter(data);
        }

        public void DeleteNewsletter(int id, string by)
        {
            email.DeleteNewsletter(id, by);
        }

        public IEnumerable<DynamicContent.Models.M_Subscribers> getAllSubscribersFromType(int id)
        {
            return email.getAllSubscribersFromType(id);
        }

        public DynamicContent.Models.M_EmailType GetEmailType(int id)
        {
            return email.GetEmailType(id);
        }

        public DynamicContent.Models.M_Param GetParam(string param_id)
        {
            return email.GetParam(param_id);
        }

        public string GetParamVal(string param_id)
        {
            var p = email.GetParam(param_id, true);
            return (email == null ? "" : p.value);
        }
        //-------------END NEWSLETTER-------------
        // Get Submenu PI
        public IEnumerable<_object.Submenu> GetSubmenu(string parent)
        {
            return pages.GetSubmenu(parent);
        }

        public DynamicContent.Models.tbl_submenu GetSubmenuCategory(int id)
        {
            return pages.GetSubmenuCategory(id);
        }

        // Get Brand PI
        public IEnumerable<_object.Brand> GetBrand()
        {
            return pages.GetBrand();
        }
        public IEnumerable<_object.Brand> GetTenantBrand(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return null;
            }
            var ids = Array.ConvertAll(id.Split(','), s => int.Parse(s));
            return pages.GetTenantBrand(ids);
        }

        // Get Wishlist
        public IEnumerable<DynamicContent.Models.tbl_wishlist> GetWishlist(int id)
        {
            return pages.GetWishlist(id);
        }

        public IEnumerable<DynamicContent.Models.tbl_wishlist> GetExistWishlist(int id, int color, int userID)
        {
            return pages.GetExistWishlist(id, color, userID);
        }

        public DynamicContent.Models.tbl_wishlist RemoveWishlist(int id)
        {
            return pages.RemoveWishlist(id);
        }

        public IEnumerable<DynamicContent.Models.tbl_cart> GetMyCart(int id)
        {
            return pages.GetMyCart(id);
        }

        public IEnumerable<DynamicContent.Models.tbl_cart> GetExistCart(int userID, string sz, int? color)
        {
            return pages.GetExistCart(userID, sz, color);
        }

        public IEnumerable<DynamicContent.Models.tbl_ReservationParent> GetMyCartReserved(int id)
        {
            return pages.GetMyCartReserved(id);
        }

        public IEnumerable<DynamicContent.Models.tbl_reservation> GetDetailReservation(int id)
        {
            return pages.GetDetailReservation(id);
        }


        public void changeQtyReservation(DynamicContent.Models.tbl_cart data)
        {
            pages.changeQtyReservation(data);
        }


        //-----------TENANT------------
        public IEnumerable<_object.Tenant> GetTenantNO()
        {
            return pages.GetTenantNO();
        }

        public IEnumerable<string> getTenantFirstLetter()
        {
            return pages.getTenantFirstLetter();
        }
        //getTenantByAlphabet

        public DynamicContent.Models.tbl_TenantCategory GetTenantCategoryIdByCode(string id)
        {
            return product.GetTenantCategoryIdByCode(id);
        }

        public DynamicContent.Models.M_User_Tenant GetTenantLogin(string user)
        {
            return pages.GetTenantLogin(user);
        }

        public IEnumerable<_object.Tenant> GetTenant(int id)
        {
            return pages.GetTenant(id);
        }

        public IEnumerable<DynamicContent.Models.tbl_tenant> GetListTenant(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return null;
            }
            var ids = Array.ConvertAll(id.Split(','), s => long.Parse(s));
            return pages.GetListTenant(ids);
        }

        public IEnumerable<_object.TenantByCategory> ProdTenantByCategory(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return null;
            }
            var ids = Array.ConvertAll(id.Split(','), s => long.Parse(s));
            return pages.ProdTenantByCategory(ids);
        }

        public IEnumerable<_object.ProductType> GetProdType(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return null;
            }
            var ids = Array.ConvertAll(id.Split(','), s => long.Parse(s));
            return pages.GetProdType(ids);
        }

        public IEnumerable<_object.ProductType> GetTypeDetail(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return null;
            }

            return pages.GetTypeDetail(id);
        }


        //-----------END TENANT------------

        //-----------PRODUCT-----------
        public IEnumerable<_object.ProductCategory> GetProdCategory()
        {
            return pages.GetProdCategory();
        }
        public IEnumerable<_object.ProductCategory> GetProdCategoryTenant(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return null;
            }
            var ids = Array.ConvertAll(id.Split(','), s => int.Parse(s));
            return pages.GetProdCategories(ids);
        }

        public IEnumerable<_object.TenantCategory> GetTenantCategory(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return null;
            }
            var ids = Array.ConvertAll(id.Split(','), s => int.Parse(s));
            return pages.GetTenantCategories(ids);
        }
        public IEnumerable<_object.SubmenuCategory> GetTenantSubcategory(string id, string parent)
        {
            if (String.IsNullOrEmpty(id))
            {
                return null;
            }
            var ids = Array.ConvertAll(id.Split(','), s => int.Parse(s));
            return pages.GetTenantSubcategories(ids, parent);
        }
        public IEnumerable<_object.ProductPi> GetProductPi()
        {
            return pages.GetProductPi();
        }
        public IEnumerable<_object.ProductBySubmenu> ProductBySubmenu(int id)
        {
            return pages.ProductBySubmenu(id);
        }

        public IEnumerable<_object.AllProduct> GetAllProduct(int? id)
        {
            return pages.GetAllProduct(id);
        }

        public IEnumerable<_object.AllProduct> GetAllProduct2(int? id)
        {
            return pages.GetAllProduct2(id);
        }

        public IEnumerable<_object.AllProduct> GetAllProductTmp(int? id)
        {
            return pages.GetAllProductTmp(id);
        }

        public IEnumerable<_object.AllProduct> GetAllProductTmp2(int? id)
        {
            return pages.GetAllProductTmp2(id);
        }

        public IEnumerable<_object.AllProduct> GetProductByTenantCategory(int id)
        {
            return product.GetProductByTenantCategory(id);
        }

        public IEnumerable<_object.AllProduct> GetProductByTenantCategory2(int id)
        {
            return product.GetProductByTenantCategory2(id);
        }


        public IEnumerable<_object.AllProduct> GrabAllProduct()
        {
            return product.GrabAllProduct();
        }


        public IEnumerable<_object.AllProduct> GetAllProductByTenant(int id)
        {
            return pages.GetAllProductByTenant(id);
        }

        public IEnumerable<_object.AllProduct> GetAllProductTmpByTenant(int id)
        {
            return pages.GetAllProductTmpByTenant(id);
        }

        public IEnumerable<_object.AllProduct> GetAllProductTmpByTenant2(int id)
        {
            return pages.GetAllProductTmpByTenant2(id);
        }

        public IEnumerable<_object.AllProduct> GetAllProductTmpWaitingApproval()
        {
            return pages.GetAllProductTmpWaitingApproval();
        }

        public IEnumerable<_object.AllProduct> GetAllProductTmpWaitingApproval2()
        {
            return pages.GetAllProductTmpWaitingApproval2();
        }
        public IEnumerable<_object.lovedProduct> GetMostLovedProduct(int id)
        {
            return pages.GetMostLovedProduct(id);
        }

        public IEnumerable<_object.lovedProduct> GetMostLovedProductSysManager()
        {
            return pages.GetMostLovedProductSysManager();
        }
        public IEnumerable<_object.lovedProduct> GetProductWishlist(int id)
        {
            return pages.GetProductWishlist(id);
        }
        public IEnumerable<DynamicContent.Models.tbl_product_color> GetProductColor(int id)
        {
            return product.GetProductColor(id);
        }

        public IEnumerable<DynamicContent.Models.tbl_product_color_tmp> GetProductColorTmp(int id)
        {
            return product.GetProductColorTmp(id);
        }

        public IEnumerable<_object.productUploadByBrand> ItemsUploadedByBrand()
        {
            return pages.ItemsUploadedByBrand();
        }
        public IEnumerable<_object.ReservationHeader> ItemReservationHeader()
        {
            return pages.ReservationHeader();
        }
        public IEnumerable<_object.viewedProduct> GetMostViewedProduct(int id)
        {
            return pages.GetMostViewedProduct(id);
        }

        //public IEnumerable<_object.viewedProduct> GetLeastViewedProductSysManager()
        //{
        //    return pages.GetLeastViewedProductSysManager();
        //}

        public IEnumerable<_object.viewedProduct> GetMostViewedProductSysManager()
        {
            return pages.GetMostViewedProductSysManager();
        }

        public IEnumerable<_object.viewedProduct> GetProductViewed(int id)
        {
            return pages.GetProductViewed(id);
        }

        public IEnumerable<_object.reservedProduct> GetMostReservedProduct(int id)
        {
            return pages.GetMostReservedProduct(id);
        }

        public IEnumerable<_object.reservedProduct> GetMostReservedProductSysManager()
        {
            return pages.GetMostReservedProductSysManager();
        }

        public IEnumerable<_object.AllProduct> GetProduct(int id)
        {
            return pages.GetProductById(id);
        }

        public IEnumerable<_object.AllProduct> GetProductTmp(int id)
        {
            return pages.GetProductTmpById(id);
        }


        public IEnumerable<DynamicContent.Models.tbl_product> GetProductByTenant(int id)
        {
            return pages.GetProductByTenant(id);
        }

        public string StatusProductCheck(bool stt)
        {
            return pages.StatusProductCheck(stt);
        }

        public DynamicContent.Models.tbl_submenu SubmenuByBrand(int id)
        {
            return pages.SubmenuByBrand(id);
        }

        //-----------END PRODUCT-----------

        public IEnumerable<_object.BrandWithParam> GetBrandWithParam(int id)
        {
            return pages.GetBrandWithParam(id);
        }


        public DynamicContent.Models.tbl_product_image GetOneImg(int id)
        {
            return product.GetOneImg(id);
        }

        public IEnumerable<DynamicContent.Models.tbl_product_image> GetProductImgColor(int id)
        {
            return product.GetProductImgColor(id);
        }


        public IEnumerable<DynamicContent.Models.tbl_product_image_tmp> GetProductImgColorTmp(int id)
        {
            return product.GetProductImgColorTmp(id);
        }

        public DynamicContent.Models.tbl_product_image GetOneImgByColor(int id)
        {
            return product.GetOneImgByColor(id);
        }


        public DynamicContent.Models.tbl_product_image_tmp GetOneImgByColorTmp(int id)
        {
            return product.GetOneImgByColorTmp(id);
        }

        public DynamicContent.Models.tbl_product_color GetProductByColor(int id)
        {
            return product.GetProductByColor(id);
        }


        public DynamicContent.Models.tbl_product_color_tmp GetProductTmpByColor(int id)
        {
            return product.GetProductTmpByColor(id);
        }

        public IEnumerable<DynamicContent.Models.tbl_product_color> GetColorByIds(int id)
        {
            return product.GetColorByIds(id);
        }

        public void DeleteColorById(int id)
        {
            product.GetColorByIds(id);
        }
        public IEnumerable<_object.ProductImg> GetProductImg(int id)
        {
            return product.GetProductImg(id);
        }
        public IEnumerable<_object.ProductImg> GetProductImgTmp(int id)
        {
            return product.GetProductImgTmp(id);
        }
        public IEnumerable<_object.ProductImg> GetProductImgByColor(int id)
        {
            return product.GetProductImgByColor(id);
        }
        public IEnumerable<_object.ProductImg> GetProductImgTmpByColor(int id)
        {
            return product.GetProductImgTmpByColor(id);
        }

        public IEnumerable<_object.ProductImg> GetProductImg2(int id)
        {
            return product.GetProductImg2(id);
        }

        public IEnumerable<_object.ProductImg> GetProductImg2Tmp(int id)
        {
            return product.GetProductImg2Tmp(id);
        }
        public void UserRegister(DynamicContent.Models.tbl_user_register data)
        {
            pages.UserRegister(data);
        }

        public void ReservationParent(DynamicContent.Models.tbl_ReservationParent data)
        {
            pages.ReservationParent(data);
        }

        public void AddReservation(DynamicContent.Models.tbl_reservation data)
        {
            pages.AddReservation(data);
        }
        public void FlashDealReservation(int ID, int QTY, Guid FlashHeaderID)
        {
            pages.FlashDealReserv(ID, QTY, FlashHeaderID);
        }


        public void AddToCart(DynamicContent.Models.tbl_cart data)
        {
            pages.AddToCart(data);
        }

        public void changePassword(DynamicContent.Models.tbl_user_register data)
        {
            user.changePassword(data);
        }
        public string generateToken()
        {
            Guid token = Guid.NewGuid();
            return token.ToString();
        }
        public bool checkToken(string token, string email)
        {
            if (user.checkToken(token, email))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void EditUserRegister(DynamicContent.Models.tbl_user_register data)
        {
            user.EditUserRegister(data);
        }


        public DynamicContent.Models.tbl_user_register GetUserActive(int id)
        {
            return user.GetUserActive(id);
        }

        //public DynamicContent.Models.tbl_wishlist GetWishlist(int id)
        //{
        //    return pages.GetWishlist(id);
        //}

        public IEnumerable<DynamicContent.Models.tbl_user_register> GetUserRegister()
        {
            return user.GetUserRegister();
        }

        public DynamicContent.Models.tbl_user_register GetUserLogin(string email)
        {
            return user.GetUserLogin(email);
        }
        public DynamicContent.Models.tbl_user_register GetUserLoginWithValidate(string email)
        {
            return user.GetUserLoginwithValidate(email);
        }
        public DynamicContent.Models.tbl_user_register GetUserByTwitter(long twitter_id)
        {
            return user.GetUserByTwitter(twitter_id);
        }

        public DynamicContent.Models.tbl_user_register CheckUserLogin(string email, string password)
        {
            return user.CheckUserLogin(email, password);
        }

        public DynamicContent.Models.M_User CheckLogin(string username, string password, int maxTry)
        {
            return user.CheckLogin(username, password, maxTry);
        }

        public DynamicContent.Models.M_User GetUser(string username)
        {
            return user.GetUser(username);
        }

        /*--------TENANT SECTION--------*/
        public DynamicContent.Models.M_User_Tenant CheckLoginTenant(string username, string password, int maxTry)
        {
            return user.CheckLoginTenant(username, password, maxTry);
        }

        public DynamicContent.Models.M_User_Tenant GetUserTenant(string username)
        {
            return user.GetUserTenant(username);
        }

        public void UpdateCounterTenant(string username, bool UpCounter)
        {
            user.UpdateCounterTenant(username, UpCounter);
        }

        /*--------END TENANT SECTION--------*/

        public DynamicContent.Models.M_User GetUserByEmail(string email)
        {
            return user.GetUserByEmail(email);
        }

        public DynamicContent.Models.M_User GetUserByHashCode(string hashcode)
        {
            return user.GetUserByHashcode(hashcode, true);
        }

        public DynamicContent.Models.tbl_user_register GetUserByHashCodeFront(string hashcode)
        {
            return user.GetUserByHashCodeFront(hashcode, true);
        }
        public static async Task<bool> SendEmail(string toEmailAddress, string emailSubject, string emailMessage)
        {
            try
            {
                var message = new MailMessage();
                message.To.Add(toEmailAddress);

                message.Subject = emailSubject;
                message.Body = emailMessage;
                message.IsBodyHtml = true;
                var hostusername = ConfigurationManager.AppSettings["GmailID"];
                var hostpassword = ConfigurationManager.AppSettings["GmailPass"];
                message.From = new MailAddress(hostusername);
                var smtpClient = new SmtpClient();

                smtpClient.Host = System.Configuration.ConfigurationManager.AppSettings["GmailHost"];
                smtpClient.Port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["GmailPort"]);
                smtpClient.EnableSsl = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["GmailSSL"]);
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(hostusername, hostpassword);
                smtpClient.SendCompleted += (s, e) =>
                    {
                        smtpClient.Dispose();
                        message.Dispose();
                    };
                await smtpClient.SendMailAsync(message);
                return true;
            }
            catch (Exception e)
            {

                return false;
            }

        }
        public IEnumerable<DynamicContent.Models.M_User> getAllUser()
        {
            return user.getAllUser();
        }

        public void UpdateCounter(string username, bool UpCounter)
        {
            user.UpdateCounter(username, UpCounter);
        }

        public void UserDelete(string username, string by)
        {
            user.UserDelete(username, by);
        }

        public void UserAdd(DynamicContent.Models.M_User data)
        {
            user.UserAdd(data);
        }

        public void UserEdit(DynamicContent.Models.M_User data)
        {
            user.UserEdit(data);
        }

        public void UserUnblock(string username, string by)
        {
            var u = user.GetUser(username);
            if (u != null)
            {
                u.modified_by = by;
                //u.password = null;
                user.UserEdit(u);
            }
        }

        public IEnumerable<DynamicContent.Models.M_PrivRole> getAllRole()
        {
            return priv.getAllRole();
        }

        public List<int> GetUserPriv(string role_id)
        {
            return priv.GetUserPriv(role_id);
        }

        public bool RoleExist(string RoleName)
        {
            return priv.RoleExist(RoleName);
        }

        public bool RoleExistFull(string RoleName)
        {
            return priv.RoleExistFull(RoleName);
        }

        public bool UserExistFull(string username)
        {
            return user.UserExistFull(username);
        }

        public DynamicContent.Models.M_PrivRole GetRole(string roleId)
        {
            return priv.GetRole(roleId);
        }

        public void UserRoleDelete(string roleId, string by)
        {
            priv.UserRoleDelete(roleId, by);
        }

        public void UpdateUserRole(string id, string desc, string by)
        {
            priv.UpdateUserRole(id, desc, by);
        }

        public void AddUserRole(DynamicContent.Models.M_PrivRole role)
        {
            priv.AddUserRole(role);
        }

        public IEnumerable<DynamicContent.Models.M_Priv> GetAllPriv()
        {
            return priv.GetAllPriv();
        }
        public IEnumerable<DynamicContent.Models.M_PrivType> GetAllPrivType()
        {
            return priv.GetAllPrivType();
        }
        public void UpdateRoleAccess(_object.UpdateRoleAccess access, string by)
        {
            priv.UpdateRoleAccess(access, by);
        }


        //generate everything
        public void generate()
        {
            //return pages.makeURLFromPage(id);
        }

        public string stringToURL(string title)
        {
            return pages.stringToURL(title);
        }

        public string wordsUpper(string title)
        {
            return pages.wordsUpper(title);
        }

        public string toIDR(string idr)
        {
            return pages.toIDR(idr);
        }

        public string priceCheck(string price)
        {
            return pages.priceCheck(price);
        }

        public bool AllowPriv(string id, string type, bool child_type_required)
        {
            return priv.PrivCheck(id, type, child_type_required);
        }

        public void UpdateSessionId(string email, string sessionId)
        {
            user.UpdateSessionId(email, sessionId);
        }

        public void CheckSessionId(int userId)
        {
            user.CheckSessionId(userId);
        }

        public int CheckLogin(int userId)
        {
            return user.CheckLogin(userId);
        }
    }

}
