//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DynamicContent.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TypeMaster
    {
        public long ID { get; set; }
        public Nullable<long> TypeCategory { get; set; }
        public Nullable<long> TypeSubCategory { get; set; }
        public string TypeValue { get; set; }
        public string TypeDesc { get; set; }
        public string CreateBy { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string EditBy { get; set; }
        public Nullable<System.DateTime> EditDate { get; set; }
        public string isActive { get; set; }
        public string isShowMenu { get; set; }
    }
}
