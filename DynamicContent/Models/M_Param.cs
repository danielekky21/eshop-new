//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DynamicContent.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class M_Param
    {
        public int id { get; set; }
        public string param_id { get; set; }
        public string value { get; set; }
        public string note { get; set; }
        public string created_by { get; set; }
        public System.DateTime created_date { get; set; }
        public string modified_by { get; set; }
        public System.DateTime modified_date { get; set; }
    }
}
