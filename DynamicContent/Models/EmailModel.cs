﻿using DynamicContent.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using System.Globalization;

namespace DynamicContent.Models
{
    public class EmailModel : Controller
    {
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();

        public class Subsribers
        {
            public int id { get; set; }
            public string name { get; set; }
            public string email { get; set; }
            public int type { get; set; }
            public string type_name { get; set; }
            public string subscribe_by { get; set; }
            public DateTime subscribe_date { get; set; }
            public string unsubscribe_by { get; set; }
            public DateTime? unsubscribe_date { get; set; }
            public bool del { get; set; }
        }

        //DynamicContent
        public IEnumerable<DynamicContent.Models.M_Newsletter> GetAllNewsletter(bool is_template)
        {
            var rs = from b in db.M_Newsletter
                     orderby b.id descending
                     where b.template == is_template && !b.del
                     select b;
            return rs.ToList();
        }

        public DynamicContent.Models.M_Newsletter GetNewsletter(int id)
        {
            var rs = from b in db.M_Newsletter
                     where b.id == id
                     select b;
            return rs.FirstOrDefault();
        }

        public int AddNewsletter(DynamicContent.Models.M_Newsletter data)
        {
            if (data != null)
            {
                data.created_date = DateTime.Now;
                data.modified_date = DateTime.Now;

                db.M_Newsletter.Add(data);
                db.SaveChanges();

                return data.id;
            }
            return 0;
        }

        public void UpdateNewsletter(DynamicContent.Models.M_Newsletter data)
        {
            if (data != null)
            {
                var DynamicContent = GetNewsletter(data.id);
                if (DynamicContent != null)
                {
                    db.Entry(DynamicContent).CurrentValues.SetValues(data);
                    db.SaveChanges();
                }
            }
        }

        public void DeleteNewsletter(int id, string by)
        {
            var DynamicContent = GetNewsletter(id);
            if (DynamicContent != null)
            {
                DynamicContent.del = true;
                DynamicContent.modified_by = by;
                DynamicContent.modified_date = DateTime.Now;

                UpdateNewsletter(DynamicContent);
            }
        }

        //email type
        public IEnumerable<DynamicContent.Models.M_EmailType> GetAllEmailType(bool contact_only)
        {
            var rs = from b in db.M_EmailType
                     orderby b.id descending
                     //where (contact_only ? b.is_contact : true)
                     select b;
            return rs.ToList();
        }

        public DynamicContent.Models.M_EmailType GetEmailType(int id)
        {
            var rs = from b in db.M_EmailType
                     where b.id == id
                     select b;
            return rs.FirstOrDefault();
        }

        public int AddEmailType(DynamicContent.Models.M_EmailType data)
        {
            if (data != null)
            {
                db.M_EmailType.Add(data);
                db.SaveChanges();

                return data.id;
            }
            return 0;
        }

        public void UpdateEmailType(DynamicContent.Models.M_EmailType data)
        {
            if (data != null)
            {
                var EmailType = GetEmailType(data.id);
                if (EmailType != null)
                {
                    db.Entry(EmailType).CurrentValues.SetValues(data);
                    db.SaveChanges();
                }
            }
        }

        //subscribers
        public IEnumerable<Subsribers> GetAllSubscribers(bool del)
        {
            var rs = from b in db.M_Subscribers
                     join c in db.M_EmailType on b.type equals c.id
                     orderby b.id descending
                     where b.del == del
                     select new Subsribers
                     {
                         id = b.id,
                         name = b.name,
                         email = b.email,
                         type = b.type,
                         type_name = c.name,
                         subscribe_by = b.subscribe_by,
                         subscribe_date = b.subscribe_date,
                         unsubscribe_by = b.unsubscribe_by,
                         unsubscribe_date = b.unsubscribe_date,
                         del = b.del
                     };
            return rs.ToList();
        }

        public IEnumerable<DynamicContent.Models.M_Subscribers> getAllSubscribersFromType(int id)
        {
            var rs = from b in db.M_Subscribers
                     orderby b.id descending
                     where b.type == id && !b.del
                     select b;

            return rs.ToList();
        }

        public DynamicContent.Models.M_Subscribers GetSubscribers(int id)
        {
            var rs = from b in db.M_Subscribers
                     where b.id == id
                     select b;
            return rs.FirstOrDefault();
        }

        public DynamicContent.Models.M_Subscribers GetHashSubscribers(string id)
        {
            var rs = from b in db.M_Subscribers
                     where CustomEncryption.HashStringSHA1(b.email + "_" + b.id.ToString()) == id
                     select b;
            return rs.FirstOrDefault();
        }

        public DynamicContent.Models.M_Subscribers CheckSubscribers(string email, int type)
        {
            var rs = from b in db.M_Subscribers
                     where b.email.ToLower() == email.ToLower() && b.type == type && b.del == false
                     select b;
            return rs.FirstOrDefault();
        }

        public int AddSubscribers(DynamicContent.Models.M_Subscribers data)
        {
            if (data != null)
            {
                if (CheckSubscribers(data.email, data.type) == null)
                {
                    data.subscribe_date = DateTime.Now;

                    db.M_Subscribers.Add(data);
                    db.SaveChanges();

                    return data.id;
                }
            }
            return 0;
        }

        public void UpdateSubscribers(DynamicContent.Models.M_Subscribers data)
        {
            if (data != null)
            {
                var Subscribers = GetSubscribers(data.id);
                var check = CheckSubscribers(data.email, data.type);
                if (Subscribers != null && (check == null || check.id == Subscribers.id))
                {
                    db.Entry(Subscribers).CurrentValues.SetValues(data);
                    db.SaveChanges();
                }
            }
        }

        public void DeleteSubscribers(int id, string by)
        {
            var subscribers = GetSubscribers(id);
            if (subscribers != null)
            {
                subscribers.del = true;
                subscribers.unsubscribe_by = by;
                subscribers.unsubscribe_date = DateTime.Now;

                UpdateSubscribers(subscribers);
            }
        }

        private string MakeRandomString(int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            return result;
        }

        private string MakeRandomString()
        {
            return MakeRandomString(8);
        }

        public DynamicContent.Models.M_Param GetParam(string param_id, bool allowAdd = false)
        {
            var rs = from b in db.M_Param
                     where b.param_id == param_id
                     orderby b.param_id ascending
                     select b;
            var result = rs.FirstOrDefault();
            if (allowAdd && result == null)
            {
                var param = new DynamicContent.Models.M_Param();
                param.param_id = param_id;
                param.value = "";
                param.note = "";
                param.created_by = "system";
                param.created_date = DateTime.Now;
                param.modified_by = "system";
                param.modified_date = DateTime.Now;

                db.M_Param.Add(param);
                db.SaveChanges();
            }
            return result;
        }
    }
}
