﻿using DynamicContent.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DynamicContent.Models
{
    public class UserModel : Controller
    {
        //
        // GET: /Manager/Home/
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();

        private DynamicContent.Helper.Generator gen = new DynamicContent.Helper.Generator();

        public DynamicContent.Models.M_User CheckLogin(string username, string password, int maxTry)
        {
            var rs = (from b in db.M_User
                      where b.username == username && b.password == password && b.pass_counter < maxTry
                      select b).FirstOrDefault();
            return rs;
        }

        public DynamicContent.Models.tbl_user_register CheckUserLogin(string email, string password)
        {
            var rs = (from b in db.tbl_user_register
                      where b.email == email && b.password == password
                      select b).FirstOrDefault();
            return rs;
        }
        
        public DynamicContent.Models.M_User GetUser(string username)
        {
            var rs = (from b in db.M_User
                      where b.username == username && (b.del == false || b.username == "superadmin")
                      select b).FirstOrDefault();
            return rs;
        }

        /*--------TENANT SECTION--------*/
        public DynamicContent.Models.M_User_Tenant CheckLoginTenant(string username, string password, int maxTry)
        {
            var rs = (from b in db.M_User_Tenant
                      where b.username == username && b.password == password && b.pass_counter < maxTry
                      select b).FirstOrDefault();
            return rs;
        }

        public DynamicContent.Models.M_User_Tenant GetUserTenant(string username)
        {
            var rs = (from b in db.M_User_Tenant
                      where b.username == username && (b.del == false || b.username == "superadmin")
                      select b).FirstOrDefault();
            return rs;
        }

        public void UpdateCounterTenant(string username, bool UpCounter)
        {
            DynamicContent.Models.M_User_Tenant user = GetUserTenant(username);
            user.modified_date = DateTime.Now;
            user.pass_counter = (UpCounter ? user.pass_counter + 1 : 0);
            db.Entry(user).CurrentValues.SetValues(user);
            db.SaveChanges();
        }
        /*--------END TENANT SECTION--------*/

        public void UpdateSessionId(string email, string sessionId)
        {
            DynamicContent.Models.tbl_user_register user = GetUserLogin(email);
            user.session_id = sessionId;
            db.Entry(user).CurrentValues.SetValues(user);
            db.SaveChanges();
        }

        public DynamicContent.Models.tbl_user_register GetUserActive(int id)
        {
            var rs = (from b in db.tbl_user_register
                      where b.ID == id
                      select b).FirstOrDefault();
            return rs;
        }

        public IEnumerable<DynamicContent.Models.tbl_user_register> GetUserRegister() 
        {
            var rs = (from b in db.tbl_user_register
                      where b.email != ""
                      select b).ToList();
            return rs;
        }

        public DynamicContent.Models.tbl_user_register GetUserLogin(string email)
        {
            var rs = (from b in db.tbl_user_register
                      where b.email == email
                      select b).FirstOrDefault();
            return rs;
        }
        public DynamicContent.Models.tbl_user_register GetUserLoginwithValidate(string email)
        {
            var rs = (from b in db.tbl_user_register
                      where b.email == email && b.isValidate == true
                      select b).FirstOrDefault();
            return rs;
        }
        public DynamicContent.Models.tbl_user_register GetUserByTwitter(long twitter_id)
        {
            var rs = (from b in db.tbl_user_register
                      where b.twitter_id == twitter_id.ToString()
                      select b).FirstOrDefault();
            return rs;
        }

        

        public void UpdateCounter(string username, bool UpCounter)
        {
            DynamicContent.Models.M_User user = GetUser(username);
            user.modified_date = DateTime.Now;
            user.pass_counter = (UpCounter ? user.pass_counter + 1 : 0);
            db.Entry(user).CurrentValues.SetValues(user);
            db.SaveChanges();
        }

        public IEnumerable<DynamicContent.Models.M_User> getAllUser()
        {
            var rs = (from b in db.M_User
                      where b.del == false
                      select b).ToList();
            return rs;
        }

        public void UserAdd(DynamicContent.Models.M_User user)
        {
            db.M_User.Add(user);
            db.SaveChanges();
        }
        
        public void changePassword(DynamicContent.Models.tbl_user_register user)
        {
            var newuser = GetUserLogin(user.email);
            if (newuser != null)
            {
                //if (string.IsNullOrEmpty(user.password))
               // {
                    newuser.password = user.password;
                    newuser.change_pass_date = DateTime.Now;
               // }

                db.Entry(user).CurrentValues.SetValues(newuser);
                db.SaveChanges();
            }
        }
        public bool checkToken(string token, string email)
        {
            var user = GetUserLogin(email);
            if (user.Token == token)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void EditUserRegister(DynamicContent.Models.tbl_user_register user)
        {
            var newuser = GetUserLogin(user.email);
            if (newuser != null)
            {
                newuser.firstName = user.firstName;
                newuser.lastName = user.lastName;
                newuser.phone = user.phone;
                newuser.address = user.address;
                newuser.city = user.city;
                newuser.zipcode = user.zipcode;
                newuser.dob = user.dob;
                newuser.gender = user.gender;
                newuser.modified_date = DateTime.Now;
                //newuser.email = user.email;             

                db.Entry(user).CurrentValues.SetValues(newuser);
                db.SaveChanges();
            }
        }

        public void UserEdit(DynamicContent.Models.M_User user)
        {
            var newuser = GetUser(user.username);
            if (newuser != null)
            {
                newuser.modified_by = user.modified_by;
                newuser.modified_date = DateTime.Now;
                newuser.email = user.email;
                newuser.pass_counter = 0;

                if (string.IsNullOrEmpty(user.password))
                {
                    newuser.password = user.password;
                    newuser.change_pass_date = DateTime.Now;
                }

                db.Entry(user).CurrentValues.SetValues(newuser);
                db.SaveChanges();
            }
        }

        public void UserDelete(string username, string by)
        {
            DynamicContent.Models.M_User user = GetUser(username);
            user.del = true;
            user.modified_by = by;
            user.modified_date = DateTime.Now;
            db.Entry(user).CurrentValues.SetValues(user);
            db.SaveChanges();
        }

        public bool UserExistFull(string username)
        {
            var temp = (from b in db.M_User
                        where b.username == username
                        select b).FirstOrDefault();
            bool rs = (temp != null ? true : false);
            return rs;
        }

        public DynamicContent.Models.M_User GetUserByEmail(string email)
        {
            if (string.IsNullOrEmpty(email)) return null;

            var r = (from a in db.M_User
                     where a.email == email
                     select a).FirstOrDefault();
            return r;
        }

        public DynamicContent.Models.M_User GetUserByHashcode(string hashcode, bool timeLimit)
        {
            var r = (from a in db.M_User
                     where (timeLimit ? DateTime.Now.AddMinutes(-10) >= a.modified_date : true)
                     select a).ToList();

            return r.Where(x => CustomEncryption.HashStringSHA1(x.username + " at " + x.modified_date.ToString()) == hashcode).FirstOrDefault();
        }

        public DynamicContent.Models.tbl_user_register GetUserByHashCodeFront(string hashcode, bool timeLimit)
        {
            var addMinutes = DateTime.Now.AddMinutes(-10);
            //var hash = hashcode;
            var r = (from a in db.tbl_user_register
                     where (timeLimit ? addMinutes <= a.modified_date : true)
                     select a).ToList();
            //string bb = "";
            //foreach(var d in r)
            //{
            //    bb = CustomEncryption.HashStringSHA1(d.email + " at " + d.modified_date.ToString());
            //}
            //var rs = r.Where(x => timeLimit ? DateTime.Now.AddMinutes(-10) >= x.modified_date : true);
            return r.Where(x => CustomEncryption.HashStringSHA1(x.email + " at " + x.modified_date.ToString()) == hashcode).FirstOrDefault();
        }

        public void CheckSessionId(int userId)
        {
            string sessionId = System.Web.HttpContext.Current.Session.SessionID;
            var data = GetUserActive(_sessionFront.userIdLogin);
            if (data != null)
            {
                if (data.session_id != sessionId)
                {
                    _sessionFront.firstName = null;
                    _sessionFront.lastName = null;
                    _sessionFront.sessionId = null;
                    _sessionFront.userIdLogin = 0;
                }
            }
        }

        public int CheckLogin(int userId)
        {
            string sessionId = System.Web.HttpContext.Current.Session.SessionID;
            var data = GetUserActive(_sessionFront.userIdLogin);
            if (data != null)
            {
                if (data.session_id != sessionId)
                {
                    _sessionFront.firstName = null;
                    _sessionFront.lastName = null;
                    _sessionFront.sessionId = null;
                    _sessionFront.userIdLogin = 0;

                    return 1;
                }
                else
                    return 0;
            }
            else
                return 0;
        }
    }
}
