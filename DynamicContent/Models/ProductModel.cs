﻿using DynamicContent.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace DynamicContent.Models
{
    public class ProductModel : Controller
    {
        //
        // GET: /ProductModel/
        
        private DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
        //DynamicContent.Models.MainModel obj = new DynamicContent.Models.MainModel();

        public DynamicContent.Models.tbl_product_image GetOneImg(int id)
        {
            //var rs = from p in db.tbl_product
            //         join pc in db.tbl_product_color on p.ID equals pc.product
            //         join pm in db.tbl_product_image on pc.ID equals pm.color
            //         where p.ID == id
            //         orderby pm.ID ascending
            //         select pm;
            var rs = from pm in db.tbl_product_image
                     join pc in db.tbl_product_color on pm.color equals pc.ID
                     join p in db.tbl_product on pc.product equals p.ID
                     where p.ID == id
                     orderby pm.ID ascending
                     select pm;
            return rs.FirstOrDefault();
        }
        
        public DynamicContent.Models.tbl_TenantCategory GetTenantCategoryIdByCode(string id)
        {
            var rs = from c in db.tbl_TenantCategory
                     where c.code == id
                     select c;
            return rs.FirstOrDefault();
        }
        
        public IEnumerable<_object.AllProduct> GrabAllProduct()
        {
            var rs = from p in db.tbl_product
                     //join c in db.TypeMasters on p.category equals c.ID
                     join s in db.tbl_submenu on p.tenant_subcategory equals s.ID
                     //join t in db.tbl_tenant on p.tenant equals t.ID
                     join tm in db.TenantMaster on p.tenant equals tm.TenantID
                     where tm.isJoin == "Y" && p.status == true && p.approved == true 
                     select new _object.AllProduct
                     {
                         id = p.ID,
                         name = p.name,
                         images = p.images,
                         price = p.price,
                         size = p.size,
                         description = p.description,
                         size_fit = p.size_fit,
                         gender = p.gender,
                         idcategory = p.category,
                         //category = c.TypeDesc,
                         idsubmenu = s.ID,
                         submenu = s.name,
                         idtenant = tm.TenantID,
                         tenant = tm.ShopName,
                         status = p.status,
                         tenant_category = p.tenant_category,
                         tenant_subcategory = p.tenant_subcategory,
                         approved_date = p.approved_date,
                         isHomepage = p.isHomepage
                     };
            return rs.ToList();
        }
        public IEnumerable<_object.AllProduct> GetProductByTenantCategory(int id)
        {
            var rs = from p in db.tbl_product
                     join s in db.tbl_submenu on p.tenant_subcategory equals s.ID
                     join tm in db.TenantMaster on p.tenant equals tm.TenantID
                     where tm.isJoin == "Y" && p.status == true && p.approved == true && p.tenant_category == id
                     select new _object.AllProduct
                     {
                         id = p.ID,
                         name = p.name,
                         images = p.images,
                         price = p.price,
                         size = p.size,
                         description = p.description,
                         size_fit = p.size_fit,
                         gender = p.gender,
                         idcategory = p.category,
                         //category = c.TypeDesc,
                         idsubmenu = s.ID,
                         submenu = s.name,
                         idtenant = tm.TenantID,
                         tenant = tm.ShopName,
                         status = p.status,
                         tenant_category = p.tenant_category,
                         tenant_subcategory = p.tenant_subcategory,
                         approved_date = p.approved_date,
                         isHomepage = p.isHomepage
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.AllProduct> GetProductByTenantCategory2(int id)
        {
            var rs = from p in db.tbl_product
                     join c in db.TypeMasters on p.category equals c.ID
                     join s in db.tbl_submenu on p.tenant_subcategory equals s.ID
                     //join t in db.tbl_tenant on p.tenant equals t.ID
                     join tm in db.TenantMaster on p.tenant equals tm.TenantID
                     where tm.isJoin == "Y" && p.status == true && p.approved == true && p.tenant_category == id
                     select new _object.AllProduct
                     {
                         id = p.ID,
                         name = p.name,
                         images = p.images,
                         price = p.price,
                         size = p.size,
                         description = p.description,
                         size_fit = p.size_fit,
                         gender = p.gender,
                         idcategory = p.category,
                         category = c.TypeDesc,
                         idsubmenu = s.ID,
                         submenu = s.name,
                         idtenant = tm.TenantID,
                         tenant = tm.ShopName,
                         status = p.status,
                         tenant_category = p.tenant_category,
                         tenant_subcategory = p.tenant_subcategory,
                         approved_date = p.approved_date,
                         isHomepage = p.isHomepage
                     };
            return rs.ToList();
        }

        //GetProductPi
        public IEnumerable<_object.ProductImg> GetProductImg(int id) 
        {
            var rs = from pm in db.tbl_product_image
                     join pc in db.tbl_product_color on pm.color equals pc.ID
                     join p in db.tbl_product on pc.product equals p.ID
                     where p.ID == id
                     orderby pm.ID ascending
                     select new _object.ProductImg
                     {
                         id = pm.ID,
                         images = pm.images,
                         color = pc.ID
                     };
            return rs.ToList();
        }
        public IEnumerable<_object.ProductImg> GetProductImgTmp(int id)
        {
            var rs = from pm in db.tbl_product_image_tmp
                     join pc in db.tbl_product_color_tmp on pm.color equals pc.ID
                     join p in db.tbl_product_tmp on pc.product equals p.ID
                     where p.ID == id
                     orderby pm.ID ascending
                     select new _object.ProductImg
                     {
                         id = pm.ID,
                         images = pm.images,
                         color = pc.ID
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.ProductImg> GetProductImgByColor(int id)
        {
            var rs = from pm in db.tbl_product_image
                     join pc in db.tbl_product_color on pm.color equals pc.ID
                     join p in db.tbl_product on pc.product equals p.ID
                     where pc.ID == id
                     orderby pm.ID ascending
                     select new _object.ProductImg
                     {
                         id = pm.ID,
                         images = pm.images,
                         color = pc.ID
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.ProductImg> GetProductImgTmpByColor(int id)
        {
            var rs = from pm in db.tbl_product_image_tmp
                     join pc in db.tbl_product_color_tmp on pm.color equals pc.ID
                     join p in db.tbl_product_tmp on pc.product equals p.ID
                     where pc.ID == id
                     orderby pm.ID ascending
                     select new _object.ProductImg
                     {
                         id = pm.ID,
                         images = pm.images,
                         color = pc.ID
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.ProductImg> GetProductImg2(int id)
        {
            var rs = from pm in db.tbl_product_image
                     where pm.color == id
                     orderby pm.ID ascending
                     select new _object.ProductImg
                     {
                         id = pm.ID,
                         images = pm.images,
                         color = pm.color
                     };
            return rs.ToList();
        }

        public IEnumerable<_object.ProductImg> GetProductImg2Tmp(int id)
        {
            var rs = from pm in db.tbl_product_image_tmp
                     where pm.color == id
                     orderby pm.ID ascending
                     select new _object.ProductImg
                     {
                         id = pm.ID,
                         images = pm.images,
                         color = pm.color
                     };
            return rs.ToList();
        }

        public IEnumerable<DynamicContent.Models.tbl_product_color> GetProductColor(int id)
        {
            var rs = from pc in db.tbl_product_color 
                     where pc.product == id
                     orderby pc.ID ascending
                     select pc;
            return rs.ToList();
        }

        public IEnumerable<DynamicContent.Models.tbl_product_color_tmp> GetProductColorTmp(int id)
        {
            var rs = from pc in db.tbl_product_color_tmp
                     where pc.product == id
                     orderby pc.ID ascending
                     select pc;
            return rs.ToList();
        }

        public DynamicContent.Models.tbl_product_color GetProductByColor(int id)
        {
            var rs = from pc in db.tbl_product_color 
                     where pc.ID == id
                     select pc;
            return rs.FirstOrDefault();
        }

        public DynamicContent.Models.tbl_product_color_tmp GetProductTmpByColor(int id)
        {
            var rs = from pc in db.tbl_product_color_tmp
                     where pc.ID == id
                     select pc;
            return rs.FirstOrDefault();
        }

        public DynamicContent.Models.tbl_product_color_tmp GetProductByColorTmp(int id)
        {
            var rs = from pc in db.tbl_product_color_tmp
                     where pc.ID == id
                     select pc;
            return rs.FirstOrDefault();
        }

        public IEnumerable<DynamicContent.Models.tbl_product_image> GetProductImgColor(int id)
        {
            var rs = from p in db.tbl_product_image
                     where p.color == id
                     select p;
            return rs.ToList();
        }

        public IEnumerable<DynamicContent.Models.tbl_product_image_tmp> GetProductImgColorTmp(int id)
        {
            var rs = from p in db.tbl_product_image_tmp
                     where p.color == id
                     select p;
            return rs.ToList();
        }


        public DynamicContent.Models.tbl_product_image GetOneImgByColor(int id) 
        {
            var rs = from p in db.tbl_product_image
                     where p.color == id
                     orderby p.ID ascending
                     select p;
            return rs.FirstOrDefault();
        }

        public DynamicContent.Models.tbl_product_image_tmp GetOneImgByColorTmp(int id)
        {
            var rs = from p in db.tbl_product_image_tmp
                     where p.color == id
                     orderby p.ID ascending
                     select p;
            return rs.FirstOrDefault();
        }

        public IEnumerable<DynamicContent.Models.tbl_product_color> GetColorByIds(int id)
        {
            var rs = from p in db.tbl_product_color
                     where p.ids == id
                     select p;
            return rs.ToList();
        }

        public void SetProductIdColor(int ids, int id)
        {
            var rs = GetColorByIds(ids);
            if (rs != null)
            {
                foreach (var c in rs)
                {
                    DynamicContent.Models.tbl_product_color pc = GetProductByColor(c.ID);
                    pc.product = id;
                    pc.ids = null;
                    db.Entry(pc).CurrentValues.SetValues(pc);
                    db.SaveChanges();
                }
            }
        }

        public void DeleteProductColor(int id)
        {
            var rs = GetColorByIds(id);
            if (rs != null)
            {
                db.tbl_product_color.Remove(rs.SingleOrDefault());
                db.SaveChanges();
                //foreach (var c in rs)
                //{
                //    DynamicContent.Models.tbl_product_color pc = GetProductByColor(c.ID);
                //    pc.product = id;
                //    pc.ids = null;
                //    db.Entry(pc).CurrentValues.SetValues(pc);
                //    db.SaveChanges();
                //}
            }
        }

    }
}
