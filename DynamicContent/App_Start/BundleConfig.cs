﻿using System.Web;
using System.Web.Optimization;

namespace DynamicContent
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/mainjs").Include(
                        "~/Scripts/jquery.js",
                        "~/Scripts/jquery-ui.js",
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/bootstrap-select.js",
                        "~/Scripts/owl.carousel.js",
                        "~/Scripts/main.js"));

            bundles.Add(new ScriptBundle("~/bundles/pijs").Include(
                        "~/Scripts/jquery-3.2.1.min.js",
                        "~/Scripts/js/megamenu.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/js/slippry.min.js",
                        "~/Scripts/js/jquery.picZoomer.js",
                        "~/Scripts/js/jquery.elevatezoom.js",
                        "~/Scripts/js/jquery.validate.min.js",
                        "~/Scripts/js/jqzoom.js",
						"~/Scripts/js/jquery-ui.min.js",
                        "~/Scripts/js/general.js"
        ));

            bundles.Add(new ScriptBundle("~/bundles/picturefill").Include(
                        "~/Scripts/picturefill.js"));

            bundles.Add(new Bundle("~/bundles/maincss", new DynamicContent.Helper.CSS3Minify()).Include(
                        "~/Content/bootstrap.css",

                        "~/Content/bootstrap-select.css",
                        "~/Content/animate.css",
                        "~/Content/owl.carousel.css",
                        "~/Content/font-awesome.css",
                        "~/Content/yamm.css",
                        "~/Content/main.css"));

            bundles.Add(new Bundle("~/bundles/picss", new DynamicContent.Helper.CSS3Minify()).Include(
                        "~/Assets/bootstrap/bootstrap.min.css",
                       "~/Content/css-pi/fontStyles.css",
                       "~/Content/css-pi/style.css",
                       "~/Content/css-pi/megamenu.css",
                       "~/Content/css-pi/jquery-picZoomer.css",
                       "~/Content/css-pi/font-awesome.min.css",
                       "~/Content/css-pi/bzoom.css",
						"~/Content/css-pi/jquery-ui.css",
                       "~/Content/css-pi/slippry.css"));

            bundles.Add(new ScriptBundle("~/manager/js").Include(
                        "~/Areas/Manager/Assets/js/bootstrap/bootstrap.js",
                        "~/Areas/Manager/Assets/js/notification/SmartNotification.js",
                        "~/Areas/Manager/Assets/js/plugin/smartwidgets/jarvis.widget.js",
                        "~/Areas/Manager/Assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.js",
                        "~/Areas/Manager/Assets/js/plugin/sparkline/jquery.sparkline.js",
                        "~/Areas/Manager/Assets/js/plugin/masked-input/jquery.maskedinput.js",
                        "~/Areas/Manager/Assets/js/plugin/select2/select2.js",
                        "~/Areas/Manager/Assets/js/plugin/bootstrap-slider/bootstrap-slider.js",
                        "~/Areas/Manager/Assets/js/plugin/msie-fix/jquery.mb.browser.js",
                        "~/Areas/Manager/Assets/js/plugin/fastclick/fastclick.js",
                        "~/Areas/Manager/Assets/js/app.js",
                        "~/Areas/Manager/Assets/js/plugin/fullcalendar/jquery.fullcalendar.js"));

            bundles.Add(new ScriptBundle("~/manager/datatable").Include(
                        "~/Areas/Manager/Assets/js/plugin/datatables/jquery.dataTables-cust.js",
                        "~/Areas/Manager/Assets/js/plugin/datatables/media/js/TableTools.js",
                        "~/Areas/Manager/Assets/js/plugin/datatables/DT_bootstrap.js"));

            bundles.Add(new ScriptBundle("~/manager/form").Include(
                        "~/Areas/Manager/Assets/js/plugin/ckeditor/ckeditor.js",
                        "~/Areas/Manager/Assets/js/plugin/colorpicker/bootstrap-colorpicker.min.js",
                        "~/Areas/Manager/Assets/js/plugin/jquery-form/jquery-form.js",
                        "~/Areas/Manager/Assets/js/plugin/jquery-validate/jquery.validate.js",
                        "~/Areas/Manager/Assets/js/plugin/jquery-validate/jquery.validate.custom.js",
                        "~/Areas/Manager/Assets/js/plugin/select2.4.0.3/select2.full.js"));

            bundles.Add(new Bundle("~/manager/css", new DynamicContent.Helper.CSS3Minify()).Include(
                        "~/Areas/Manager/Assets/css/bootstrap.css",
                        "~/Areas/Manager/Assets/css/font-awesome.css",
                        "~/Areas/Manager/Assets/css/smartadmin-production.css",
                        "~/Areas/Manager/Assets/css/smartadmin-skins.css",
                        "~/Areas/Manager/Assets/css/demo.css",
                        "~/Areas/Manager/Assets/css/custom.css",
                        "~/Content/colors/cyan.css",
                        "~/Areas/Manager/Assets/css/select2.4.0.3/select2.css"));

            bundles.Add(new ScriptBundle("~/manager/ckeditor").Include(
                "~/Areas/SysManager/Assets/js/vendor/ckeditor/ckeditor.js"));

            //MANAGER TENANT
            bundles.Add(new Bundle("~/TenantManager/css").Include(
                "~/Areas/TenantManager/Assets/css/vendor/bootstrap.min.css",
                "~/Areas/TenantManager/Assets/css/vendor/animate.css",
                "~/Areas/TenantManager/Assets/css/vendor/font-awesome.min.css",
                "~/Areas/TenantManager/Assets/css/animsition/css/animsition.min.css",
                "~/Areas/TenantManager/Assets/css/main.css",
                "~/Areas/TenantManager/Assets/css/custom.css"
                ));

            bundles.Add(new ScriptBundle("~/modernizr").Include("~/Areas/TenantManager/Assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js"));

            //FORM VALIDATION
            bundles.Add(new Bundle("~/formvalidation/css").Include("~/Areas/TenantManager/Assets/js/vendor/formvalidation/css/formvalidation.min.css"));
            bundles.Add(new ScriptBundle("~/formvalidation/js").Include(
                "~/Areas/TenantManager/Assets/js/vendor/formvalidation/js/formvalidation.min.js", 
                "~/Areas/TenantManager/Assets/js/vendor/formvalidation/js/framework/bootstrap.min.js"));

            //SUMMERNOTE
            bundles.Add(new Bundle("~/summernote/css").Include("~/Areas/TenantManager/Assets/js/vendor/summernote/summernote.css"));
            bundles.Add(new ScriptBundle("~/summernote/js").Include("~/Areas/TenantManager/Assets/js/vendor/summernote/summernote.min.js"));

            //COLOR PICKER
            bundles.Add(new Bundle("~/colorpicker/css").Include("~/Areas/TenantManager/Assets/js/vendor/colorpicker/css/bootstrap-colorpicker.min.css"));
            bundles.Add(new ScriptBundle("~/colorpicker/js").Include("~/Areas/TenantManager/Assets/js/vendor/colorpicker/js/bootstrap-colorpicker.min.js"));


            bundles.Add(new Bundle("~/TenantManager/Datatables/css").Include(
                "~/Areas/TenantManager/Assets/js/vendor/datatables/datatables.bootstrap.min.css",
                "~/Areas/TenantManager/Assets/js/vendor/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css",
                "~/Areas/TenantManager/Assets/js/vendor/datatables/extensions/Responsive/css/dataTables.responsive.css"    ,
                "~/Areas/TenantManager/Assets/js/vendor/datatables/extensions/ColVis/css/dataTables.colVis.min.css"        ,
                "~/Areas/TenantManager/Assets/js/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.min.css",
                "~/Areas/TenantManager/Assets/js/vendor/datatables/extensions/button/buttons.dataTables.min.css"
                ));

            bundles.Add(new ScriptBundle("~/TenantManager/Datatables/js").Include(
                "~/Areas/TenantManager/Assets/js/vendor/datatables/js/jquery.dataTables.min.js",
                "~/Areas/TenantManager/Assets/js/vendor/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js",
                "~/Areas/TenantManager/Assets/js/vendor/datatables/extensions/Responsive/js/dataTables.responsive.min.js",
                "~/Areas/TenantManager/Assets/js/vendor/datatables/extensions/ColVis/js/dataTables.colVis.min.js",
                "~/Areas/TenantManager/Assets/js/vendor/datatables/extensions/TableTools/js/dataTables.tableTools.min.js",
                "~/Areas/TenantManager/Assets/js/vendor/datatables/extensions/dataTables.bootstrap.js",
                "~/Areas/TenantManager/Assets/js/vendor/datatables/extensions/button/dataTables.buttons.min.js",
                "~/Areas/TenantManager/Assets/js/vendor/datatables/extensions/button/buttons.html5.min.js",
                "~/Areas/TenantManager/Assets/js/vendor/datatables/extensions/button/buttons.flash.min.js"
                ));

            bundles.Add(new ScriptBundle("~/TenantManager/js").Include(
                "~/Areas/TenantManager/Assets/js/vendor/bootstrap/bootstrap.min.js",
                "~/Areas/TenantManager/Assets/js/vendor/jRespond/jRespond.min.js",
                "~/Areas/TenantManager/Assets/js/vendor/sparkline/jquery.sparkline.min.js",
                "~/Areas/TenantManager/Assets/js/vendor/slimscroll/jquery.slimscroll.min.js",
                "~/Areas/TenantManager/Assets/js/vendor/animsition/js/jquery.animsition.min.js",
                "~/Areas/TenantManager/Assets/js/animsition/js/jquery.animsition.min.js",
                "~/Areas/TenantManager/Assets/js/screenfull/screenfull.min.js",
                "~/Areas/TenantManager/Assets/js/vendor/filestyle/bootstrap-filestyle.min.js",
                "~/Areas/TenantManager/Assets/js/main.js"
                ));
            /*
            <link rel="stylesheet" href="assets/js/vendor/datatables/datatables.bootstrap.min.css">
            <link rel="stylesheet" href="assets/js/vendor/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"   >
            <link rel="stylesheet" href="assets/js/vendor/datatables/extensions/Responsive/css/dataTables.responsive.css"       >
            <link rel="stylesheet" href="assets/js/vendor/datatables/extensions/ColVis/css/dataTables.colVis.min.css"           >
            <link rel="stylesheet" href="assets/js/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.min.css"       >


            */
            BundleTable.EnableOptimizations = false;
        }
    }
}