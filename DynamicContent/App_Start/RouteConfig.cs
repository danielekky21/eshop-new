﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace DynamicContent
{
    public class RouteConfig
    {

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Category",
                url: "Category/{category}/{id}/{submenu}",
                defaults: new { controller = "Category", action = "Index", id = UrlParameter.Optional, category = UrlParameter.Optional, submenu = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Stores",
                url: "stores/{*id}",
                defaults: new { controller = "Stores", action = "Index", id = UrlParameter.Optional, category = UrlParameter.Optional, submenu = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Product",
               url: "{controller}/{id}/{product}",
               defaults: new { controller = "Product", action = "Index", id = UrlParameter.Optional, category = UrlParameter.Optional, submenu = UrlParameter.Optional },
               namespaces: new string[] { "DynamicContent.Controllers" }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "DynamicContent.Controllers" }
            );

            routes.MapRoute(
                name: "Customer",
                url: "{controller}/{action}",
                defaults: new { controller = "Customer", action = "Index" }
            );

        }
    }
}

//namespace DynamicContent
//{
//    public class RouteConfig
//    {

//        public static void RegisterRoutes(RouteCollection routes)
//        {
//            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

//            //Redirect Url from DB
//            var obj = new DynamicContent.Models.MainModel();
//            var urls = obj.GetActiveRedirectUrl();
//            var urlConstraints = new List<string>();
//            foreach (var item in urls)
//            {
//                urlConstraints.Add(item.url_from.Trim('/'));
//            }
//            routes.MapRoute(
//                name: "Redirect Url",
//                url: "{*id}",
//                defaults: new { controller = "Redirect", action = "Index", id = UrlParameter.Optional },
//                constraints: new
//                {
//                    id = string.Join("|", urlConstraints)
//                },
//                namespaces: new string[] { "DynamicContent.Controllers" }
//            );

//            routes.MapRoute(
//                name: "Controller",
//                url: "{controller}/{action}/{id}",
//                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
//                constraints: new
//                {
//                    controller = "Captcha|SocialWall|Process"
//                },
//                namespaces: new string[] { "DynamicContent.Controllers" }
//            );

//            routes.MapRoute(
//                name: "Category",
//                url: "{controller}/{category}/{id}/{submenu}",
//                defaults: new { controller = "Category", action = "Index", id = UrlParameter.Optional, category = "", submenu = "" }               
//            );

//            routes.MapRoute(
//                name: "Single Handler",
//                url: "{action}/{id}",
//                defaults: new { controller = "Process", action = "Index", id = UrlParameter.Optional },
//                constraints: new
//                {
//                    action = "Search"
//                },
//                namespaces: new string[] { "DynamicContent.Controllers" }
//            );

//            //routes.MapRoute(
//            //    name: "Default",
//            //    url: "{controller}/{action}/{id}",
//            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
//            //);
//            routes.MapRoute(
//                name: "Default",
//                url: "{*id}",
//                defaults: new { controller = "Content", action = "Index", id = "Home" },
//                namespaces: new string[] { "DynamicContent.Controllers" }
//            );
//        }
//    }
//}