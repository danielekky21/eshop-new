﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Emall.Edit" %>
<%@ Register TagPrefix="UserControl" TagName="Header" Src="Head.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="Footer" Src="Footer.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="Menu" Src="Menu.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<UserControl:Header runat="server" />
<body id="minovate" class="appWrapper scheme-black">
    <style>
        table td, table th {padding:5px;}
    </style>
    <form id="form2" runat="server">
        <UserControl:Menu runat="server" />

        <div id="wrap" class="animsition">
            <section id="content">

                <div class="page page-product-list">
                    <div class="pageheader">
                        <h2><asp:Label ID="myTitle" runat="server" CssClass="pageheader-title"></asp:Label> <span>Today: <% Response.Write(DateTime.Now); %>)</span></h2>
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <section class="tile" fullscreen="isFullscreen02" style="padding: 10px;overflow-x: auto;">
                                <table>
                                    <tr>
                                        <td style="margin-left: 50px;">
                                            <asp:Table ID="myTblInformation" runat="server"></asp:Table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="margin-left: 50px;">
                                            <asp:Table ID="mytbl" runat="server"></asp:Table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Table ID="mytbl1" runat="server"></asp:Table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:label ID="myLabel1" runat="server"></asp:label> 
                                            <br/>
                                        </td>
                                    </tr>
                                </table>
                            </section>
                        </div>
                    </div>

                </div>

            </section>

        </div>

    </form>

    <UserControl:Footer runat="server" />
</body>
</html>