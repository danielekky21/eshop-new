﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="Emall.Footer" %>
<%@ Import Namespace="DynamicContent.Areas.SysManager.Helper" %>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery/jquery-1.11.2.min.js"><\/script>')</script>
<script src="/Areas/TenantManager/Assets/js/vendor/bootstrap/bootstrap.min.js"></script>
<script src="/Areas/TenantManager/Assets/js/vendor/jRespond/jRespond.min.js"></script>
<script src="/Areas/TenantManager/Assets/js/vendor/sparkline/jquery.sparkline.min.js"></script>
<script src="/Areas/TenantManager/Assets/js/vendor/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/Areas/TenantManager/Assets/js/vendor/animsition/js/jquery.animsition.min.js"></script>
<script src="/Areas/TenantManager/Assets/js/vendor/filestyle/bootstrap-filestyle.min.js"></script>
<script src="/Areas/TenantManager/Assets/js/main.js"></script>


<script type="text/javascript">
    $(document).ajaxError(function (e, xhr) {
        if (xhr.status == 403) {
            var response = $.parseJSON(xhr.responseText);
            if (response.msg != undefined || response.msg != '') {
                alert(response.msg);
            }
            window.location = response.logonurl;
        }
        
    });
    <% if (!String.IsNullOrEmpty(Session_.Username)) { %>
        setInterval(function () {
            $.ajax({
                url: '/SysManager/Account/CheckSession',
                method: 'POST'
            });
        }, 5000);
    <% } %>
    
    
</script>

