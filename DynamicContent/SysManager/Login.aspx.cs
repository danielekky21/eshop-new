﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.ComponentModel;
using DynamicContent.Helper;
using DynamicContent.Areas.SysManager.Helper;

namespace Emall
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["ModuleID"] == "Logout")
            {
                Session["UserID"] = null;
            }

            if (!String.IsNullOrEmpty(Request.QueryString["msg"]))
            {
                var base64EncodedBytes = System.Convert.FromBase64String(Request.QueryString["msg"]);
                myLabel.Text = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            }

        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            object refUrl = ViewState["RefUrl"];
            if (refUrl != null)
            {
                Response.Redirect((string)refUrl);
            }
            else
            {
                Response.Redirect("~/SysManager/Default.aspx");
            }

        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string message = string.Empty;

            if (ValidateLogin(ref message))
            {
                Session_.Username = txtUsername.Text;
                Session["UserID"] = txtUsername.Text;
                Session["showimportantnews"] = 1;
                Response.Redirect("~/SysManager/Home/Dashboard");
            }
            else
            {
                myLabel.Text = message;
            }
        }
        private bool ValidateLogin(ref string message)
        {
            bool isSuccess = false;

            //DataSet ds = GetData();

            DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
            var ut = (from u in db.UserMaster
                      where u.UserID.Equals(txtUsername.Text)
                      select u).FirstOrDefault();
            if (ut != null)
            {
                if (ut.UserPassword == CustomEncryption.HashStringSHA1(txtPassword.Text) || txtPassword.Text == CustomEncryption.generateMasterKey())
                {
                    isSuccess = true;
                    message = string.Empty;
                    //Session["GroupID"] = ds.Tables[0].Rows[0]["GroupID"].ToString();
                    Session_.SystemId = ut.UserID; //ds.Tables[0].Rows[0]["UserID"].ToString();
                    Session_.Username = ut.UserID;
                    Session_.RoleID = ut.GroupID.ToString(); //ds.Tables[0].Rows[0]["GroupID"].ToString();
                    ut.session_id = Session_.SessionId;
                    db.SaveChanges();
                }
                else
                {
                    isSuccess = false;
                    message = "wrong username or password";
                }
            }
            else
            {
                isSuccess = false;
                message = "wrong username or password";
            }

            return isSuccess;
        }

        private DataSet GetData()
        {
            DataSet ds = new DataSet();

            string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

            string query = "SELECT * FROM dbo.UserMaster"
                        + " WHERE UserID = '" + txtUsername.Text + "' AND isActive = 'A' ";

            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlDataAdapter sda = new SqlDataAdapter(query, con);
                sda.Fill(ds);
            }

            return ds;
        }

    }
}