﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Emall.Default" %>
<%@ Import Namespace="DynamicContent.Areas.SysManager.Helper" %>
<%@ Register TagPrefix="UserControl" TagName="Header" Src="Head.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="Footer" Src="Footer.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="Menu" Src="Menu.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<UserControl:Header runat="server" />
<body id="minovate" class="appWrapper scheme-black">
    
    <form id="form1" runat="server">             
        <UserControl:Menu runat="server" />
    </form>
    
    <div id="wrap" class="animsition">
        <section id="content">
            <div class="page page-dashboard">
                <div class="pageheader">
                    <h2>Dashboard <span>Today: 6/22/2016 02:58:13 PM</span></h2>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <section class="tile" fullscreen="isFullscreen02">
                            <div class="tile-header dvd dvd-btm">
                                <h1 class="custom-font"><strong>Welcome </strong>to PI Management Section</h1>
                            </div>
                            <div class="tile-body p-0" style="text-align: center">
                                <img src="/Images/logo.jpg" class="img-responsive" alt="Plaza Indonesia">
                            </div>
                        </section>
                    </div>
                    <div class="col-md-4">
                        <section class="tile" fullscreen="isFullscreen02">
                            <div class="tile-header dvd dvd-btm">
                                <h1 class="custom-font"><strong>Information </strong></h1>
                            </div>
                            <div class="tile-body">
                                <p>
                                    Username: <% Response.Write(Session_.Username.ToString()); %>
                                    <br/>
                                    Role: PI
                                </p>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <UserControl:Footer runat="server" />
</body>
</html>