﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.ComponentModel;
//Menu
using System.IO;
using System.Configuration;
//Menu
namespace Emall
{
    public partial class Detail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string ID, ModuleID;
            if (!this.IsPostBack)
            {
                if (Request.QueryString["ModuleID"] == null)
                {
                    Response.Redirect("~/SysManager/Login.aspx");
                }
                if (Request.QueryString["ID"] == null)
                {
                    Response.Redirect("~/SysManager/Login.aspx");
                }
                ModuleID = Request.QueryString["ModuleID"].ToString();
                ID = Request.QueryString["ID"].ToString();
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();
                ViewState["Action"] = "";
                ViewState["ActionFlag"] = "";
                ViewState["ID"] = ID;
                ViewState["ModuleID"] = ModuleID;
                //MsgBox(ModuleID);


            }
            ID = Request.QueryString["ID"].ToString();


            //con.Close();
            //ValidateRight(Int32.Parse(ID));
            ValidateRight(ID);
            PopulateDisplay();
            GetList(ID); //cek lagi

            //generatePdf();
            //pdf2();
            //WordPdf();

            //string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            //MessageBox.Show("Hello World");
        }


        private void MsgBox(string StrMsg)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + StrMsg + "');", true);
        }
        private void GetList(string ID)
        {
            //int ActionID = 0;

            //if (!string.IsNullOrEmpty(ViewState["ActionID"].ToString()))
            //{
            //    ActionID = Int32.Parse(ViewState["ActionID"].ToString());
            //}

            //PopulateInput(ActionID, ID);

            System.Web.UI.WebControls.TableRow trow;
            System.Web.UI.WebControls.TableCell tcell1, tcell2;

            trow = new System.Web.UI.WebControls.TableRow();
            tcell1 = new System.Web.UI.WebControls.TableCell();
            tcell2 = new System.Web.UI.WebControls.TableCell();

            //if (!string.IsNullOrEmpty(ViewState["ActionID"].ToString()))
            //{
            //    Button ApproveButton = new Button();
            //    ApproveButton.ID = "SaveButton";
            //    ApproveButton.Text = "Approve";
            //    ApproveButton.Click += new EventHandler(Approve_Click);

            //    tcell1.Controls.Add(ApproveButton);
            //    trow.Cells.Add(tcell1);
            //}
            Button BackButton = new Button();
            BackButton.ID = "BackButton";
            BackButton.Text = "Back";
            BackButton.Click += new EventHandler(Back_Click);

            tcell2.Controls.Add(BackButton);
            trow.Cells.Add(tcell2);
            mytbl1.Rows.Add(trow);
        }

        private void PopulateDisplay()
        {
            System.Web.UI.WebControls.TableRow trow;
            System.Web.UI.WebControls.TableCell tcell1, tcell2, tcell3;
            string query = ViewState["ActionList"].ToString();
            if (!string.IsNullOrEmpty(query))
            {
                string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;


                using (SqlConnection con = new SqlConnection(constr))
                {
                    DataTable dt = new DataTable();
                    using (SqlCommand cmd = new SqlCommand(query))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        //cmd.Parameters.Add(cmd.CreateParameter("AssetID"));
                        using (SqlDataAdapter sda = new SqlDataAdapter())
                        {
                            //cmd.Parameters.AddWithValue("@ModuleId", ModuleID);
                            cmd.CommandType = CommandType.Text;
                            cmd.Connection = con;
                            sda.SelectCommand = cmd;
                            sda.Fill(dt);

                            //if (!dt.)
                            //{
                            //    myTitle.Text = "NULL";
                            //} else
                            //{
                            trow = new System.Web.UI.WebControls.TableRow();
                            tcell1 = new System.Web.UI.WebControls.TableCell();

                            Label myLabelTitle = new Label();
                            myLabelTitle.Text = "Information";
                            myLabelTitle.Font.Bold = true;
                            myLabelTitle.Font.Underline = true;
                            myLabelTitle.Font.Size = 16;

                            tcell1.Controls.Add(myLabelTitle);
                            tcell1.ColumnSpan = 3;
                            trow.Cells.Add(tcell1);

                            mytbl.Rows.Add(trow);
                            trow = new System.Web.UI.WebControls.TableRow();
                            mytbl.Rows.Add(trow);
                            foreach (DataRow row in dt.Rows)
                            {
                                foreach (DataColumn Column in dt.Columns)
                                {
                                    trow = new System.Web.UI.WebControls.TableRow();
                                    tcell1 = new System.Web.UI.WebControls.TableCell();
                                    tcell2 = new System.Web.UI.WebControls.TableCell();
                                    tcell3 = new System.Web.UI.WebControls.TableCell();

                                    Label myLabel = new Label();
                                    myLabel.Text = Column.ColumnName.ToString();
                                    myLabel.Font.Bold = true;

                                    Label myLabelSeparator = new Label();
                                    myLabelSeparator.Text = ":";

                                    Label myLabelValue = new Label();
                                    //myLabelValue.Text = row[Column].ToString().Replace(Environment.NewLine,"<br>");
                                    myLabelValue.Text = row[Column].ToString();

                                    tcell1.Controls.Add(myLabel);
                                    tcell2.Controls.Add(myLabelSeparator);
                                    tcell3.Controls.Add(myLabelValue);
                                    tcell1.VerticalAlign = VerticalAlign.Top;
                                    tcell2.VerticalAlign = VerticalAlign.Top;
                                    tcell3.VerticalAlign = VerticalAlign.Top;
                                    trow.Cells.Add(tcell1);
                                    trow.Cells.Add(tcell2);
                                    trow.Cells.Add(tcell3);

                                    mytbl.Rows.Add(trow);
                                }
                            }

                            //}
                        }
                    }
                    con.Close();
                }

            }
        }

        private DataTable GetDDLData(String FieldName, int value)
        {
            string query = "SELECT * from DDLList where DDLName = '" + FieldName + "'";
            //string query = "SELECT * from DDLList  where FieldName = @FieldName";
            //MsgBox("hehehe" + FieldName);
            if (value == 1)
            {
                query = query + " and isValue = 1";
            }

            string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
            //SqlConnection con = new SqlConnection(
            //        WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString);
            using (SqlConnection con = new SqlConnection(constr))
            {
                DataTable dt = new DataTable();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        //cmd.Parameters.AddWithValue("@FieldName", FieldName);
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        sda.Fill(dt);
                    }
                }
                return dt;
            }
        }
        private void SaveAction(int ActionID, int ID)
        {

        }
        private void Approve_Click(object sender, EventArgs e)
        {
            ViewState["Action"] = "Approve";

            SaveAction(Int32.Parse(ViewState["ActionID"].ToString()), Int32.Parse(ViewState["ID"].ToString()));
            string strInsert = " exec sp_ActionApprove " + ViewState["ID"].ToString() + ",'" + Session["UserID"].ToString() + "', '" + ViewState["ActionFlag"].ToString() + "' ";
            myLabel.Text = strInsert;


            SqlConnection con = new SqlConnection(
                    WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString
            );
            SqlCommand cmd1 = new SqlCommand(strInsert, con);
            con.Open();
            cmd1.ExecuteNonQuery();
            con.Close();
            PrevPage();
        }
        protected void Back_Click(object sender, EventArgs e)
        {
            PrevPage();
        }
        private void PrevPage()
        {
            object refUrl = ViewState["RefUrl"];
            if (refUrl != null)
                Response.Redirect((string)refUrl);
        }
        private void ValidateRight(string ID)
        {
            string query = "SELECT top 1 a.[ModuleID], a.[ModuleRole],a.[TitleDesc], a.[MasterTable], a.[ActionList],a.[UserName] FROM [vw_UserRight] a " +
                            "WHERE a.UserID = '" + Session["UserID"] + "' and a.ModuleID = '" + ViewState["ModuleID"] + "'";
            string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
            //myLabel.Text = query;
            using (SqlConnection con = new SqlConnection(constr))
            {
                DataTable dt = new DataTable();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    //cmd.Parameters.Add(cmd.CreateParameter("AssetID"));
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        //cmd.Parameters.AddWithValue("@ModuleId", ModuleID);
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        sda.Fill(dt);

                        //if (!dt.)
                        //{
                        //    myTitle.Text = "NULL";
                        //} else
                        //{
                        foreach (DataRow row in dt.Rows)
                        {
                            myTitle.Text = row["TitleDesc"].ToString();
                            //ViewState["ActionID"] = row["ActionID"].ToString();
                            ViewState["ActionList"] = row["ActionList"].ToString().Replace("@ID", ID.ToString());
                            ViewState["UserName"] = row["UserName"].ToString();
                            ViewState["ModuleID"] = row["ModuleID"].ToString();
                        }

                        //}
                    }
                }
            }
        }

    }
}