﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Emall.List" %>

<%@ Register TagPrefix="UserControl" TagName="Header" Src="Head.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="Footer" Src="Footer.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="Menu" Src="Menu.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<UserControl:Header runat="server" />
<body id="minovate" class="appWrapper scheme-black">
    <style>
        table td, table th {padding:5px;}
    </style>
    <form id="form2" runat="server">
        <UserControl:Menu runat="server" />

        <div id="wrap" class="animsition">
            <section id="content">

                <div class="page page-product-list">
                    <div class="pageheader">
                        <h2><asp:Label ID="myTitle" runat="server" CssClass="pageheader-title"></asp:Label> <span>Today: <% Response.Write(DateTime.Now); %>)</span></h2>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <section class="tile" fullscreen="isFullscreen02" style="padding: 10px;overflow-x: auto;">
                                
                                <table>
                                    <tr>
                                        <td style="margin-left: 50px;">
                                            <asp:Table ID="mytbl" runat="server"></asp:Table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Table ID="mytbl1" runat="server"></asp:Table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="myLabel" runat="server"></asp:Label>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <asp:GridView ID="GridView1" HeaderStyle-BackColor="#996600" HeaderStyle-ForeColor="White" EmptyDataRowStyle-BackColor="White" RowStyle-BackColor="White" RowStyle-ForeColor="Black" EmptyDataText ="No Data to Display" EmptyDataRowStyle-ForeColor="Black" ShowHeaderWhenEmpty="true" OnPageIndexChanging="OnPageIndexChanging" OnRowDataBound="OnRowDataBound" AllowPaging="True" PageSize="10" runat="server">
                                        
                                            </asp:GridView>

                                        </td>
                                    </tr>
                                </table>

                            </section>
                        </div>
                    </div>

                </div>

            </section>

        </div>

    </form>

    <UserControl:Footer runat="server" />
</body>
</html>
