﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Emall.Login" %>
<%@ Register TagPrefix="UserControl" TagName="Header" Src="Head.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="Footer" Src="Footer.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<UserControl:Header runat="server" />
<body id="minovate" class="appWrapper">
    
    <div id="wrap" class="animsition">
        <div class="page page-core page-login">

            <div class="text-center"><h3 class="text-light text-white">Plaza Indonesia</h3></div>

            <div class="container w-420 p-15 bg-white mt-40 text-center">

                <% if(!string.IsNullOrEmpty(myLabel.Text)){ %>
                <div class="alert alert-lightred fade in">
                    <asp:Literal ID="myLabel" runat="server"> </asp:Literal>
                </div>
                <% } %>

                <h2 class="text-light text-greensea">Log In</h2>

                <form id="form2" runat="server">

                    <div class="form-group">
                        <asp:TextBox ID="txtUsername" runat="server" placeholder="Username" CssClass="form-control underline-input"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" placeholder="Password" CssClass="form-control underline-input"></asp:TextBox>
                    </div>

                    <div class="form-group text-left mt-20">
                        <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" CssClass="btn btn-greensea b-0 br-2 mr-5"> </asp:Button>
                        <a href="/SysManager/Account/ForgetPassword" class="pull-right mt-10">Forgot Credential?</a>
                    </div>

                </form>
        
            </div>

        </div>

        <UserControl:Footer runat="server" />

    </div> 
        
</body>
</html>
