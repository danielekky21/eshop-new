﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Head.ascx.cs" Inherits="Emall.Head" %>
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <link rel="icon" type="image/ico" href="/TenantManager/assets/images/favicon.ico">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/Areas/TenantManager/Assets/css/vendor/bootstrap.min.css" rel="stylesheet">
    <link href="/Areas/TenantManager/Assets/css/vendor/animate.css" rel="stylesheet">
    <link href="/Areas/TenantManager/Assets/css/vendor/font-awesome.min.css" rel="stylesheet">
    <link href="/Areas/TenantManager/Assets/css/main.css" rel="stylesheet">
    <link href="/Areas/TenantManager/Assets/css/custom.css" rel="stylesheet">

    <script src="/Areas/TenantManager/Assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>