﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Emall
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Session["UserID"] as string))
            {
                Response.Redirect("~/SysManager/Login.aspx");
            }
            else
            {
                //Response.Redirect("~/SysManager/Home/Dashboard");
            }
        }
    }
}