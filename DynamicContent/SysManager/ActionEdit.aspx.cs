﻿using System;

using System.Collections.Generic;

using System.Linq;

using System.Web;

using System.Web.UI;

using System.Web.UI.WebControls;



using System.Data;

using System.Data.SqlClient;

using System.Web.Configuration;

using System.ComponentModel;

using System.Drawing;

//Menu

using System.IO;

using System.Configuration;

//Menu

namespace Emall

{

    public partial class ActionEdit : System.Web.UI.Page

    {

        protected void Page_Load(object sender, EventArgs e)

        {

            string ModuleID, ActionID;

            int i;

            i = 0;

            ViewState["RemoveLine"] = "";

            if (!this.IsPostBack)

            {



                if (Request.QueryString["ModuleID"] == null || Request.QueryString["ActionID"] == null)

                {

                    Response.Redirect("~/SysManager/Login.aspx");

                }

                ModuleID = Request.QueryString["ModuleID"].ToString();

                ActionID = Request.QueryString["ActionID"].ToString();

                ID = Request.QueryString["ID"].ToString();

                ViewState["ModuleID"] = ModuleID;

                ViewState["ActionID"] = ActionID;

                ViewState["ID"] = ID;

                //ViewState["ModuleID"] =



                if (Request.UrlReferrer != null)

                {

                    ViewState["RefUrl"] = Request.UrlReferrer.ToString();

                }

                else

                {

                    ViewState["RefUrl"] = "";

                }

                //ViewState["RefUrl"] = "";

                ViewState["Action"] = "";

                ViewState["LineNo"] = 1;

                //MsgBox(ModuleID);



                //myLabel1.Text = ViewState["LineNo"].ToString();



            }

            ModuleID = Request.QueryString["ModuleID"].ToString();

            ActionID = Request.QueryString["ActionID"].ToString();

            ID = Request.QueryString["ID"].ToString();

            int TotalStep, CurrentStep;

            TotalStep = 0;

            CurrentStep = Int32.Parse(ActionID);



            //myLabel1.Text = Session["UserID"].ToString();

            //MsgBox(ModuleID);

            ValidateRight(Int32.Parse(ModuleID), Int32.Parse(ActionID));

            PopulateDisplay();

            //myTitleStep.Text = "Step " + CurrentStep.ToString() + " from " + TotalStep.ToString() + " - " + ViewState["ActionTitle"];

            myTitleStep.Text = "";







            if (ViewState["isMultiple"].ToString() != "Y")

            {

                PopulateInput(Int32.Parse(ModuleID), Int32.Parse(ActionID), 0);

            }

            else

            {

                i = i + 1;

                PopulateInput(Int32.Parse(ModuleID), Int32.Parse(ActionID), i);

                while (i < int.Parse(ViewState["LineNo"].ToString()))

                {

                    i = i + 1;

                    PopulateInputList(Int32.Parse(ModuleID), Int32.Parse(ActionID), i);

                }

            }



        }

        private void ValidateRight(int moduleID, int actionID)

        {

            //string query = "SELECT top 1 [ModuleRole],[TitleDesc], [MasterTable], [MasterField], [UploadFolder], [UserName], [InputList] FROM [vw_UserRight] WHERE ModuleId = " + moduleID + " and UserID = '" + Session["UserID"] + "' ";

            string query = "Select * FROM [vw_ModuleAction] WHERE ModuleId = " + moduleID + " and ActionID = " + actionID;

            string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;



            ViewState["MasterTable"] = "";

            //ViewState["ModuleRole"] = "";

            //ViewState["UserName"] = "";

            ViewState["InputList"] = "";

            ViewState["MasterField"] = "";

            ViewState["UploadFolder"] = "";

            ViewState["ActionTitle"] = "";

            ViewState["ActionDesc"] = "";

            ViewState["isMultiple"] = "";

            ViewState["DeleteScript"] = "";

            ViewState["EditMethod"] = "";



            using (SqlConnection con = new SqlConnection(constr))

            {

                DataTable dt = new DataTable();

                using (SqlCommand cmd = new SqlCommand(query))

                {

                    cmd.CommandType = CommandType.Text;

                    cmd.Connection = con;

                    //cmd.Parameters.Add(cmd.CreateParameter("AssetID"));

                    using (SqlDataAdapter sda = new SqlDataAdapter())

                    {

                        //cmd.Parameters.AddWithValue("@ModuleId", ModuleID);

                        cmd.CommandType = CommandType.Text;

                        cmd.Connection = con;

                        sda.SelectCommand = cmd;

                        sda.Fill(dt);



                        //if (!dt.)

                        //{

                        //    myTitle.Text = "NULL";

                        //} else

                        //{

                        foreach (DataRow row in dt.Rows)

                        {

                            myTitle.Text = row["TitleDesc"].ToString();

                            ViewState["MasterTable"] = row["MasterTable"].ToString();

                            //ViewState["ModuleRole"] = row["ModuleRole"].ToString();

                            //ViewState["UserName"] = Session["UserName"].ToString();//row["UserName"].ToString();

                            ViewState["InputList"] = row["InputList"].ToString();

                            ViewState["MasterField"] = row["MasterField"].ToString();

                            ViewState["UploadFolder"] = row["UploadFolder"].ToString();

                            ViewState["ActionTitle"] = row["ActionTitle"].ToString();

                            ViewState["ActionDesc"] = row["ActionDesc"].ToString();

                            ViewState["isMultiple"] = row["isMultiple"].ToString();

                            ViewState["DeleteScript"] = row["DeleteScript"].ToString();

                            ViewState["EditMethod"] = row["EditMethod"].ToString();

                            if (row["ActionDesc"] != null)

                            {

                                myTitleDesc.Text = row["ActionDesc"].ToString();

                            }

                        }



                        //}

                    }

                }

            }

        }

        private void PopulateAcknowledge()

        {

            TableRow trow;

            TableCell tcell1, tcell2;



            Label myLabel = new Label();

            myLabel.ID = "Acknowledge";

            myLabel.Text = "I understand that giving any incorrect or misleading information, or any omissions made with the intention of misleading PT Plaza Indonesia Realty could lead to the withdrawal of any offer of employment or my dismissal. I accept all of PT Plaza Indonesia Realty terms and conditions as stated in my application. <br/><br/>" +

                           "By submitting my application, I confirm my acceptance of the terms and conditions of the Privacy Statement and consent to the using, storing or otherwise processing of my Information (including any sensitive personal data) worldwide in accordance therewith. I further acknowledge and agree that the information I submit may be transferred to, processed and stored in countries that may not have data protection laws or may laws less protective of my Information than those in my home country. <br/><br/>";

            //myLabel.



            trow = new TableRow();

            tcell1 = new TableCell();



            tcell1.Controls.Add(myLabel);

            tcell1.ColumnSpan = 2;

            trow.Cells.Add(tcell1);



            mytbl1.Rows.Add(trow);



            trow = new TableRow();

            tcell1 = new TableCell();

            tcell2 = new TableCell();

            //if (ViewState["RefUrl"].ToString() == "")

            //{

            //Button BackButton = new Button();

            //BackButton.ID = "BackButton";

            //BackButton.Text = "Back";

            //BackButton.Click += new EventHandler(Back_Click);



            //tcell1.Controls.Add(BackButton);

            //}



            trow.Cells.Add(tcell1);

            Button SaveButton = new Button();

            SaveButton.ID = "ApproveButton";

            SaveButton.Text = "Agree";

            SaveButton.Click += new EventHandler(Acknowledge_Click);



            tcell1.Controls.Add(SaveButton);









            mytbl1.Rows.Add(trow);

        }

        private void PopulateDisplay()

        {

            TableRow trow;

            TableCell tcell1, tcell2, tcell3;

            if (!string.IsNullOrEmpty(ViewState["InputList"].ToString()))

            {

                string query = ViewState["InputList"].ToString();



                string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;





                using (SqlConnection con = new SqlConnection(constr))

                {

                    DataTable dt = new DataTable();

                    using (SqlCommand cmd = new SqlCommand(query))

                    {

                        cmd.CommandType = CommandType.Text;

                        cmd.Connection = con;

                        //cmd.Parameters.Add(cmd.CreateParameter("AssetID"));

                        using (SqlDataAdapter sda = new SqlDataAdapter())

                        {

                            //cmd.Parameters.AddWithValue("@ModuleId", ModuleID);

                            cmd.CommandType = CommandType.Text;

                            cmd.Connection = con;

                            sda.SelectCommand = cmd;

                            sda.Fill(dt);



                            //if (!dt.)

                            //{

                            //    myTitle.Text = "NULL";

                            //} else

                            //{

                            trow = new TableRow();

                            tcell1 = new TableCell();



                            Label myLabelTitle = new Label();

                            myLabelTitle.Text = "Information";

                            myLabelTitle.Font.Bold = true;

                            myLabelTitle.Font.Underline = true;

                            myLabelTitle.Font.Size = 16;



                            tcell1.Controls.Add(myLabelTitle);

                            tcell1.ColumnSpan = 3;

                            trow.Cells.Add(tcell1);



                            mytbl.Rows.Add(trow);

                            trow = new TableRow();

                            mytbl.Rows.Add(trow);

                            foreach (DataRow row in dt.Rows)

                            {

                                foreach (DataColumn Column in dt.Columns)

                                {

                                    trow = new TableRow();

                                    tcell1 = new TableCell();

                                    tcell2 = new TableCell();

                                    tcell3 = new TableCell();



                                    Label myLabel = new Label();

                                    myLabel.Text = Column.ColumnName.ToString();

                                    myLabel.Font.Bold = true;



                                    Label myLabelSeparator = new Label();

                                    myLabelSeparator.Text = ":";



                                    Label myLabelValue = new Label();

                                    myLabelValue.Text = row[Column].ToString().Replace(Environment.NewLine, "<br>");



                                    tcell1.Controls.Add(myLabel);

                                    tcell2.Controls.Add(myLabelSeparator);

                                    tcell3.Controls.Add(myLabelValue);

                                    trow.Cells.Add(tcell1);

                                    trow.Cells.Add(tcell2);

                                    trow.Cells.Add(tcell3);



                                    mytbl.Rows.Add(trow);

                                }

                            }



                            //}

                        }

                    }

                    con.Close();

                }



            }

        }

        private void PopulateInputList(int ModuleID, int ActionID, int LineNo)

        {

            DataTable dtInput = this.GetInputData(ModuleID, ActionID, 0);

            DataTable dtData = this.GetData(ModuleID, ActionID, 0);

            if (!this.IsPostBack)

            {

                int strLineNo = dtData.Rows.Count;

                if (strLineNo != 0 && LineNo <= 1)

                {

                    ViewState["LineNo"] = strLineNo.ToString();

                }



            }

            String strScript = "";

            String MyID = "";

            String MyIDData = "";

            int DataIdx = 0;

            //SqlConnection con = new SqlConnection(

            //WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString);

            TableRow trow;

            TableCell tcell1, tcell2, tcell3;

            Button RemoveButton = new Button();

            trow = new TableRow();

            tcell1 = new TableCell();



            Label myLabelTitle = new Label();

            if (ViewState["isMultiple"].ToString() == "Y")

            {

                if (ViewState["isMultiple"].ToString() != "Y")

                {

                    myLabelTitle.Text = ViewState["ActionTitle"].ToString();

                }

                else

                {

                    myLabelTitle.Text = ViewState["ActionTitle"].ToString() + " (" + LineNo.ToString() + ")";

                    DataIdx = LineNo - 1;

                }

                myLabelTitle.Font.Bold = true;

                myLabelTitle.Font.Underline = true;

                myLabelTitle.Font.Size = 16;



                tcell1.Controls.Add(myLabelTitle);

                if (ViewState["isMultiple"].ToString() == "Y")

                {

                    RemoveButton = new Button();

                    RemoveButton.ID = "RemoveButton" + LineNo.ToString();

                    RemoveButton.Text = "Remove";

                    RemoveButton.Click += new EventHandler(Remove_Click);

                    RemoveButton.CausesValidation = false;



                    Label myLbl = new Label();

                    myLbl.Text = "   ";

                    tcell1.Controls.Add(myLbl);

                    tcell1.Controls.Add(RemoveButton);

                }

                tcell1.ColumnSpan = 3;

                trow.Cells.Add(tcell1);

                mytbl.Rows.Add(trow);

            }



            trow = new TableRow();

            mytbl.Rows.Add(trow);



            int i = 0, j = 0;

            //for (int i = 0; i < 30; i++)

            foreach (DataRow row in dtInput.Rows)

            {

                if (ViewState["isMultiple"].ToString() != "Y")

                {

                    MyID = row["FieldName"].ToString();

                    MyIDData = MyID;

                }

                else

                {

                    MyID = row["FieldName"].ToString() + LineNo.ToString();



                    if (ViewState["RemoveLine"].ToString() != "")

                    {

                        if (LineNo >= Int32.Parse(ViewState["RemoveLine"].ToString()))

                        {

                            MyIDData = row["FieldName"].ToString() + (Int32.Parse(ViewState["RemoveLine"].ToString()) + 1).ToString();

                        }

                        else

                        {

                            MyIDData = row["FieldName"].ToString() + LineNo.ToString();

                        }

                    }

                    else

                    {

                        MyIDData = row["FieldName"].ToString() + LineNo.ToString();

                    }

                }



                if (row["isShow"].ToString() == "Y")

                {



                    Label myLabel = new Label();

                    myLabel.Text = row["FieldTitle"].ToString();

                    myLabel.Font.Bold = true;

                    myLabel.Font.Size = 10;

                    myLabel.ForeColor = Color.DarkGoldenrod;



                    trow = new TableRow();

                    tcell1 = new TableCell();

                    tcell2 = new TableCell();

                    tcell3 = new TableCell();



                    tcell1.Controls.Add(myLabel);

                    if (row["isMandatory"].ToString() == "Y")

                    {

                        Label myLabelSeparator = new Label();

                        myLabelSeparator.Text = "*";

                        myLabelSeparator.Font.Bold = true;

                        myLabelSeparator.Font.Size = 10;

                        myLabelSeparator.ForeColor = Color.Red;

                        tcell1.Controls.Add(myLabelSeparator);

                    }





                    trow.Cells.Add(tcell1);

                    mytbl.Rows.Add(trow);



                    if (row["FieldDesc"] != null || row["FieldDesc"].ToString() != "")

                    {

                        Label myLabelDesc = new Label();

                        myLabelDesc.Text = row["FieldDesc"].ToString();

                        myLabelDesc.Font.Size = 9;

                        myLabelDesc.ForeColor = Color.Silver;



                        trow = new TableRow();

                        tcell3 = new TableCell();

                        tcell3.Controls.Add(myLabelDesc);

                        trow.Cells.Add(tcell3);

                        mytbl.Rows.Add(trow);

                    }





                    trow = new TableRow();



                    if (row["InputType"].ToString().ToUpper() == "UPLOADDOC")

                    {

                        FileUpload myFU = new FileUpload();

                        //myFU.ID = row["FieldName"].ToString();

                        myFU.ID = MyID;

                        //myTextbox.Attributes.Add("width",1000);







                        //if (!string.IsNullOrEmpty(Request.Form[row["FieldName"].ToString()]))

                        //{

                        //    myFU. = Request.Form[row["FieldName"].ToString()];

                        //}



                        tcell2.Controls.Add(myFU); //, Text = "hello :)"



                    }



                    if (row["InputType"].ToString().ToUpper() == "RADIOBUTTON")

                    {

                        string RblValue = "";

                        RadioButtonList myRBL = new RadioButtonList();

                        //myRBL.ID = row["FieldName"].ToString();

                        myRBL.ID = MyID;

                        //myTextbox.Attributes.Add("width",1000);

                        DataTable dtRBL = this.GetDDLData(row["DDLScript"].ToString(), 1);

                        myRBL.DataSource = dtRBL;

                        myRBL.DataTextField = "DDLText";

                        myRBL.DataValueField = "DDLValue";

                        myRBL.DataBind();





                        if (!string.IsNullOrEmpty(Request.Form[MyIDData]))

                        {

                            RblValue = Request.Form[MyIDData];

                            myRBL.SelectedValue = RblValue.ToString();

                        }

                        if (!this.IsPostBack && dtData.Rows.Count != 0)

                        {

                            if (dtData.Rows[DataIdx][row["TargetField"].ToString()] == null || dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString() != "")

                            {

                                RblValue = dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString();

                                myRBL.SelectedValue = RblValue.ToString();

                            }



                        }

                        tcell2.Controls.Add(myRBL); //, Text = "hello :)"



                    }



                    if (row["InputType"].ToString().ToUpper() == "TEXTBOX" || row["InputType"].ToString().ToUpper() == "PASSWORD")

                    {

                        TextBox myTextbox = new TextBox();

                        myTextbox.ID = row["FieldName"].ToString();

                        //myTextbox.Attributes.Add("width",1000);

                        if (row["InputType"].ToString().ToUpper() == "PASSWORD")

                        {

                            myTextbox.TextMode = TextBoxMode.Password;

                        }

                        if (!string.IsNullOrEmpty(row["Width"].ToString()))

                        {

                            myTextbox.Width = Int32.Parse(row["Width"].ToString());

                        }

                        if (!string.IsNullOrEmpty(row["Line"].ToString()))

                        {

                            myTextbox.TextMode = TextBoxMode.MultiLine;

                            myTextbox.Rows = Int32.Parse(row["Line"].ToString());

                        }

                        if (!string.IsNullOrEmpty(Request.Form[MyIDData]))

                        {

                            myTextbox.Text = Request.Form[MyIDData];

                            //myLabel.Text = Request.Form[row["FieldName"].ToString()];

                        }

                        if (!this.IsPostBack && dtData.Rows.Count != 0)

                        {

                            if (dtData.Rows[DataIdx][row["TargetField"].ToString()] == null || dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString() != "")

                            {

                                myTextbox.Text = dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString();

                            }

                        }

                        tcell2.Controls.Add(myTextbox); //, Text = "hello :)"

                    }

                    if (row["InputType"].ToString().ToUpper() == "DATE3")

                    {

                        Label myChar = new Label();

                        myChar.Text = " / ";

                        Label myChar2 = new Label();

                        myChar2.Text = " / ";

                        TextBox myDay = new TextBox();

                        myDay.ID = MyID + "Day";

                        myDay.Width = 20;



                        TextBox myMonth = new TextBox();

                        myMonth.ID = MyID + "Month";

                        myMonth.Width = 20;



                        TextBox myYear = new TextBox();

                        myYear.ID = MyID + "Year";

                        myYear.Width = 40;



                        if (!string.IsNullOrEmpty(Request.Form[MyIDData + "Day"]))

                        {

                            myDay.Text = Request.Form[MyIDData + "Day"];

                        }

                        if (!string.IsNullOrEmpty(Request.Form[MyIDData + "Month"]))

                        {

                            myMonth.Text = Request.Form[MyIDData + "Month"];

                        }

                        if (!string.IsNullOrEmpty(Request.Form[MyIDData + "Year"]))

                        {

                            myYear.Text = Request.Form[MyIDData + "Year"];

                        }

                        if (!this.IsPostBack && dtData.Rows.Count != 0)

                        {

                            if (dtData.Rows[DataIdx][row["TargetField"].ToString()] == null || dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString() != "")

                            {

                                myDay.Text = DateTime.Parse(dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString()).ToString("dd");

                                myMonth.Text = DateTime.Parse(dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString()).ToString("MM");

                                myYear.Text = DateTime.Parse(dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString()).ToString("yyyy");

                            }

                        }

                        tcell2.Controls.Add(myDay);

                        tcell2.Controls.Add(myChar);

                        tcell2.Controls.Add(myMonth);

                        tcell2.Controls.Add(myChar2);

                        tcell2.Controls.Add(myYear);



                        RangeValidator myRVDay = new RangeValidator();

                        myRVDay.ID = "myCV" + MyID.ToString() + "Day";

                        myRVDay.ControlToValidate = MyID.ToString() + "Day";

                        myRVDay.Type = ValidationDataType.Integer;

                        myRVDay.MinimumValue = "1";

                        myRVDay.MaximumValue = "31";

                        //myRFV.ErrorMessage = "  " + row["FieldTitle"].ToString() + " Must Filled";

                        myRVDay.ErrorMessage = "  Day value must between 1 to 31 !!";

                        myRVDay.ForeColor = Color.Red;

                        myRVDay.Display = ValidatorDisplay.Dynamic;

                        tcell2.Controls.Add(myRVDay);



                        RangeValidator myRVMonth = new RangeValidator();

                        myRVMonth.ID = "myCV" + MyID.ToString() + "Month";

                        myRVMonth.ControlToValidate = MyID.ToString() + "Month";

                        myRVMonth.Type = ValidationDataType.Integer;

                        myRVMonth.Display = ValidatorDisplay.Dynamic;

                        myRVMonth.MinimumValue = "1";

                        myRVMonth.MaximumValue = "12";

                        myRVMonth.ErrorMessage = "  Month value must between 1 to 12 !!";

                        myRVMonth.ForeColor = Color.Red;

                        tcell2.Controls.Add(myRVMonth);



                        RangeValidator myRVYear = new RangeValidator();

                        myRVYear.ID = "myCV" + MyID.ToString() + "Year";

                        myRVYear.ControlToValidate = MyID.ToString() + "Year";

                        myRVYear.Type = ValidationDataType.Integer;

                        myRVYear.MinimumValue = "1950";

                        myRVYear.MaximumValue = "2020";

                        //myRFV.ErrorMessage = "  " + row["FieldTitle"].ToString() + " Must Filled";

                        myRVYear.Display = ValidatorDisplay.Dynamic;

                        myRVYear.ErrorMessage = "  Year value must between 1950 to 2020 !!";

                        myRVYear.ForeColor = Color.Red;

                        tcell2.Controls.Add(myRVYear);



                    }

                    if (row["InputType"].ToString().ToUpper() == "DATE32")

                    {

                        Label myLabelFrom = new Label();

                        myLabelFrom.Text = "From : ";

                        tcell2.Controls.Add(myLabelFrom);



                        string FieldNameFrom = MyID;

                        string FieldNameTo = MyID.Replace("_From", "_To");



                        string FieldNameFromData = MyIDData;

                        string FieldNameToData = MyIDData.Replace("_From", "_To");



                        Label myChar = new Label();

                        myChar.Text = " / ";

                        Label myChar2 = new Label();

                        myChar2.Text = " / ";

                        TextBox myDay = new TextBox();

                        myDay.ID = FieldNameFrom + "Day";

                        myDay.Width = 20;



                        TextBox myMonth = new TextBox();

                        myMonth.ID = FieldNameFrom + "Month";

                        myMonth.Width = 20;



                        TextBox myYear = new TextBox();

                        myYear.ID = FieldNameFrom + "Year";

                        myYear.Width = 40;



                        if (!string.IsNullOrEmpty(Request.Form[FieldNameFromData + "Day"]))

                        {

                            myDay.Text = Request.Form[FieldNameFromData + "Day"];

                        }

                        if (!string.IsNullOrEmpty(Request.Form[FieldNameFromData + "Month"]))

                        {

                            myMonth.Text = Request.Form[FieldNameFromData + "Month"];

                        }

                        if (!string.IsNullOrEmpty(Request.Form[FieldNameFromData + "Year"]))

                        {

                            myYear.Text = Request.Form[FieldNameFromData + "Year"];

                        }

                        if (!this.IsPostBack && dtData.Rows.Count != 0)

                        {

                            if (dtData.Rows[DataIdx][row["TargetField"].ToString()] == null || dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString() != "")

                            {

                                myDay.Text = DateTime.Parse(dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString()).ToString("dd");

                                myMonth.Text = DateTime.Parse(dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString()).ToString("MM");

                                myYear.Text = DateTime.Parse(dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString()).ToString("yyyy");

                            }

                        }

                        tcell2.Controls.Add(myDay);

                        tcell2.Controls.Add(myChar);

                        tcell2.Controls.Add(myMonth);

                        tcell2.Controls.Add(myChar2);

                        tcell2.Controls.Add(myYear);



                        RangeValidator myRVDay = new RangeValidator();

                        myRVDay.ID = "myCV" + FieldNameFrom.ToString() + "Day";

                        myRVDay.ControlToValidate = FieldNameFrom.ToString() + "Day";

                        myRVDay.Type = ValidationDataType.Integer;

                        myRVDay.MinimumValue = "1";

                        myRVDay.MaximumValue = "31";

                        //myRFV.ErrorMessage = "  " + row["FieldTitle"].ToString() + " Must Filled";

                        myRVDay.ErrorMessage = "  Day value must between 1 to 31 !!";

                        myRVDay.ForeColor = Color.Red;

                        myRVDay.Display = ValidatorDisplay.Dynamic;

                        tcell2.Controls.Add(myRVDay);



                        RangeValidator myRVMonth = new RangeValidator();

                        myRVMonth.ID = "myCV" + FieldNameFrom.ToString() + "Month";

                        myRVMonth.ControlToValidate = FieldNameFrom.ToString() + "Month";

                        myRVMonth.Type = ValidationDataType.Integer;

                        myRVMonth.Display = ValidatorDisplay.Dynamic;

                        myRVMonth.MinimumValue = "1";

                        myRVMonth.MaximumValue = "12";

                        myRVMonth.ErrorMessage = "  Month value must between 1 to 12 !!";

                        myRVMonth.ForeColor = Color.Red;

                        tcell2.Controls.Add(myRVMonth);



                        RangeValidator myRVYear = new RangeValidator();

                        myRVYear.ID = "myCV" + FieldNameFrom.ToString() + "Year";

                        myRVYear.ControlToValidate = FieldNameFrom.ToString() + "Year";

                        myRVYear.Type = ValidationDataType.Integer;

                        myRVYear.MinimumValue = "1950";

                        myRVYear.MaximumValue = "2020";

                        //myRFV.ErrorMessage = "  " + row["FieldTitle"].ToString() + " Must Filled";

                        myRVYear.Display = ValidatorDisplay.Dynamic;

                        myRVYear.ErrorMessage = "  Year value must between 1950 to 2020 !!";

                        myRVYear.ForeColor = Color.Red;

                        tcell2.Controls.Add(myRVYear);



                        Label myLabelTo = new Label();

                        myLabelTo.Text = " To : ";

                        tcell2.Controls.Add(myLabelTo);



                        myChar = new Label();

                        myChar.Text = " / ";

                        myChar2 = new Label();

                        myChar2.Text = " / ";

                        myDay = new TextBox();

                        myDay.ID = FieldNameTo + "Day";

                        myDay.Width = 20;



                        myMonth = new TextBox();

                        myMonth.ID = FieldNameTo + "Month";

                        myMonth.Width = 20;



                        myYear = new TextBox();

                        myYear.ID = FieldNameTo + "Year";

                        myYear.Width = 40;



                        if (!string.IsNullOrEmpty(Request.Form[FieldNameToData + "Day"]))

                        {

                            myDay.Text = Request.Form[FieldNameToData + "Day"];

                        }

                        if (!string.IsNullOrEmpty(Request.Form[FieldNameToData + "Month"]))

                        {

                            myMonth.Text = Request.Form[FieldNameToData + "Month"];

                        }

                        if (!string.IsNullOrEmpty(Request.Form[FieldNameToData + "Year"]))

                        {

                            myYear.Text = Request.Form[FieldNameToData + "Year"];

                        }

                        if (!this.IsPostBack && dtData.Rows.Count != 0)

                        {

                            if (dtData.Rows[DataIdx][row["TargetField"].ToString()] == null || dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString() != "")

                            {

                                myDay.Text = DateTime.Parse(dtData.Rows[DataIdx][row["TargetField"].ToString().Replace("_From", "_To")].ToString()).ToString("dd");

                                myMonth.Text = DateTime.Parse(dtData.Rows[DataIdx][row["TargetField"].ToString().Replace("_From", "_To")].ToString()).ToString("MM");

                                myYear.Text = DateTime.Parse(dtData.Rows[DataIdx][row["TargetField"].ToString().Replace("_From", "_To")].ToString()).ToString("yyyy");

                            }

                        }

                        tcell2.Controls.Add(myDay);

                        tcell2.Controls.Add(myChar);

                        tcell2.Controls.Add(myMonth);

                        tcell2.Controls.Add(myChar2);

                        tcell2.Controls.Add(myYear);



                        myRVDay = new RangeValidator();

                        myRVDay.ID = "myCV" + FieldNameTo.ToString() + "Day";

                        myRVDay.ControlToValidate = FieldNameTo.ToString() + "Day";

                        myRVDay.Type = ValidationDataType.Integer;

                        myRVDay.MinimumValue = "1";

                        myRVDay.MaximumValue = "31";

                        //myRFV.ErrorMessage = "  " + row["FieldTitle"].ToString() + " Must Filled";

                        myRVDay.ErrorMessage = "  Day value must between 1 to 31 !!";

                        myRVDay.ForeColor = Color.Red;

                        myRVDay.Display = ValidatorDisplay.Dynamic;

                        tcell2.Controls.Add(myRVDay);



                        myRVMonth = new RangeValidator();

                        myRVMonth.ID = "myCV" + FieldNameTo.ToString() + "Month";

                        myRVMonth.ControlToValidate = FieldNameTo.ToString() + "Month";

                        myRVMonth.Type = ValidationDataType.Integer;

                        myRVMonth.Display = ValidatorDisplay.Dynamic;

                        myRVMonth.MinimumValue = "1";

                        myRVMonth.MaximumValue = "12";

                        myRVMonth.ErrorMessage = "  Month value must between 1 to 12 !!";

                        myRVMonth.ForeColor = Color.Red;

                        tcell2.Controls.Add(myRVMonth);



                        myRVYear = new RangeValidator();

                        myRVYear.ID = "myCV" + FieldNameTo.ToString() + "Year";

                        myRVYear.ControlToValidate = FieldNameTo.ToString() + "Year";

                        myRVYear.Type = ValidationDataType.Integer;

                        myRVYear.MinimumValue = "1950";

                        myRVYear.MaximumValue = "2020";

                        //myRFV.ErrorMessage = "  " + row["FieldTitle"].ToString() + " Must Filled";

                        myRVYear.Display = ValidatorDisplay.Dynamic;

                        myRVYear.ErrorMessage = "  Year value must between 1950 to 2020 !!";

                        myRVYear.ForeColor = Color.Red;

                        tcell2.Controls.Add(myRVYear);

                    }

                    if (row["InputType"].ToString().ToUpper() == "AJAXDATEPICKER")

                    {

                        TextBox myDatepicker = new TextBox();

                        myDatepicker.ID = MyID;

                        myDatepicker.ReadOnly = true;

                        myDatepicker.Width = 80;



                        if (!string.IsNullOrEmpty(Request.Form[MyIDData]))

                        {

                            myDatepicker.Text = Request.Form[MyIDData];

                        }

                        if (!this.IsPostBack && dtData.Rows.Count != 0)

                        {

                            if (dtData.Rows[DataIdx][row["TargetField"].ToString()] == null || dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString() != "")

                            {

                                myDatepicker.Text = DateTime.Parse(dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString()).ToString("dd/MM/yyyy");

                            }

                        }

                        tcell2.Controls.Add(myDatepicker);



                        ImageButton myImgBtn = new ImageButton();

                        myImgBtn.ImageAlign = ImageAlign.Bottom;

                        myImgBtn.ImageUrl = "Images/calendar.png";

                        myImgBtn.ID = MyID + "PopUp";

                        tcell2.Controls.Add(myImgBtn);



                        AjaxControlToolkit.CalendarExtender myCE = new AjaxControlToolkit.CalendarExtender();

                        myCE.ID = MyID + "CE";

                        myCE.PopupButtonID = MyID + "PopUp";

                        myCE.TargetControlID = MyID;

                        myCE.Format = "dd/MM/yyyy";

                        tcell2.Controls.Add(myCE);

                    }

                    if (row["InputType"].ToString().ToUpper() == "AJAXDATEPICKER2")

                    {

                        Label myLabelFrom = new Label();

                        myLabelFrom.Text = "From : ";

                        tcell2.Controls.Add(myLabelFrom);



                        string FieldNameFrom = MyID;

                        string FieldNameTo = MyID.Replace("_From", "_To");



                        string FieldNameFromData = MyIDData;

                        string FieldNameToData = MyIDData.Replace("_From", "_To");



                        TextBox myDatepicker = new TextBox();

                        myDatepicker.ID = MyID;

                        myDatepicker.ReadOnly = true;

                        myDatepicker.Width = 80;



                        if (!string.IsNullOrEmpty(Request.Form[FieldNameFromData]))

                        {

                            myDatepicker.Text = Request.Form[FieldNameFromData];

                        }

                        if (!this.IsPostBack && dtData.Rows.Count != 0)

                        {

                            if (dtData.Rows[DataIdx][row["TargetField"].ToString()] == null || dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString() != "")

                            {

                                myDatepicker.Text = DateTime.Parse(dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString()).ToString("dd/MM/yyyy");

                            }

                        }

                        tcell2.Controls.Add(myDatepicker);



                        ImageButton myImgBtn = new ImageButton();

                        myImgBtn.ImageAlign = ImageAlign.Bottom;

                        myImgBtn.ImageUrl = "Images/calendar.png";

                        myImgBtn.ID = FieldNameFrom + "PopUp";

                        tcell2.Controls.Add(myImgBtn);



                        AjaxControlToolkit.CalendarExtender myCE = new AjaxControlToolkit.CalendarExtender();

                        myCE.ID = FieldNameFrom + "CE";

                        myCE.PopupButtonID = FieldNameFrom + "PopUp";

                        myCE.TargetControlID = FieldNameFrom;

                        myCE.Format = "dd/MM/yyyy";

                        tcell2.Controls.Add(myCE);





                        Label myLabelTo = new Label();

                        myLabelTo.Text = " To : ";

                        tcell2.Controls.Add(myLabelTo);



                        myDatepicker = new TextBox();

                        myDatepicker.ID = FieldNameTo;

                        myDatepicker.ReadOnly = true;

                        myDatepicker.Width = 80;



                        if (!string.IsNullOrEmpty(Request.Form[FieldNameToData]))

                        {

                            myDatepicker.Text = Request.Form[FieldNameToData];

                        }

                        if (!this.IsPostBack && dtData.Rows.Count != 0)

                        {

                            if (dtData.Rows[DataIdx][row["TargetField"].ToString().Replace("_From", "_To")] == null || dtData.Rows[DataIdx][row["TargetField"].ToString().Replace("_From", "_To")].ToString() != "")

                            {

                                myDatepicker.Text = DateTime.Parse(dtData.Rows[DataIdx][row["TargetField"].ToString().Replace("_From", "_To")].ToString()).ToString("dd/MM/yyyy");

                            }

                        }

                        tcell2.Controls.Add(myDatepicker);



                        myImgBtn = new ImageButton();

                        myImgBtn.ImageAlign = ImageAlign.Bottom;

                        myImgBtn.ImageUrl = "Images/calendar.png";

                        myImgBtn.ID = FieldNameTo + "PopUp";

                        tcell2.Controls.Add(myImgBtn);



                        myCE = new AjaxControlToolkit.CalendarExtender();

                        myCE.ID = FieldNameTo + "CE";

                        myCE.PopupButtonID = FieldNameTo + "PopUp";

                        myCE.TargetControlID = FieldNameTo;

                        myCE.Format = "dd/MM/yyyy";

                        tcell2.Controls.Add(myCE);



                    }

                    if (row["InputType"].ToString().ToUpper() == "DATEPICKER")

                    {

                        TextBox myDatepicker = new TextBox();

                        myDatepicker.ID = MyID;

                        myDatepicker.ReadOnly = true;

                        myDatepicker.Width = 80;



                        if (!string.IsNullOrEmpty(Request.Form[MyIDData]))

                        {

                            myDatepicker.Text = Request.Form[MyIDData];

                        }

                        if (!this.IsPostBack && dtData.Rows.Count != 0)

                        {

                            if (dtData.Rows[DataIdx][row["TargetField"].ToString()] == null || dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString() != "")

                            {

                                myDatepicker.Text = DateTime.Parse(dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString()).ToString("dd/MM/yyyy");

                            }

                        }

                        tcell2.Controls.Add(myDatepicker);



                        strScript += "$(\"[id$=" + MyID + "]\").datepicker({";

                        strScript += "  showOn: 'button',";

                        strScript += "  buttonImageOnly: false,";

                        strScript += "  buttonText: '...'";

                        strScript += "  });; ";



                    }

                    if (row["InputType"].ToString().ToUpper() == "DATEPICKER2")

                    {

                        Label myLabelFrom = new Label();

                        myLabelFrom.Text = "From : ";

                        tcell2.Controls.Add(myLabelFrom);



                        string FieldNameFrom = MyID;

                        string FieldNameTo = MyID.Replace("_From", "_To");



                        string FieldNameFromData = MyIDData;

                        string FieldNameToData = MyIDData.Replace("_From", "_To");



                        TextBox myDatepicker = new TextBox();

                        myDatepicker.ID = FieldNameFrom;

                        myDatepicker.ReadOnly = true;

                        myDatepicker.Width = 80;



                        if (!string.IsNullOrEmpty(Request.Form[FieldNameFromData]))

                        {

                            myDatepicker.Text = Request.Form[FieldNameFromData];

                        }

                        if (!this.IsPostBack && dtData.Rows.Count != 0)

                        {

                            if (dtData.Rows[DataIdx][row["TargetField"].ToString()] == null || dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString() != "")

                            {

                                myDatepicker.Text = DateTime.Parse(dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString()).ToString("dd/MM/yyyy");

                            }

                        }

                        tcell2.Controls.Add(myDatepicker);



                        strScript += "$(\"[id$=" + FieldNameFrom + "]\").datepicker({";

                        strScript += "  showOn: 'button',";

                        strScript += "  buttonImageOnly: false,";

                        strScript += "  buttonText: '...'";

                        strScript += "  });; ";



                        Label myLabelTo = new Label();

                        myLabelTo.Text = " To : ";

                        tcell2.Controls.Add(myLabelTo);



                        TextBox myDatepicker1 = new TextBox();

                        myDatepicker1.ID = FieldNameTo;

                        myDatepicker1.ReadOnly = true;

                        myDatepicker1.Width = 80;



                        if (!string.IsNullOrEmpty(Request.Form[FieldNameToData]))

                        {

                            myDatepicker1.Text = Request.Form[FieldNameToData];

                        }

                        if (!this.IsPostBack && dtData.Rows.Count != 0)

                        {

                            if (dtData.Rows[DataIdx][row["TargetField"].ToString().Replace("_From", "_To")] == null || dtData.Rows[DataIdx][row["TargetField"].ToString().Replace("_From", "_To")].ToString() != "")

                            {

                                myDatepicker.Text = DateTime.Parse(dtData.Rows[DataIdx][row["TargetField"].ToString().Replace("_From", "_To")].ToString()).ToString("dd/MM/yyyy");

                            }

                        }

                        tcell2.Controls.Add(myDatepicker1);



                        strScript += "$(\"[id$=" + FieldNameTo + "]\").datepicker({";

                        strScript += "  showOn: 'button',";

                        strScript += "  buttonImageOnly: false,";

                        strScript += "  buttonText: '...'";

                        strScript += "  });; ";



                    }

                    if (row["InputType"].ToString().ToUpper() == "DROPDOWNLIST")

                    {

                        string query = row["DDLScript"].ToString();

                        String Value = "";

                        string controlName = "";

                        if (row["DDLDepend"].ToString() != "")

                        {



                            if (ViewState["isMultiple"].ToString() == "Y")

                            {

                                Value = ViewState[row["DDLDepend"].ToString() + LineNo.ToString() + "_Value"].ToString();

                                query = query.Replace("@Value", Value);

                            }

                            else

                            {

                                Value = ViewState[row["DDLDepend"].ToString() + "_Value"].ToString();

                                query = query.Replace("@Value", Value);

                            }

                        }

                        DataTable dtDDL = this.GetDDLData(query, 0);

                        DropDownList myDDL = new DropDownList();

                        myDDL.ID = MyID;

                        myDDL.DataSource = dtDDL;

                        myDDL.DataTextField = "DDLText";

                        myDDL.DataValueField = "DDLValue";



                        myDDL.DataBind();

                        if (!string.IsNullOrEmpty(Request.Form[MyIDData]))

                        {

                            myDDL.SelectedValue = Request.Form[MyIDData];

                        }

                        if (!this.IsPostBack && dtData.Rows.Count != 0)

                        {

                            if (dtData.Rows[DataIdx][row["TargetField"].ToString()] == null || dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString() != "")

                            {

                                myDDL.SelectedValue = dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString();

                            }

                        }

                        if (row["DDLPostback"].ToString() == "Y")

                        {

                            myDDL.AutoPostBack = true;

                            if (ViewState["isMultiple"].ToString() == "Y")

                            {

                                ViewState[row["FieldName"].ToString() + LineNo.ToString() + "_Value"] = myDDL.SelectedValue;

                            }

                            else

                            {

                                ViewState[row["FieldName"].ToString() + "_Value"] = myDDL.SelectedValue;

                            }

                        }



                        tcell2.Controls.Add(myDDL); //, Text = "hello :)"

                    }

                    if (row["InputType"].ToString().ToUpper() == "DROPDOWNLIST_OTH")

                    {

                        DataTable dtDDL = this.GetDDLData(row["DDLScript"].ToString(), 0);

                        DropDownList myDDL = new DropDownList();

                        myDDL.ID = MyID;

                        myDDL.DataSource = dtDDL;

                        myDDL.DataTextField = "DDLText";

                        myDDL.DataValueField = "DDLValue";

                        myDDL.DataBind();



                        myDDL.Items.Insert(myDDL.Items.Count, new ListItem("Other", "Other"));

                        if (!string.IsNullOrEmpty(Request.Form[MyIDData]))

                        {

                            myDDL.SelectedValue = Request.Form[MyIDData];

                        }

                        if (!this.IsPostBack && dtData.Rows.Count != 0)

                        {

                            if (dtData.Rows[DataIdx][row["TargetField"].ToString()] == null || dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString() != "")

                            {

                                myDDL.SelectedValue = dtData.Rows[DataIdx][row["TargetField"].ToString()].ToString();

                            }

                        }

                        tcell2.Controls.Add(myDDL); //, Text = "hello :)"



                        TextBox myDDLOth = new TextBox();

                        myDDLOth.ID = MyID + "Desc";

                        myDDLOth.Width = Int32.Parse(row["Width"].ToString());



                        if (!string.IsNullOrEmpty(Request.Form[MyIDData + "Desc"]))

                        {

                            myDDLOth.Text = Request.Form[MyIDData + "Desc"];

                        }

                        if (!this.IsPostBack && dtData.Rows.Count != 0)

                        {

                            if (dtData.Rows[DataIdx][row["TargetField"].ToString() + "Desc"] == null || dtData.Rows[DataIdx][row["TargetField"].ToString() + "Desc"].ToString() != "")

                            {

                                myDDLOth.Text = dtData.Rows[DataIdx][row["TargetField"].ToString() + "Desc"].ToString();

                            }

                        }

                        tcell2.Controls.Add(myDDLOth);

                    }

                    //tcell3 = new TableCell();

                    //tcell3.Controls.Add(new DropDownList());



                    //trow.Cells.Add(tcell1);

                    //trow.Cells.Add(tcell3);

                    if (row["isMandatory"].ToString().ToUpper() == "Y")

                    {

                        if (row["InputType"].ToString().ToUpper() == "DATE3")

                        {

                            RequiredFieldValidator myRFV = new RequiredFieldValidator();

                            myRFV.ID = "RFV" + MyID.ToString() + "Day";

                            myRFV.ControlToValidate = MyID.ToString() + "Day";

                            myRFV.Display = ValidatorDisplay.Dynamic;

                            myRFV.ErrorMessage = " Day Required Information";

                            myRFV.ForeColor = Color.Red;

                            tcell2.Controls.Add(myRFV);



                            myRFV.ID = "RFV" + MyID.ToString() + "Month";

                            myRFV.ControlToValidate = MyID.ToString() + "Month";

                            myRFV.ErrorMessage = " Month Required Information";

                            tcell2.Controls.Add(myRFV);



                            myRFV.ID = "RFV" + MyID.ToString() + "Year";

                            myRFV.ControlToValidate = MyID.ToString() + "Year";

                            myRFV.ErrorMessage = " Year Required Information";

                            tcell2.Controls.Add(myRFV);



                        }

                        else

                        {

                            RequiredFieldValidator myRFV = new RequiredFieldValidator();

                            myRFV.ID = "RFV" + MyID.ToString();

                            myRFV.ControlToValidate = MyID.ToString();

                            myRFV.Display = ValidatorDisplay.Dynamic;

                            //myRFV.ErrorMessage = "  " + row["FieldTitle"].ToString() + " Must Filled";

                            myRFV.ErrorMessage = "  Required Information";

                            myRFV.ForeColor = Color.Red;

                            tcell2.Controls.Add(myRFV);

                        }



                    }

                    if (!string.IsNullOrEmpty(row["CompareTo"].ToString()))

                    {

                        CompareValidator myCV = new CompareValidator();

                        myCV.ID = "myCV" + MyID.ToString();

                        myCV.ControlToValidate = MyID.ToString();

                        myCV.ControlToCompare = row["CompareTo"].ToString();

                        myCV.Display = ValidatorDisplay.Dynamic;

                        //myRFV.ErrorMessage = "  " + row["FieldTitle"].ToString() + " Must Filled";

                        myCV.ErrorMessage = "  " + row["CompareTo"].ToString() + " Not Equal!!";

                        myCV.ForeColor = Color.Red;

                        tcell2.Controls.Add(myCV);

                    }

                    if (!string.IsNullOrEmpty(row["RegexValidation"].ToString()))

                    {

                        RegularExpressionValidator myREV = new RegularExpressionValidator();

                        myREV.ID = "myREV" + MyID.ToString();

                        myREV.ControlToValidate = MyID.ToString();

                        //myREV.ValidationExpression = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";

                        myREV.ValidationExpression = row["RegexValidation"].ToString();

                        myREV.ErrorMessage = row["RegexValidationMsg"].ToString();

                        myREV.Display = ValidatorDisplay.Dynamic;

                        myREV.ForeColor = Color.Red;

                        tcell2.Controls.Add(myREV);

                    }

                    if (row["FieldName"].ToString().IndexOf("Email") >= 0)

                    {

                        RegularExpressionValidator myREV = new RegularExpressionValidator();

                        myREV.ID = "myREV" + MyID.ToString();

                        myREV.ControlToValidate = MyID.ToString();

                        myREV.ValidationExpression = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";

                        myREV.ErrorMessage = "Invalid Email Format";

                        myREV.Display = ValidatorDisplay.Dynamic;

                        myREV.ForeColor = Color.Red;

                        tcell2.Controls.Add(myREV);

                    }

                    trow.Cells.Add(tcell2);



                    mytbl.Rows.Add(trow);

                    if (strScript != "")

                    {

                        strScript = "   $(function () {" +

                            strScript +

                           "   });";

                        ClientScript.RegisterClientScriptBlock(this.GetType(), "DTPickerScript", strScript, true);

                    }

                    //myLabel.Text = strSqlScript;

                }

                i = i + 1;

            }

        }

        private void PopulateInput(int ModuleID, int ActionID, int LineNo)

        {

            TableRow trow;

            TableCell tcell1, tcell2;

            trow = new TableRow();

            mytbl.Rows.Add(trow);

            mytbl.Rows.Add(trow);





            PopulateInputList(ModuleID, ActionID, LineNo);

            if (ViewState["isMultiple"].ToString() == "Y")

            {

                LinkButton myLB = new LinkButton();

                myLB.ID = "Add";

                myLB.Text = "Add";

                myLB.OnClientClick = "Add_Click";



                Button AddButton = new Button();

                AddButton.ID = "AddButton";

                AddButton.Text = "Add";

                AddButton.Click += new EventHandler(Add_Click);

                AddButton.CausesValidation = false;



                trow = new TableRow();

                tcell1 = new TableCell();

                tcell1.Controls.Add(AddButton);

                tcell1.ColumnSpan = 2;

                trow.Cells.Add(tcell1);

                mytbl1.Rows.Add(trow);

            }



            trow = new TableRow();

            tcell1 = new TableCell();

            tcell2 = new TableCell();



            //if (ActionID > 1)

            //{

            Button BackButton = new Button();

            BackButton.ID = "BackButton";

            BackButton.Text = "Back";

            BackButton.Click += new EventHandler(Back_Click);



            tcell2.Controls.Add(BackButton);

            trow.Cells.Add(tcell2);

            //}

            Button SaveButton = new Button();

            SaveButton.ID = "SaveButton";

            SaveButton.Text = "Save";

            SaveButton.Click += new EventHandler(Save_Click);



            tcell1.Controls.Add(SaveButton);

            trow.Cells.Add(tcell1);



            mytbl1.Rows.Add(trow);

        }

        protected void Acknowledge_Click(object sender, EventArgs e)

        {

            if (Session["JobID"] != null)

            {

                String StrApply = "Exec sp_JobApply '" + Session["UserID"] + "'," + Session["JobID"] + " ";

                SqlConnection con = new SqlConnection(

                    WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString

                );

                SqlCommand cmd1 = new SqlCommand(StrApply, con);

                con.Open();

                cmd1.ExecuteNonQuery();



                con.Close();

                SweetMsgBox("You have completed your aplication. Thank You for your interest in Plaza Indonesia Realty, Tbk!");

            }

            else

            {

                /*String StrApply = "Exec sp_Mail 2,'" + Session["UserID"] + "'";

                SqlConnection con = new SqlConnection(

                   WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString

                );

                SqlCommand cmd1 = new SqlCommand(StrApply, con);

                con.Open();

                cmd1.ExecuteNonQuery();

 

                con.Close();*/

                SweetMsgBox("Thank You for your interest in Plaza Indonesia Realty, Tbk!");

            }

            //Response.Redirect("~/Default.aspx");

        }



        private void MsgBox(string StrMsg)

        {

            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + StrMsg + "');", true);

        }

        private void SweetMsgBox(string StrMsg)

        {

            string url = "";

            //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "swal('" + StrMsg + "');$('.confirm').click(function () { window.location = 'http://localhost:56192/MenuList.aspx?ParentMenuID=4' });", true);

            if (Session["JobID"] == null)

            {

                url = "http://" + HttpContext.Current.Request.Url.Authority + "/Search.aspx?ModuleID=2";

            }

            else

            {

                url = "http://" + HttpContext.Current.Request.Url.Authority + "/Detail2.aspx?JobID=" + Session["JobID"].ToString();



            }

            //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "swal('" + StrMsg + "');$('.confirm').click(function () { window.location = '" + url + "' });", true);

            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "swal({ html:true, title:'', text:'" + StrMsg + "'});$('.confirm').click(function () { window.location = '" + url + "' });", true);

        }

        protected void Save_Click(object sender, EventArgs e)

        {

            if (Page.IsValid)

            {

                ViewState["Action"] = "Save";

                //Button button = sender as Button;

                // identify which button was clicked and perform necessary actions

                //myLabel1.Text = ViewState["Action"].ToString();



                SaveData();



                //int ModuleID = Int32.Parse(ViewState["ModuleID"].ToString());

                //int ActionID = Int32.Parse(ViewState["ActionID"].ToString());



                //Response.Redirect("~/Action.aspx?ModuleID=" + ModuleID.ToString() + "&ActionID=" + ActionID.ToString());

                PrevPage();

            }

        }

        protected void Add_Click(object sender, EventArgs e)

        {

            ViewState["LineNo"] = int.Parse(ViewState["LineNo"].ToString()) + 1;

            //myLabel1.Text = ViewState["LineNo"].ToString() + "hohohoho";



            PopulateInputList(Int32.Parse(ViewState["ModuleID"].ToString()), Int32.Parse(ViewState["ActionID"].ToString()), Int32.Parse(ViewState["LineNo"].ToString()));

            //SaveData();

        }

        protected void Remove_Click(object sender, EventArgs e)

        {

            Button btn = (Button)sender;

            int LineNo = Int32.Parse(btn.ID.ToString().Replace("RemoveButton", ""));



            if (int.Parse(ViewState["LineNo"].ToString()) > 1)

            {

                ViewState["LineNo"] = int.Parse(ViewState["LineNo"].ToString()) - 1;

            }

            ViewState["RemoveLine"] = LineNo.ToString();

            //myLabel1.Text = ViewState["LineNo"].ToString() + "hohohoho";



            mytbl.Rows.Clear();

            int i = 0;



            while (i < int.Parse(ViewState["LineNo"].ToString()))

            {

                i = i + 1;

                PopulateInputList(Int32.Parse(ViewState["ModuleID"].ToString()), Int32.Parse(ViewState["ActionID"].ToString()), i);

            }

            ViewState["RemoveLine"] = "";

            //PopulateInputList(Int32.Parse(ViewState["ModuleID"].ToString()), Int32.Parse(ViewState["ActionID"].ToString()), Int32.Parse(ViewState["LineNo"].ToString()));

            //SaveData();

        }

        private void SaveData()

        {

            int ModuleID = Int32.Parse(ViewState["ModuleID"].ToString());

            int ActionID = Int32.Parse(ViewState["ActionID"].ToString());

            int LineNo = Int32.Parse(ViewState["LineNo"].ToString());



            DataTable dtInput = this.GetInputData(ModuleID, ActionID, 1);

            //String strScript = "";

            String strSqlScript = "";

            string strSqlScriptHeader = "";

            string strSqlScriptUpdate = "";

            string MyID = "";





            SqlConnection con = new SqlConnection(

                WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString

            );



            //SqlCommand Comm1 = new SqlCommand("Select * From Test", con);

            //con.Open();

            //SqlDataReader DR1 = Comm1.ExecuteReader();

            /*if (DR1.Read())

            {

                //textbox.Text = DR1.GetValue(0).ToString();

            }*/



            string ID = ViewState["ID"].ToString();

            //string strDelete = "Delete from " + ViewState["MasterTable"] + " Where " + ViewState["MasterField"] + " = '" + ID + "'";

            string strDelete = ViewState["DeleteScript"].ToString().Replace("@UserID", Session["UserID"].ToString()).Replace("@ID", ID);



            if (ActionID == 7)

            {

                strDelete = strDelete + " and JobID = " + Session["JobID"].ToString();

            }

            SqlCommand cmd1;

            if (strDelete != "")

            {

                cmd1 = new SqlCommand(strDelete, con);

                con.Open();

                cmd1.ExecuteNonQuery();

                con.Close();

            }



            int i = 0;

            //for (int i = 0; i < 30; i++)

            while (i < LineNo)

            {

                strSqlScript = "";

                strSqlScriptHeader = "";

                strSqlScriptUpdate = "";

                i = i + 1;

                foreach (DataRow row in dtInput.Rows)

                {

                    ViewState["MasterTable"] = row["TargetTable"].ToString();

                    if (ViewState["isMultiple"].ToString() != "Y")

                    {

                        MyID = row["FieldName"].ToString();

                    }

                    else

                    {

                        MyID = row["FieldName"].ToString() + i.ToString();

                    }

                    if (row["InputType"].ToString().ToUpper() == "UPLOADDOC")

                    {

                        try

                        {

                            string FileName = Request.Files[MyID].FileName.ToString();

                            string FileExt = Path.GetExtension(FileName);

                            string AppPath = HttpContext.Current.Server.MapPath("~");

                            string NewFileName = MyID + FileExt;

                            FileName = Path.GetFileName(FileName);



                            AppPath += "Document\\" + ViewState["UploadFolder"].ToString();

                            myLabel1.Text = AppPath + "\\" + FileName;

                            if (!Directory.Exists(AppPath))

                            {

                                //myLabel1.Text = "hehehe";

                                Directory.CreateDirectory(AppPath);

                            }

                            //myLabel1.Text = HttpContext.Current.Server.MapPath("~") + " hehehe";

                            //myLabel1.Text = strInsert;



                            //string filename = Path.GetFileName(FileUploadControl.FileName);

                            Request.Files[MyID].SaveAs(AppPath + "\\" + NewFileName);

                            //myLabel1.Text = "Upload status: File uploaded!";

                        }

                        catch (Exception ex)

                        {

                            myLabel1.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;

                        }

                    }

                    else

                    {

                        strSqlScriptHeader = strSqlScriptHeader + row["TargetField"].ToString() + ",";

                        if (row["InputType"].ToString().ToUpper() == "DATEPICKER2" || row["InputType"].ToString().ToUpper() == "AJAXDATEPICKER2" || row["InputType"].ToString().ToUpper() == "DATE32")

                        {

                            strSqlScriptHeader = strSqlScriptHeader + row["TargetField"].ToString().Replace("_From", "_To") + ",";

                        }

                        if (row["InputType"].ToString().ToUpper() == "RADIOBUTTON" || row["InputType"].ToString().ToUpper() == "DROPDOWNLIST_OTH")

                        {

                            strSqlScriptHeader = strSqlScriptHeader + row["TargetField"].ToString() + "Desc,";

                        }



                        if (row["isShow"].ToString() == "Y")

                        {

                            if (strSqlScriptUpdate != "")

                            {

                                strSqlScriptUpdate = strSqlScriptUpdate + ",";

                            }

                            if (row["DataType"].ToString() == "1")

                            {

                                if (row["InputType"].ToString().ToUpper() == "AJAXDATEPICKER" || row["InputType"].ToString().ToUpper() == "AJAXDATEPICKER2")

                                {

                                    strSqlScript = strSqlScript + "convert(datetime,'" + Request.Form[MyID] + "',103),";



                                    strSqlScriptUpdate = strSqlScriptUpdate + row["TargetField"].ToString() + " = " + "convert(datetime,'" + Request.Form[MyID] + "',103)";

                                }

                                else if (row["InputType"].ToString().ToUpper() == "DATE3" || row["InputType"].ToString().ToUpper() == "DATE32")

                                {

                                    strSqlScript = strSqlScript + "convert(datetime,'" + Request.Form[MyID + "Day"] + "/" + Request.Form[MyID + "Month"] + "/" + Request.Form[MyID + "Year"] + "',103),";

                                    strSqlScriptUpdate = strSqlScriptUpdate + row["TargetField"].ToString() + " = " + "convert(datetime,'" + Request.Form[MyID + "Day"] + "/" + Request.Form[MyID + "Month"] + "/" + Request.Form[MyID + "Year"] + "',103)";

                                }

                                else

                                {

                                    strSqlScript = strSqlScript + "'" + Request.Form[MyID] + "',";

                                    strSqlScriptUpdate = strSqlScriptUpdate + row["TargetField"].ToString() + " = " + "'" + Request.Form[MyID] + "'";

                                }

                                if (row["InputType"].ToString().ToUpper() == "DATEPICKER2")

                                {

                                    strSqlScript = strSqlScript + "'" + Request.Form[MyID.Replace("_From", "_To")] + "',";

                                    strSqlScriptUpdate = strSqlScriptUpdate + row["TargetField"].ToString().Replace("_From", "_To") + " = " + "'" + Request.Form[MyID].Replace("_From", "_To") + "'";

                                }

                                else if (row["InputType"].ToString().ToUpper() == "DATE32")

                                {

                                    strSqlScript = strSqlScript + "convert(datetime,'" + Request.Form[MyID.Replace("_From", "_To") + "Day"] + "/" + Request.Form[MyID.Replace("_From", "_To") + "Month"] + "/" + Request.Form[MyID.Replace("_From", "_To") + "Year"] + "',103),";

                                    strSqlScriptUpdate = strSqlScriptUpdate + row["TargetField"].ToString().Replace("_From", "_To") + " = " + "convert(datetime,'" + Request.Form[MyID.Replace("_From", "_To") + "Day"] + "/" + Request.Form[MyID.Replace("_From", "_To") + "Month"] + "/" + Request.Form[MyID.Replace("_From", "_To") + "Year"] + "',103)";



                                }

                                if (row["InputType"].ToString().ToUpper() == "AJAXDATEPICKER2")

                                {

                                    strSqlScript = strSqlScript + "convert(datetime,'" + Request.Form[MyID.Replace("_From", "_To")] + "',103),";

                                    strSqlScriptUpdate = strSqlScriptUpdate + row["TargetField"].ToString().Replace("_From", "_To") + " = " + "convert(datetime,'" + Request.Form[MyID.Replace("_From", "_To")] + "',103)";



                                }

                                if (row["InputType"].ToString().ToUpper() == "RADIOBUTTON")

                                {

                                    if (!string.IsNullOrEmpty(Request.Form["Txt" + Request.Form[MyID].ToString().Replace(" ", "_")]))

                                    {

                                        strSqlScript = strSqlScript + "'" + Request.Form["Txt" + Request.Form[MyID].ToString().Replace(" ", "_")] + "',";

                                    }

                                    else

                                    {

                                        strSqlScript = strSqlScript + "NULL,";

                                    }

                                    //myTitle.Text = "Txt" + Request.Form[row["FieldName"].ToString()].ToString().Replace(" ", "_");

                                    //myTitle.Text = Request.Form["TxtArea_Khusus"] + "hehehr";

                                }

                                if (row["InputType"].ToString().ToUpper() == "DROPDOWNLIST_OTH")

                                {

                                    if (Request.Form[MyID].ToString().ToUpper() == "OTHER")

                                    {

                                        strSqlScript = strSqlScript + "'" + Request.Form[MyID + "Desc"].ToString() + "',";

                                    }

                                    else

                                    {

                                        strSqlScript = strSqlScript + "NULL,";

                                    }

                                    //myTitle.Text = "Txt" + Request.Form[row["FieldName"].ToString()].ToString().Replace(" ", "_");

                                    //myTitle.Text = Request.Form["TxtArea_Khusus"] + "hehehr";

                                }

                            }

                            else

                            {

                                if (Request.Form[MyID] != "" && Request.Form[MyID] != null)

                                {

                                    strSqlScript = strSqlScript + Request.Form[MyID] + ",";

                                    strSqlScriptUpdate = strSqlScriptUpdate + row["TargetField"].ToString().Replace("_From", "_To") + " = " + Request.Form[MyID];

                                }

                                else

                                {

                                    strSqlScript = strSqlScript + "NULL,";

                                    strSqlScriptUpdate = strSqlScriptUpdate + row["TargetField"].ToString().Replace("_From", "_To") + " = NULL";

                                }





                            }

                        }

                        else

                        {

                            if (row["DataType"].ToString() == "1")

                            {

                                strSqlScript = strSqlScript + "'" + row["FieldValue"].ToString() + "',";

                            }

                            else

                            {

                                strSqlScript = strSqlScript + row["FieldValue"].ToString() + ",";

                            }

                        }

                    }

                }





                if (strSqlScriptHeader.Length > 0)

                {

                    strSqlScriptHeader = strSqlScriptHeader.Substring(0, strSqlScriptHeader.Length - 1);

                }

                if (strSqlScript.Length > 0)

                {

                    strSqlScript = strSqlScript.Substring(0, strSqlScript.Length - 1);

                }

                strSqlScript = strSqlScript.Replace("@UserID", Session["UserID"].ToString());

                strSqlScript = strSqlScript.Replace("@ModuleID", ViewState["ModuleID"].ToString());

                strSqlScript = strSqlScript.Replace("@ID", ViewState["ID"].ToString());

                if (Session["JobID"] != null && Session["JobID"] != "")

                {

                    strSqlScript = strSqlScript.Replace("@JobID", Session["JobID"].ToString());

                }



                string strInsert = " Insert into " + ViewState["MasterTable"] + " (" + strSqlScriptHeader + ") select " + strSqlScript + " ";

                //strInsert = strInsert + " exec sp_insert_Request " + ModuleID.ToString() + ",'" + Session["UserID"].ToString() + "' ";



                //myLabel1.Text = Request.Files["RefrenceLetter"].FileName.ToString() + " hehehe";

                //if (!string.IsNullOrEmpty(Request.Files["RefrenceLetter"].FileName.ToString()))

                //{



                //}



                string strJobID = "Select top 1 " + ViewState["MasterField"] + " from " + ViewState["MasterTable"] + " where Createby ='" + Session["UserID"].ToString() + "' order by CreateDate desc";



                cmd1 = new SqlCommand(strInsert, con);

                con.Open();

                cmd1.ExecuteNonQuery();



                //object returnvalue;



                //if (ViewState["MasterTable"].ToString().ToUpper() == "CANDIDATEMASTER")

                //{

                //    returnvalue = Session["UserID"];

                //}

                //else

                //{

                //    SqlCommand cmdJobID = new SqlCommand(strJobID, con);

                //    returnvalue = cmdJobID.ExecuteScalar();



                //}

                //returnvalue = "";



                con.Close();

                //if (!string.IsNullOrEmpty(returnvalue.ToString()))

                //{

                //    UploadDoc(ModuleID, ActionID, returnvalue.ToString());

                //}

            }



            //con.Close();



            if (ActionID == 6 && Session["JobID"] == null)

            {

                ActionID = 8;

            }

            else

            {

                ActionID += 1;

            }



            ViewState["ActionID"] = ActionID;

            //PrevPage();

        }

        private void UploadDoc(int ModuleID, int ActionID, string UserID)

        {

            DataTable dtInput = this.GetInputData(ModuleID, ActionID, 2);

            String strSqlScript = "";





            SqlConnection con = new SqlConnection(

                WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString

            );



            foreach (DataRow row in dtInput.Rows)

            {

                if (row["InputType"].ToString().ToUpper() == "UPLOADDOC")

                {

                    try

                    {

                        string FileName = Request.Files[row["FieldName"].ToString()].FileName.ToString();

                        if (FileName != "")

                        {

                            string FileExt = Path.GetExtension(FileName);

                            string AppPath = HttpContext.Current.Server.MapPath("~");

                            string NewFileName = UserID.ToString() + "-" + row["FieldName"].ToString() + FileExt;

                            FileName = Path.GetFileName(FileName);



                            myLabel1.Text = AppPath + "\\" + ViewState["UploadFolder"].ToString();

                            AppPath += "Document\\" + ViewState["UploadFolder"].ToString();

                            myLabel1.Text = AppPath + "\\" + FileName;

                            if (!Directory.Exists(AppPath))

                            {

                                //myLabel1.Text = "hehehe";

                                Directory.CreateDirectory(AppPath);

                            }

                            //myLabel1.Text = HttpContext.Current.Server.MapPath("~") + " hehehe";

                            //myLabel1.Text = strInsert;



                            strSqlScript = "update " + ViewState["MasterTable"] + " set " + row["FieldName"].ToString() + " = '" + NewFileName + "' where " + ViewState["MasterField"] + " = '" + UserID.ToString() + "'";

                            //string filename = Path.GetFileName(FileUploadControl.FileName);

                            Request.Files[row["FieldName"].ToString()].SaveAs(AppPath + "\\" + NewFileName);

                            SqlCommand cmd2 = new SqlCommand(strSqlScript, con);

                            con.Open();

                            cmd2.ExecuteNonQuery();

                            con.Close();

                        }



                        //myLabel1.Text = "Upload status: File uploaded!";

                    }

                    catch (Exception ex)

                    {

                        myLabel1.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;

                    }

                }

                //    if (row["InputType"].ToString().ToUpper() == "UPLOADDOC")

                //    {

                //        //string filename = re

                //        String FileName = JobID.ToString() + "-" + row["FieldName"].ToString();

                //        SqlCommand cmd2 = new SqlCommand(strSqlScript, con);

                //        con.Open();

                //        cmd2.ExecuteNonQuery();

                //        con.Close();

                //    }               

            }

        }

        protected void Back_Click(object sender, EventArgs e)

        {

            PrevPage();

        }

        private void PrevPage()

        {

            //object refUrl = ViewState["RefUrl"];

            int ActionID = Int32.Parse(ViewState["ActionID"].ToString()) - 1;

            object refUrl = "~/SysManager/List.aspx?ModuleID=" + ViewState["ModuleID"].ToString();

            myLabel1.Text = refUrl.ToString();

            if (refUrl != null)

                Response.Redirect((string)refUrl);

        }

        private DataTable GetData(int ModuleId, int ActionID, int Tipe)

        {

            string ID = ViewState["ID"].ToString();

            string query = "SELECT * from " + ViewState["MasterTable"] + " where " + ViewState["MasterField"] + " = '" + ID + "'";

            if (ActionID == 7)

            {

                query = query + " and JobID = " + Session["JobID"].ToString();

            }

            string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

            //SqlConnection con = new SqlConnection(

            //        WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString);

            using (SqlConnection con = new SqlConnection(constr))

            {

                DataTable dt = new DataTable();

                using (SqlCommand cmd = new SqlCommand(query))

                {

                    using (SqlDataAdapter sda = new SqlDataAdapter())

                    {

                        //cmd.Parameters.AddWithValue("@ParentMenuId", parentMenuId);

                        cmd.CommandType = CommandType.Text;

                        cmd.Connection = con;

                        sda.SelectCommand = cmd;

                        sda.Fill(dt);

                    }

                }

                return dt;

            }

        }

        private DataTable GetInputData(int ModuleId, int ActionID, int Tipe)

        {

            string query = "SELECT * from vw_ModuleActionList where ModuleID = " + ModuleId + " and ActionID = " + ActionID;

            if (Tipe == 1) //Non Doc

            {

                query += " and InputType <> 'UploadDoc' ";

            }

            else if (Tipe == 2) //Doc

            {

                query += " and InputType = 'UploadDoc' ";

            }

            query += " order by FieldIdx";



            string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

            //SqlConnection con = new SqlConnection(

            //        WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString);

            using (SqlConnection con = new SqlConnection(constr))

            {

                DataTable dt = new DataTable();

                using (SqlCommand cmd = new SqlCommand(query))

                {

                    using (SqlDataAdapter sda = new SqlDataAdapter())

                    {

                        //cmd.Parameters.AddWithValue("@ParentMenuId", parentMenuId);

                        cmd.CommandType = CommandType.Text;

                        cmd.Connection = con;

                        sda.SelectCommand = cmd;

                        sda.Fill(dt);

                    }

                }

                return dt;

            }

        }

        private DataTable GetDDLData(String DDLScript, int value)

        {

            //string query = "SELECT * from DDLList where DDLName = '" + FieldName + "'";

            //string query = "SELECT * from DDLList  where FieldName = @FieldName";

            string query = DDLScript;



            //MsgBox("hehehe" + FieldName);

            if (value == 1)

            {

                query = query + " and isValue = 1";

            }



            string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

            //SqlConnection con = new SqlConnection(

            //        WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString);

            using (SqlConnection con = new SqlConnection(constr))

            {

                DataTable dt = new DataTable();

                using (SqlCommand cmd = new SqlCommand(query))

                {

                    using (SqlDataAdapter sda = new SqlDataAdapter())

                    {

                        //cmd.Parameters.AddWithValue("@FieldName", FieldName);

                        cmd.CommandType = CommandType.Text;

                        cmd.Connection = con;

                        sda.SelectCommand = cmd;

                        sda.Fill(dt);

                    }

                }

                return dt;

            }

        }

    }

}