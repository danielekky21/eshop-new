﻿using System;

using System.Collections.Generic;

using System.Linq;

using System.Web;

using System.Web.UI;

using System.Web.UI.WebControls;



using System.Data;

using System.Data.SqlClient;

using System.Web.Configuration;

using System.ComponentModel;

using System.Drawing;

//Menu

using System.IO;

using System.Configuration;

//Menu



using DynamicContent.Helper;

namespace Emall

{

    public partial class Edit : System.Web.UI.Page

    {

        protected void Page_Load(object sender, EventArgs e)

        {

            string ModuleID;

            if (!this.IsPostBack)

            {

                if (Request.QueryString["ID"] == null)

                {

                    Response.Redirect("~/SysManager/Login.aspx");

                }

                if (Request.QueryString["ModuleID"] == null)

                {

                    Response.Redirect("~/SysManager/Login.aspx");

                }

                ModuleID = Request.QueryString["ModuleID"].ToString();

                ID = Request.QueryString["ID"].ToString();

                ViewState["ID"] = ID;

                ViewState["ModuleID"] = ModuleID;

                if (Request.UrlReferrer != null)

                {

                    ViewState["RefUrl"] = Request.UrlReferrer.ToString();

                }

                else

                {

                    ViewState["RefUrl"] = "";

                }



                ViewState["Action"] = "";

                //MsgBox(ModuleID);





            }

            ModuleID = Request.QueryString["ModuleID"].ToString();

            ID = Request.QueryString["ID"].ToString();

            //MsgBox(ModuleID);

            ValidateRight(Int32.Parse(ModuleID));

            //PopulateDisplay();

            PopulateInput(Int32.Parse(ModuleID), ID.ToString());

        }

        private void MsgBox(string StrMsg)

        {

            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + StrMsg + "');", true);

        }

        private void ValidateRight(int moduleID)

        {

            string query = "SELECT top 1 [ModuleRole],[TitleDesc], [MasterTable], [MasterField], [UploadFolder], [UserName], [InputList] FROM [vw_UserRight] WHERE ModuleId = " + moduleID + " and UserID = '" + Session["UserID"] + "' ";

            string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;



            ViewState["MasterTable"] = "";

            ViewState["ModuleRole"] = "";

            ViewState["UserName"] = "";

            ViewState["InputList"] = "";

            ViewState["MasterField"] = "";

            ViewState["UploadFolder"] = "";



            using (SqlConnection con = new SqlConnection(constr))

            {

                DataTable dt = new DataTable();

                using (SqlCommand cmd = new SqlCommand(query))

                {

                    cmd.CommandType = CommandType.Text;

                    cmd.Connection = con;

                    //cmd.Parameters.Add(cmd.CreateParameter("AssetID"));

                    using (SqlDataAdapter sda = new SqlDataAdapter())

                    {

                        //cmd.Parameters.AddWithValue("@ModuleId", ModuleID);

                        cmd.CommandType = CommandType.Text;

                        cmd.Connection = con;

                        sda.SelectCommand = cmd;

                        sda.Fill(dt);



                        //if (!dt.)

                        //{

                        //    myTitle.Text = "NULL";

                        //} else

                        //{

                        foreach (DataRow row in dt.Rows)

                        {

                            myTitle.Text = row["TitleDesc"].ToString();

                            ViewState["MasterTable"] = row["MasterTable"].ToString();

                            ViewState["ModuleRole"] = row["ModuleRole"].ToString();

                            ViewState["UserName"] = row["UserName"].ToString();

                            ViewState["InputList"] = row["InputList"].ToString();

                            ViewState["MasterField"] = row["MasterField"].ToString();

                            ViewState["UploadFolder"] = row["UploadFolder"].ToString();

                        }



                        //}

                    }

                }

            }

        }



        private void PopulateDisplay()

        {

            TableRow trow;

            TableCell tcell1, tcell2, tcell3;

            if (!string.IsNullOrEmpty(ViewState["InputList"].ToString()))

            {

                string query = ViewState["InputList"].ToString();



                string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;





                using (SqlConnection con = new SqlConnection(constr))

                {

                    DataTable dt = new DataTable();

                    using (SqlCommand cmd = new SqlCommand(query))

                    {

                        cmd.CommandType = CommandType.Text;

                        cmd.Connection = con;

                        //cmd.Parameters.Add(cmd.CreateParameter("AssetID"));

                        using (SqlDataAdapter sda = new SqlDataAdapter())

                        {

                            //cmd.Parameters.AddWithValue("@ModuleId", ModuleID);

                            cmd.CommandType = CommandType.Text;

                            cmd.Connection = con;

                            sda.SelectCommand = cmd;

                            sda.Fill(dt);



                            //if (!dt.)

                            //{

                            //    myTitle.Text = "NULL";

                            //} else

                            //{

                            trow = new TableRow();

                            tcell1 = new TableCell();



                            Label myLabelTitle = new Label();

                            myLabelTitle.Text = "Information";

                            myLabelTitle.Font.Bold = true;

                            myLabelTitle.Font.Underline = true;

                            myLabelTitle.Font.Size = 16;



                            tcell1.Controls.Add(myLabelTitle);

                            tcell1.ColumnSpan = 3;

                            trow.Cells.Add(tcell1);



                            mytbl.Rows.Add(trow);

                            trow = new TableRow();

                            mytbl.Rows.Add(trow);

                            foreach (DataRow row in dt.Rows)

                            {

                                foreach (DataColumn Column in dt.Columns)

                                {

                                    trow = new TableRow();

                                    tcell1 = new TableCell();

                                    tcell2 = new TableCell();

                                    tcell3 = new TableCell();



                                    Label myLabel = new Label();

                                    myLabel.Text = Column.ColumnName.ToString();

                                    myLabel.Font.Bold = true;



                                    Label myLabelSeparator = new Label();

                                    myLabelSeparator.Text = ":";



                                    Label myLabelValue = new Label();

                                    myLabelValue.Text = row[Column].ToString().Replace(Environment.NewLine, "<br>");



                                    tcell1.Controls.Add(myLabel);

                                    tcell2.Controls.Add(myLabelSeparator);

                                    tcell3.Controls.Add(myLabelValue);

                                    trow.Cells.Add(tcell1);

                                    trow.Cells.Add(tcell2);

                                    trow.Cells.Add(tcell3);



                                    mytbl.Rows.Add(trow);

                                }

                            }



                            //}

                        }

                    }

                    con.Close();

                }



            }

        }

        private void PopulateInput(int ModuleID, string ID)

        {

            DataTable dtInput = this.GetInputData(ModuleID, ID, 0);

            DataTable dtData = this.GetData(ModuleID, ID, 0);

            String strScript = "";

            //SqlConnection con = new SqlConnection(

            //WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString);

            TableRow trow;

            TableCell tcell1, tcell2, tcell3;

            string MyID = "";

            //SqlCommand Comm1 = new SqlCommand("Select * From Test", con);

            //con.Open();

            //SqlDataReader DR1 = Comm1.ExecuteReader();

            /*if (DR1.Read())

            {

                //textbox.Text = DR1.GetValue(0).ToString();

            }*/



            trow = new TableRow();

            mytbl.Rows.Add(trow);

            mytbl.Rows.Add(trow);



            trow = new TableRow();

            tcell1 = new TableCell();



            /*

            Label myLabelTitle = new Label();

            myLabelTitle.Text = myTitle.Text;

            myLabelTitle.Font.Bold = true;

            myLabelTitle.Font.Underline = true;

            myLabelTitle.Font.Size = 16;

           

            tcell1.Controls.Add(myLabelTitle);

            tcell1.ColumnSpan = 3;

            trow.Cells.Add(tcell1);

            */



            mytbl.Rows.Add(trow);

            trow = new TableRow();

            mytbl.Rows.Add(trow);



            int i = 0;

            //for (int i = 0; i < 30; i++)

            foreach (DataRow row in dtInput.Rows)

            {

                MyID = row["FieldName"].ToString();

                if (row["isShow"].ToString() == "Y")

                {

                    Label myLabel = new Label();

                    myLabel.Text = row["FieldTitle"].ToString();

                    myLabel.Font.Bold = true;

                    myLabel.Font.Size = 10;

                    myLabel.ForeColor = Color.DarkGoldenrod;

                    Label myLabelSeparator = new Label();

                    myLabelSeparator.Text = ":";

                    myLabelSeparator.Font.Bold = true;

                    myLabelSeparator.Font.Size = 10;

                    myLabelSeparator.ForeColor = Color.DarkGoldenrod;



                    trow = new TableRow();

                    tcell1 = new TableCell();

                    tcell2 = new TableCell();

                    tcell3 = new TableCell();

                    if (row["isMandatory"].ToString().ToUpper() == "Y")

                    {

                        Label myMandatory = new Label();

                        myMandatory.Text = " *";

                        myMandatory.ForeColor = Color.Red;

                        myMandatory.Font.Bold = true;

                        tcell1.Controls.Add(myMandatory);

                    }

                    tcell1.Controls.Add(myLabel);

                    //tcell3.Controls.Add(myLabelSeparator);

                    trow.Cells.Add(tcell1);

                    //trow.Cells.Add(tcell3);

                    mytbl.Rows.Add(trow);



                    trow = new TableRow();

                    if (row["InputType"].ToString().ToUpper() == "UPLOADDOC")

                    {

                        FileUpload myFU = new FileUpload();

                        myFU.ID = row["FieldName"].ToString();

                        //myTextbox.Attributes.Add("width",1000);



                        if (row["isEditable"].ToString().ToUpper() == "N")

                        {

                            myFU.Enabled = false;

                        }





                        //if (!string.IsNullOrEmpty(Request.Form[row["FieldName"].ToString()]))

                        //{

                        //    myFU. = Request.Form[row["FieldName"].ToString()];

                        //}



                        tcell2.Controls.Add(myFU); //, Text = "hello :)"



                    }



                    if (row["InputType"].ToString().ToUpper() == "RADIOBUTTON")

                    {

                        string RblValue = "";

                        RadioButtonList myRBL = new RadioButtonList();

                        myRBL.ID = row["FieldName"].ToString();

                        //myTextbox.Attributes.Add("width",1000);

                        DataTable dtRBL = this.GetDDLData(row["DDLScript"].ToString(), 1);

                        myRBL.DataSource = dtRBL;

                        myRBL.DataTextField = "DDLText";

                        myRBL.DataValueField = "DDLValue";

                        myRBL.DataBind();



                        if (row["isEditable"].ToString().ToUpper() == "N")

                        {

                            myRBL.Enabled = false;

                        }





                        if (!string.IsNullOrEmpty(Request.Form[row["FieldName"].ToString()]))

                        {

                            RblValue = Request.Form[row["FieldName"].ToString()];

                            myRBL.SelectedValue = RblValue.ToString();

                        }

                        else

                        {

                            myRBL.SelectedValue = dtData.Rows[0][row["TargetField"].ToString()].ToString();

                        }

                        tcell2.Controls.Add(myRBL); //, Text = "hello :)"



                    }



                    if (row["InputType"].ToString().ToUpper() == "TEXTBOX" || row["InputType"].ToString().ToUpper() == "PASSWORD")

                    {

                        TextBox myTextbox = new TextBox();

                        myTextbox.ID = row["FieldName"].ToString();

                        //myTextbox.Attributes.Add("width",1000);

                        if (row["InputType"].ToString().ToUpper() == "PASSWORD")

                        {

                            myTextbox.TextMode = TextBoxMode.Password;

                        }

                        if (!string.IsNullOrEmpty(row["Width"].ToString()))

                        {

                            myTextbox.Width = Int32.Parse(row["Width"].ToString());

                        }

                        if (!string.IsNullOrEmpty(row["Line"].ToString()))

                        {

                            myTextbox.TextMode = TextBoxMode.MultiLine;

                            myTextbox.Rows = Int32.Parse(row["Line"].ToString());

                        }

                        if (!string.IsNullOrEmpty(Request.Form[row["FieldName"].ToString()]))

                        {

                            myTextbox.Text = Request.Form[row["FieldName"].ToString()];

                            //myLabel.Text = Request.Form[row["FieldName"].ToString()];

                        }

                        else

                        {

                            myTextbox.Text = dtData.Rows[0][row["TargetField"].ToString()].ToString();

                        }

                        if (row["isEditable"].ToString().ToUpper() == "N")

                        {

                            myTextbox.Enabled = false;

                        }

                        tcell2.Controls.Add(myTextbox); //, Text = "hello :)"

                    }

                    if (row["InputType"].ToString().ToUpper() == "AJAXDATEPICKER")

                    {

                        TextBox myDatepicker = new TextBox();

                        myDatepicker.ID = MyID;

                        myDatepicker.ReadOnly = true;

                        myDatepicker.Width = 80;



                        if (!string.IsNullOrEmpty(Request.Form[MyID]))

                        {

                            myDatepicker.Text = Request.Form[MyID];

                        }

                        else

                        {

                            myDatepicker.Text = DateTime.Parse(dtData.Rows[0][MyID].ToString()).ToString("dd/MM/yyyy");

                        }

                        tcell2.Controls.Add(myDatepicker);



                        ImageButton myImgBtn = new ImageButton();

                        myImgBtn.ImageAlign = ImageAlign.Bottom;

                        myImgBtn.ImageUrl = "Images/calendar.png";

                        myImgBtn.ID = MyID + "PopUp";

                        tcell2.Controls.Add(myImgBtn);

                        if (row["isEditable"].ToString().ToUpper() == "N")

                        {

                            myImgBtn.Enabled = false;

                        }



                        AjaxControlToolkit.CalendarExtender myCE = new AjaxControlToolkit.CalendarExtender();

                        myCE.ID = MyID + "CE";

                        myCE.PopupButtonID = MyID + "PopUp";

                        myCE.TargetControlID = MyID;

                        myCE.Format = "dd/MM/yyyy";

                        tcell2.Controls.Add(myCE);

                    }

                    if (row["InputType"].ToString().ToUpper() == "AJAXDATEPICKER2")

                    {

                        Label myLabelFrom = new Label();

                        myLabelFrom.Text = "From : ";

                        tcell2.Controls.Add(myLabelFrom);



                        string FieldNameFrom = MyID;

                        string FieldNameTo = MyID.Replace("_From", "_To");



                        TextBox myDatepicker = new TextBox();

                        myDatepicker.ID = MyID;

                        myDatepicker.ReadOnly = true;

                        myDatepicker.Width = 80;



                        if (!string.IsNullOrEmpty(Request.Form[FieldNameFrom]))

                        {

                            myDatepicker.Text = Request.Form[FieldNameFrom];

                        }

                        else

                        {

                            myDatepicker.Text = DateTime.Parse(dtData.Rows[0][FieldNameFrom].ToString()).ToString("dd/MM/yyyy");

                        }

                        tcell2.Controls.Add(myDatepicker);



                        ImageButton myImgBtn = new ImageButton();

                        myImgBtn.ImageAlign = ImageAlign.Bottom;

                        myImgBtn.ImageUrl = "Images/calendar.png";

                        myImgBtn.ID = FieldNameFrom + "PopUp";



                        if (row["isEditable"].ToString().ToUpper() == "N")

                        {

                            myImgBtn.Enabled = false;

                        }



                        tcell2.Controls.Add(myImgBtn);



                        AjaxControlToolkit.CalendarExtender myCE = new AjaxControlToolkit.CalendarExtender();

                        myCE.ID = FieldNameFrom + "CE";

                        myCE.PopupButtonID = FieldNameFrom + "PopUp";

                        myCE.TargetControlID = FieldNameFrom;

                        myCE.Format = "dd/MM/yyyy";

                        tcell2.Controls.Add(myCE);





                        Label myLabelTo = new Label();

                        myLabelTo.Text = " To : ";

                        tcell2.Controls.Add(myLabelTo);



                        myDatepicker = new TextBox();

                        myDatepicker.ID = FieldNameTo;

                        myDatepicker.ReadOnly = true;

                        myDatepicker.Width = 80;



                        if (!string.IsNullOrEmpty(Request.Form[FieldNameTo]))

                        {

                            myDatepicker.Text = Request.Form[FieldNameTo];

                        }

                        else

                        {

                            myDatepicker.Text = DateTime.Parse(dtData.Rows[0][FieldNameTo].ToString()).ToString("dd/MM/yyyy");

                        }

                        tcell2.Controls.Add(myDatepicker);



                        myImgBtn = new ImageButton();

                        myImgBtn.ImageAlign = ImageAlign.Bottom;

                        myImgBtn.ImageUrl = "Images/calendar.png";

                        myImgBtn.ID = FieldNameTo + "PopUp";

                        if (row["isEditable"].ToString().ToUpper() == "N")

                        {

                            myImgBtn.Enabled = false;

                        }

                        tcell2.Controls.Add(myImgBtn);



                        myCE = new AjaxControlToolkit.CalendarExtender();

                        myCE.ID = FieldNameTo + "CE";

                        myCE.PopupButtonID = FieldNameTo + "PopUp";

                        myCE.TargetControlID = FieldNameTo;

                        myCE.Format = "dd/MM/yyyy";

                        tcell2.Controls.Add(myCE);



                    }

                    if (row["InputType"].ToString().ToUpper() == "DATEPICKER")

                    {

                        TextBox myDatepicker = new TextBox();

                        myDatepicker.ID = row["FieldName"].ToString();

                        myDatepicker.ReadOnly = true;

                        myDatepicker.Width = 80;



                        if (!string.IsNullOrEmpty(Request.Form[row["FieldName"].ToString()]))

                        {

                            myDatepicker.Text = Request.Form[row["FieldName"].ToString()];

                        }

                        else

                        {

                            myDatepicker.Text = dtData.Rows[0][row["TargetField"].ToString().Replace("_From", "_To")].ToString();

                        }

                        tcell2.Controls.Add(myDatepicker);



                        strScript += "$(\"[id$=" + row["FieldName"].ToString() + "]\").datepicker({";

                        strScript += "  showOn: 'button',";

                        strScript += "  buttonImageOnly: false,";

                        strScript += "  buttonText: '...'";

                        strScript += "  });; ";



                    }

                    if (row["InputType"].ToString().ToUpper() == "DATEPICKER2")

                    {

                        Label myLabelFrom = new Label();

                        myLabelFrom.Text = "From : ";

                        tcell2.Controls.Add(myLabelFrom);



                        string FieldNameFrom = row["FieldName"].ToString();

                        string FieldNameTo = row["FieldName"].ToString().Replace("_From", "_To");



                        TextBox myDatepicker = new TextBox();

                        myDatepicker.ID = FieldNameFrom;

                        myDatepicker.ReadOnly = true;

                        myDatepicker.Width = 80;



                        if (!string.IsNullOrEmpty(Request.Form[FieldNameFrom]))

                        {

                            myDatepicker.Text = Request.Form[FieldNameFrom];

                        }

                        else

                        {

                            myDatepicker.Text = dtData.Rows[0][row["TargetField"].ToString()].ToString();

                        }

                        tcell2.Controls.Add(myDatepicker);



                        strScript += "$(\"[id$=" + FieldNameFrom + "]\").datepicker({";

                        strScript += "  showOn: 'button',";

                        strScript += "  buttonImageOnly: false,";

                        strScript += "  buttonText: '...'";

                        strScript += "  });; ";



                        Label myLabelTo = new Label();

                        myLabelTo.Text = " To : ";

                        tcell2.Controls.Add(myLabelTo);



                        TextBox myDatepicker1 = new TextBox();

                        myDatepicker1.ID = FieldNameTo;

                        myDatepicker1.ReadOnly = true;

                        myDatepicker1.Width = 80;



                        if (!string.IsNullOrEmpty(Request.Form[FieldNameTo]))

                        {

                            myDatepicker1.Text = Request.Form[FieldNameTo];

                        }

                        else

                        {

                            myDatepicker1.Text = dtData.Rows[0][row["TargetField"].ToString() + "_To"].ToString();

                        }

                        tcell2.Controls.Add(myDatepicker1);



                        strScript += "$(\"[id$=" + FieldNameTo + "]\").datepicker({";

                        strScript += "  showOn: 'button',";

                        strScript += "  buttonImageOnly: false,";

                        strScript += "  buttonText: '...'";

                        strScript += "  });; ";



                    }

                    if (row["InputType"].ToString().ToUpper() == "DROPDOWNLIST")

                    {

                        string query = row["DDLScript"].ToString();

                        string Value = "";

                        if (row["DDLDepend"].ToString() != "")

                        {

                            Value = ViewState[row["DDLDepend"].ToString() + "_Value"].ToString();

                            query = query.Replace("@Value", Value);

                        }

                        DataTable dtDDL = this.GetDDLData(query, 0);

                        DropDownList myDDL = new DropDownList();

                        myDDL.ID = row["FieldName"].ToString();

                        myDDL.DataSource = dtDDL;

                        myDDL.DataTextField = "DDLText";

                        myDDL.DataValueField = "DDLValue";

                        myDDL.DataBind();

                        if (!string.IsNullOrEmpty(Request.Form[row["FieldName"].ToString()]))

                        {

                            myDDL.SelectedValue = Request.Form[row["FieldName"].ToString()];

                        }

                        else

                        {

                            myDDL.SelectedValue = dtData.Rows[0][row["TargetField"].ToString()].ToString();

                        }

                        if (row["isEditable"].ToString().ToUpper() == "N")

                        {

                            myDDL.Enabled = false;

                        }

                        if (row["DDLPostback"].ToString() == "Y")

                        {

                            myDDL.AutoPostBack = true;

                            ViewState[row["FieldName"].ToString() + "_Value"] = myDDL.SelectedValue;

                        }







                        tcell2.Controls.Add(myDDL); //, Text = "hello :)"

                    }

                    //tcell3 = new TableCell();

                    //tcell3.Controls.Add(new DropDownList());



                    //trow.Cells.Add(tcell1);

                    //trow.Cells.Add(tcell3);

                    //tcell2.ColumnSpan = 2;

                    if (row["isMandatory"].ToString().ToUpper() == "Y")

                    {

                        RequiredFieldValidator myRFV = new RequiredFieldValidator();

                        myRFV.ID = "RFV" + row["FieldName"].ToString();

                        myRFV.ControlToValidate = row["FieldName"].ToString();

                        myRFV.Display = ValidatorDisplay.Dynamic;

                        //myRFV.ErrorMessage = "  " + row["FieldTitle"].ToString() + " Must Filled";

                        myRFV.ErrorMessage = "  Required Information";

                        myRFV.ForeColor = Color.Red;

                        tcell2.Controls.Add(myRFV);

                    }

                    if (!string.IsNullOrEmpty(row["CompareTo"].ToString()))

                    {

                        CompareValidator myCV = new CompareValidator();

                        myCV.ID = "myCV" + row["FieldName"].ToString();

                        myCV.ControlToValidate = row["FieldName"].ToString();

                        myCV.ControlToCompare = row["CompareTo"].ToString();

                        myCV.Display = ValidatorDisplay.Dynamic;

                        //myRFV.ErrorMessage = "  " + row["FieldTitle"].ToString() + " Must Filled";

                        myCV.ErrorMessage = "  " + row["CompareTo"].ToString() + " Not Equal!!";

                        myCV.ForeColor = Color.Red;

                        tcell2.Controls.Add(myCV);

                    }

                    if (!string.IsNullOrEmpty(row["RegexValidation"].ToString()))

                    {

                        RegularExpressionValidator myREV = new RegularExpressionValidator();

                        myREV.ID = "myREV" + row["FieldName"].ToString();

                        myREV.ControlToValidate = row["FieldName"].ToString();

                        //myREV.ValidationExpression = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";

                        myREV.ValidationExpression = row["RegexValidation"].ToString();

                        myREV.ErrorMessage = row["RegexValidationMsg"].ToString();

                        myREV.Display = ValidatorDisplay.Dynamic;

                        myREV.ForeColor = Color.Red;

                        tcell2.Controls.Add(myREV);

                    }

                    if (row["FieldName"].ToString().IndexOf("Email") >= 0)

                    {

                        RegularExpressionValidator myREV = new RegularExpressionValidator();

                        myREV.ID = "myREV" + row["FieldName"].ToString();

                        myREV.ControlToValidate = row["FieldName"].ToString();

                        myREV.ValidationExpression = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";

                        myREV.ErrorMessage = "Invalid Email Format";

                        myREV.Display = ValidatorDisplay.Dynamic;

                        myREV.ForeColor = Color.Red;

                        tcell2.Controls.Add(myREV);

                    }

                    trow.Cells.Add(tcell2);



                    mytbl.Rows.Add(trow);

                    if (strScript != "")

                    {

                        strScript = "   $(function () {" +

                            strScript +

                            "   });";

                        ClientScript.RegisterClientScriptBlock(this.GetType(), "DTPickerScript", strScript, true);

                    }

                    //myLabel.Text = strSqlScript;

                }

                i = i + 1;

            }



            trow = new TableRow();

            tcell1 = new TableCell();

            tcell2 = new TableCell();



            Button SaveButton = new Button();

            SaveButton.ID = "SaveButton";

            SaveButton.Text = "Save";

            SaveButton.Click += new EventHandler(Save_Click);



            tcell1.Controls.Add(SaveButton);

            trow.Cells.Add(tcell1);



            Button BackButton = new Button();

            BackButton.ID = "BackButton";

            BackButton.Text = "Back";

            BackButton.Click += new EventHandler(Back_Click);

            BackButton.CausesValidation = false;



            tcell2.Controls.Add(BackButton);

            trow.Cells.Add(tcell2);

            mytbl1.Rows.Add(trow);



        }

        protected void Save_Click(object sender, EventArgs e)

        {

            if (Page.IsValid)

            {

                ViewState["Action"] = "Save";

                //Button button = sender as Button;

                // identify which button was clicked and perform necessary actions

                //myLabel1.Text = ViewState["Action"].ToString();



                SaveData();

            }

        }

        private void SaveData()

        {

            int ModuleID = Int32.Parse(ViewState["ModuleID"].ToString());

            DataTable dtInput = this.GetInputData(ModuleID, ViewState["ID"].ToString(), 1);

            DataTable dtData = this.GetData(ModuleID, ViewState["ID"].ToString(), 0);

            //String strScript = "";

            String strSqlScript = "";

            string strSqlScriptHeader = "";

            string strSqlScriptField = "";

            string strSqlScriptUpdate = "";



            SqlConnection con = new SqlConnection(

                WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString

            );



            //SqlCommand Comm1 = new SqlCommand("Select * From Test", con);

            //con.Open();

            //SqlDataReader DR1 = Comm1.ExecuteReader();

            /*if (DR1.Read())

            {

                //textbox.Text = DR1.GetValue(0).ToString();

            }*/





            int i = 0;

            //for (int i = 0; i < 30; i++)

            foreach (DataRow row in dtInput.Rows)

            {

                if (row["isEditable"].ToString().ToUpper() != "N")

                {

                    if (row["InputType"].ToString().ToUpper() == "UPLOADDOC")

                    {

                        try

                        {

                            string FileName = Request.Files[row["FieldName"].ToString()].FileName.ToString();

                            string FileExt = Path.GetExtension(FileName);

                            string AppPath = HttpContext.Current.Server.MapPath("~");

                            string NewFileName = row["FieldName"].ToString() + FileExt;

                            FileName = Path.GetFileName(FileName);



                            if (!Directory.Exists("Document"))

                            {

                                //myLabel1.Text = "hehehe";

                                Directory.CreateDirectory("Document");

                            }



                            AppPath += "Document\\" + ViewState["UploadFolder"].ToString();

                            myLabel1.Text = AppPath + "\\" + FileName;

                            if (!Directory.Exists(AppPath))

                            {

                                //myLabel1.Text = "hehehe";

                                Directory.CreateDirectory(AppPath);

                            }

                            //myLabel1.Text = HttpContext.Current.Server.MapPath("~") + " hehehe";

                            //myLabel1.Text = strInsert;



                            //string filename = Path.GetFileName(FileUploadControl.FileName);

                            Request.Files[row["FieldName"].ToString()].SaveAs(AppPath + "\\" + NewFileName);

                            //myLabel1.Text = "Upload status: File uploaded!";

                        }

                        catch (Exception ex)

                        {

                            myLabel1.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;

                        }

                    }

                    else

                    {

                        strSqlScriptHeader = strSqlScriptHeader + row["TargetField"].ToString() + ",";

                        if (row["InputType"].ToString().ToUpper() == "DATEPICKER2" || row["InputType"].ToString().ToUpper() == "AJAXDATEPICKER2")

                        {

                            strSqlScriptHeader = strSqlScriptHeader + row["TargetField"].ToString().Replace("_From", "_To") + ",";



                        }

                        if (row["InputType"].ToString().ToUpper() == "RADIOBUTTON")

                        {

                            strSqlScriptHeader = strSqlScriptHeader + row["TargetField"].ToString() + "Desc,";

                        }



                        if (row["isShow"].ToString() == "Y")

                        {

                            if (row["DataType"].ToString() == "1")

                            {

                                strSqlScript = strSqlScript + "'" + Request.Form[row["FieldName"].ToString()].Replace("'", "''") + "',";



                                if (row["InputType"].ToString().ToUpper() == "PASSWORD")

                                {

                                    if (CustomEncryption.HashStringSHA1(Request.Form[row["FieldName"].ToString()].ToString().Trim()) != dtData.Rows[0][row["TargetField"].ToString()].ToString().Trim())

                                    {

                                        strSqlScriptField = strSqlScriptField + row["TargetField"].ToString() + " = '" + Request.Form[row["FieldName"].ToString()].ToString().Replace("'", "''") + "'";

                                    }

                                }

                                if (Request.Form[row["FieldName"].ToString()].ToString().Trim() != dtData.Rows[0][row["TargetField"].ToString()].ToString().Trim())

                                {

                                    string str1 = Request.Form[row["FieldName"].ToString()].ToString().Trim();

                                    string str2 = dtData.Rows[0][row["TargetField"].ToString()].ToString().Trim();

                                    if (strSqlScriptField != null && strSqlScriptField != "")

                                    {

                                        strSqlScriptField = strSqlScriptField + ", ";

                                    }

                                    if (row["InputType"].ToString().ToUpper() == "DATEPICKER2")

                                    {

                                        strSqlScriptField = strSqlScriptField + row["TargetField"].ToString() + " = Convert(datetime,'" + Request.Form[row["FieldName"].ToString()].ToString() + "',103)";

                                    }

                                    else

                                    {

                                        strSqlScriptField = strSqlScriptField + row["TargetField"].ToString() + " = '" + Request.Form[row["FieldName"].ToString()].ToString().Replace("'", "''") + "'";

                                    }



                                }



                                if (row["InputType"].ToString().ToUpper() == "DATEPICKER2")

                                {

                                    strSqlScript = strSqlScript + "'" + Request.Form[row["FieldName"].ToString().Replace("_From", "_To")] + "',";

                                    if (Request.Form[row["FieldName"].ToString().Replace("_From", "_To")].ToString().Trim() != dtData.Rows[0][row["TargetField"].ToString().Replace("_From", "_To")].ToString().Trim())

                                    {

                                        if (strSqlScriptField != null && strSqlScriptField != "")

                                        {

                                            strSqlScriptField = strSqlScriptField + ", ";

                                        }

                                        strSqlScriptField = strSqlScriptField + row["TargetField"].ToString().Replace("_From", "_To") + " = '" + Request.Form[row["FieldName"].ToString().Replace("_From", "_To")].ToString() + "'";



                                    }

                                }

                                if (row["InputType"].ToString().ToUpper() == "AJAXDATEPICKER2")

                                {

                                    strSqlScript = strSqlScript + "'" + Request.Form[row["FieldName"].ToString().Replace("_From", "_To")] + "',";

                                    if (Request.Form[row["FieldName"].ToString().Replace("_From", "_To")].ToString().Trim() != dtData.Rows[0][row["TargetField"].ToString().Replace("_From", "_To")].ToString().Trim())

                                    {

                                        if (strSqlScriptField != null && strSqlScriptField != "")

                                        {

                                            strSqlScriptField = strSqlScriptField + ", ";

                                        }

                                        strSqlScriptField = strSqlScriptField + row["TargetField"].ToString().Replace("_From", "_To") + " = convert(datetime,'" + Request.Form[row["FieldName"].ToString().Replace("_From", "_To")].ToString() + "',103)";



                                    }

                                }



                                if (row["InputType"].ToString().ToUpper() == "RADIOBUTTON")

                                {

                                    if (!string.IsNullOrEmpty(Request.Form["Txt" + Request.Form[row["FieldName"].ToString()].ToString().Replace(" ", "_")]))

                                    {

                                        strSqlScript = strSqlScript + "'" + Request.Form["Txt" + Request.Form[row["FieldName"].ToString()].ToString().Replace(" ", "_")] + "',";

                                        if (Request.Form["Txt" + Request.Form[row["FieldName"].ToString()].ToString().Replace(" ", "_")].Trim() != dtData.Rows[0][row["TargetField"].ToString() + "Desc"].ToString().Trim())

                                        {

                                            if (strSqlScriptField != null && strSqlScriptField != "")

                                            {

                                                strSqlScriptField = strSqlScriptField + ", ";

                                            }

                                            strSqlScriptField = strSqlScriptField + row["TargetField"].ToString() + "Desc" + " = '" + Request.Form["Txt" + Request.Form[row["FieldName"].ToString()].ToString().Replace(" ", "_")] + "'";



                                        }

                                    }

                                    else

                                    {

                                        strSqlScript = strSqlScript + "NULL,";

                                    }

                                    //myTitle.Text = "Txt" + Request.Form[row["FieldName"].ToString()].ToString().Replace(" ", "_");

                                    //myTitle.Text = Request.Form["TxtArea_Khusus"] + "hehehr";

                                }

                            }

                            else

                            {

                                strSqlScript = strSqlScript + Request.Form[row["FieldName"].ToString()] + ",";

                                if (Request.Form[row["FieldName"].ToString()].ToString().Trim() != dtData.Rows[0][row["TargetField"].ToString()].ToString().Trim())

                                {

                                    if (strSqlScriptField != null && strSqlScriptField != "")

                                    {

                                        strSqlScriptField = strSqlScriptField + ", ";

                                    }



                                    if (Request.Form[row["FieldName"].ToString()] != null && Request.Form[row["FieldName"].ToString()].ToString() != "")

                                    {

                                        strSqlScriptField = strSqlScriptField + row["TargetField"].ToString() + " = " + Request.Form[row["FieldName"].ToString()].ToString();

                                    }

                                    else

                                    {

                                        strSqlScriptField = strSqlScriptField + row["TargetField"].ToString() + " = Null ";

                                    }

                                }

                            }

                        }

                        else

                        {

                            if (row["DataType"].ToString() == "1")

                            {

                                strSqlScript = strSqlScript + "'" + row["FieldValue"].ToString() + "',";

                            }

                            else

                            {

                                strSqlScript = strSqlScript + row["FieldValue"].ToString() + ",";

                            }

                        }

                        i = i + 1;

                    }

                }

            }







            strSqlScriptHeader = strSqlScriptHeader.Substring(0, strSqlScriptHeader.Length - 1);

            strSqlScript = strSqlScript.Substring(0, strSqlScript.Length - 1);

            strSqlScript = strSqlScript.Replace("@UserID", Session["UserID"].ToString());

            strSqlScript = strSqlScript.Replace("@ID", ViewState["ID"].ToString());

            strSqlScript = strSqlScript.Replace("@ModuleID", ViewState["ModuleID"].ToString());

            //string strInsert = "Insert into " + ViewState["MasterTable"] + "_tmp" + " (" + strSqlScriptHeader + ") select " + strSqlScript + " go";

            //strInsert = strInsert + " exec sp_insert_Request " + ModuleID.ToString() + ",'" + Session["UserID"].ToString() + "' ";

            strSqlScriptUpdate = "";

            if (strSqlScriptField != "")

            {

                strSqlScriptUpdate = "Update " + ViewState["MasterTable"].ToString() + " Set " + strSqlScriptField + " where " + ViewState["MasterField"].ToString() + "= '" + ViewState["ID"].ToString() + "'";



                SqlCommand cmd1 = new SqlCommand(strSqlScriptUpdate, con);

                con.Open();

                cmd1.ExecuteNonQuery();

            }

            //myLabel1.Text = Request.Files["RefrenceLetter"].FileName.ToString() + " hehehe";

            //if (!string.IsNullOrEmpty(Request.Files["RefrenceLetter"].FileName.ToString()))

            //{



            //}



            //string strJobID = "Select top 1 " + ViewState["MasterField"] + " from " + ViewState["MasterTable"] + " where Requestby ='" + Session["UserID"].ToString() + "' order by " + ViewState["MasterField"] + " desc";



            object returnvalue;

            //SqlCommand cmdJobID = new SqlCommand(strJobID, con);

            //returnvalue = cmdJobID.ExecuteScalar();

            //returnvalue = "";

            returnvalue = ViewState["ID"];

            //myLabel1.Text = strSqlScriptUpdate.ToString();



            con.Close();

            if (!string.IsNullOrEmpty(returnvalue.ToString()))

            {

                UploadDoc(ModuleID, returnvalue.ToString());

            }



            if (myLabel1.Text == "")

            {

                PrevPage();

            }

        }

        private void UploadDoc(int ModuleID, string ID)

        {

            DataTable dtInput = this.GetInputData(ModuleID, ViewState["ID"].ToString(), 2);

            String strSqlScript = "";





            SqlConnection con = new SqlConnection(

                WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString

            );



            foreach (DataRow row in dtInput.Rows)

            {

                if (row["InputType"].ToString().ToUpper() == "UPLOADDOC" && Request.Files[row["FieldName"].ToString()].FileName.ToString() != "")

                {

                    try

                    {

                        string FileName = Request.Files[row["FieldName"].ToString()].FileName.ToString();

                        string FileExt = Path.GetExtension(FileName);

                        string AppPath = HttpContext.Current.Server.MapPath("~");

                        //string NewFileName = ID.ToString() + "-" + row["FieldName"].ToString() + "_tmp" + FileExt;

                        string NewFileName = ID.ToString() + "-" + row["FieldName"].ToString() + FileExt;



                        AppPath += "Document\\" + ViewState["UploadFolder"].ToString();

                        //myLabel1.Text = HttpContext.Current.Server.MapPath("~") + " hehehe";

                        //myLabel1.Text = strInsert;

                        if (!Directory.Exists(AppPath))

                        {

                            //myLabel1.Text = "hehehe";

                            Directory.CreateDirectory(AppPath);

                        }

                        strSqlScript = "update " + ViewState["MasterTable"] + " set " + row["FieldName"].ToString() + " = '" + NewFileName + "' where " + ViewState["MasterField"] + " = " + ID.ToString();

                        //string filename = Path.GetFileName(FileUploadControl.FileName);

                        Request.Files[row["FieldName"].ToString()].SaveAs(AppPath + "\\" + NewFileName);

                        SqlCommand cmd2 = new SqlCommand(strSqlScript, con);

                        con.Open();

                        cmd2.ExecuteNonQuery();

                        con.Close();

                        //myLabel1.Text = "Upload status: File uploaded!";

                    }

                    catch (Exception ex)

                    {

                        myLabel1.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;

                    }

                }

                //    if (row["InputType"].ToString().ToUpper() == "UPLOADDOC")

                //    {

                //        //string filename = re

                //        String FileName = JobID.ToString() + "-" + row["FieldName"].ToString();

                //        SqlCommand cmd2 = new SqlCommand(strSqlScript, con);

                //        con.Open();

                //        cmd2.ExecuteNonQuery();

                //        con.Close();

                //    }               

            }

        }

        protected void Back_Click(object sender, EventArgs e)

        {

            PrevPage();

        }

        private void PrevPage()

        {

            object refUrl = ViewState["RefUrl"];

            myLabel1.Text = refUrl.ToString();

            if (refUrl != null)

                Response.Redirect((string)refUrl);

        }

        private DataTable GetData(int ModuleId, string ID, int Tipe)

        {

            string query = "SELECT * from " + ViewState["MasterTable"] + " where " + ViewState["MasterField"] + " = '" + ID + "'";

            string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

            //SqlConnection con = new SqlConnection(

            //        WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString);

            using (SqlConnection con = new SqlConnection(constr))

            {

                DataTable dt = new DataTable();

                using (SqlCommand cmd = new SqlCommand(query))

                {

                    using (SqlDataAdapter sda = new SqlDataAdapter())

                    {

                        //cmd.Parameters.AddWithValue("@ParentMenuId", parentMenuId);

                        cmd.CommandType = CommandType.Text;

                        cmd.Connection = con;

                        sda.SelectCommand = cmd;

                        sda.Fill(dt);

                    }

                }

                return dt;

            }

        }

        private DataTable GetInputData(int ModuleId, string ID, int Tipe)

        {

            string query = "SELECT * from vw_ModuleInputList_Edit where ModuleID = " + ModuleId;

            if (Tipe == 1) //Non Doc

            {

                query += " and InputType <> 'UploadDoc' ";

            }

            else if (Tipe == 2) //Doc

            {

                query += " and InputType = 'UploadDoc' ";

            }

            query += " order by FieldIdx";



            string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

            //SqlConnection con = new SqlConnection(

            //        WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString);

            using (SqlConnection con = new SqlConnection(constr))

            {

                DataTable dt = new DataTable();

                using (SqlCommand cmd = new SqlCommand(query))

                {

                    using (SqlDataAdapter sda = new SqlDataAdapter())

                    {

                        //cmd.Parameters.AddWithValue("@ParentMenuId", parentMenuId);

                        cmd.CommandType = CommandType.Text;

                        cmd.Connection = con;

                        sda.SelectCommand = cmd;

                        sda.Fill(dt);

                    }

                }

                return dt;

            }

        }

        private DataTable GetDDLData(String DDLScript, int value)

        {

            //string query = "SELECT * from DDLList where DDLName = '" + FieldName + "'";

            //string query = "SELECT * from DDLList  where FieldName = @FieldName";

            string query = DDLScript;

            //MsgBox("hehehe" + FieldName);

            if (value == 1)

            {

                query = query + " and isValue = 1";

            }



            string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

            //SqlConnection con = new SqlConnection(

            //        WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString);

            using (SqlConnection con = new SqlConnection(constr))

            {

                DataTable dt = new DataTable();

                using (SqlCommand cmd = new SqlCommand(query))

                {

                    using (SqlDataAdapter sda = new SqlDataAdapter())

                    {

                        //cmd.Parameters.AddWithValue("@FieldName", FieldName);

                        cmd.CommandType = CommandType.Text;

                        cmd.Connection = con;

                        sda.SelectCommand = cmd;

                        sda.Fill(dt);

                    }

                }

                return dt;

            }

        }



    }

}