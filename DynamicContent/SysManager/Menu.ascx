﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Menu.ascx.cs" Inherits="Emall.Menu" %>
<%@ Import Namespace="DynamicContent.Areas.SysManager.Controllers" %>
<%@ Import Namespace="DynamicContent.Areas.SysManager.Helper" %>
<style>
    .navigation{
        width:100%;
    }
    .navigation > ul.level1{
        width:100% !important;
    }
    .navigation > ul.level1 > li > a:after {
        font: normal normal normal 14px/1 FontAwesome;
        content: "\f067";
        font-size:10px;
        position: absolute;
        right: 10px;
    }
    
    ul.level2.dynamic > li > a:before{
        font: normal normal normal 14px/1 FontAwesome;
        content: "\f0da";
        margin-right:15px;
    }
    li.static {
        float:none !important;
        display:block !important;
        padding:10px 0;
    }
    ul.level2.dynamic{
        position:relative !important;
        background:none !important;
    }
    .navigation a{color: rgba(255, 255, 255, 0.5) !important; font-weight:bold;}
    .navigation a:hover{color: #fff !important;}
    .navigation .fa-plus:before{content:"" !important;}
    .navigation > ul{
        padding-left:17px;
    }
    .navigation > ul > li >a {
        padding-left:15px;
    }
    .navigation > ul >li:before {
        font: normal normal normal 14px/1 FontAwesome;
        content: "\f0e4";
    }
    @media(max-width: 767px) {
        ul.level2.dynamic{
            position:absolute !important;
            left: 100% !important;
            background-color: #10181e !important;
            width: 220px;
            top:0px !important;
        }
        .navigation > ul > li >a{
            display:none;
        }
        .navigation > ul {
            padding-left: 13px;
        }
    }
    @media(min-device-width: 768px) and (max-device-width: 1024px) and (orientation: portrait){
        .navigation > ul > li >a{font-size:10px; padding-left:0;}
         ul.level2.dynamic{
            position:absolute !important;
            left: 100% !important;
            background-color: #10181e !important;
            width: 220px;
            top:0px !important;
        }
        .navigation > ul.level1 > li > a:after{content:"" !important;}
        .navigation > ul.level1 > li > a:before {
            content:" ";
            display:block;
            clear:both;
        }
        .navigation > ul >li:before{
            font-size:22px;
            padding-left:10px;
        }
    }
</style>
<section id="header" class="scheme-black">
    <header class="clearfix">
        <div class="branding scheme-black"><a class="brand" href="index.html"><span>Plaza <strong>Indonesia</strong></span> </a><a role="button" tabindex="0" class="offcanvas-toggle visible-xs-inline"><i class="fa fa-bars"></i></a></div>
        <ul class="nav-left pull-left list-unstyled list-inline"></ul>
        <ul class="nav-right pull-right list-inline">
            <li class="dropdown nav-profile"><a href="" class="dropdown-toggle" data-toggle="dropdown">
                <img src="//placehold.it/30x30" alt="" class="img-circle size-30x30">
                <span><% Response.Write(Session_.Username); %> <i class="fa fa-angle-down"></i></span></a>
                <ul class="dropdown-menu animated littleFadeInRight" role="menu">
                    <li><a href="/SysManager/Account/Logout" role="button" tabindex="0"><i class="fa fa-sign-out"></i>Logout </a>
                </ul>
        </ul>
    </header>
</section>
<!--<div id="controls">
    <aside id="sidebar" class="scheme-black">
        <div id="sidebar-wrap">
            <div class="panel-group slim-scroll" role="tablist">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab">
                        <h4 class="panel-title"><a data-toggle="collapse" href="#sidebarNav">Navigation <i class="fa fa-angle-up"></i></a></h4>
                    </div>
                    <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
                        <div class="panel-body">
                            <ul  id="navigation">
                                <li class=""><a href="/SysManager/Home/Dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                                <li class=""><a href="/SysManager/Approval"><i class="fa fa-dashboard"></i> <span>Approval</span></a></li>
                                <li><a href="#"><i class="fa fa-dashboard"></i> <span>Newsletter</span></a></li>
                                <li><a href="/SysManager/Customer"><i class="fa fa-dashboard"></i> <span>Customer</span></a></li>
                                <li>
                                    <a role="button" tabindex="0"><i class="fa fa-suitcase"></i> <span>Report</span></a>
                                    <ul>
                                        <li>
                                            <a href="/SysManager/Report/SubscibersSignUp" role="button" tabindex="0"><i class="fa fa-caret-right"></i> Subscribers Sign Up</a>
                                        </li>
                                        <li>
                                            <a href="/SysManager/Report/BrandItemsUpload" role="button" tabindex="0"><i class="fa fa-caret-right"></i> Items Uploaded by Brand</a>
                                        </li>
                                        <li>
                                            <a href="/SysManager/Report/CustomerRegistration" role="button" tabindex="0"><i class="fa fa-caret-right"></i> Customer Database Registration</a>
                                        </li>
                                        <li>
                                            <a href="/SysManager/Report/LeastViewedItems" role="button" tabindex="0"><i class="fa fa-caret-right"></i> Least Viewed Items</a>
                                        </li>
                                        <li>
                                            <a href="/SysManager/Report/MostViewedItems" role="button" tabindex="0"><i class="fa fa-caret-right"></i> Most Viewed Items</a>
                                        </li>
                                        <li>
                                            <a href="/SysManager/Report/MostLovedItems" role="button" tabindex="0"><i class="fa fa-caret-right"></i> Most Loved Items</a>
                                        </li>
                                        <li>
                                            <a href="/SysManager/Report/MostReservedItems" role="button" tabindex="0"><i class="fa fa-caret-right"></i> Most Reserved Items</a>
                                        </li>
                                    </ul>
                                  </li>
                                    <asp:Menu ID="Menu1" CssClass="navigation" runat="server" Orientation="Horizontal" IncludeStyleBlock="false">
                                        <LevelMenuItemStyles>
                                            <asp:MenuItemStyle CssClass="main_menu" />
                                            <asp:MenuItemStyle CssClass="level_menu" />
                                        </LevelMenuItemStyles>
                                        <StaticSelectedStyle CssClass="active open" />
                                    </asp:Menu>
                                
                                
                            </ul>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </aside>
</div>-->

<% AuthenticatedController.RenderPartial("RenderAction"); %>