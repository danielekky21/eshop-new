﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.ComponentModel;
//Menu
using System.IO;
using System.Configuration;
//Menu
using DynamicContent.Areas.SysManager.Helper;
namespace Emall
{
    public partial class Head : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            if (!string.IsNullOrEmpty(Session_.Username as string))
            {
                DynamicContent.Models.Entities db = new DynamicContent.Models.Entities();
                var ut = (from u in db.UserMaster.AsNoTracking()
                          where u.UserID.Equals(Session_.Username)
                          select u).FirstOrDefault();
                if (ut == null || ut.session_id != Session_.SessionId)
                {
                    Session_.destroy();
                }
                //MyLoginTitle.Text = "Welcome " + Session["UserID"].ToString() + "(<a href = \"/ChangePassword.aspx\" runat=\"Server\">ChangePassword</a> | <a href = \"/Login.aspx?ModuleID=Logout\" runat=\"Server\">LogOut</a>)";
            }
            else
            {
                if (path == "/SysManager/Login.aspx" || path == "/SysManager/SignUp.aspx" || path == "/SysManager/ChangePassword.aspx")
                {
                    //MyLoginTitle.Text = "";
                }
                else
                {
                    Response.Redirect("/SysManager/Login.aspx");
                }

            }
        }
    }
}