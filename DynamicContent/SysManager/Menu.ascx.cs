﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.ComponentModel;

//Menu
using System.IO;
using System.Configuration;
//Menu
namespace Emall
{
    public partial class Menu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                DataTable dtMenu = this.GetMenuData(0);
                PopulateMenu(dtMenu, 0, null);
            }

        }
        private DataTable GetMenuData(int parentMenuId)
        {
            string query = "SELECT Distinct [MenuId], [Title], [TitleDesc], [Url], [MenuIdx] FROM [vw_MenuList] WHERE ParentMenuId = @ParentMenuId";

            if (Session["UserID"] == null)
            {
                query = query + " and UserID in ('Guest','') ";
            }
            else
            {
                query = query + " and UserID in ('" + Session["UserID"] + "','') ";
            }
            query = query + " Order by MenuIdx";
            //string query = "SELECT [MenuId], [Title], [TitleDesc], [Url] FROM [MenuMaster] WHERE ParentMenuId = @ParentMenuId";

            string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
            //SqlConnection con = new SqlConnection(
            //        WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString);
            using (SqlConnection con = new SqlConnection(constr))
            {
                DataTable dt = new DataTable();
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Parameters.AddWithValue("@ParentMenuId", parentMenuId);
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        sda.Fill(dt);
                    }
                }
                return dt;
            }
        }

        private void PopulateMenu(DataTable dt, int parentMenuId, MenuItem parentMenuItem)
        {
            string currentPage = Path.GetFileName(Request.Url.AbsolutePath);
            string Url = "";

            foreach (DataRow row in dt.Rows)
            {
                Url = row["Url"].ToString();
                if (Url.IndexOf("?") != -1)
                {
                    Url = Url.Substring(0, Url.IndexOf("?") - 1);
                }
                MenuItem menuItem = new MenuItem
                {
                    Value = row["MenuId"].ToString(),
                    Text = row["Title"].ToString(),
                    NavigateUrl = row["Url"].ToString(),
                    Selected = Url.EndsWith(currentPage, StringComparison.CurrentCultureIgnoreCase)
                };
                if (Url.EndsWith(currentPage, StringComparison.CurrentCultureIgnoreCase))
                {
                    Page.Title = row["Title"].ToString();
                }

                if (parentMenuId == 0)
                {
                    Menu1.Items.Add(menuItem);
                    DataTable dtChild = this.GetMenuData(int.Parse(menuItem.Value));
                    PopulateMenu(dtChild, int.Parse(menuItem.Value), menuItem);
                }
                else
                {
                    parentMenuItem.ChildItems.Add(menuItem);
                }
            }
        }
    }
}