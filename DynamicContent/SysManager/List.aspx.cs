﻿using System;

using System.Collections.Generic;

using System.Linq;

using System.Web;

using System.Web.UI;

using System.Web.UI.WebControls;



using System.Data;

using System.Data.SqlClient;

using System.Web.Configuration;

using System.ComponentModel;

//Menu

using System.IO;

using System.Configuration;

//Menu

namespace Emall

{

    public partial class List : System.Web.UI.Page

    {

        protected void Page_Load(object sender, EventArgs e)

        {

            string ModuleID;

            if (!this.IsPostBack)

            {

                if (Request.QueryString["ModuleID"] == null || Session["UserID"] == null)

                {

                    Response.Redirect("~/SysManager/Login.aspx");

                }

                ModuleID = Request.QueryString["ModuleID"].ToString();





                //MsgBox(ModuleID);





            }

            ModuleID = Request.QueryString["ModuleID"].ToString();

            ViewState["ModuleID"] = ModuleID.ToString();

            //MsgBox(ModuleID);



            ValidateRight(Int32.Parse(ModuleID));

            PopulateFilter(Int32.Parse(ModuleID));

        }

        private void ValidateRight(int moduleID)

        {

            string query = "SELECT top 1 [ModuleRole],[TitleDesc],[ListTable], [FieldList], [SearchList],[MasterField],[ListFilter] FROM [vw_UserRight] WHERE ModuleId = " + moduleID;

            if (Session["UserID"] == null)

            {

                query = query + " and UserID = 'Guest' ";

            }

            else

            {

                query = query + " and UserID = '" + Session["UserID"] + "' ";

            }

            string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;





            using (SqlConnection con = new SqlConnection(constr))

            {

                DataTable dt = new DataTable();

                using (SqlCommand cmd = new SqlCommand(query))

                {

                    cmd.CommandType = CommandType.Text;

                    cmd.Connection = con;

                    //cmd.Parameters.Add(cmd.CreateParameter("AssetID"));

                    using (SqlDataAdapter sda = new SqlDataAdapter())

                    {

                        //cmd.Parameters.AddWithValue("@ModuleId", ModuleID);

                        cmd.CommandType = CommandType.Text;

                        cmd.Connection = con;

                        sda.SelectCommand = cmd;

                        sda.Fill(dt);



                        //if (!dt.)

                        //{

                        //    myTitle.Text = "NULL";

                        //} else

                        //{

                        foreach (DataRow row in dt.Rows)

                        {

                            myTitle.Text = row["TitleDesc"].ToString();

                            ViewState["ListTable"] = row["ListTable"].ToString();

                            ViewState["FieldList"] = row["FieldList"].ToString();

                            ViewState["SearchList"] = row["SearchList"].ToString();

                            ViewState["MasterField"] = row["MasterField"].ToString();

                            ViewState["ListFilter"] = row["ListFilter"].ToString();

                        }



                        //}

                    }

                }

            }

        }

        private void MsgBox(string StrMsg)

        {

            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + StrMsg + "');", true);

        }

        private void PopulateRequestList(int ModuleID, string strFilter)

        {

            DataTable dtList = this.GetListData(ModuleID, strFilter);

            GridView1.DataSource = dtList;

            //GridView1.PageCount = 10;

            GridView1.AllowPaging = true;

            GridView1.PageIndex = 0;

            GridView1.DataBind();

            foreach (GridViewRow gr in GridView1.Rows)

            {

                HyperLink hp = new HyperLink();

                hp.Text = gr.Cells[0].Text;

                //hp.NavigateUrl = "~/Detail.aspx?ModuleID=" + ModuleID + "&ID=" + hp.Text;

                hp.NavigateUrl = "~/SysManager/Edit.aspx?ModuleID=" + ModuleID + "&ID=" + hp.Text;

                gr.Cells[0].Controls.Add(hp);

            }

        }

        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)

        {

            //FillGrid();

            //PopulateFilter(Int32.Parse(ViewState["ModuleID"].ToString()));

            GridView1.PageIndex = e.NewPageIndex;

            GridView1.DataBind();

            foreach (GridViewRow gr in GridView1.Rows)

            {

                HyperLink hp = new HyperLink();

                hp.Text = gr.Cells[0].Text;

                //hp.NavigateUrl = "~/Detail.aspx?JobID=" + hp.Text;

                hp.NavigateUrl = "~/SysManager/Edit.aspx?JobID=" + hp.Text;

                gr.Cells[0].Controls.Add(hp);

            }

        }

        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)

        {

            //encode html

            GridView gv = (GridView)sender;

            int j = 0;

            //if(e.Row.Cells.Count <= gv.PageSize)

            //{

            j = e.Row.Cells.Count - 1;

            //j = e.Row.Cells.Count;

            //}

            //else

            //{

            //    j = gv.PageSize;

            //}



            //for (int i = 0; i < j; i++)

            for (int i = j; i > 0; i--)

                e.Row.Cells[i].Text = HttpUtility.HtmlDecode(e.Row.Cells[i].Text);

        }

        private DataTable GetListData(int ModuleID, string strQuery)

        {

            string Field = "";

            if (string.IsNullOrEmpty(ViewState["FieldList"].ToString()))

            {

                Field = "*";

            }

            else

            {

                Field = ViewState["FieldList"].ToString();

            }

            if (string.IsNullOrEmpty(ViewState["ListFilter"].ToString()))

            {

                ViewState["ListFilter"] = "1=1";

            }

            string query = "SELECT " + Field + " FROM " + ViewState["ListTable"] + " WHERE " + ViewState["ListFilter"] + " " + strQuery + " Order by " + ViewState["MasterField"] + " Desc";

            //string query = "SELECT * FROM " + ViewState["ListTable"] + " WHERE ModuleId = @ModuleId  Order by RequestDate Desc";

            //myLabel.Text = strQuery;

            //string query = "SELECT [MenuId], [Title], [TitleDesc], [Url] FROM [MenuMaster] WHERE ParentMenuId = @ParentMenuId";



            if (Session["UserID"] != null)

            {

                query = query.Replace("@UserID", Session["UserID"].ToString());

            }



            string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

            //SqlConnection con = new SqlConnection(

            //        WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString);

            //myLabel.Text = query.ToString();

            using (SqlConnection con = new SqlConnection(constr))

            {

                DataTable dt = new DataTable();

                using (SqlCommand cmd = new SqlCommand(query))

                {

                    using (SqlDataAdapter sda = new SqlDataAdapter())

                    {

                        cmd.Parameters.AddWithValue("@ModuleId", ModuleID);

                        cmd.CommandType = CommandType.Text;

                        cmd.Connection = con;

                        sda.SelectCommand = cmd;

                        sda.Fill(dt);

                    }

                }

                return dt;

            }

        }



        private void PopulateFilter(int ModuleID)

        {

            DataTable dtFilter = this.GetFilterData(ModuleID);

            String strScript = "";



            String strSqlScript = "";

            //SqlConnection con = new SqlConnection(

            //WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString);

            TableRow trow;

            TableCell tcell1, tcell2;



            //SqlCommand Comm1 = new SqlCommand("Select * From Test", con);

            //con.Open();

            //SqlDataReader DR1 = Comm1.ExecuteReader();

            /*if (DR1.Read())

            {

                //textbox.Text = DR1.GetValue(0).ToString();

            }*/





            int i = 0;

            //for (int i = 0; i < 30; i++)

            foreach (DataRow row in dtFilter.Rows)

            {

                Label myLabelSearch = new Label();

                myLabelSearch.Text = row["SearchDesc"].ToString();

                trow = new TableRow();

                tcell1 = new TableCell();

                tcell2 = new TableCell();



                tcell1.Controls.Add(myLabelSearch);



                if (row["SearchType"].ToString().ToUpper() == "TEXTBOX")

                {

                    TextBox myTextbox = new TextBox();

                    myTextbox.ID = row["SearchName"].ToString();

                    if (!string.IsNullOrEmpty(Request.Form[row["SearchName"].ToString()]))

                    {

                        myTextbox.Text = Request.Form[row["SearchName"].ToString()];

                        strSqlScript += row["SearchSyntax"].ToString().Replace("@Value", Request.Form[row["SearchName"].ToString()]);

                    }



                    tcell2.Controls.Add(myTextbox); //, Text = "hello :)"

                }

                if (row["SearchType"].ToString().ToUpper() == "DROPDOWNLIST")

                {

                    string query = row["DDLScript"].ToString();

                    String Value = "";

                    if (row["DDLDepend"].ToString() != "")

                    {

                        Value = ViewState[row["DDLDepend"].ToString() + "_Value"].ToString();

                        query = query.Replace("@Value", Value);

                    }

                    DataTable dtDDL = this.GetDDLData(query, 0);

                    DropDownList myDDL = new DropDownList();

                    myDDL.ID = row["SearchName"].ToString();

                    myDDL.DataSource = dtDDL;

                    myDDL.DataTextField = "DDLText";

                    myDDL.DataValueField = "DDLValue";

                    myDDL.DataBind();

                    myDDL.Items.Insert(0, new ListItem("All", ""));

                    if (!string.IsNullOrEmpty(Request.Form[row["SearchName"].ToString()]))

                    {

                        myDDL.SelectedValue = Request.Form[row["SearchName"].ToString()];

                        strSqlScript += row["SearchSyntax"].ToString().Replace("@Value", Request.Form[row["SearchName"].ToString()]);

                    }

                    if (row["DDLPostback"].ToString() == "Y")

                    {

                        myDDL.AutoPostBack = true;

                        ViewState[row["SearchName"].ToString() + "_Value"] = myDDL.SelectedValue;

                    }

                    tcell2.Controls.Add(myDDL); //, Text = "hello :)"

                }

                if (row["SearchType"].ToString().ToUpper() == "DATEPICKER")

                {

                    TextBox myDatepicker = new TextBox();

                    myDatepicker.ID = row["SearchName"].ToString();

                    myDatepicker.ReadOnly = true;

                    myDatepicker.Width = 80;



                    if (!string.IsNullOrEmpty(Request.Form[row["SearchName"].ToString()]))

                    {

                        myDatepicker.Text = Request.Form[row["SearchName"].ToString()];

                        strSqlScript += row["SearchSyntax"].ToString().Replace("@Value", Request.Form[row["SearchName"].ToString()]);

                    }

                    tcell2.Controls.Add(myDatepicker);



                    strScript += "$(\"[id$=" + row["SearchName"].ToString() + "]\").datepicker({";

                    strScript += "  showOn: 'button',";

                    strScript += "  buttonImageOnly: false,";

                    strScript += "  buttonText: '...'";

                    strScript += "  });; ";



                }

                if (row["SearchType"].ToString().ToUpper() == "DATEPICKER2")

                {

                    Label myLabelFrom = new Label();

                    myLabelFrom.Text = "From : ";

                    tcell2.Controls.Add(myLabelFrom);



                    string strFilter = "";



                    TextBox myDatepicker = new TextBox();

                    myDatepicker.ID = row["SearchName"].ToString() + "_From";

                    myDatepicker.ReadOnly = true;

                    myDatepicker.Width = 80;

                    strFilter = row["SearchSyntax"].ToString();



                    if (!string.IsNullOrEmpty(Request.Form[row["SearchName"].ToString() + "_From"]))

                    {

                        myDatepicker.Text = Request.Form[row["SearchName"].ToString() + "_From"];

                        strFilter = strFilter.Replace("@ValueFrom", Request.Form[row["SearchName"].ToString() + "_From"]);



                    }

                    tcell2.Controls.Add(myDatepicker);



                    strScript += "$(\"[id$=" + row["SearchName"].ToString() + "_From" + "]\").datepicker({";

                    strScript += "  showOn: 'button',";

                    strScript += "  buttonImageOnly: false,";

                    strScript += "  buttonText: '...'";

                    strScript += "  });; ";



                    Label myLabelTo = new Label();

                    myLabelTo.Text = " To : ";

                    tcell2.Controls.Add(myLabelTo);



                    TextBox myDatepicker1 = new TextBox();

                    myDatepicker1.ID = row["SearchName"].ToString() + "_To";

                    myDatepicker1.ReadOnly = true;

                    myDatepicker1.Width = 80;



                    if (!string.IsNullOrEmpty(Request.Form[row["SearchName"].ToString() + "_To"]))

                    {

                        myDatepicker1.Text = Request.Form[row["SearchName"].ToString() + "_To"];

                        strFilter = strFilter.Replace("@ValueTo", Request.Form[row["SearchName"].ToString() + "_To"]);

                    }



                    if (!string.IsNullOrEmpty(Request.Form[row["SearchName"].ToString() + "_From"]) && !string.IsNullOrEmpty(Request.Form[row["SearchName"].ToString() + "_To"]))

                    {

                        strSqlScript += strFilter;

                    }



                    tcell2.Controls.Add(myDatepicker1);



                    strScript += "$(\"[id$=" + row["SearchName"].ToString() + "_To" + "]\").datepicker({";

                    strScript += "  showOn: 'button',";

                    strScript += "  buttonImageOnly: false,";

                    strScript += "  buttonText: '...'";

                    strScript += "  });; ";



                }



                //tcell3 = new TableCell();

                //tcell3.Controls.Add(new DropDownList());



                trow.Cells.Add(tcell1);

                trow.Cells.Add(tcell2);



                mytbl.Rows.Add(trow);



                i = i + 1;

            }



            trow = new TableRow();

            if (dtFilter.Rows.Count > 0)

            {



                tcell1 = new TableCell();



                Button SearchButton = new Button();

                SearchButton.ID = "SearchButton";

                SearchButton.Text = "Search";

                SearchButton.Click += new EventHandler(Search_Click);



                tcell1.Controls.Add(SearchButton);

                trow.Cells.Add(tcell1);



            }

            //mytbl1.Rows.Add(trow);



            tcell1 = new TableCell();



            Button AddButton = new Button();

            AddButton.ID = "AddButton";

            AddButton.Text = "Add";

            AddButton.Click += new EventHandler(Add_Click);



            tcell1.Controls.Add(AddButton);

            trow.Cells.Add(tcell1);

            mytbl1.Rows.Add(trow);



            if (strScript != "")

            {

                strScript = "   $(function () {" +

                    strScript +

                    "   });";

                ClientScript.RegisterClientScriptBlock(this.GetType(), "DTPickerScript", strScript, true);

            }

            //myLabel.Text = strSqlScript;

            PopulateRequestList(ModuleID, strSqlScript);

        }

        protected void Add_Click(object sender, EventArgs e)

        {

            //Button button = sender as Button;

            Response.Redirect("~/SysManager/Input.aspx?ModuleID=" + Request.QueryString["ModuleID"].ToString());

        }

        protected void Search_Click(object sender, EventArgs e)

        {

            //Button button = sender as Button;

            // identify which button was clicked and perform necessary actions

            //myLabel.Text = "hehehe";

        }

        private DataTable GetFilterData(int ModuleId)

        {

            string query = "SELECT * from ModuleSearchList where ModuleID = " + ModuleId;

            if (ViewState["SearchList"] == null || ViewState["SearchList"].ToString() == "")

            {

                query = query + " and SearchName in ('')";

            }

            else

            {

                query = query + " and SearchName in (" + ViewState["SearchList"].ToString() + ")";

            }

            //myLabel.Text = query;

            string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

            //SqlConnection con = new SqlConnection(

            //        WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString);

            using (SqlConnection con = new SqlConnection(constr))

            {

                DataTable dt = new DataTable();

                using (SqlCommand cmd = new SqlCommand(query))

                {

                    using (SqlDataAdapter sda = new SqlDataAdapter())

                    {

                        //cmd.Parameters.AddWithValue("@ParentMenuId", parentMenuId);

                        cmd.CommandType = CommandType.Text;

                        cmd.Connection = con;

                        sda.SelectCommand = cmd;

                        sda.Fill(dt);

                    }

                }

                return dt;

            }

        }

        private DataTable GetDDLData(String DDLScript, int value)

        {

            //string query = "SELECT * from DDLList where DDLName = '" + SearchName + "'";

            string query = DDLScript;

            //string query = "SELECT * from DDLList  where SearchName = @SearchName";

            //MsgBox("hehehe" + SearchName);

            if (value == 1)

            {

                query = query + " and isValue = 1";

            }



            string constr = WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

            //SqlConnection con = new SqlConnection(

            //        WebConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString);

            using (SqlConnection con = new SqlConnection(constr))

            {

                DataTable dt = new DataTable();

                using (SqlCommand cmd = new SqlCommand(query))

                {

                    using (SqlDataAdapter sda = new SqlDataAdapter())

                    {

                        //cmd.Parameters.AddWithValue("@SearchName", SearchName);

                        cmd.CommandType = CommandType.Text;

                        cmd.Connection = con;

                        sda.SelectCommand = cmd;

                        sda.Fill(dt);

                    }

                }

                return dt;

            }

        }

    }

}